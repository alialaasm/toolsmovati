<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/config.php");

define('APIKEY','28453842');
define('VENUE_NORTH','2003');
define('VENUE_SOUTH','2004');
define('APIBASE','http://api.clubcom.com/WSGroupX.svc/');

$con = mysql_connect(DB_HOST, DB_USERNAME, DB_PASSWORD);
mysql_select_db(DB_NAME);
  
function importStudios($club){
  global $con;
  // Get Studios
  $studios = array();
  $result = mysql_query(sprintf('SELECT ID, StudioName FROM Studio WHERE ClubId = %d',$club),$con);
  if($result){
    while($row = mysql_fetch_array($result)){
      $studios[] = array(
          'GroupXCustomerLocationID' => $row['ID'],
          'LocationName' => $row['StudioName'],
      );
    }
  }
  if(!empty($studios)){
    $url = getURL('GXLocationsImport', $club);
    $response = curl_json($url,$studios);
    return $response;
  }
  return NULL;
}



function importClasses($club){
  global $con;
  // Get Classes
  $classes = array();
  $result = mysql_query(sprintf('SELECT DISTINCT E.ClassId, C.ClassName, C.Description FROM Event E LEFT JOIN Class AS C ON C.ID = E.ClassId WHERE E.ClubId = %d',$club),$con) or trigger_error('error');
  if($result){
    while($row = mysql_fetch_array($result)){
      $classes[] = array(
          'CustomerClassID' => $row['ClassId'],
          'ClassName' => $row['ClassName'],
          'ClassDescription' => substr(strip_tags($row['Description']),0,512),
      );
    }
  }
  if(!empty($classes)){
    $url = getURL('GXClassesImport', $club);
    $response = curl_json($url,$classes);
    return $response;
  }
  return NULL;
}


function importSchedules($club){
  global $con;
  $result = mysql_query(sprintf("SELECT 
  E.ClubId,
  E.ClassId,
  E.id AS 'ScheduleId',
  Cb.ClubName,
  St.ID AS 'LocationId',
  Cs.ClassName,
  Cs.Description,
  E.Day,
  CONCAT(E.StartEventHour,':',E.StartEventMin,' ',E.StartEventAP) AS 'StartTime',
  CONCAT(E.EndEventHour,':',E.EndEventMin,' ',E.EndEventAP) AS 'EndTime'

  FROM `Event` E 
    LEFT JOIN Class AS Cs ON Cs.ID = E.ClassId
    LEFT JOIN Club AS Cb ON Cb.ID = E.ClubId
    LEFT JOIN Studio AS St ON St.ID = E.StudioId

  WHERE E.ClubId = %d ORDER BY StudioId
  ",$club),$con);

  $schedules = array();

  if($result){
    while($row = mysql_fetch_array($result)){

      $schedules[] = array(
        "CustomerTimelineID" => $row['ScheduleId'], 
        "GroupXCustomerClassID" => $row['ClassId'],
        "GroupXCustomerLocationID" => $row['LocationId'],
        "BeginDate"   => date('Ym').'01',
        "EndDate"     => date('Ym').date('t'),
        "BeginTime"   => date('Hi',strtotime($row['StartTime'])),
        "EndTime"     => date('Hi',strtotime($row['EndTime'])),
        "OnSunday"    => $row['Day'] == '7' ? 'true' : 'false',
        "OnMonday"    => $row['Day'] == '1' ? 'true' : 'false',
        "OnTuesday"   => $row['Day'] == '2' ? 'true' : 'false',
        "OnWednesday" => $row['Day'] == '3' ? 'true' : 'false',
        "OnThursday"  => $row['Day'] == '4' ? 'true' : 'false',
        "OnFriday"    => $row['Day'] == '5' ? 'true' : 'false',
        "OnSaturday"  => $row['Day'] == '6' ? 'true' : 'false',
      );
    }
  }
  if(!empty($schedules)){
    $url = getURL('GXTimelinesImport', $club);
    $response = curl_json($url,$schedules);
    return $response;
  }else{
    return NULL;
  } 
}

//$response = importStudios('2');
//var_dump($response);


function getURL($base, $club){
  if($club == '1'){
    $venue = VENUE_NORTH;
  }elseif($club == '2'){
    $venue = VENUE_SOUTH;
  }else{
    return NULL;
  }
  return APIBASE.$base.'?APIKey='.APIKEY.'&SiteID='.$venue;
}

function curl_download($url){
 
  // is cURL installed yet?
  if (!function_exists('curl_init')){
      die('Sorry cURL is not installed!');
  }
  try {
    
    // OK cool - then let's create a new cURL resource handle
    $ch = curl_init();

    // Now set some options (most are optional)

    // Set URL to download
    curl_setopt($ch, CURLOPT_URL, $url);

    // Set a referer
    curl_setopt($ch, CURLOPT_REFERER, "http://www.theathleticclubs.ca/");

    // User agent
    curl_setopt($ch, CURLOPT_USERAGENT, "TheAtheticClubs");

    // Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_HEADER, 0);

    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Timeout in seconds
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);

    // Download the given URL, and return output
    $output = curl_exec($ch);
    
    // Close the cURL resource, and free system resources
    curl_close($ch);

  } catch (Exception $ex) {
    $output = 'Error: '.$url;
  }
  return $output;
}

function curl_json($url,$json_data){
 
  // is cURL installed yet?
  if (!function_exists('curl_init')){
      die('Sorry cURL is not installed!');
  }
  // Check to see if it's a single value or multiple and adjust accordingly
  if(count($json_data) == 1){
    $json_data = array_pop($json_data);
  }
  $content = json_encode($json_data);
  print '<pre>'.$content.'</pre>';
  $curl = curl_init($url);
  curl_setopt($curl, CURLOPT_HEADER, 0);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER,
          array('Content-type: application/json','Cache-Control:no-cache'));
  curl_setopt($curl, CURLOPT_POST, true);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

  $json_response = curl_exec($curl);

  $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

  if ( $status != 200 ) {
      die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
  }

  curl_close($curl);

  $response = json_decode($json_response, true);
  
  return $response;
}

/***********************************
 * Import this stuff
 ***********************************/

$import_clubs = array(
    '1',
//    '2',
    );
foreach($import_clubs as $imp_clb){
  $response = array();
  // Import studios first
//  $response[$imp_clb]['studio'] = importStudios($imp_clb);
  // Import the classes
//  $response[$imp_clb]['classes'] = importClasses($imp_clb);
  // Import the schedules
  $response[$imp_clb]['schedules'] = importSchedules($imp_clb);
}
if(!empty($response)){
  print '<pre>';
  var_dump($response);  
  print '</pre>';
}

/*********************************
 * TESTS
 ********************************/
print '<h2>Current Data</h2><pre>';
print 'Testing....'.PHP_EOL;
// Check current values
$getData = array(
    'GXLocations',
    'GXEmployees',
    'GXClasses',
    'GXTimelines',
);

foreach($getData as $data){
  $content = curl_download(APIBASE.$data.'?APIKey='.APIKEY.'&SiteID='.VENUE_NORTH);
  print $content.PHP_EOL;
  $content2 = curl_download(APIBASE.$data.'?APIKey='.APIKEY.'&SiteID='.VENUE_SOUTH);
  print $content2.PHP_EOL;
}


print '</pre>';
