<?php
session_start();

$HOME = "/home/ubuntu/toolsmovati";
$_SERVER["DOCUMENT_ROOT"] = $HOME;


require_once "$HOME/vendor/propel/runtime/lib/Propel.php";

// Initialize Propel with the runtime configuration
Propel::init("$HOME/build/conf/site-conf.php");

// Add the generated 'classes' directory to the include path
set_include_path("$HOME/build/classes" . PATH_SEPARATOR . get_include_path());



require_once($_SERVER["DOCUMENT_ROOT"] . "/config.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClubManager.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClassManager.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/StudioManager.php");

//restore_error_handler();

$clubID = 0;
$clubManager = new ClubManager();
$clubs = $clubManager->SelectAll();
$studioManager = new StudioManager();



$myClub = 0;

$classManager= new ClassManager();
$instructor= new InstructorQuery();

$_instructor = 0;
$is_admin = 0;

if (isset($_SESSION["Instructor"])){
$_instructor = $instructor->findPK( $_SESSION["Instructor"]["id"] );
$is_admin = $_instructor && $_instructor->getIsAdmin() == 1;
}



function print_class($c,$member, $has_class=0){
?>
<li class="class_item_<?= $c->EventID ?>">
<h4><?= $c->Name ?> <span class="small nobreak"><?= $c->Time ?></span>
<br/><span class="instructor small"><?= $c->Instructor ?></span>
</h4>
<p><?= $c->Studio ?></p>

<? if ($member){
	}


}

function has_class( $member, $c ){
	return 0;	
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Movati Athletic <?= print_r($_SESSION["Instructor"]) ?></title>

	<meta name="viewport" content="width=default-width, initial-scale=1" />
	
	<script type="text/javascript" charset="utf-8" src="phonegap-0.9.3.js"></script>
	<link href="date.css" rel="stylesheet">
	<link rel="stylesheet" href="jquery.mobile-1.0rc2.min.css" /> 
	<link rel="stylesheet" href="default.css" /> 
	
	
	


	<style>
		li.divider { 
			background-color: #999999 !important;
			background-image: -webkit-linear-gradient(top,#999999,#888888) !important;
			background-image: -webkit-linear-gradient(top,#999999,#888888) !important;
			background-image: -moz-linear-gradient(top,#999999,#888888) !important;
			background-image: -ms-linear-gradient(top,#999999,#888888) !important;
			background-image: -o-linear-gradient(top,#999999,#888888) !important;
			background-image: linear-gradient(top,#999999,#888888) !important;
		}


		.ui-bar-b{
			background-color: #999999 !important;
			background-image: none;
		}
		
		.small { font-weight: normal; } 
		.detail{ display: none; }
		.club_image{ float: left;  width: 250px; padding-right: 20px; }
		.fb { display: block; clear: left; }
		.ui-header{
			background-image: url(images/logo_small.png);
			background-repeat: no-repeat;
			background-position: 95% 50%;
			background-color: #7fc251; 
		}
		
		.ui-header h1 {
			text-align: left !important;			
			margin-left: 45px !important;
			color: #000;
		}

		.classes li, .classes li p, .classes h4 {
			text-overflow: ellipsis !important;
			overflow: visible !important;
			white-space: normal !important;
		}

		.nobreak { 
			white-space: nowrap;
		}
		

		#home {
			background-image: url(/instructors/whitelogo.png);
			background-repeat: no-repeat;
			background-position: top center;
			padding-top: 100px;
		}

		.ui-bar-a {
			color: #000;
			font-weight: bold;
			text-shadow: 0 1px 1px white;
		}
		
		a {
			text-decoration: none;
						
		}

		.views a {
			margin-left: 20px;
			color: #7fc251 !important;
		}
		
		.intro a {
                        color: #7fc251 !important;
		}
		
		.intro {
			text-align: center;
		}

        .left_menu {
            width: 20%;
            position: absolute;
            top: 1em;
            left: 1em;
        }   

        .right_content {
            padding-left: 25%;
            min-height: 20em;
        }

        #ui-datepicker-div {
            z-index: 10 !important;
        }

		.payroll_calendar th {
			text-align: left;
		}

        .payroll_calendar td {
        	width: 120px;
        	height: 100px;
        	border: 2px solid #333;;
        }

        .payroll_calendar .dom {
        	display: block;

        	padding-top: 5px;
        	width: 30px;
        	height: 30px;
        	text-align: center;
        	
        	background-color: #333;
        	color: white;
        }

        .payroll_calendar td {
        	vertical-align: top;
        }

        .payroll_calendar .class, .payroll_calendar .hours {
        	font-size: 10pt;
		}

		.payroll_calendar .row {
			display: block;
			padding-bottom: 1em;
		}
        	</style>


	<script src="jquery-1.6.4.min.js"></script> 
	<script src="jquery.mobile-1.0rc2.min.js"></script>

	<script src="app.js"></script> 
	<script src="data.js"></script> 
	<script src="jquery-ui.js" type="text/javascript"></script>
	<script src="underscore-min.js" type="text/javascript"></script>
	<script src="moment.min.js" type="text/javascript"></script>
	
	<script>
		$(function() {
			$( "#class_date, #date_from, #date_to, #date_from2, #date_to2" ).datepicker();					
		});
	</script>
	<script type="text/javascript">

		
	var App = new _App();
	
	$( function() {
		
		$('#searchButton').click(function() {
		  			
  			
		});
		
		$(".classes li").click(function(a, b){
			$(this).find(".detail").toggle();
		});

		$('.add_class').click(function(event,a) {

		 	event.stopPropagation();	
			$(event.target).html("Class Added!");
			class_id = event.target.className;

			var rex = new RegExp(' class_([0-9]*)');
			matches = rex.exec( class_id );
			class_id = matches[1];

			$.post("/instructors/member.php","action=add_class&class=" + class_id);
	
			return false;
		});

                $('.remove_class').click(function(event,a) {
                        event.stopPropagation();
		        class_id = event.target.className;

			var rex = new RegExp(' class_([0-9]*)');
			matches = rex.exec( class_id );
			class_id = matches[1];
												
                        $.post("/instructors/member.php","action=remove_class&class=" + class_id);

			$("#my-schedule li.class_item_" + class_id).hide();

			$(event.target).hide();

			return false;
                });

		$('.ui-header').click(function(event){
			$("#home").show();
		});

		$('.logout').click(function(event,a) {
                        event.stopPropagation();
                        $.post("/instructors/member.php","action=logout");
                });

	
		$("#btn_register").click(function(event){
			if(validate_form(document.getElementById('form_register'))=== true)
			{
				event.preventDefault(); 
				$.mobile.showPageLoadingMsg();	
				$.ajax({  
				  type: "POST",  
				  url: "/instructors/member.php",  
				  data: $("#form_register").serialize(),  
				  success: function(data){
					$.mobile.hidePageLoadingMsg();
					if (data == 'OK'){
						$(".register_message").html("<div class='intro'>Thank you for registering! <a href='index.php?refresh=1' data-ajax='false'>Continue</a></div>");  
						$(".register_message").show();
						$("#form_register").hide();
						
					}else{
						$(".register_message").html( data );
											$(".register_message").show();
					}
				  },
				});
			}
			else
			{
				
				return false;
			}
		});

		$("#form_login").submit(function(event){
				event.preventDefault();

				$.ajax({
				  type: "POST",
				  url: "/instructors/member.php",
				  data: $("#form_login").serialize(),
				  success: function(data){
						if (data == 'OK'){

			window.location = "/instructors/index.php?refresh=1";
						}else{
								$(".login_message").html( data );
								$(".login_message").show();
						}
				  },
				});

				return false;
		});

		$("#btn_add_class").click(function(event){
		
			if(validate_class(document.getElementById('form_addclass'))=== true)
			{
				event.preventDefault();
				$.mobile.showPageLoadingMsg();
				$.ajax({					
				  type: "POST",
				  url: "/instructors/member.php",
				  data: $("#form_addclass").serialize(),
				  success: function(data){
					 $.mobile.hidePageLoadingMsg();
					if (data == 'OK'){
					$(".addclass_message").html("<div class='intro'>Thank you for adding class! <a href='index.php?refresh=1' data-ajax='false'>Continue</a></div>");  
					$(".addclass_message").show();
					$("#form_addclass").hide();
					
				}else{
					$(".addclass_message").html( data );
                    $(".addclass_message").show();
				}
				  },
				});
			}

				return false;
		});
    /**
    * BEGIN NEW jQuery for Class Type          
    */       

		$("#btn_add_classtype").click(function(event){
		 $('select#select_club').trigger('change');
			if(validate_classtype(document.getElementById('form_add_classtype'))=== true)
			{
				event.preventDefault();
				$.mobile.showPageLoadingMsg();
				$.ajax({					
				  type: "POST",
				  url: "/instructors/member.php",
				  data: $("#form_add_classtype").serialize(),
				  success: function(data){
					 $.mobile.hidePageLoadingMsg();
					if (data == 'OK'){
					$(".add_classtype_message").html("<div class='intro'>Thank you for adding a class type! <a href='index.php?refresh=1' data-ajax='false'>Continue</a></div>");  
					$(".add_classtype_message").show();
					$("#form_add_classtype").hide();
					
				}else{
					$(".add_classtype_message").html( data );
                    $(".add_classtype_message").show();
				}
				  },
				});
			}

				return false;
		});    
    
    
    $("select#edit_classtypeid").change(function(){
		$(".edit_classtype_message").hide();
		$.mobile.showPageLoadingMsg();
		$.getJSON("/instructors/member.php",{classtypeId: $(this).val(), ajax: 'true'}, function(j){
			$.mobile.hidePageLoadingMsg();
			$('input#edit_classtypeid').val(j.classtypeid);
			$('input#edit_classname').val(j.classname);
			$('textarea#edit_classdescription').val(j.classdescription);
			$('input#edit_classvideolink').val(j.classvideolink);
			$('select#edit_class_dropCategory option[value="'+j.catid+'"]').attr("selected", "selected");
      $('select#edit_class_dropCategory').trigger('change');
			$('select#edit_class_dropClassCode option[value="'+j.classcode+'"]').attr("selected", "selected");
      $('select#edit_class_dropClassCode').trigger('change');
			if(j.classisactive==1)
			{
				$("input#edit_classisactive").attr("checked", true);
				$('input#edit_classisactive').trigger('click');
			}
			else
			{
				$("input#edit_classisactive").attr("checked", false);
				$('input#edit_classisactive').trigger('click');
			}
		})
	  })
		
	$("#btn_edit_classtype").click(function(event){
		if(validate_classtype_edit(document.getElementById('form_edit_classtype'))=== true)
		{
			event.preventDefault(); 
			$.mobile.showPageLoadingMsg();    
			$.ajax({  
			  type: "POST",  
			  url: "/instructors/member.php",  
			  data: $("#form_edit_classtype").serialize(),  
			  success: function(data){
				$.mobile.hidePageLoadingMsg();
				if (data == 'OK'){
					$(".edit_classtype_message").html("Class Type Data Updated.");  
					$(".edit_classtype_message").show();
					//$("#form_edit_instructor").hide();
				}else{
					$(".edit_classtype_message").html( data );
                    $(".edit_classtype_message").show();
				}
			  },
			});
		}

			return false;
		});
    
    
    /**
    * END NEW jQuery for Class Type
     */

	$("select#class_club").change(function(){
		$.mobile.showPageLoadingMsg();
		$("select#class_class").empty();
		$.getJSON("/instructors/member.php",{clubId: $(this).val(), ajax: 'true'}, function(j){
		  $.mobile.hidePageLoadingMsg();
		  var options = '';
		  for (var i = 0; i < j.length; i++) {
			 options += '<option value="' + j[i].optionValue + '">' + j[i].optionDisplay + '</option>';
		 }
		  $("select#class_class").html(options);
		  $('select#class_class').trigger('change');
		});

        $("select#class_instructor").empty();
		$.getJSON("/instructors/member.php",{clubId: $(this).val(), ajax: 'true', instructors: 'true'}, function(j){
		  $.mobile.hidePageLoadingMsg();
		  var options = '';
		  for (var i = 0; i < j.length; i++) {
            if (j[i].optionValue > 0){
			 options += '<option value="' + j[i].optionValue + '">' + j[i].optionDisplay + '</option>';
             }else{
                if (i > 0){
                    options += '</optgroup>';
                }
                options += '<optgroup label="' + j[i].optionDisplay + '">';
             }
		 }
		  $("select#class_instructor").html(options);
		  $('select#class_instructor').trigger('change');
    
         <?php   if ( $_instructor ){ ?>
		if ( $('#class_instructor option[value=<?php echo $_instructor->getId() ?>]').length > 0  ){
            $("select#class_instructor").val( <?php echo $_instructor->getId() ?> );
            $("#_select_instructor .ui-btn-text").text("<?php echo $_instructor->getFirstname() . " " . $_instructor->getLastname() ?>");
			}else{
				$("select#class_instructor").val(undefined);
				$("#_select_instructor .ui-btn-text").text("");
			}
                  <?php } ?>

		});



	  })


	$("select#select_instructor").change(function(){
		$(".edit_instructor_message").hide();
		$.mobile.showPageLoadingMsg();
		$.getJSON("/instructors/member.php",{instructorId: $(this).val(), ajax: 'true'}, function(j){
			$.mobile.hidePageLoadingMsg();
			$('input#firstname').val(j.FirstName);
			$('input#lastname').val(j.LastName);
			$('input#username').val(j.Email);
			$('select#select_club option[value="'+j.ClubId+'"]').attr("selected", "selected");
			$('select#select_club').trigger('change');
			if(j.is_admin==1)
			{
				$("input#isadmin").attr("checked", true);
				$('input#isadmin').trigger('click');
			}
			else
			{
				$("input#isadmin").attr("checked", false);
				$('input#isadmin').trigger('click');
			}
		})
	  })
		
	$("#btn_edit_instructor").click(function(event){
		if(validate_form(document.getElementById('form_edit_instructor'))=== true)
		{
			event.preventDefault(); 
			$.mobile.showPageLoadingMsg();    
			$.ajax({  
			  type: "POST",  
			  url: "/instructors/member.php",  
			  data: $("#form_edit_instructor").serialize(),  
			  success: function(data){
				$.mobile.hidePageLoadingMsg();
				if (data == 'OK'){
					$(".edit_instructor_message").html("Instructor Data Updated.");  
					$(".edit_instructor_message").show();
					//$("#form_edit_instructor").hide();
				}else{
					$(".edit_instructor_message").html( data );
                    $(".edit_instructor_message").show();
				}
			  },
			});
		}

			return false;
		});
		
	} );


    $("#home").live('pageshow',function() {
	    

    });


    $("#add_class").live('pageshow', function(e,ui){
            $("select#class_club").trigger('change');
    });

    $("#my_classes").live('pageshow', function(e,ui){
        updateMyClasses();
    });



    $(".club_page").live('pageshow', function(event, ui){
		    var club_id = this.id.replace('club-','');
		    var phone = $( '#' + this.id + ' .club_phone').html();
    		    var email = $( '#' + this.id + ' .club_email').html();
		    $('.contact_phone').html( phone );
		    $('.contact_email').html( email );

		 $('#select_club')[0].value = club_id;
	   });


	function _debug(s){
		console.log(s);
	}



	document.addEventListener("deviceready", onDeviceReady, false);

    // PhoneGap is ready
    //
    function onDeviceReady() {
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
    }
	


var onSuccess = function(position) {
    alert('Latitude: '          + position.coords.latitude          + '\n' +
          'Longitude: '         + position.coords.longitude         + '\n' +
          'Altitude: '          + position.coords.altitude          + '\n' +
          'Accuracy: '          + position.coords.accuracy          + '\n' +
          'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
          'Heading: '           + position.coords.heading           + '\n' +
          'Speed: '             + position.coords.speed             + '\n' +
          'Timestamp: '         + new Date(position.timestamp)      + '\n');
};

// onError Callback receives a PositionError object
//
function onError(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}


function isInvalidateEmail(str)
{
	var atpos=str.indexOf("@");
	var dotpos=str.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=str.length)
	  {
	  return true;
	  }
	return false;
}

function validate_report(theForm)
{
	if(theForm.date_from.value == "" || theForm.date_from.value == "YYYY-MM-DD")
	{
		alert("Please enter valid Date From.");
		theForm.date_from.focus();
		theForm.date_from.value="";
		return (false);
	}
	if(theForm.date_to.value == "" || theForm.date_to.value == "YYYY-MM-DD")
	{
		alert("Please enter valid Date To.");
		theForm.date_to.focus();
		theForm.date_to.value="";
		return (false);
	}
	return true;
}
function validate_class(theForm)
{
	if(theForm.class_date.value == "" || theForm.class_date.value == "YYYY-MM-DD")
	{
		alert("Please enter valid Date.");
		theForm.class_date.focus();
		theForm.class_date.value="";
		return (false);
	}
	if(theForm.participant.value == "")
	{
		alert("Please enter valid Participant.");
		theForm.participant.focus();
		theForm.participant.value="";
		return (false);
	}
	return true;
}
function validate_classtype(theForm)
{
	if(theForm.class_dropCategory.value == "")
	{
		alert("Please choose a category.");
		theForm.class_dropCategory.focus();
		theForm.class_dropCategory.value="";
		return (false);
	}
	if(theForm.add_classname.value == "")
	{
		alert("Please enter a class name.");
		theForm.add_classname.focus();
		theForm.add_classname.value="";
		return (false);
	}
	return true;
}
function validate_classtype_edit(theForm)
{
	if(theForm.edit_class_dropCategory.value == "")
	{
		alert("Please choose a category.");
		theForm.edit_class_dropCategory.focus();
		theForm.edit_class_dropCategory.value="";
		return (false);
	}
	if(theForm.edit_classname.value == "")
	{
		alert("Please enter a class name.");
		theForm.edit_classname.focus();
		theForm.edit_classname.value="";
		return (false);
	}
	return true;
}

function validate_form(theForm)
{
	if(theForm.firstname.value == "" || theForm.firstname.value == "First Name")
	{
		alert("Please enter your First Name.");
		theForm.firstname.focus();
		theForm.firstname.value="";
		return (false);
	}
	if(theForm.lastname.value == "" || theForm.lastname.value == "Last Name")
	{
		alert("Please enter your Last Name.");
		theForm.lastname.focus();
		theForm.lastname.value="";
		return (false);
	}

	if(theForm.email.value == "" || theForm.email.value == "Email")
	{
		alert("Please enter a value for the \"Email\" field.");
		theForm.email.focus();
		theForm.email.value="";
		return (false);
	}

	if((theForm.email.value.indexOf("\@") == -1) || (theForm.email.value.indexOf(".")== -1) || isInvalidateEmail(theForm.email.value))
	{
		alert("Invalid Email Address");
		theForm.email.focus();
		theForm.email.value="";
		return (false);
	}
	if (theForm.club.options[theForm.club.selectedIndex].value  == "" || theForm.club.options[theForm.club.selectedIndex].value  == "Select One")
	{
		alert("Please select Your Club");
        theForm.club.focus();
        return (false);
	}
	if(theForm.password1.value == "")
	{
		alert("Please enter a value for the \"Password\" field.");
		theForm.password1.focus();
		theForm.password1.value="";
		return (false);
	}
	if(theForm.password2.value != theForm.password1.value)
	{
		alert("Confirm Password does not match with Password");
		theForm.password2.focus();
		theForm.password2.value="";
		return (false);
	}
	return true;
}
	

</script>  
  
</head>
<body>

  <div data-role="page" id="home" data-theme="a">
  <div data-role="content">
	  <div class="intro">
			<h3>Welcome to the Movati Athletic Instructor App</h3>
		<? if ($_instructor){ ?>
                <p><a href="#add_class" data-role="button" data-ajax="false">Add Class</a></p>

                <p><a href="#my_classes" data-role="button" data-ajax="false">My Classes</a></p>

                <? if ($is_admin){ ?>
                  <p><a href="#admin" data-role="button" data-ajax="false">Admin</a></p>
                <? } ?>

                <p><a href="/instructors/member.php?action=logout" data-role="button" class="logout" data-ajax="false">Logout</a></p>
        <? }else{ ?>
                <p><a href="#members" data-role="button">Login</a></p>
        <? } ?>
        </div>

  </div>
</div>


<div data-role="page" id="members" data-theme="a"> 
 
<div data-role="header" data-position="fixed"> 
	  <h1>Login</h1> 
  </div>


  <div class="ui-body"> 
<p>Please login to continue.</p>

<form method="post" id="form_login">
<input type="hidden" name="action" value="login"/>
<div data-role="fieldcontain" class="aui-hide-label">
	<label for="username">Email:</label>
	<input type="text" name="email" id="username" value="" placeholder="Email"/>
</div>

<div data-role="fieldcontain" class="aui-hide-label">
	<label for="password">Password:</label>
	<input type="password" name="password" id="password" value="" placeholder="Password"/>
</div>

<div data-role="fieldcontain" class="aui-hide-label">
	<h3 class="login_message" style="display: none;"></h3>
	<input type="submit" name="submit" id="submit" value="Login"/>
</div>

</form>
  </div>

  <div data-role="content"> 

    </div>  
  </div> 
 
</div>



<div data-role="page" id="register" data-theme="a"> 	 
	<div data-role="header" data-position="fixed"> 
		  <h1>Instructor Registration</h1> 
    </div>
	<div class="ui-body"> 
		<div class="regsiter_form">
			<h3 class="register_message" style="display: none;"></h3>
			<form id="form_register" >
				<p>Please fill out the form below:</p>

				<input type="hidden" name="action" value="register"/>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="username">First Name:</label>
					<input type="text" name="firstname" id="firstname" value="" placeholder="First Name"/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="username">Last Name:</label>
					<input type="text" name="lastname" id="lastname" value="" placeholder="Last Name"/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="username">Email:</label>
					<input type="text" name="email" id="username" value="" placeholder="Email"/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="select_club">Your Club:</label>
					<select name="club" id="select_club">
				<?
				$clubs = $clubManager->SelectAll();
				 while ($club = $clubs->NextItem()) { ?>
						<?  if ($club->ID > 0) { ?>
						<option value="<?= $club->ID ?>"><?= $club->ClubName ?></option>
					<? }
					} ?>

					</select>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">

					<label for="password">Password:</label>
					<input type="password" name="password1" id="password" value="" placeholder=""/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="password">Password (confirm):</label>
					<input type="password" name="password2" id="password" value="" placeholder=""/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="isadmin">Is Admin</label>
					<input type="checkbox" name="isadmin" id="isadmin" value="1" />
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<input type="button" name="btn_register" id="btn_register" value="Register"/>
				</div>
				</div>
			</form>
	  </div>

	  <div data-role="content"> </div>  
</div> 
 
</div>


<div data-role="page" id="edit_instructor" data-theme="a"> 	 
	<div data-role="header" data-position="fixed"> 
		  <h1>Edit Instructor</h1> 
    </div>
	<div class="ui-body"> 
		<div class="edit_instructor">
			
			<form id="form_edit_instructor">
				<input type="hidden" name="action" value="edit_instructor"/>
				<div data-role="fieldcontain" class="aui-hide-label">
                    <label for="select_instructor">Select Instructor:</label>
                    <select name="instructor" id="select_instructor">
                        <option value="">--Please Select--</option>
						<?
							$inst=$instructor->orderByFirstname()->selectall();
							foreach($inst as $row)
							{
						?>
						<option value="<?= $row["ID"] ?>"><?= $row["FIRSTNAME"] ?> <?= $row["LASTNAME"] ?></option>
						<?}?>
                    </select>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="username">First Name:</label>
					<input type="text" name="firstname" id="firstname" value="" placeholder="First Name"/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="username">Last Name:</label>
					<input type="text" name="lastname" id="lastname" value="" placeholder="Last Name"/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="username">Email:</label>
					<input type="text" name="email" id="username" value="" placeholder="Email"/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="select_club">Instructor Club:</label>
					<select name="club" id="select_club">
					<option value="">--Select Club--</option>
				<?
				$clubs = $clubManager->SelectAll();
				 while ($club = $clubs->NextItem()) { ?>
						<?  if ($club->ID > 0) { ?>
						<option value="<?= $club->ID ?>"><?= $club->ClubName ?></option>
					<? }
					} ?>

					</select>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">

					<label for="password">Password:</label>
					<input type="password" name="password1" id="password" value="" placeholder=""/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="password">Password (confirm):</label>
					<input type="password" name="password2" id="password" value="" placeholder=""/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="isadmin">Is Admin</label>
					<input type="checkbox" name="isadmin" id="isadmin" value="1" />
				</div>

				<div data-role="fieldcontain" class="aui-hide-label">
					<h3 class="edit_instructor_message" style="display: none;"></h3>
					<input type="button" name="btn_edit_instructor" id="btn_edit_instructor" value="Update"/>
				</div>
				</div>
			</form>
	  </div>

	  <div data-role="content"> </div>  
</div> 
 
</div>

<!-- Begin the Add/Edit Class Type -->
<?php $classManager = new ClassManager(); 

$categories = $classManager->SelectClassCategories();
?>
<div data-role="page" id="add_classtype" data-theme="a"> 	 
	<div data-role="header" data-position="fixed"> 
		  <h1>Add Class Type</h1> 
    </div>
	<div class="ui-body"> 
		<div class="add_classtype_form">
			<h3 class="add_classtype_message" style="display: none;"></h3>
			<form id="form_add_classtype" >
				<p>Please fill out the form below:</p>

				<input type="hidden" name="action" value="add_classtype"/>
        
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="class_dropCategory">Category:</label>
        <select id="class_dropCategory" name="class_dropCategory">
          <option value="">-- Select --</option>
					<? for ($i = 0; $i < count($categories); $i++) { ?>
						<option value="<?=$categories[$i+1][2] ?>" <? if ($categories[$i+1][2] == $class->ID) { echo "selected"; } ?>>
							<?=$categories[$i+1][1] ?>
						</option>
					<? } ?>

				</select>
				</div>
        
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="add_classname">Class Name:</label>
					<input type="text" name="add_classname" id="add_classname" value="" placeholder="Class Name"/>
				</div>
        
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="add_classvideolink">Video Link:</label>
					<input type="text" name="add_classvideolink" id="add_classvideolink" value="" placeholder="Video Link"/>
				</div>
        
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="add_classdescription">Class Description:</label>
					<textarea name="add_classdescription" id="add_classdescription"></textarea>
				</div>
        
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="class_dropClassCode">Category:</label>
          <select id="class_dropClassCode" name="class_dropClassCode">
            <option value="B">B - Beginner</option>
            <option value="E">E - Everyone Welcome</option>
            <option value="I">I - Intermediate Class </option>
            <option value="A">A - Advanced Class</option>
            <option value="INT">INT - A VERY intense class</option>
          </select>
        </div>
        
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="add_classisactive">Is Active</label>
					<input type="checkbox" name="add_classisactive" id="add_classisactive" checked="checked" value="1" />
				</div>
        
        
				<div data-role="fieldcontain" class="aui-hide-label">
					<h3 class="add_classtype_message" style="display: none;"></h3>
					<input type="button" name="btn_add_classtype" id="btn_add_classtype" value="Add Class Type"/>
				</div>
				</div>
			</form>
	  </div>

	  <div data-role="content"> </div>  
</div> 
 
</div>


<div data-role="page" id="edit_classtype" data-theme="a"> 	 
	<div data-role="header" data-position="fixed"> 
		  <h1>Edit Class Type</h1> 
    </div>
	<div class="ui-body"> 
		<div class="edit_classtype">
			
			<form id="form_edit_classtype">
				<input type="hidden" name="action" value="edit_classtype"/>
				
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="edit_classtypeid">Class:</label>
					<select name="edit_classtypeid" id="edit_classtypeid" >
					<?php
						if($_SESSION["Instructor"]["CLUBID"])
						{
							$classQuery=new _ClassQuery();
							$clubs=$classQuery->GetClassByClub($_SESSION["Instructor"]["CLUBID"]);
							foreach ($clubs as $row)
							{
					?>			
								<option value="<?= $row["id"] ?>"><?= $row["ClassName"] ?></option>
					<?php
							}
						}
					?>
          </select>
				</div>
        
        <div data-role="fieldcontain" class="aui-hide-label">
					<label for="edit_class_dropCategory">Category:</label>
        <select id="edit_class_dropCategory" name="edit_class_dropCategory">
          <option value="">-- Select --</option>
					<? for ($i = 0; $i < count($categories); $i++) { ?>
						<option value="<?=$categories[$i+1][2] ?>" <? if ($categories[$i+1][2] == $class->ID) { echo "selected"; } ?>>
							<?=$categories[$i+1][1] ?>
						</option>
					<? } ?>

				</select>
				</div>
        
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="edit_classname">Class Name:</label>
					<input type="text" name="edit_classname" id="edit_classname" value="" placeholder="Class Name"/>
				</div>
        
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="edit_classvideolink">Video Link:</label>
					<input type="text" name="edit_classvideolink" id="edit_classvideolink" value="" placeholder="Video Link"/>
				</div>
        
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="edit_classdescription">Class Description:</label>
					<textarea name="edit_classdescription" id="edit_classdescription"></textarea>
				</div>
        
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="edit_class_dropClassCode">Category:</label>
          <select id="edit_class_dropClassCode" name="edit_class_dropClassCode">
            <option value="B">B - Beginner</option>
            <option value="E">E - Everyone Welcome</option>
            <option value="I">I - Intermediate Class </option>
            <option value="A">A - Advanced Class</option>
            <option value="INT">INT - A VERY intense class</option>
          </select>
        </div>
        
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="edit_classisactive">Is Active</label>
					<input type="checkbox" name="edit_classisactive" id="edit_classisactive" checked="checked" value="1" />
				</div>
        
				<div data-role="fieldcontain" class="aui-hide-label">
					<h3 class="edit_classtype_message" style="display: none;"></h3>
					<input type="button" name="btn_edit_classtype" id="btn_edit_classtype" value="Update Class Type"/>
				</div>
				</div>
			</form>
	  </div>

	  <div data-role="content"> </div>  
</div> 
 
</div>
<!-- End the Add/Edit Class Type -->


<div data-role="page" id="admin" data-theme="a">
    <div data-role="header" data-position="fixed">
              <h1>Admin</h1>
    </div>
    <div class="ui-body">
            <ul data-role="listview" data-theme="c" data-dividertheme="d" class="left_menu">
                    <li data-role="list-divider">Admin Options</li>
                    <li><a href="#register">Add Instructors</a></li>
					<li><a href="#edit_instructor">Edit Instructors</a></li>
            <li><a href="#add_classtype">Add Class Type</a></li>
            <li><a href="#edit_classtype">Edit Class Type</a></li>
                    <li data-role="list-divider">Reports</li>
                    <li><a href="#">Payroll Report</a></li>
                    <li><a href="#">Class Report</a></li>
            </ul>

            <div class="right_content">

                    <div class="content_option active">
                        <h2>Payroll Report</h2>

                        <form method="post" action="report.php" name="form_payroll" data-ajax="false" onsubmit="javascript: return validate_report(form_payroll);">
                            <input type="hidden" name="report" value="payroll"/>

                           <div data-role="fieldcontain" class="aui-hide-label">
                            <label for="select_club">Club:</label>
                            <select name="club" id="select_club">
                                <option value=0>All clubs</option>
                <?
                $clubs = $clubManager->SelectAll();
                 while ($club = $clubs->NextItem()) { ?>
                        <?  if ($club->ID > 0) { ?>
                        <option value="<?= $club->ID ?>"><?= $club->ClubName ?></option>
                    <? }
                    } ?>
                               </select>
                            </div>

                <div data-role="fieldcontain" class="aui-hide-label">
                    <label for="date_from">Date From:</label>
                    <input type="text" name="date_from" id="date_from" value="" readonly="true" placeholder="YYYY-MM-DD"/>
                </div>
                <div data-role="fieldcontain" class="aui-hide-label">
                    <label for="date_to">Date To:</label>
                    <input type="text" name="date_to" id="date_to" value="" readonly="true" placeholder="YYYY-MM-DD"/>
                </div>

                        <input type="submit" value="Generate Report"/>
                            
                        </form>
                    </div>

                    <div class="content_option active">
                        <br/><br/><br/><br/>
                        <h2>Class Report</h2>

                        <form method="post" action="report.php" name="form_class" data-ajax="false" onsubmit="javascript: return validate_report(form_class);">
                            <input type="hidden" name="report" value="class"/>

                           <div data-role="fieldcontain" class="aui-hide-label">
                            <label for="select_club">Club:</label>
                            <select name="club" id="select_club">
                                <option value=0>All clubs</option>
                <?
                $clubs = $clubManager->SelectAll();
                 while ($club = $clubs->NextItem()) { ?>
                        <?  if ($club->ID > 0) { ?>
                        <option value="<?= $club->ID ?>"><?= $club->ClubName ?></option>
                    <? }
                    } ?>
                               </select>
                            </div>

                <div data-role="fieldcontain" class="aui-hide-label">
                    <label for="date_from2">Date From:</label>
                    <input type="text" name="date_from" id="date_from2" value="" readonly="true" placeholder="YYYY-MM-DD"/>
                </div>
                <div data-role="fieldcontain" class="aui-hide-label">
                    <label for="date_to2">Date To:</label>
                    <input type="text" name="date_to" id="date_to2" value="" readonly="true" placeholder="YYYY-MM-DD"/>
                </div>

                        <input type="submit" value="Generate Report"/>

                        </form>
                    </div>

                    <!-- <pre>A
                    Edit Instructors: List Instructors to delete/edit add.

                    Payroll Report: From Date, To Date.  Option to filter by club
                            Table shows;  Instructor1 Club1  ClassA   Date  1 hours
                                                      Club1  ClassB   Date  1.5 hours
                                                Instructor Total 2.5 hours/

                    Class Report:  From Date, To Date, Class select, Option to Filter by Club
                                        ClassA   Date   Participants
                                                 Date   Participants
                                                 Total Classes
                                                 Averge Participants
                    </pre> -->
            </div>
    </div>
</div>




<!-- my classses -->
<div data-role="page" id="my_classes" data-theme="a">
    <div data-role="header" data-position="fixed">
              <h1>My Classes</h1>
     </div>
     <div class="ui-body">
      <div class="myclasses">
            <div>
                 <h2>Pay Period: <span class="from"></span> to <span class="to"></span></h2>
            <a href="#" id="previous_payperiod" onclick="gotopay(-1);">&lt; Previous </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="#" id="next_payperiod" onclick="gotopay(1);">Next &gt;</a>

            <table style="width: 1000px;" class="payroll_calendar">
                <thead>
                       <tr>
                       <th>Mon</th>
                       <th>Tue</th>
                       <th>Wed</th>
                       <th>Thu</th>
                       <th>Fri</th>
                       <th>Sat</th>
                       <th>Sun</th>
                       </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

            Total Hours: <span class="total_hours"></span>
            </div>
      </div>
     </div>
</div>




<!--add class form-->

<div data-role="page" id="add_class" data-theme="a"> 	 
	<div data-role="header" data-position="fixed"> 
		  <h1> Add Class</h1> 
    </div>
	<div class="ui-body"> 
		<div class="addclass_form">
			<h3 class="addclass_message" style="display: none;"></h3>
			<form id="form_addclass" name="form_addclass">
				<p>Please fill out the form below:</p>

				<input type="hidden" name="action" value="add_class"/>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="class_club">Club:</label>
					<select name="clubid" id="class_club">
					
				<?
				$clubs = $clubManager->SelectAll();
				 while ($club = $clubs->NextItem()) { ?>
						<?  if ($club->ID > 0) { ?>
						<option <?php echo ($club->ID == $_SESSION["Instructor"]["CLUBID"]) ? 'selected':''  ?> value="<?= $club->ID ?>"><?= $club->ClubName ?></option>
					<? }
					} ?>

					</select>
				</div>
   				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="class_class">Class:</label>
					<select name="classid" id="class_class" >
					<?php
						if($_SESSION["Instructor"]["CLUBID"])
						{
							$classQuery=new _ClassQuery();
							$clubs=$classQuery->GetClassByClub($_SESSION["Instructor"]["CLUBID"]);
							foreach ($clubs as $row)
							{
					?>			
								<option value="<?= $row["id"] ?>"><?= $row["ClassName"] ?></option>
					<?php
							}
						}
					?>
                      </select>
				</div>

                <div data-role="fieldcontain" class="aui-hide-label" id="_select_instructor">
					<label for="class_club">Instructor:</label>
					<select name="instructorid" id="class_instructor">
                    </select>
                </div>



				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="class_date">Date:</label>
					<input type="text" name="class_date" id="class_date" value="" readonly="true" placeholder="YYYY-MM-DD"/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">

					<label for="class_participants">Participants:</label>
					<input type="text" name="participant" id="class_participants" value="0"/>
				</div>
                <div data-role="fieldcontain" class="aui-hide-label">
                    <label for="class_hours">Hours:</label>
					<select name="hours" id="class_hours" >
					<option value="1">1</option>
					<option value="1.25">1.25</option>
					<option value="1.5">1.5</option>
					</select>
                </div>
                <div data-role="fieldcontain" class="aui-hide-label">
                    <label for="class_time">Time:</label>
					<select name="time" id="class_time" >
                    <?php 
                      for ($x = 7; $x<=12; $x++){
                        foreach (array ('00','15','30','45') as $y){ ?> 
                        <option value="<?php echo $x ?>:<?php echo $y ?> <?php echo $x == 12 ? 'pm' : 'am'; ?>"><?php echo $x ?>:<?php echo $y ?> <?php echo $x == 12 ? 'pm' : 'am'; ?></option>
                    <?php } } ?>

                    <?php 
                      for ($x = 1; $x<=9; $x++){
                        foreach (array ('00','15','30','45') as $y){ ?> 
                        <option value="<?php echo $x ?>:<?php echo $y ?> pm"><?php echo $x ?>:<?php echo $y ?> pm</option>
                    <?php } } ?>
					</select>
                </div>
                <div data-role="fieldcontain" class="aui-hide-label">
                    <label for="class_substitute">Is Substitute:</label>
					<input type="checkbox" name="subinstructor" id="class_substitute" />
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<input type="button" name="btn_add_class" id="btn_add_class" value="Add Class"/>
				</div>
				</div>
			</form>
	  </div>

	  <div data-role="content"> </div>  
</div> 
 
</div>

 
</body>
</html>
