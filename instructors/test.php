<?php
session_start();
error_log('here');

//$HOME = "/home/56630/users/.home/domains/theathleticclubs.ca/html/";
$HOME = "/home/ubuntu/toolsmovati";
$_SERVER["DOCUMENT_ROOT"] = $HOME;

require_once "$HOME/vendor/propel/runtime/lib/Propel.php";

// Initialize Propel with the runtime configuration
Propel::init("$HOME/build/conf/site-conf.php");

// Add the generated 'classes' directory to the include path
set_include_path("$HOME/build/classes" . PATH_SEPARATOR . get_include_path());

$is_admin = 0;

if (isset($_SESSION["Instructor"])){
$instructor= new InstructorQuery();
$_instructor = $instructor->findPK( $_SESSION["Instructor"]["id"] );
$is_admin = $_instructor && $_instructor->getIsAdmin() ? 1 : 0;
}


instructor_login();

if($_REQUEST['action']=="login")
{
	instructor_login($_REQUEST);
}
else if(isset($_SESSION["Instructor"]))
{
	if(isset($_REQUEST['action']) || isset($_REQUEST['ajax']))
	{
		if($_REQUEST['action']=="add_class" )
		{
			instructor_add_class($_REQUEST);
		}
		else if($_REQUEST['action']=="register")
		{
			instructor_register($_REQUEST);
		}
		else if($_REQUEST['action']=="edit_instructor")
		{
			if($is_admin) 
			{
				edit_instructor($_REQUEST);
			}
			else
			{
				echo "You are not Administrator.";
				exit;
			}
		}
		else if($_REQUEST['action']=="logout")
		{
			unset($_SESSION["Instructor"]);
			$_SESSION["Instructor"]["id"]=0;
			header("Location: /instructors");
		}
		else if(isset($_REQUEST['instructorId']) && isset($_REQUEST['ajax'])  )
		{
			if($is_admin) #$_SESSION["Instructor"]["isadmin"])##
			{
				get_instructor_data($_REQUEST);
			}
			else
			{
				echo "You are not Administrator.";
				exit;
			}
		}else if(isset($_REQUEST['clubId']) && isset($_REQUEST['ajax']) && isset($_REQUEST['instructors']) )
        {
            get_instructors($_REQUEST);
        }
		else if(isset($_REQUEST['clubId']) && isset($_REQUEST['ajax']) )
		{
			get_clubs($_REQUEST);
		}
		else
		{
			echo "Invalid OP";
		}
	}
	else
	{
		echo "Not action/ajax";
	}
}
else
{
	echo "Not logged In";
}

exit;

function get_instructor_data($data)
{
	$instructor_qry= new InstructorQuery();
	$instructor_obj = $instructor_qry->findPK( $data["instructorId"] );

	$value="";
	$value.="{";
	$value.="\"Email\":\"".$instructor_obj->getEmail()."\",";
	$value.="\"FirstName\":\"".$instructor_obj->getFirstname()."\",";
	$value.="\"LastName\":\"".$instructor_obj->getLastname()."\",";
	$value.="\"ClubId\":\"".$instructor_obj->getClubid()."\",";
	$value.="\"is_admin\":\"".$instructor_obj->getIsAdmin()."\"";
	$value.="}";
	echo $value;
	exit;
}

function edit_instructor($data)
{
	$instructor_qry= new InstructorQuery();
	$instructor_obj = $instructor_qry->findPK( $data["instructor"] );
	$instructor_obj->setEmail($data["email"]);
	if(trim($data["password1"]) != "" && trim($data["password2"]) != "" )
	{
		if($data["password2"] == $data["password1"])
		{
			$instructor_obj->setPassword($data["password1"]);
		}
		else
		{
			echo "Passwords do not match.";exit;
		}
	}
	$instructor_obj->setFirstname($data["firstname"]);
	$instructor_obj->setLastname($data["lastname"]);
	$instructor_obj->setClubid($data["club"]);
	if(isset($data["isadmin"]))
	{
		$instructor_obj->setIsAdmin(1);	
	}
	else
	{
		$instructor_obj->setIsAdmin(0);	
	}
	$q = $instructor_obj->save();
	echo "OK";exit;
}





function instructor_register($data)
{
    global $is_admin;

	if(isset($_SESSION["Instructor"]))
	{
		#if($_SESSION["Instructor"]["isadmin"])
        if ($is_admin == 1)
		{
			if($data['password1']==$data['password2'])
			{
				$instructor= new Instructor();
				$instructor->setEmail($data["email"]);
				$instructor->setPassword($data["password1"]);
				$instructor->setFirstname($data["firstname"]);
				$instructor->setLastname($data["lastname"]);
				$instructor->setClubid($data["club"]);
				if(isset($data["isadmin"]))
				{
					$instructor->setIsAdmin(1);	
				}
				else
				{
					$instructor->setIsAdmin(0);	
				}
				$q = $instructor->save();
				echo "OK";exit;
			}
			echo "failure";
		}
		else
		{
			echo "You do not have enough permission to perform task!";
		}
	}
	else
	{
		echo "Please login before registering an instructor.";
	}
	
}
function instructor_login($data)
{

		$instructor= new InstructorQuery();
		
		$obj=$instructor->Instructor_login('mnolan@the-athletic-club.ca','test');
		if($obj)
		{
			$_SESSION["Instructor"]["id"]=$obj->getid();
			$_SESSION["Instructor"]["fname"]=$obj->getFirstname();
			$_SESSION["Instructor"]["lname"]=$obj->getLastname();
			$_SESSION["Instructor"]["isadmin"]=$obj->getIsAdmin();
			$_SESSION["Instructor"]["CLUBID"]=$obj->getClubid();
			echo "OK";exit;
		}
	
}
function instructor_add_class($data)
{
	if(isset($_SESSION["Instructor"]))
	{
		$Iclass= new Classresult();
		$Iclass->setClassid($data["classid"]);
		$Iclass->setInstructorid( $data["instructorid"] ); #$_SESSION["Instructor"]["id"]);

		if(isset($data["subinstructor"]))
		{
			$Iclass->setSubstituteforinstructorid(1);
		}
		else
		{
			$Iclass->setSubstituteforinstructorid(0);
		}
		$Iclass->setDate($data["class_date"]);
		$Iclass->setNotes($data["notes"]);
		$Iclass->setClubid($data["clubid"]);
		$Iclass->setParticipants($data["participant"]);
		$Iclass->setHours($data["hours"]);
        $Iclass->setStartTime($data["time"]);

		$date=date("Y-m-d H:i:s");
		$Iclass->setLastUpdated($date);
		$q= $Iclass->save();
		echo "OK";
	}
	else
	{
		echo "Please login before adding class.";
	}
	exit;
}


function get_clubs($data)
{
	$classQuery=new _ClassQuery();
		
	$obj=$classQuery->GetClassByClub($data['clubId']);
	$value="";
	$i=0;
	foreach ($obj as $row)
	{	
		//echo $row["id"]."<br/>";
		if($i != 0)
		{
			$value.=',{"optionValue": '.$row["id"].', "optionDisplay": "'.str_replace('"', '', $row["ClassName"]).'"}';
		}
		else
		{
			$value.='[{"optionValue": '.$row["id"].', "optionDisplay": "'.str_replace('"', '', $row["ClassName"]).'"}';
		}
		$i++;
	}
	echo $value."]";
	exit;
}

function get_instructors($data)
{
	$iQuery=new InstructorQuery();
		
	$obj=$iQuery->joinClub()->orderBy('Club.clubname')->orderBy('Instructor.firstname');

	$value="[";
	$i=0;
    $last_club = "";

	foreach ($obj->find() as $row)
	{
        $club = $row->getClub()->getClubname();
		//echo $row["id"]."<br/>";
		if($i != 0)
		{
			$value.=',';
        }

        if ($last_club != $club ){
            $value .= '{"optionValue": -1, "optionDisplay":"' . $club . '"},';
            $last_club = $club;
        }


            $value .= '{"optionValue": '.$row->getId().', "optionDisplay": "'.str_replace('"', '', $row->getFirstname() . " " .   $row->getLastname() ).'"}';

		$i++;
	}
	echo $value."]";
	exit;
}

?>
