<?

$title = "Payroll Report";

$HOME = "/home/ubuntu/toolsmovati";
//$HOME = "/home/ian/projects/athleticclub/site";

require_once "$HOME/vendor/propel/runtime/lib/Propel.php";

// Initialize Propel with the runtime configuration
Propel::init("$HOME/build/conf/site-conf.php");

// Add the generated 'classes' directory to the include path
set_include_path("$HOME/build/classes" . PATH_SEPARATOR . get_include_path());

$classes = new ClassresultQuery();
$classes = $classes->joinClub()->join_Class("c")->joinInstructorRelatedByInstructorid("i");

if ($_POST["club"] > 0){
    $classes = $classes->where("Club.id = ?", $_POST["club"]);
}
$classes = $classes->where(" ClassResult.Date between '" . $_POST["date_from"] . "' and '" . $_POST["date_to"] . "'");



$type = "payroll";


#c->addJoin(ArticlePeer::AUTHOR_ID, AuthorPeer::ID);  
# $c->add(AuthorPeer::NAME, 'John Doe');  
# $articles = ArticlePeer::doSelect($c);  
# // Propel 1.5  
# $articles = ArticleQuery::create()  
#   ->useAuthorQuery()  
#       ->filterByName('John Doe')  
#         ->endUse()  
#           ->find()  

//echo"<pre>";print_r($classes->find());echo"</pre>";exit;
# echo $classes->toString();
$rows = $classes->find();

$show_details = 1;
if ($_POST["report"] == "class"){
    $type="class";

    $title = "Class Report";
}
else if($_POST["report"] == "payroll_csv")
{
	$type = "payroll";
	$title = "Payroll Report";
	$filename="Payroll_Report_".$_POST["club"]."_".$_POST["date_from"]."_".$_POST["date_to"].".csv";


	$csvData="Instructor,Class,Date,Hours\n";

	$last_instructor = new Instructor();
    $instructor = 0;
	$total_hours = 0;
    foreach ($rows as $row )
	{ 
        $instructor = $row->getInstructorRelatedByInstructorid();
        if ($instructor->getId() != $last_instructor->getId()) 
		{ 
			 if ($last_instructor->getId() > 0)
			 {
				 $csvData.=",,Total:,".$total_hours."\n";
			 }
			$csvData.=$instructor->getFirstName().$instructor->getLastName()."\n";
			$last_instructor = $instructor;
			$total_hours = 0;
		}
		$total_hours += $row->getHours(); 
		$csvData.=",".$row->get_Class()->getClassName().",".$row->getDate('M d') .",". $row->getHours()."\n"; 
	}
	$csvData.=",,Total:,".$total_hours."\n";

	header('Content-Description: File Transfer');
	header('Content-Type: text/csv');
	header('Content-Disposition: attachment; filename=Payroll_Report.csv');
	header("Expires: 0");
	header("Pragma: public");
	echo $csvData;exit;
	
	
}
else if($_POST["report"] == "class_csv")
{
	 $type="class";
    $title = "Class Report";
	$csvData="Class,Instructor,Date,Participants\n";

	$last_class = new _Class();
    $class = 0;
    foreach ($rows as $row )
	{
        $class = $row;
        $instructor = $row->getInstructorRelatedByInstructorid();

        if ($class->getId() != $last_class->getId()) 
		{
			if ($last_class->getId() > 0)
			{
				 $csvData.=",,Total Participants:,".$total_participants."\n";
			}
			$csvData.=$class->getName()."\n";
	
            $last_class = $class;
            $total_participants = 0;
            $count = 0;
		}
		$total_participants += $row->getParticipants();
		$csvData.=",".$instructor->getFirstName().$instructor->getLastName().",".$row->getDate('M d') .",". $row->getParticipants()."\n"; 
      }
	  $csvData.=",,Total Participants:,".$total_participants."\n";



	header('Content-Description: File Transfer');
	header('Content-Type: text/csv');
	header('Content-Disposition: attachment; filename=Class_Report.csv');
	header("Expires: 0");
	header("Pragma: public");
	echo $csvData;exit;
	
	
}



	
?>
<html>
<head>
    <style>
    body { font-family: arial, sans-serif; }

    table {
        width: 80%;    
    }

    th {
        background-color: #f1f1f1;
        border-bottom: 2px solid #ccc;
    }

    tr.total td {
        border-bottom: 1px solid #f1f1f1;
    }

    .report {
        margin: 2em;
        border: 1px solid red;
    }  
    </style>
</head>
<body>  
    <div class="report">
    <h1><?= $title ?></h1>
  
  <? if ($type == "payroll") { ?>
	<form action="report.php" method="POST">
		<input type="hidden" value="payroll_csv" name="report"/>
		<input type="hidden" value="<?php echo $_POST["club"];?>" name="club"/>
		<input type="hidden" value="<?php echo $_POST["date_from"];?>" name="date_from"/>
		<input type="hidden" value="<?php echo $_POST["date_to"];?>" name="date_to"/>
		<input type="Submit" value="Export To CSV" id="btn_submit"/>

	</form>


    <table >
    <tr><th>Instructor</th>
        <th>Class</th>
        <th>Date</th>
        <th>Hours</th>
    </tr>
    <? $last_instructor = new Instructor();
    $instructor = 0;
    foreach ($rows as $row ){ 
        $instructor = $row->getInstructorRelatedByInstructorid();

        if ($instructor->getId() != $last_instructor->getId()) { 
        if ($last_instructor->getId() > 0){


    ?>  
        <tr class="total">  
            <td colspan=2></td>
             <td>Total Hours:</td>
             <td><?= $total_hours ?></td>               
        </tr>
    <? } ?>
        <tr>
                <td><?= $instructor->getFirstName() ?> <?= $instructor->getLastName() ?></td>
        </tr>
    <? 
            $last_instructor = $instructor;
            $total_hours = 0;
    } ?>


          <? $total_hours += $row->getHours() ?>
        <tr>
            <td></td>
        <td><?= $row->get_Class()->getClassName() ?></td>
        <td class="date"><?= $row->getDate('M d') ?></td>
        <td class="right"><?= $row->getHours() ?></td>
        </tr>
     <? } ?>

             <tr class="total"> 
                <td colspan=2></td>
                <td>Total Hours:</td>
                <td><?= $total_hours ?></td> 
             </tr>

    </table>

    <? }else{ ?>
	<form action="report.php" method="POST">
		<input type="hidden" value="class_csv" name="report"/>
		<input type="hidden" value="<?php echo $_POST["club"];?>" name="club"/>
		<input type="hidden" value="<?php echo $_POST["date_from"];?>" name="date_from"/>
		<input type="hidden" value="<?php echo $_POST["date_to"];?>" name="date_to"/>
		<input type="Submit" value="Export To CSV" id="btn_submit"/>

	</form>

    <table>
    <tr><th>Class</th>
        <th>Instructor</th>
        <th>Date</th>
        <th>Participants</th>
    </tr>
    <? 
    $last_class = new _Class();
    $class = 0;
    foreach ($rows as $row ){
        $class = $row;
        $instructor = $row->getInstructorRelatedByInstructorid();

        if ($class->getId() != $last_class->getId()) {
        if ($last_class->getId() > 0){


    ?>
        <tr class="total"> 
            <td colspan=2></td>
             <td>Total Participants:</td>
             <td><?= $total_participants ?></td>
        </tr>
    <? } ?>
        <tr>
                <td><?= $class->getName() ?></td>
        </tr>
    <?
            $last_class = $class;
            $total_participants = 0;
            $count = 0;
    } ?>


          <? $total_participants += $row->getParticipants() ?>
        <tr>
            <td></td>
        <td>$<?= $instructor->getFirstName() ?> <?= $instructor->getLastName() ?></td>
        <td class="date"><?= $row->getDate('M d') ?></td>
        <td class="right"><?= $row->getParticipants() ?></td>
        </tr>
     <? } ?>

             <tr class="total">
                <td colspan=2></td>
                <td>Total Participants:</td>
                <td><?= $total_participants ?></td>
             </tr>

    </table>


    <? } ?>

    </div>
</body>
</html>
