<?php
session_start();
error_log('here');

$HOME = "/home/ubuntu/toolsmovati";
$_SERVER["DOCUMENT_ROOT"] = $HOME;

require_once "$HOME/vendor/propel/runtime/lib/Propel.php";

// Initialize Propel with the runtime configuration
Propel::init("$HOME/build/conf/site-conf.php");

// Add the generated 'classes' directory to the include path
set_include_path("$HOME/build/classes" . PATH_SEPARATOR . get_include_path());

$is_admin = 0;

error_reporting(-1);

if (isset($_SESSION["Instructor"])){
$instructor= new InstructorQuery();
$_instructor = $instructor->findPK( $_SESSION["Instructor"]["id"] );
$is_admin = $_instructor && $_instructor->getIsAdmin() ? 1 : 0;
}


if($_REQUEST['action']=="login")
{
	instructor_login($_REQUEST);
}
else if(isset($_SESSION["Instructor"]))
{
	if(isset($_REQUEST['action']) || isset($_REQUEST['ajax']))
	{
		if($_REQUEST['action']=="add_class" )
		{
			instructor_add_class($_REQUEST);
		}
		else if($_REQUEST['action']=="get_payroll_data")
		{
			get_payroll_data($_REQUEST);
		}
                else if($_REQUEST['action']=="delete_class")
                {
                        delete_class($_REQUEST);
                }
		else if($_REQUEST['action']=="add_classtype")
		{
			add_classtype($_REQUEST);
		}
		else if($_REQUEST['action']=="edit_classtype")
		{
			if($is_admin) 
			{
				edit_classtype($_REQUEST);
			}
			else
			{
				echo "You are not Administrator.";
				exit;
			}
		}
		else if($_REQUEST['action']=="register")
		{
			instructor_register($_REQUEST);
		}
		else if($_REQUEST['action']=="edit_instructor")
		{
			if($is_admin) 
			{
				edit_instructor($_REQUEST);
			}
			else
			{
				echo "You are not Administrator.";
				exit;
			}
		}
		else if($_REQUEST['action']=="logout")
		{
			unset($_SESSION["Instructor"]);
			$_SESSION["Instructor"]["id"]=0;
			header("Location: /instructors");
		}
		else if(isset($_REQUEST['instructorId']) && isset($_REQUEST['ajax'])  )
		{
			if($is_admin) #$_SESSION["Instructor"]["isadmin"])##
			{
				get_instructor_data($_REQUEST);
			}
			else
			{
				echo "You are not Administrator.";
				exit;
			}
		}
		else if(isset($_REQUEST['classtypeId']) && isset($_REQUEST['ajax'])  )
		{
			if($is_admin) #$_SESSION["Instructor"]["isadmin"])##
			{
				get_classtype_data($_REQUEST);
			}
			else
			{
				echo "You are not Administrator.";
				exit;
			}
		}else if(isset($_REQUEST['clubId']) && isset($_REQUEST['ajax']) && isset($_REQUEST['instructors']) )
        {
            get_instructors($_REQUEST);
        }
		else if(isset($_REQUEST['clubId']) && isset($_REQUEST['ajax']) )
		{
			get_clubs($_REQUEST);
		}
		else
		{
			echo "Invalid OP";
		}
	}
	else
	{
		echo "Not action/ajax";
	}
}
else
{
	echo "Not logged In";
}

exit;

function get_instructor_data($data)
{
	$instructor_qry= new InstructorQuery();
	$instructor_obj = $instructor_qry->findPK( $data["instructorId"] );

	$value="";
	$value.="{";
	$value.="\"Email\":\"".$instructor_obj->getEmail()."\",";
	$value.="\"FirstName\":\"".$instructor_obj->getFirstname()."\",";
	$value.="\"LastName\":\"".$instructor_obj->getLastname()."\",";
	$value.="\"ClubId\":\"".$instructor_obj->getClubid()."\",";
	$value.="\"is_admin\":\"".$instructor_obj->getIsAdmin()."\"";
	$value.="}";
	echo $value;
	exit;
}


function get_classtype_data($data)
{
	$classtype_qry= new _ClassQuery();
	$classtype_obj = $classtype_qry->findPK( $data["classtypeId"] );
  
  $data = array(
      'classtypeid' => $classtype_obj->getId(),
      'catid' => $classtype_obj->getCatid(),
      'classname' => $classtype_obj->getClassname(),
      'classdescription' => $classtype_obj->getDescription(),
      'classvideolink' => $classtype_obj->getVideolink(),
      'classisactive' => $classtype_obj->getActive(),
      'classcode' => $classtype_obj->getClasscode(),
  );
  echo json_encode($data);
  exit;
}

function edit_instructor($data)
{
	$instructor_qry= new InstructorQuery();
	$instructor_obj = $instructor_qry->findPK( $data["instructor"] );
	$instructor_obj->setEmail($data["email"]);
	if(trim($data["password1"]) != "" && trim($data["password2"]) != "" )
	{
		if($data["password2"] == $data["password1"])
		{
			$instructor_obj->setPassword(md5($data["password1"]));
		}
		else
		{
			echo "Passwords do not match.";exit;
		}
	}
	$instructor_obj->setFirstname($data["firstname"]);
	$instructor_obj->setLastname($data["lastname"]);
	$instructor_obj->setClubid($data["club"]);
	if(isset($data["isadmin"]))
	{
		$instructor_obj->setIsAdmin(1);	
	}
	else
	{
		$instructor_obj->setIsAdmin(0);	
	}
	$q = $instructor_obj->save();
	echo "OK";exit;
}





function instructor_register($data)
{
    global $is_admin;

	if(isset($_SESSION["Instructor"]))
	{
		#if($_SESSION["Instructor"]["isadmin"])
        if ($is_admin == 1)
		{
			if($data['password1']==$data['password2'])
			{
				$instructor= new Instructor();
				$instructor->setEmail($data["email"]);
				$instructor->setPassword(md5($data["password1"]));
				$instructor->setFirstname($data["firstname"]);
				$instructor->setLastname($data["lastname"]);
				$instructor->setClubid($data["club"]);
				if(isset($data["isadmin"]))
				{
					$instructor->setIsAdmin(1);	
				}
				else
				{
					$instructor->setIsAdmin(0);	
				}
				$q = $instructor->save();
				echo "OK";exit;
			}
			echo "failure";
		}
		else
		{
			echo "You do not have enough permission to perform task!";
		}
	}
	else
	{
		echo "Please login before registering an instructor.";
	}
	
}
function instructor_login($data)
{

	if(($data["email"]!="")&&($data["password"]!=""))
	{
		$instructor= new InstructorQuery();
		
		$obj=$instructor->Instructor_login($data["email"],$data["password"]);
		if($obj)
		{
			$_SESSION["Instructor"]["id"]=$obj->getid();
			$_SESSION["Instructor"]["fname"]=$obj->getFirstname();
			$_SESSION["Instructor"]["lname"]=$obj->getLastname();
			$_SESSION["Instructor"]["isadmin"]=$obj->getIsAdmin();
			$_SESSION["Instructor"]["CLUBID"]=$obj->getClubid();
			echo "OK";exit;
		}
		echo "?";
	
	}
	echo "Please enter valid email & password";
}
function instructor_add_class($data)
{
	error_log('adding_class');
	if(isset($_SESSION["Instructor"]))
	{
		error_log('yes');
		$Iclass= new Classresult();
		$Iclass->setClassid($data["classid"]);
		$Iclass->setInstructorid( $data["instructorid"] ); #$_SESSION["Instructor"]["id"]);

		if(isset($data["subinstructor"]))
		{
			$Iclass->setSubstituteforinstructorid(1);
		}
		else
		{
			$Iclass->setSubstituteforinstructorid(0);
		}
		$Iclass->setDate($data["class_date"]);
		if (isset($data["notes"])){
			$Iclass->setNotes($data["notes"]);
		}
		error_log('added notes');

		$Iclass->setClubid($data["clubid"]);
		$Iclass->setParticipants($data["participant"]);
		$Iclass->setHours($data["hours"]);
        $Iclass->setStartTime($data["time"]);

		$date=date("Y-m-d H:i:s");
		$Iclass->setLastUpdated($date);
			error_log('saving');

		$q= $Iclass->save();

		error_log('done, ok');
		echo "OK";
	}
	else
	{
		echo "Please login before adding class.";
	}
	error_log('exiting');
	exit;
}

function add_classtype($data)
{
	error_log('adding_classtype');
	if(isset($_SESSION["Instructor"]))
	{
		error_log('yes');
		$classtype = new _Class();
    $classtype->setCatid($data["class_dropCategory"]);
    $classtype->setClassname($data["add_classname"]);
    $classtype->setDescription($data["add_classdescription"]);
    $classtype->setVideolink($data["add_classvideolink"]);
    $classtype->setClasscode($data["class_dropClassCode"]);
    if(isset($data["add_classisactive"]))
    {
      $classtype->setActive(1);	
    }
    else
    {
      $classtype->setActive(0);	
    }
    $q = $classtype->save();
    
		error_log('done, ok');
		echo "OK";
	}
	else
	{
		echo "Please login before adding class type.";
	}
	error_log('exiting');
	exit;
}

function edit_classtype($data)
{
	$classtype_qry= new _ClassQuery();
	$classtype_obj = $classtype_qry->findPK( $data["edit_classtypeid"] );
  $classtype_obj->setCatid($data["edit_class_dropCategory"]);
  $classtype_obj->setClassname($data["edit_classname"]);
  $classtype_obj->setDescription($data["edit_classdescription"]);
  $classtype_obj->setVideolink($data["edit_classvideolink"]);
  $classtype_obj->setClasscode($data["edit_class_dropClassCode"]);
  if(isset($data["edit_classisactive"]))
  {
    $classtype_obj->setActive(1);	
  }
  else
  {
    $classtype_obj->setActive(0);	
  }
	$q = $classtype_obj->save();
	echo "OK";exit;
}


function get_clubs($data)
{
	$classQuery=new _ClassQuery();
		
	$obj=$classQuery->GetClassByClub($data['clubId']);
	$value="";
	$i=0;
	foreach ($obj as $row)
	{	
		//echo $row["id"]."<br/>";
		if($i != 0)
		{
			$value.=',{"optionValue": '.$row["id"].', "optionDisplay": "'.str_replace('"', '', $row["ClassName"]).'"}';
		}
		else
		{
			$value.='[{"optionValue": '.$row["id"].', "optionDisplay": "'.str_replace('"', '', $row["ClassName"]).'"}';
		}
		$i++;
	}
	echo $value."]";
	exit;
}

function get_instructors($data)
{
	$iQuery=new InstructorQuery();

	//Make 150 15 for Barrhaven
	if ( $_GET["clubId"] == 8 || $_GET["clubId"] == 9 || $_GET["clubId"] == 14 || $_GET["clubId"] == 15 || $_GET["clubId"] == 16){
		$obj=$iQuery->joinClub()->where("Club.id in (8,9,14,15,16,?) ", $_GET["clubId"])->orderBy('Club.clubName')->orderBy('Instructor.lastname')->orderBy('Instructor.firstname');
	}else{
		$obj=$iQuery->joinClub()->where("Club.id = ?", $_GET["clubId"])->orderBy('Club.clubName')->orderBy('Instructor.lastname')->orderBy('Instructor.firstname');
	}

	$value="[";
	$i=0;
    $last_club = "";

	foreach ($obj->find() as $row)
	{
        $club = $row->getClub()->getClubname();
		//echo $row["id"]."<br/>";
		if($i != 0)
		{
			$value.=',';
        }

        if ($last_club != $club ){
            $value .= '{"optionValue": -1, "optionDisplay":"' . $club . '"},';
            $last_club = $club;
        }


            $value .= '{"optionValue": '.$row->getId().', "optionDisplay": "'.str_replace('"', '', $row->getLastname() . ", " .   $row->getFirstname() ).'"}';

		$i++;
	}

	$value =str_replace("\\'","'",$value);
	echo $value."]";
	exit;
}



function aget_payroll_data($data){
	echo json_encode("test");
}

function delete_class($data){
	$classes = new ClassresultQuery();
	$to_delete = $classes->findPK(  $_REQUEST['classId'] );
	if($to_delete->getInstructorid() == $_SESSION["Instructor"]["id"]){
		$to_delete->delete();
		echo "Class Deleted";
	}else{
		echo "Error";
	}
}

function get_payroll_data($data){
$classes = new ClassresultQuery();
$classes = $classes->joinClub()->join_Class("c")->leftJoinInstructor("i");


$date_from = new DateTime( $_REQUEST['date'] );
$date_to = clone $date_from;
$date_to->add( new DateInterval('P13D') );



$classes = $classes->where(" ClassResult.Instructorid = ?", $_SESSION["Instructor"]["id"] );

$classes = $classes->where(" ClassResult.Date between '" . ymd($date_from) . "' and '" . ymd($date_to) . "'");

    $result = array( pay_start_date=>ymd($date_from), pay_end_date=>ymd($date_to), pay_number => $pay_number, );
    $result["classes"] = array();
    $deleteable = 0;
    $today = ymd(new DateTime());
    if ($today >= ymd($date_from) && $today <= ymd($date_to)){
	$deleteable = 1;
    }
foreach ($classes->find() as $row){
    if ($row->get_Class() != null){
    array_push( $result["classes"], array( 
            'id' => $row->getID(),
            'class'=> $row->get_Class()->getClassname(),
            'hours'=> $row->getHours(),
            'date'=>$row->getDate(),
            'day_index'=>get_day_offset(  $row->getDate(), ymd($date_from) ),
	    'deleteable'=> $deleteable
            ) );
	}
}
    echo json_encode($result);
};

function get_day_offset( $date, $first_day ){
    $seconds = strtotime($date) - strtotime($first_day);
    return $seconds / (60 * 60 *  24);
}

function ymd($d){
    return $d->format('Y-m-d');
}

?>
