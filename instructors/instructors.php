<?php
ini_set('session.gc_maxlifetime', 86400);
session_start();

//$HOME = "/home/ian/projects/athleticclub/site";
$HOME = "/home/ubuntu/toolsmovati";

require_once "$HOME/vendor/propel/runtime/lib/Propel.php";

// Initialize Propel with the runtime configuration
Propel::init("$HOME/build/conf/site-conf.php");

// Add the generated 'classes' directory to the include path
set_include_path("$HOME/build/classes" . PATH_SEPARATOR . get_include_path());


if($_REQUEST['action']=="login")
{
	instructor_login($_REQUEST);
}
else if(isset($_SESSION["Instructor"]))
{
	if(isset($_REQUEST['action']) || isset($_REQUEST['ajax']))
	{
		if($_REQUEST['action']=="add_class" )
		{
			instructor_add_class($_REQUEST);
		}
		else if($_REQUEST['action']=="register")
		{
			instructor_register($_REQUEST);
		}
		else if($_REQUEST['action']=="edit_instructor")
		{
			if($_SESSION["Instructor"]["isadmin"])#
			{
				edit_instructor($_REQUEST);
			}
			else
			{
				echo "You are not Administrator.";
				exit;
			}
		}
		else if($_REQUEST['action']=="logout")
		{
			unset($_SESSION["Instructor"]);
			$_SESSION["Instructor"]["id"]=0;
			header("Location: /instructors");
		}
		else if(isset($_REQUEST['instructorId']) && isset($_REQUEST['ajax'])  )
		{
			if($_SESSION["Instructor"]["isadmin"])##
			{
				get_instructor_data($_REQUEST);
			}
			else
			{
				echo "You are not Administrator.";
				exit;
			}
		}
		else if(isset($_REQUEST['clubId']) && isset($_REQUEST['ajax']) )
		{
			get_clubs($_REQUEST);
		}
		else
		{
			echo "Invalid OP";
		}
	}
	else
	{
		echo "Not action/ajax";
	}
}
else
{
	echo "Not logged In";
}

exit;

function get_instructor_data($data)
{
	$instructor_qry= new InstructorQuery();
	$instructor_obj = $instructor_qry->findPK( $data["instructorId"] );

	$value="";
	$value.="{";
	$value.="\"Email\":\"".$instructor_obj->getEmail()."\",";
	$value.="\"FirstName\":\"".$instructor_obj->getFirstname()."\",";
	$value.="\"LastName\":\"".$instructor_obj->getLastname()."\",";
	$value.="\"ClubId\":\"".$instructor_obj->getClubid()."\",";
	$value.="\"is_admin\":\"".$instructor_obj->getIsAdmin()."\"";
	$value.="}";
	echo $value;
	exit;
}

function edit_instructor($data)
{
	$instructor_qry= new InstructorQuery();
	$instructor_obj = $instructor_qry->findPK( $data["instructor"] );
	$instructor_obj->setEmail($data["email"]);
	if(trim($data["password1"]) != "" && trim($data["password2"]) != "" )
	{
		if($data["password2"] == $data["password1"])
		{
			$instructor_obj->setPassword($data["password1"]);
		}
		else
		{
			echo "Passwords do not match.";exit;
		}
	}
	$instructor_obj->setFirstname($data["firstname"]);
	$instructor_obj->setLastname($data["lastname"]);
	$instructor_obj->setClubid($data["club"]);
	if(isset($data["isadmin"]))
	{
		$instructor_obj->setIsAdmin(1);	
	}
	else
	{
		$instructor_obj->setIsAdmin(0);	
	}
	$q = $instructor_obj->save();
	echo "OK";exit;
}





function instructor_register($data)
{
	if(isset($_SESSION["Instructor"]))
	{
		if($_SESSION["Instructor"]["isadmin"])
		{
			if($data['password1']==$data['password2'])
			{
				$instructor= new Instructor();
				$instructor->setEmail($data["email"]);
				$instructor->setPassword($data["password1"]);
				$instructor->setFirstname($data["firstname"]);
				$instructor->setLastname($data["lastname"]);
				$instructor->setClubid($data["club"]);
				if(isset($data["isadmin"]))
				{
					$instructor->setIsAdmin(1);	
				}
				else
				{
					$instructor->setIsAdmin(0);	
				}
				$q = $instructor->save();
				echo "OK";exit;
			}
			echo "failure";
		}
		else
		{
			echo "You do not have enough permission to perform task.";
		}
	}
	else
	{
		echo "Please login before registering an instructor.";
	}
	
}
function instructor_login($data)
{

	if(($data["email"]!="")&&($data["password"]!=""))
	{
		$instructor= new InstructorQuery();
		
		$obj=$instructor->Instructor_login($data["email"],$data["password"]);
		if($obj)
		{
			$_SESSION["Instructor"]["id"]=$obj->getid();
			$_SESSION["Instructor"]["fname"]=$obj->getFirstname();
			$_SESSION["Instructor"]["lname"]=$obj->getLastname();
			$_SESSION["Instructor"]["isadmin"]=$obj->getIsAdmin();
			$_SESSION["Instructor"]["CLUBID"]=$obj->getClubid();
			echo "OK";exit;
		}
	
	}
	echo "Please enter valid email & password";
}
function instructor_add_class($data)
{
	if(isset($_SESSION["Instructor"]))
	{
		$Iclass= new Classresult();
		$Iclass->setClassid($data["classid"]);
		$Iclass->setInstructorid($_SESSION["Instructor"]["id"]);
		if(isset($data["subinstructor"]))
		{
			$Iclass->setSubstituteforinstructorid(1);
		}
		else
		{
			$Iclass->setSubstituteforinstructorid(0);
		}
		$Iclass->setDate($data["class_date"]);
		$Iclass->setNotes($data["notes"]);
		$Iclass->setClubid($data["clubid"]);
		$Iclass->setParticipants($data["participant"]);
		$Iclass->setHours($data["hours"]);
		$date=date("Y-m-d H:i:s");
		$Iclass->setLastUpdated($date);
		$q= $Iclass->save();
		echo "OK";
	}
	else
	{
		echo "Please login before adding class.";
	}
	exit;
}

function get_clubs($data)
{
	$classQuery=new _ClassQuery();
		
	$obj=$classQuery->GetClassByClub($data['clubId']);
	$value="";
	$i=0;
	foreach ($obj as $row)
	{	
		//echo $row["id"]."<br/>";
		if($i != 0)
		{
			$value.=',{"optionValue": '.$row["id"].', "optionDisplay": "'.str_replace('"', '', $row["ClassName"]).'"}';
		}
		else
		{
			$value.='[{"optionValue": '.$row["id"].', "optionDisplay": "'.str_replace('"', '', $row["ClassName"]).'"}';
		}
		$i++;
	}
	echo $value."]";
	exit;
}

?>
