<?

function myErrorHandler($errno, $errstr, $errfile, $errline)
{
    switch ($errno) {
    case E_USER_ERROR:
        echo "<b>My ERROR</b> [$errno] $errstr<br />\n";
        echo "  Fatal error on line $errline in file $errfile";
        echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
        echo "Aborting...<br />\n";
        exit(1);
        break;

    case E_USER_WARNING:
        echo "<b>My WARNING</b> [$errno] $errstr<br />\n";
        break;

    case E_USER_NOTICE:
        echo "<b>My NOTICE</b> [$errno] $errstr<br />\n";
        break;

    default:
        echo "Unknown error type: [$errno] $errstr<br />\n";
        break;
    }

	echo "error?";
    /* Don't execute PHP internal error handler */
    return true;
}

$title = "Payroll Report";

$HOME = "/home/ubuntu/toolsmovati";
$_SERVER["DOCUMENT_ROOT"] = $HOME;


require_once "$HOME/vendor/propel/runtime/lib/Propel.php";

// Initialize Propel with the runtime configuration
Propel::init("$HOME/build/conf/site-conf.php");

// Add the generated 'classes' directory to the include path
set_include_path("$HOME/build/classes" . PATH_SEPARATOR . get_include_path());

$classes = new ClassresultQuery();
$classes = $classes->joinClub()->join_Class("c")->leftJoinInstructor("i");

$filter =  "";
if ($_POST["filter"] > ""){
	$filter = $_POST["filter"];
}

if ($_POST["club"] > 0){
    $classes = $classes->where("Club.id = ?", $_POST["club"]);
}
$classes = $classes->where(" ClassResult.Date between '" . $_POST["date_from"] . "' and '" . $_POST["date_to"] . "'");

set_error_handler('myErrorHandler');
error_reporting(-1);
error_log('test');

$type = "payroll";


#c->addJoin(ArticlePeer::AUTHOR_ID, AuthorPeer::ID);  
# $c->add(AuthorPeer::NAME, 'John Doe');  
# $articles = ArticlePeer::doSelect($c);  
# // Propel 1.5  
# $articles = ArticleQuery::create()  
#   ->useAuthorQuery()  
#       ->filterByName('John Doe')  
#         ->endUse()  
#           ->find()  

//echo"<pre>";print_r($classes->find());echo"</pre>";exit;
# echo $classes->toString();

$show_details = 1;

if ($_POST["report"] == "class"){

    $type="class";
    $title = "Class Report";
	#$rows=$classes->getClassresults($_POST["club"],$_POST["date_from"],$_POST["date_to"]);
	#$rows = $classes->find();
    #print_r($classes);
    $rows = $classes->orderBy('c.Classname')->find();
}
else if($_POST["report"] == "payroll_csv")
{
$rows = $classes->orderBy('i.Lastname')->orderBy('i.Firstname')->find();

	$type = "payroll";
	$title = "Payroll Report";
	$csvData="Instructor,Class,Date,Hours\n";

	$last_instructor = new Instructor();
    $instructor = 0;
	$total_hours = 0;
    foreach ($rows as $row )
	{ 
        $instructor = $row->getInstructor();
        if ($instructor->getId() != $last_instructor->getId()) 
		{ 
			 if ($last_instructor->getId() > 0)
			 {
				 $csvData.=",,Total:,".$total_hours."\n";
			 }
			$csvData.=$instructor->getFirstName().$instructor->getLastName()."\n";
			$last_instructor = $instructor;
			$total_hours = 0;
		}
		$total_hours += $row->getHours(); 
		$csvData.=",".$row->get_Class()->getClassName().",".$row->getDate('M d') .",". $row->getHours()."\n"; 
	}
	$csvData.=",,Total:,".$total_hours."\n";

	header('Content-Description: File Transfer');
	header('Content-Type: text/csv');
	header('Content-Disposition: attachment; filename=Payroll_Report.csv');
	header("Expires: 0");
	header("Pragma: public");
	echo $csvData;exit;
	
	
}
else if($_POST["report"] == "class_csv")
{
$rows = $classes->orderBy('i.Lastname')->orderBy('i.Firstname')->find();
	 $type="class";
    $title = "Class Report";
	$rows=$classes->getClassresults($_POST["club"],$_POST["date_from"],$_POST["date_to"]);
	$csvData="Class,Instructor,Date,Participants\n";

	$classID = 0;
	$loop=0;
	$total_participant=0;
    foreach ($rows as $row )
	{
		
        if($classID == $row['ClassID'])
		{
			$csvData.=",".$row['FirstName']." ".$row['LastName'].",".$row['start_date'].",".$row['Participants']."\n";			
		}
		else
		{
			if($loop>0)
			{
				$csvData.=",,Total Participant:,".$total_participant."\n";	
				$total_participant=0;
			}
			
			$csvData.=$row['ClassName']."\n";
			$csvData.=",".$row['FirstName']." ".$row['LastName'].",".$row['start_date'].",".$row['Participants']."\n";
			$classID=$row['ClassID'];
			$loop++;

		}
		$total_participant+=$row['Participants'];

        
	}
	$csvData.=",,Total Participant:,".$total_participant."\n";
	  header('Content-Description: File Transfer');
	  header('Content-Type: text/csv');
	  header('Content-Disposition: attachment; filename=Class_Report.csv');
	  header("Expires: 0");
	  header("Pragma: public");
	  echo $csvData;exit;
	
	
}else{
	$rows = $classes->orderBy('i.Lastname')->orderBy('i.Firstname')->orderBy('Date')->find();
}



	
?>
<html>
<head>
    <style>
    body { font-family: arial, sans-serif; }

    table {
        width: 70%;    
    }

    th {
        background-color: #f1f1f1;
        border-bottom: 2px solid #ccc;
    }
	tr.head{
		background-color: #354C61;
		color:#FFFFFF;
	}
	tr.altrow{
		background-color: #DDDDDD;
	}

    tr.total td {
        border-bottom: 1px solid #f1f1f1;
		text-align:right;
		
    }
	.right{
		text-align:right;
	}

    .report {
        margin: 2em;
    }  
    </style>
</head>
<body>  
    <div class="report">
    <h1><?= $title ?></h1>
  
  <? if ($type == "payroll") { ?>
	<form action="report.php" method="POST">
		<input type="hidden" value="payroll_csv" name="report"/>
		<input type="hidden" value="<?php echo $_POST["club"];?>" name="club"/>
		<input type="hidden" value="<?php echo $_POST["date_from"];?>" name="date_from"/>
		<input type="hidden" value="<?php echo $_POST["date_to"];?>" name="date_to"/>

		<input type="Submit" value="Filter" id="btn_submit"/>
	</form>




    <table >
    <tr><th>Instructor</th>
        <th>Class</th>
        <th>Date</th>
        <th>Hours</th>
    </tr>
	
    <? $last_instructor = new Instructor();
    $instructor = 0;
    foreach ($rows as $row ){ 
	#echo $row->getId();

        $instructor = $row->getInstructor();

        if ($instructor->getId() != $last_instructor->getId()) { 
        if ($last_instructor->getId() > 0){


    ?>  
        <tr class="total">  
            <td colspan=2></td>
             <td>Total Hours:</td>
             <td><?= $total_hours ?></td>               
        </tr>
    <? } ?>
        <tr class="head">
                <td colspan="4"><b><?= $instructor->getLastName() ?>, <?= $instructor->getFirstName() ?></b></td>
        </tr>
    <? 
            $last_instructor = $instructor;
            $total_hours = 0;
    } ?>


          <? $total_hours += $row->getHours() ?>
        <tr class="altrow">
            <td></td>
	
       <td><?= $row->get_Class() != null ? $row->get_Class()->getClassName() : 'Deleted Class'  ?>  <?= $row->getSubstituteForInstructorID() > 0 ? '<b>Subbed</b>' : '' ?> </td>
        <td class="date"><?= $row->getDate('M d') ?> <?= $row->getStartTime() ?></td>
        <td class="right"><?= $row->getHours() ?></td>
        </tr>
     <? } ?>

             <tr class="total"> 
                <td colspan=2></td>
                <td>Total Hours:</td>
                <td><?= $total_hours ?></td> 
             </tr>

    </table>

    <? }else{ ?>
	<form action="report.php" method="POST">
                <input type="hidden" value="class" name="report"/>
		<input type="hidden" value="<?php echo $_POST["club"];?>" name="club"/>
		<input type="hidden" value="<?php echo $_POST["date_from"];?>" name="date_from"/>
		<input type="hidden" value="<?php echo $_POST["date_to"];?>" name="date_to"/>

                <input type="Submit" value="Refresh" id="btn_submit"/>
	</form>

    <table>
    <tr><th>Class</th>
        <th>Instructor</th>
        <th>Date</th>
        <th>Participants</th>
    </tr>
    <?php  $classID = 0;$loop=0;$total_participant=0;$class="altrow"; $classCount = 1;?>

	<?php foreach ($rows as $row ):?>
		<?php #echo $row->getClassID() ?>
    <? $instructor = $row->getInstructor(); ?>

			<?php if($classID==$row->getClassID()):?>
				<tr class="">
					<td>&nbsp;</td>
					<td><?= $instructor->getFirstName() ?> <?= $instructor->getLastName() ?></td>
					<td><?= $row->getDate('M d') ?> <?= $row->getStartTime() ?></td>
					<td style="text-align:right;"><?= $row->getParticipants() ?></td>
				</tr>
							
			<?php else:?>
				<?php if($loop>0):?>
					<tr class="total">
						<td colspan=2></td>
						<td>Total Participants:</td>
						<td><?= $total_participant ?></td>
					 </tr>
                                        <tr class="total">
                                                <td colspan=2></td>
                                                <td>Average Participants:</td>
                                                <td><?= round($total_participant / $classCount,1) ?> (in <?= $classCount ?> classes)</td>
                                         </tr>
				<?php endif;			
				      $total_participant=0;
					  $classID=$row->getClassID();
					  $loop++;
				?>
				<? $classCount = 0 ?>
				 <tr class="head">
						<td colspan="4"><b> <?= $row->get_Class()->getClassName() ?></b></td>
				 </tr>
				 <tr class="<?= $class ?>" >
					<td>&nbsp;</td>
					<td><?= $instructor->getFirstName() ?> <?= $instructor->getLastName() ?> <?= $row->getSubstituteForInstructorID() > 0 ? '<b>Subbed</b>' : '' ?></td>
					<td><?= $row->getDate('M d')  ?> <?= $row->getStartTime() ?></td>
					<td style="text-align:right;"><?= $row->getParticipants() ?></td>
				</tr>
		  <?php endif;?> 
		<?php $total_participant += $row->getParticipants(); ?>
		<? $classCount++ ?>
	<?php endforeach;?>
	<tr class="total">
		<td colspan=2></td>
		<td>Total Participants: ?</td>
		<td><?= $total_participant ?></td>
	 </tr>


    </table>


    <? } ?>

    </div>
</body>
</html>
