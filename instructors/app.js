var _App = function(){ 
  
  this.name;
  this.attach(); 
 
}; 

_App.prototype = { 
  // now we define the prototype for Disabler 
  defaults: { 
    attr: 'checked', 
    expected: true, 
    onChange: function(){} 
  }, 
 
  update: function(){
	alert( data );
  },

  test: function(element){ 
   
  }, 
 
  attach: function(){ 
   
  } 
 
}; 

var App = new _App();

var day = '2011-11-01';

function gotopay(direction){
	window.payPeriodDate = window.payPeriodDate.add({days: 14 * direction});
	
	updateMyClasses();	
}

function updateMyClasses(){
		payPeriodsSince = Math.floor( Math.floor( (moment() - moment('2014-01-13')) / (24 * 60 * 60 * 1000) ) / 14 );
		
		
		if (window.payPeriodDate == undefined){
			window.payPeriodDate = moment('2014-01-13').add({days: 14 * payPeriodsSince });
		}
	
	
	$.get('member.php', {action: 'get_payroll_data', date: window.payPeriodDate.format('YYYY-MM-DD')}	).done(function(data){
		eval( "data=" + data );
		
		refreshClasses(data);
	});
}


function deleteClass(classId){
	if (confirm("Are you sure you want to delete this class?")){
		 $.get('member.php', {action: 'delete_class',classId: classId}       ).done(function(data){
		alert(data);	
		updateMyClasses();		
		});
	}
return false;
}
	function refreshClasses(data){
		
		// {"pay_start_date":"2014-06-30","pay_end_date":"2014-07-13","pay_number":"13","classes":[{"id":26732,"class":"Basic Pilates","hours":"1.00","date":"2014-07-04 00:00:00","day_index":4}]};
		
	    day =  moment( data.pay_start_date);
		
		$(".payroll_calendar tbody").html("");
		var total_hours = 0;
		
		var tr = $("<tr></tr>");
		for(var day_loop = 0; day_loop < 14; day_loop++){
			today = moment( data.pay_start_date).add({days: day_loop});
			td = $("<td><span class='dom'>" + today.format('D') + "</span></td>");
			
			day_data = _.where( data.classes, {day_index: day_loop});
			if(day_data.length > 0){
				_.each( day_data, function(d){
					row = $("<span class='row'/>");
					
					row.append( $("<span class='class'/>").text(d.class) );
					row.append( $(" <span class='hours'/>").text(" " + d.hours) );
					if (d.deleteable){
						row.append( $(" <a href='#' class='del' onclick='return deleteClass(" + d.id + ")'>[X]</a>"));
					}
					td.append( row );
					
					total_hours += parseFloat( d.hours );
				});
			}
			
			tr.append( td );
			
			if (day_loop == 6 || day_loop == 13){
				$(".payroll_calendar tbody").append( tr );	
				tr = $("<tr></tr>");
				
				
			}
			
		}
		
		$('.total_hours').text( total_hours );
		$('.from').text( data.pay_start_date );
		$('.to').text( data.pay_end_date );
		
	}
	
		
	