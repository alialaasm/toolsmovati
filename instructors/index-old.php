<?php
session_start();

$HOME = "/home/ian/projects/athleticclub/site";

require_once "$HOME/vendor/propel/runtime/lib/Propel.php";

// Initialize Propel with the runtime configuration
Propel::init("$HOME/build/conf/site-conf.php");

// Add the generated 'classes' directory to the include path
set_include_path("$HOME/build/classes" . PATH_SEPARATOR . get_include_path());



require_once($_SERVER["DOCUMENT_ROOT"] . "/config.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClubManager.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClassManager.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/StudioManager.php");



$clubID = 0;
$clubManager = new ClubManager();
$clubs = $clubManager->SelectAll();
$studioManager = new StudioManager();



$myClub = 0;

$classManager= new ClassManager();
$instructor= new InstructorQuery();

$_instructor = 0;
$is_admin = 0;

if (isset($_SESSION["Instructor"])){
$_instructor = $instructor->findPK( $_SESSION["Instructor"]["id"] );
$is_admin = $_instructor && $_instructor->getIsAdmin() == 1;
}



function print_class($c,$member, $has_class=0){
?>
<li class="class_item_<?= $c->EventID ?>">
<h4><?= $c->Name ?> <span class="small nobreak"><?= $c->Time ?></span>
<br/><span class="instructor small"><?= $c->Instructor ?></span>
</h4>
<p><?= $c->Studio ?></p>

<? if ($member){
	}


}

function has_class( $member, $c ){
	return 0;	
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>The Athletic Club</title>

	<meta name="viewport" content="width=default-width, initial-scale=1" />
	
	<script type="text/javascript" charset="utf-8" src="phonegap-0.9.3.js"></script>
	<link href="date.css" rel="stylesheet">
	<link rel="stylesheet" href="jquery.mobile-1.0rc2.min.css" /> 
	<link rel="stylesheet" href="default.css" /> 
	
	
	


	<style>
		li.divider { 
			background-color: #999999 !important;
			background-image: -webkit-linear-gradient(top,#999999,#888888) !important;
			background-image: -webkit-linear-gradient(top,#999999,#888888) !important;
			background-image: -moz-linear-gradient(top,#999999,#888888) !important;
			background-image: -ms-linear-gradient(top,#999999,#888888) !important;
			background-image: -o-linear-gradient(top,#999999,#888888) !important;
			background-image: linear-gradient(top,#999999,#888888) !important;
		}


		.ui-bar-b{
			background-color: #999999 !important;
			background-image: none;
		}
		
		.small { font-weight: normal; } 
		.detail{ display: none; }
		.club_image{ float: left;  width: 250px; padding-right: 20px; }
		.fb { display: block; clear: left; }
		.ui-header{
			background-image: url(images/logo_small.png);
			background-repeat: no-repeat;
			background-position: 95% 50%;
			background-color: #7fc251; 
		}
		
		.ui-header h1 {
			text-align: left !important;			
			margin-left: 45px !important;
			color: #000;
		}

		.classes li, .classes li p, .classes h4 {
			text-overflow: ellipsis !important;
			overflow: visible !important;
			white-space: normal !important;
		}

		.nobreak { 
			white-space: nowrap;
		}
		

		#home {
			background-image: url(/mobile/images/grass-logo2.png);
			background-repeat: no-repeat;
			background-position: top center;
			padding-top: 100px;
		}

		.ui-bar-a {
			color: #000;
			font-weight: bold;
			text-shadow: 0 1px 1px white;
		}
		
		a {
			text-decoration: none;
						
		}

		.views a {
			margin-left: 20px;
			color: #7fc251 !important;
		}
		
		.intro a {
                        color: #7fc251 !important;
		}
		
		.intro {
			text-align: center;
		}

        .left_menu {
            width: 20%;
            position: absolute;
            top: 1em;
            left: 1em;
        }   

        .right_content {
            padding-left: 25%;
            min-height: 20em;
        }
	</style>


	<script src="jquery-1.6.4.min.js"></script> 
	<script src="jquery.mobile-1.0rc2.min.js"></script>

	<script src="app.js"></script> 
	<script src="data.js"></script> 
	<script src="jquery-ui.js" type="text/javascript"></script>
	
	
	<script>
		$(function() {
			$( "#class_date, #date_from, #date_to, #date_from2, #date_to2" ).datepicker();					
		});
	</script>
	<script type="text/javascript">

		
	var App = new _App();
	
	$( function() {
		
		$('#searchButton').click(function() {
		  			
  			
		});
		
		$(".classes li").click(function(a, b){
			$(this).find(".detail").toggle();
		});

		$('.add_class').click(function(event,a) {
		 	event.stopPropagation();	
			$(event.target).html("Class Added!");
			class_id = event.target.className;

			var rex = new RegExp(' class_([0-9]*)');
			matches = rex.exec( class_id );
			class_id = matches[1];

			$.post("/instructors/member.php","action=add_class&class=" + class_id);
	
			return false;
		});

                $('.remove_class').click(function(event,a) {
                        event.stopPropagation();
		        class_id = event.target.className;

			var rex = new RegExp(' class_([0-9]*)');
			matches = rex.exec( class_id );
			class_id = matches[1];
												
                        $.post("/instructors/member.php","action=remove_class&class=" + class_id);

			$("#my-schedule li.class_item_" + class_id).hide();

			$(event.target).hide();

			return false;
                });

		$('.ui-header').click(function(event){
			$("#home").show();
		});

		$('.logout').click(function(event,a) {
                        event.stopPropagation();
                        $.post("/instructors/member.php","action=logout");
                });

	
		$("#form_register").submit(function(event){
			event.preventDefault(); 
			
			$.mobile.showPageLoadingMsg();	
			$.ajax({  
			  type: "POST",  
			  url: "/instructors/member.php",  
			  data: $("#form_register").serialize(),  
			  success: function(data){
				$.mobile.hidePageLoadingMsg();
				if (data == 'OK'){
					$(".register_message").html("<div class='intro'>Thank you for registering! <a href='index.php?refresh=1' data-ajax='false'>Continue</a></div>");  
					$(".register_message").show();
					$("#form_register").hide();
					
				}else{
					$(".register_message").html( data );
                                        $(".register_message").show();
				}
			  },
			});

			return false;
		});

		$("#form_login").submit(function(event){
				event.preventDefault();

				$.ajax({
				  type: "POST",
				  url: "/instructors/member.php",
				  data: $("#form_login").serialize(),
				  success: function(data){
						if (data == 'OK'){

			window.location = "/instructors/index.php?refresh=1";
						}else{
								$(".login_message").html( data );
								$(".login_message").show();
						}
				  },
				});

				return false;
		});

		$("#form_addclass").submit(function(event){
				event.preventDefault();
				$.mobile.showPageLoadingMsg();
				$.ajax({					
				  type: "POST",
				  url: "/instructors/member.php",
				  data: $("#form_addclass").serialize(),
				  success: function(data){
					 $.mobile.hidePageLoadingMsg();
					if (data == 'OK'){
					$(".addclass_message").html("<div class='intro'>Thank you for adding class! <a href='index.php?refresh=1' data-ajax='false'>Continue</a></div>");  
					$(".addclass_message").show();
					$("#form_addclass").hide();
					
				}else{
					$(".addclass_message").html( data );
                    $(".addclass_message").show();
				}
				  },
				});

				return false;
		});

	$("select#class_club").change(function(){
		$.mobile.showPageLoadingMsg();
		$("select#class_class").empty();
		$.getJSON("/instructors/member.php",{clubId: $(this).val(), ajax: 'true'}, function(j){
		  $.mobile.hidePageLoadingMsg();
		  var options = '';
		  for (var i = 0; i < j.length; i++) {
			 options += '<option value="' + j[i].optionValue + '">' + j[i].optionDisplay + '</option>';
		 }
		  $("select#class_class").html(options);
		  $('select#class_class').trigger('change');
		})
	  })

	$("select#select_instructor").change(function(){
		$(".edit_instructor_message").hide();
		$.mobile.showPageLoadingMsg();
		$.getJSON("/instructors/member.php",{instructorId: $(this).val(), ajax: 'true'}, function(j){
			$.mobile.hidePageLoadingMsg();
			$('input#firstname').val(j.FirstName);
			$('input#lastname').val(j.LastName);
			$('input#username').val(j.Email);
			$('select#select_club option[value="'+j.ClubId+'"]').attr("selected", "selected");
			$('select#select_club').trigger('change');
			if(j.is_admin==1)
			{
				$("input#isadmin").attr("checked", true);
				$('input#isadmin').trigger('click');
			}
			else
			{
				$("input#isadmin").attr("checked", false);
				$('input#isadmin').trigger('click');
			}
		})
	  })
		
	$("#form_edit_instructor").submit(function(event){
			event.preventDefault(); 
			$.mobile.showPageLoadingMsg();    
			$.ajax({  
			  type: "POST",  
			  url: "/instructors/member.php",  
			  data: $("#form_edit_instructor").serialize(),  
			  success: function(data){
				$.mobile.hidePageLoadingMsg();
				if (data == 'OK'){
					$(".edit_instructor_message").html("Instructor Data Updated.");  
					$(".edit_instructor_message").show();
					//$("#form_edit_instructor").hide();
				}else{
					$(".edit_instructor_message").html( data );
                    $(".edit_instructor_message").show();
				}
			  },
			});

			return false;
		});
		
	} );


    $("#home").live('pageshow',function() {
	    

    });
    $(".club_page").live('pageshow', function(event, ui){
		    var club_id = this.id.replace('club-','');
		    var phone = $( '#' + this.id + ' .club_phone').html();
    		    var email = $( '#' + this.id + ' .club_email').html();
		    $('.contact_phone').html( phone );
		    $('.contact_email').html( email );

		 $('#select_club')[0].value = club_id;
	   });


	function _debug(s){
		console.log(s);
	}

	document.addEventListener("deviceready", onDeviceReady, false);

    // PhoneGap is ready
    //
    function onDeviceReady() {
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
    }
	


var onSuccess = function(position) {
    alert('Latitude: '          + position.coords.latitude          + '\n' +
          'Longitude: '         + position.coords.longitude         + '\n' +
          'Altitude: '          + position.coords.altitude          + '\n' +
          'Accuracy: '          + position.coords.accuracy          + '\n' +
          'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
          'Heading: '           + position.coords.heading           + '\n' +
          'Speed: '             + position.coords.speed             + '\n' +
          'Timestamp: '         + new Date(position.timestamp)      + '\n');
};

// onError Callback receives a PositionError object
//
function onError(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}

	

</script>  
  
</head>
<body>

  <div data-role="page" id="home" data-theme="a">
  <div data-role="content">
	  <div class="intro">
			<h3>Welcome to the Athletic Club Instructor App</h3>
		<? if ($_instructor){ ?>
                <p><a href="#add_class" data-role="button" data-ajax="false">Add Class</a></p>

                <? if ($is_admin){ ?>
                  <p><a href="#admin" data-role="button" data-ajax="false">Admin</a></p>
                <? } ?>

                <p><a href="/instructors/member.php?action=logout" data-role="button" class="logout" data-ajax="false">Logout</a></p>
        <? }else{ ?>
                <p><a href="#members" data-role="button">Login</a></p>
        <? } ?>
        </div>

  </div>
</div>


<div data-role="page" id="members" data-theme="a"> 
 
<div data-role="header" data-position="fixed"> 
	  <h1>Login</h1> 
  </div>


  <div class="ui-body"> 
<p>Please login to continue.</p>

<form method="post" id="form_login">
<input type="hidden" name="action" value="login"/>
<div data-role="fieldcontain" class="aui-hide-label">
	<label for="username">Email:</label>
	<input type="text" name="email" id="username" value="" placeholder="Email"/>
</div>

<div data-role="fieldcontain" class="aui-hide-label">
	<label for="password">Password:</label>
	<input type="password" name="password" id="password" value="" placeholder="Password"/>
</div>

<div data-role="fieldcontain" class="aui-hide-label">
	<h3 class="login_message" style="display: none;"></h3>
	<input type="submit" name="submit" id="submit" value="Login"/>
</div>

</form>
  </div>

  <div data-role="content"> 

    </div>  
  </div> 
 
</div>



<div data-role="page" id="register" data-theme="a"> 	 
	<div data-role="header" data-position="fixed"> 
		  <h1>Instructor Registration</h1> 
    </div>
	<div class="ui-body"> 
		<div class="regsiter_form">
			<h3 class="register_message" style="display: none;"></h3>
			<form id="form_register">
				<p>Please fill out the form below:</p>

				<input type="hidden" name="action" value="register"/>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="username">First Name:</label>
					<input type="text" name="firstname" id="firstname" value="" placeholder="First Name"/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="username">Last Name:</label>
					<input type="text" name="lastname" id="lastname" value="" placeholder="Last Name"/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="username">Email:</label>
					<input type="text" name="email" id="username" value="" placeholder="Email"/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="select_club">Your Club:</label>
					<select name="club" id="select_club">
				<?
				$clubs = $clubManager->SelectAll();
				 while ($club = $clubs->NextItem()) { ?>
						<?  if ($club->ID > 0) { ?>
						<option value="<?= $club->ID ?>"><?= $club->ClubName ?></option>
					<? }
					} ?>

					</select>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">

					<label for="password">Password:</label>
					<input type="password" name="password" id="password" value="" placeholder=""/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="password">Password (confirm):</label>
					<input type="password" name="password2" id="password" value="" placeholder=""/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="isadmin">Is Admin</label>
					<input type="checkbox" name="isadmin" id="isadmin" value="1" />
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<input type="submit" name="submit" id="submit" value="Register"/>
				</div>
				</div>
			</form>
	  </div>

	  <div data-role="content"> </div>  
</div> 
 
</div>


<div data-role="page" id="edit_instructor" data-theme="a"> 	 
	<div data-role="header" data-position="fixed"> 
		  <h1>Edit Instructor</h1> 
    </div>
	<div class="ui-body"> 
		<div class="edit_instructor">
			
			<form id="form_edit_instructor">
				<input type="hidden" name="action" value="edit_instructor"/>
				<div data-role="fieldcontain" class="aui-hide-label">
                    <label for="select_instructor">Select Instructor:</label>
                    <select name="instructor" id="select_instructor">
                        <option value="">--Please Select--</option>
						<?
							$inst=$instructor->selectall();
							foreach($inst as $row)
							{
						?>
						<option value="<?= $row["ID"] ?>"><?= $row["FIRSTNAME"] ?> <?= $row["LASTNAME"] ?></option>
						<?}?>
                    </select>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="username">First Name:</label>
					<input type="text" name="firstname" id="firstname" value="" placeholder="First Name"/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="username">Last Name:</label>
					<input type="text" name="lastname" id="lastname" value="" placeholder="Last Name"/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="username">Email:</label>
					<input type="text" name="email" id="username" value="" placeholder="Email"/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="select_club">Instructor Club:</label>
					<select name="club" id="select_club">
					<option value="">--Select Club--</option>
				<?
				$clubs = $clubManager->SelectAll();
				 while ($club = $clubs->NextItem()) { ?>
						<?  if ($club->ID > 0) { ?>
						<option value="<?= $club->ID ?>"><?= $club->ClubName ?></option>
					<? }
					} ?>

					</select>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">

					<label for="password">Password:</label>
					<input type="password" name="password" id="password" value="" placeholder=""/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="password">Password (confirm):</label>
					<input type="password" name="password2" id="password" value="" placeholder=""/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="isadmin">Is Admin</label>
					<input type="checkbox" name="isadmin" id="isadmin" value="1" />
				</div>

				<div data-role="fieldcontain" class="aui-hide-label">
					<h3 class="edit_instructor_message" style="display: none;"></h3>
					<input type="submit" name="submit" id="submit" value="Update"/>
				</div>
				</div>
			</form>
	  </div>

	  <div data-role="content"> </div>  
</div> 
 
</div>


<div data-role="page" id="admin" data-theme="a">
    <div data-role="header" data-position="fixed">
              <h1>Admin</h1>
    </div>
    <div class="ui-body">
            <ul data-role="listview" data-theme="c" data-dividertheme="d" class="left_menu">
                    <li data-role="list-divider">Admin Options</li>
                    <li><a href="#register">Add Instructors</a></li>
					<li><a href="#edit_instructor">Edit Instructors</a></li>
                    <li data-role="list-divider">Reports</li>
                    <li><a href="#">Payroll Report</a></li>
                    <li><a href="#">Class Report</a></li>
            </ul>

            <div class="right_content">

                    <div class="content_option active">
                        <h2>Payroll Report</h2>

                        <form method="post" action="report.php" data-ajax="false">
                            <input type="hidden" name="report" value="payroll"/>

                           <div data-role="fieldcontain" class="aui-hide-label">
                            <label for="select_club">Club:</label>
                            <select name="club" id="select_club">
                                <option value=0>All clubs</option>
                <?
                $clubs = $clubManager->SelectAll();
                 while ($club = $clubs->NextItem()) { ?>
                        <?  if ($club->ID > 0) { ?>
                        <option value="<?= $club->ID ?>"><?= $club->ClubName ?></option>
                    <? }
                    } ?>
                               </select>
                            </div>

                <div data-role="fieldcontain" class="aui-hide-label">
                    <label for="date_from">Date From:</label>
                    <input type="text" name="date_from" id="date_from" value="" readonly="true" placeholder="YYYY-MM-DD"/>
                </div>
                <div data-role="fieldcontain" class="aui-hide-label">
                    <label for="date_to">Date To:</label>
                    <input type="text" name="date_to" id="date_to" value="" readonly="true" placeholder="YYYY-MM-DD"/>
                </div>

                        <input type="submit" value="Generate Report"/>
                            
                        </form>
                    </div>

                    <div class="content_option active">
                        <br/><br/><br/><br/>
                        <h2>Class Report</h2>

                        <form method="post" action="report.php" data-ajax="false">
                            <input type="hidden" name="report" value="class"/>

                           <div data-role="fieldcontain" class="aui-hide-label">
                            <label for="select_club">Club:</label>
                            <select name="club" id="select_club">
                                <option value=0>All clubs</option>
                <?
                $clubs = $clubManager->SelectAll();
                 while ($club = $clubs->NextItem()) { ?>
                        <?  if ($club->ID > 0) { ?>
                        <option value="<?= $club->ID ?>"><?= $club->ClubName ?></option>
                    <? }
                    } ?>
                               </select>
                            </div>

                <div data-role="fieldcontain" class="aui-hide-label">
                    <label for="date_from2">Date From:</label>
                    <input type="text" name="date_from" id="date_from2" value="" readonly="true" placeholder="YYYY-MM-DD"/>
                </div>
                <div data-role="fieldcontain" class="aui-hide-label">
                    <label for="date_to2">Date To:</label>
                    <input type="text" name="date_to" id="date_to2" value="" readonly="true" placeholder="YYYY-MM-DD"/>
                </div>

                        <input type="submit" value="Generate Report"/>

                        </form>
                    </div>

                    <!-- <pre>A
                    Edit Instructors: List Instructors to delete/edit add.

                    Payroll Report: From Date, To Date.  Option to filter by club
                            Table shows;  Instructor1 Club1  ClassA   Date  1 hours
                                                      Club1  ClassB   Date  1.5 hours
                                                Instructor Total 2.5 hours

                    Class Report:  From Date, To Date, Class select, Option to Filter by Club
                                        ClassA   Date   Participants
                                                 Date   Participants
                                                 Total Classes
                                                 Averge Participants
                    </pre> -->
            </div>
    </div>
</div>



<!--add class form-->

<div data-role="page" id="add_class" data-theme="a"> 	 
	<div data-role="header" data-position="fixed"> 
		  <h1> Add Class</h1> 
    </div>
	<div class="ui-body"> 
		<div class="addclass_form">
			<h3 class="addclass_message" style="display: none;"></h3>
			<form id="form_addclass" name="form_addclass">
				<p>Please fill out the form below:</p>

				<input type="hidden" name="action" value="add_class"/>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="class_club">Club:</label>
					<select name="clubid" id="class_club">
					<option value="">Please select Club</option>
				<?
				$clubs = $clubManager->SelectAll();
				 while ($club = $clubs->NextItem()) { ?>
						<?  if ($club->ID > 0) { ?>
						<option <?php echo ($club->ID == $_SESSION["Instructor"]["CLUBID"]) ? 'selected':''  ?> value="<?= $club->ID ?>"><?= $club->ClubName ?></option>
					<? }
					} ?>

					</select>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="class_class">Class:</label>
					<select name="classid" id="class_class" >
					<?php
						if($_SESSION["Instructor"]["CLUBID"])
						{
							$classQuery=new _ClassQuery();
							$clubs=$classQuery->GetClassByClub($_SESSION["Instructor"]["CLUBID"]);
							foreach ($clubs as $row)
							{
					?>			
								<option value="<?= $row["id"] ?>"><?= $row["ClassName"] ?></option>
					<?php
							}
						}
					?>
                      </select>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<label for="class_date">Date:</label>
					<input type="text" name="date" id="class_date" value="" readonly="true" placeholder="YYYY-MM-DD"/>
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">

					<label for="class_participants">Participants:</label>
					<input type="text" name="participant" id="class_participants" value="0"/>
				</div>
                <div data-role="fieldcontain" class="aui-hide-label">
                    <label for="class_hours">Hours:</label>
					<select name="hours" id="class_hours" >
					<option value="1">1</option>
					<option value="1.25">1.25</option>
					<option value="1.5">1.5</option>
					</select>
                </div>
                <div data-role="fieldcontain" class="aui-hide-label">
                    <label for="class_substitute">Is Substitute:</label>
					<input type="checkbox" name="subinstructor" id="class_substitute" />
				</div>
				<div data-role="fieldcontain" class="aui-hide-label">
					<input type="submit" name="submit" id="submit" value="Add Class"/>
				</div>
				</div>
			</form>
	  </div>

	  <div data-role="content"> </div>  
</div> 
 
</div>

 
</body>
</html>
