<?php


/**
 * Base class that represents a query for the 'Class' table.
 *
 * 
 *
 * @method     _ClassQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method     _ClassQuery orderByCategory($order = Criteria::ASC) Order by the Category column
 * @method     _ClassQuery orderByCatid($order = Criteria::ASC) Order by the CatId column
 * @method     _ClassQuery orderByClassname($order = Criteria::ASC) Order by the ClassName column
 * @method     _ClassQuery orderByDescription($order = Criteria::ASC) Order by the Description column
 * @method     _ClassQuery orderByVideolink($order = Criteria::ASC) Order by the VideoLink column
 * @method     _ClassQuery orderByClasscode($order = Criteria::ASC) Order by the ClassCode column
 * @method     _ClassQuery orderByActive($order = Criteria::ASC) Order by the Active column
 *
 * @method     _ClassQuery groupById() Group by the ID column
 * @method     _ClassQuery groupByCategory() Group by the Category column
 * @method     _ClassQuery groupByCatid() Group by the CatId column
 * @method     _ClassQuery groupByClassname() Group by the ClassName column
 * @method     _ClassQuery groupByDescription() Group by the Description column
 * @method     _ClassQuery groupByVideolink() Group by the VideoLink column
 * @method     _ClassQuery groupByClasscode() Group by the ClassCode column
 * @method     _ClassQuery groupByActive() Group by the Active column
 *
 * @method     _ClassQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     _ClassQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     _ClassQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     _ClassQuery leftJoinClassresult($relationAlias = null) Adds a LEFT JOIN clause to the query using the Classresult relation
 * @method     _ClassQuery rightJoinClassresult($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Classresult relation
 * @method     _ClassQuery innerJoinClassresult($relationAlias = null) Adds a INNER JOIN clause to the query using the Classresult relation
 *
 * @method     _ClassQuery leftJoinEvent($relationAlias = null) Adds a LEFT JOIN clause to the query using the Event relation
 * @method     _ClassQuery rightJoinEvent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Event relation
 * @method     _ClassQuery innerJoinEvent($relationAlias = null) Adds a INNER JOIN clause to the query using the Event relation
 *
 * @method     _Class findOne(PropelPDO $con = null) Return the first _Class matching the query
 * @method     _Class findOneOrCreate(PropelPDO $con = null) Return the first _Class matching the query, or a new _Class object populated from the query conditions when no match is found
 *
 * @method     _Class findOneById(int $ID) Return the first _Class filtered by the ID column
 * @method     _Class findOneByCategory(string $Category) Return the first _Class filtered by the Category column
 * @method     _Class findOneByCatid(int $CatId) Return the first _Class filtered by the CatId column
 * @method     _Class findOneByClassname(string $ClassName) Return the first _Class filtered by the ClassName column
 * @method     _Class findOneByDescription(string $Description) Return the first _Class filtered by the Description column
 * @method     _Class findOneByVideolink(string $VideoLink) Return the first _Class filtered by the VideoLink column
 * @method     _Class findOneByClasscode(string $ClassCode) Return the first _Class filtered by the ClassCode column
 * @method     _Class findOneByActive(boolean $Active) Return the first _Class filtered by the Active column
 *
 * @method     array findById(int $ID) Return _Class objects filtered by the ID column
 * @method     array findByCategory(string $Category) Return _Class objects filtered by the Category column
 * @method     array findByCatid(int $CatId) Return _Class objects filtered by the CatId column
 * @method     array findByClassname(string $ClassName) Return _Class objects filtered by the ClassName column
 * @method     array findByDescription(string $Description) Return _Class objects filtered by the Description column
 * @method     array findByVideolink(string $VideoLink) Return _Class objects filtered by the VideoLink column
 * @method     array findByClasscode(string $ClassCode) Return _Class objects filtered by the ClassCode column
 * @method     array findByActive(boolean $Active) Return _Class objects filtered by the Active column
 *
 * @package    propel.generator.site.om
 */
abstract class Base_ClassQuery extends ModelCriteria
{
    
    /**
     * Initializes internal state of Base_ClassQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'site', $modelName = '_Class', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new _ClassQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     _ClassQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return _ClassQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof _ClassQuery) {
            return $criteria;
        }
        $query = new _ClassQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   _Class|_Class[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = _ClassPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(_ClassPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   _Class A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `CATEGORY`, `CATID`, `CLASSNAME`, `DESCRIPTION`, `VIDEOLINK`, `CLASSCODE`, `ACTIVE` FROM `Class` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new _Class();
            $obj->hydrate($row);
            _ClassPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return _Class|_Class[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|_Class[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return _ClassQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(_ClassPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return _ClassQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(_ClassPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return _ClassQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(_ClassPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the Category column
     *
     * Example usage:
     * <code>
     * $query->filterByCategory('fooValue');   // WHERE Category = 'fooValue'
     * $query->filterByCategory('%fooValue%'); // WHERE Category LIKE '%fooValue%'
     * </code>
     *
     * @param     string $category The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return _ClassQuery The current query, for fluid interface
     */
    public function filterByCategory($category = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($category)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $category)) {
                $category = str_replace('*', '%', $category);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(_ClassPeer::CATEGORY, $category, $comparison);
    }

    /**
     * Filter the query on the CatId column
     *
     * Example usage:
     * <code>
     * $query->filterByCatid(1234); // WHERE CatId = 1234
     * $query->filterByCatid(array(12, 34)); // WHERE CatId IN (12, 34)
     * $query->filterByCatid(array('min' => 12)); // WHERE CatId > 12
     * </code>
     *
     * @param     mixed $catid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return _ClassQuery The current query, for fluid interface
     */
    public function filterByCatid($catid = null, $comparison = null)
    {
        if (is_array($catid)) {
            $useMinMax = false;
            if (isset($catid['min'])) {
                $this->addUsingAlias(_ClassPeer::CATID, $catid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($catid['max'])) {
                $this->addUsingAlias(_ClassPeer::CATID, $catid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(_ClassPeer::CATID, $catid, $comparison);
    }

    /**
     * Filter the query on the ClassName column
     *
     * Example usage:
     * <code>
     * $query->filterByClassname('fooValue');   // WHERE ClassName = 'fooValue'
     * $query->filterByClassname('%fooValue%'); // WHERE ClassName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $classname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return _ClassQuery The current query, for fluid interface
     */
    public function filterByClassname($classname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($classname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $classname)) {
                $classname = str_replace('*', '%', $classname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(_ClassPeer::CLASSNAME, $classname, $comparison);
    }

    /**
     * Filter the query on the Description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE Description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE Description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return _ClassQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(_ClassPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the VideoLink column
     *
     * Example usage:
     * <code>
     * $query->filterByVideolink('fooValue');   // WHERE VideoLink = 'fooValue'
     * $query->filterByVideolink('%fooValue%'); // WHERE VideoLink LIKE '%fooValue%'
     * </code>
     *
     * @param     string $videolink The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return _ClassQuery The current query, for fluid interface
     */
    public function filterByVideolink($videolink = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($videolink)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $videolink)) {
                $videolink = str_replace('*', '%', $videolink);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(_ClassPeer::VIDEOLINK, $videolink, $comparison);
    }

    /**
     * Filter the query on the ClassCode column
     *
     * Example usage:
     * <code>
     * $query->filterByClasscode('fooValue');   // WHERE ClassCode = 'fooValue'
     * $query->filterByClasscode('%fooValue%'); // WHERE ClassCode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $classcode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return _ClassQuery The current query, for fluid interface
     */
    public function filterByClasscode($classcode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($classcode)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $classcode)) {
                $classcode = str_replace('*', '%', $classcode);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(_ClassPeer::CLASSCODE, $classcode, $comparison);
    }

    /**
     * Filter the query on the Active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(true); // WHERE Active = true
     * $query->filterByActive('yes'); // WHERE Active = true
     * </code>
     *
     * @param     boolean|string $active The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return _ClassQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (is_string($active)) {
            $Active = in_array(strtolower($active), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(_ClassPeer::ACTIVE, $active, $comparison);
    }

    /**
     * Filter the query by a related Classresult object
     *
     * @param   Classresult|PropelObjectCollection $classresult  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   _ClassQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByClassresult($classresult, $comparison = null)
    {
        if ($classresult instanceof Classresult) {
            return $this
                ->addUsingAlias(_ClassPeer::ID, $classresult->getClassid(), $comparison);
        } elseif ($classresult instanceof PropelObjectCollection) {
            return $this
                ->useClassresultQuery()
                ->filterByPrimaryKeys($classresult->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByClassresult() only accepts arguments of type Classresult or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Classresult relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return _ClassQuery The current query, for fluid interface
     */
    public function joinClassresult($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Classresult');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Classresult');
        }

        return $this;
    }

    /**
     * Use the Classresult relation Classresult object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ClassresultQuery A secondary query class using the current class as primary query
     */
    public function useClassresultQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinClassresult($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Classresult', 'ClassresultQuery');
    }

    /**
     * Filter the query by a related Event object
     *
     * @param   Event|PropelObjectCollection $event  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   _ClassQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByEvent($event, $comparison = null)
    {
        if ($event instanceof Event) {
            return $this
                ->addUsingAlias(_ClassPeer::ID, $event->getClassid(), $comparison);
        } elseif ($event instanceof PropelObjectCollection) {
            return $this
                ->useEventQuery()
                ->filterByPrimaryKeys($event->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEvent() only accepts arguments of type Event or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Event relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return _ClassQuery The current query, for fluid interface
     */
    public function joinEvent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Event');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Event');
        }

        return $this;
    }

    /**
     * Use the Event relation Event object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   EventQuery A secondary query class using the current class as primary query
     */
    public function useEventQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinEvent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Event', 'EventQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   _Class $_Class Object to remove from the list of results
     *
     * @return _ClassQuery The current query, for fluid interface
     */
    public function prune($_Class = null)
    {
        if ($_Class) {
            $this->addUsingAlias(_ClassPeer::ID, $_Class->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

} // Base_ClassQuery