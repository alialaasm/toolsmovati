<?php


/**
 * Base class that represents a query for the 'ModuleData' table.
 *
 * 
 *
 * @method     ModuledataQuery orderByModulename($order = Criteria::ASC) Order by the ModuleName column
 * @method     ModuledataQuery orderByVar1($order = Criteria::ASC) Order by the Var1 column
 * @method     ModuledataQuery orderByVar2($order = Criteria::ASC) Order by the Var2 column
 * @method     ModuledataQuery orderByVar3($order = Criteria::ASC) Order by the Var3 column
 * @method     ModuledataQuery orderByVar4($order = Criteria::ASC) Order by the Var4 column
 * @method     ModuledataQuery orderByVar5($order = Criteria::ASC) Order by the Var5 column
 * @method     ModuledataQuery orderByVar6($order = Criteria::ASC) Order by the Var6 column
 * @method     ModuledataQuery orderByVar7($order = Criteria::ASC) Order by the Var7 column
 * @method     ModuledataQuery orderByVar8($order = Criteria::ASC) Order by the Var8 column
 *
 * @method     ModuledataQuery groupByModulename() Group by the ModuleName column
 * @method     ModuledataQuery groupByVar1() Group by the Var1 column
 * @method     ModuledataQuery groupByVar2() Group by the Var2 column
 * @method     ModuledataQuery groupByVar3() Group by the Var3 column
 * @method     ModuledataQuery groupByVar4() Group by the Var4 column
 * @method     ModuledataQuery groupByVar5() Group by the Var5 column
 * @method     ModuledataQuery groupByVar6() Group by the Var6 column
 * @method     ModuledataQuery groupByVar7() Group by the Var7 column
 * @method     ModuledataQuery groupByVar8() Group by the Var8 column
 *
 * @method     ModuledataQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ModuledataQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ModuledataQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     Moduledata findOne(PropelPDO $con = null) Return the first Moduledata matching the query
 * @method     Moduledata findOneOrCreate(PropelPDO $con = null) Return the first Moduledata matching the query, or a new Moduledata object populated from the query conditions when no match is found
 *
 * @method     Moduledata findOneByModulename(string $ModuleName) Return the first Moduledata filtered by the ModuleName column
 * @method     Moduledata findOneByVar1(string $Var1) Return the first Moduledata filtered by the Var1 column
 * @method     Moduledata findOneByVar2(string $Var2) Return the first Moduledata filtered by the Var2 column
 * @method     Moduledata findOneByVar3(string $Var3) Return the first Moduledata filtered by the Var3 column
 * @method     Moduledata findOneByVar4(string $Var4) Return the first Moduledata filtered by the Var4 column
 * @method     Moduledata findOneByVar5(string $Var5) Return the first Moduledata filtered by the Var5 column
 * @method     Moduledata findOneByVar6(string $Var6) Return the first Moduledata filtered by the Var6 column
 * @method     Moduledata findOneByVar7(string $Var7) Return the first Moduledata filtered by the Var7 column
 * @method     Moduledata findOneByVar8(string $Var8) Return the first Moduledata filtered by the Var8 column
 *
 * @method     array findByModulename(string $ModuleName) Return Moduledata objects filtered by the ModuleName column
 * @method     array findByVar1(string $Var1) Return Moduledata objects filtered by the Var1 column
 * @method     array findByVar2(string $Var2) Return Moduledata objects filtered by the Var2 column
 * @method     array findByVar3(string $Var3) Return Moduledata objects filtered by the Var3 column
 * @method     array findByVar4(string $Var4) Return Moduledata objects filtered by the Var4 column
 * @method     array findByVar5(string $Var5) Return Moduledata objects filtered by the Var5 column
 * @method     array findByVar6(string $Var6) Return Moduledata objects filtered by the Var6 column
 * @method     array findByVar7(string $Var7) Return Moduledata objects filtered by the Var7 column
 * @method     array findByVar8(string $Var8) Return Moduledata objects filtered by the Var8 column
 *
 * @package    propel.generator.site.om
 */
abstract class BaseModuledataQuery extends ModelCriteria
{
    
    /**
     * Initializes internal state of BaseModuledataQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'site', $modelName = 'Moduledata', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ModuledataQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     ModuledataQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ModuledataQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ModuledataQuery) {
            return $criteria;
        }
        $query = new ModuledataQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Moduledata|Moduledata[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ModuledataPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ModuledataPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   Moduledata A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `MODULENAME`, `VAR1`, `VAR2`, `VAR3`, `VAR4`, `VAR5`, `VAR6`, `VAR7`, `VAR8` FROM `ModuleData` WHERE `MODULENAME` = :p0';
        try {
            $stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Moduledata();
            $obj->hydrate($row);
            ModuledataPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Moduledata|Moduledata[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Moduledata[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ModuledataQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ModuledataPeer::MODULENAME, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ModuledataQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ModuledataPeer::MODULENAME, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ModuleName column
     *
     * Example usage:
     * <code>
     * $query->filterByModulename('fooValue');   // WHERE ModuleName = 'fooValue'
     * $query->filterByModulename('%fooValue%'); // WHERE ModuleName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modulename The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ModuledataQuery The current query, for fluid interface
     */
    public function filterByModulename($modulename = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modulename)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modulename)) {
                $modulename = str_replace('*', '%', $modulename);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ModuledataPeer::MODULENAME, $modulename, $comparison);
    }

    /**
     * Filter the query on the Var1 column
     *
     * Example usage:
     * <code>
     * $query->filterByVar1('fooValue');   // WHERE Var1 = 'fooValue'
     * $query->filterByVar1('%fooValue%'); // WHERE Var1 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $var1 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ModuledataQuery The current query, for fluid interface
     */
    public function filterByVar1($var1 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($var1)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $var1)) {
                $var1 = str_replace('*', '%', $var1);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ModuledataPeer::VAR1, $var1, $comparison);
    }

    /**
     * Filter the query on the Var2 column
     *
     * Example usage:
     * <code>
     * $query->filterByVar2('fooValue');   // WHERE Var2 = 'fooValue'
     * $query->filterByVar2('%fooValue%'); // WHERE Var2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $var2 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ModuledataQuery The current query, for fluid interface
     */
    public function filterByVar2($var2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($var2)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $var2)) {
                $var2 = str_replace('*', '%', $var2);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ModuledataPeer::VAR2, $var2, $comparison);
    }

    /**
     * Filter the query on the Var3 column
     *
     * Example usage:
     * <code>
     * $query->filterByVar3('fooValue');   // WHERE Var3 = 'fooValue'
     * $query->filterByVar3('%fooValue%'); // WHERE Var3 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $var3 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ModuledataQuery The current query, for fluid interface
     */
    public function filterByVar3($var3 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($var3)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $var3)) {
                $var3 = str_replace('*', '%', $var3);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ModuledataPeer::VAR3, $var3, $comparison);
    }

    /**
     * Filter the query on the Var4 column
     *
     * Example usage:
     * <code>
     * $query->filterByVar4('fooValue');   // WHERE Var4 = 'fooValue'
     * $query->filterByVar4('%fooValue%'); // WHERE Var4 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $var4 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ModuledataQuery The current query, for fluid interface
     */
    public function filterByVar4($var4 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($var4)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $var4)) {
                $var4 = str_replace('*', '%', $var4);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ModuledataPeer::VAR4, $var4, $comparison);
    }

    /**
     * Filter the query on the Var5 column
     *
     * Example usage:
     * <code>
     * $query->filterByVar5('fooValue');   // WHERE Var5 = 'fooValue'
     * $query->filterByVar5('%fooValue%'); // WHERE Var5 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $var5 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ModuledataQuery The current query, for fluid interface
     */
    public function filterByVar5($var5 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($var5)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $var5)) {
                $var5 = str_replace('*', '%', $var5);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ModuledataPeer::VAR5, $var5, $comparison);
    }

    /**
     * Filter the query on the Var6 column
     *
     * Example usage:
     * <code>
     * $query->filterByVar6('fooValue');   // WHERE Var6 = 'fooValue'
     * $query->filterByVar6('%fooValue%'); // WHERE Var6 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $var6 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ModuledataQuery The current query, for fluid interface
     */
    public function filterByVar6($var6 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($var6)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $var6)) {
                $var6 = str_replace('*', '%', $var6);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ModuledataPeer::VAR6, $var6, $comparison);
    }

    /**
     * Filter the query on the Var7 column
     *
     * Example usage:
     * <code>
     * $query->filterByVar7('fooValue');   // WHERE Var7 = 'fooValue'
     * $query->filterByVar7('%fooValue%'); // WHERE Var7 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $var7 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ModuledataQuery The current query, for fluid interface
     */
    public function filterByVar7($var7 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($var7)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $var7)) {
                $var7 = str_replace('*', '%', $var7);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ModuledataPeer::VAR7, $var7, $comparison);
    }

    /**
     * Filter the query on the Var8 column
     *
     * Example usage:
     * <code>
     * $query->filterByVar8('fooValue');   // WHERE Var8 = 'fooValue'
     * $query->filterByVar8('%fooValue%'); // WHERE Var8 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $var8 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ModuledataQuery The current query, for fluid interface
     */
    public function filterByVar8($var8 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($var8)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $var8)) {
                $var8 = str_replace('*', '%', $var8);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ModuledataPeer::VAR8, $var8, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Moduledata $moduledata Object to remove from the list of results
     *
     * @return ModuledataQuery The current query, for fluid interface
     */
    public function prune($moduledata = null)
    {
        if ($moduledata) {
            $this->addUsingAlias(ModuledataPeer::MODULENAME, $moduledata->getModulename(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

} // BaseModuledataQuery