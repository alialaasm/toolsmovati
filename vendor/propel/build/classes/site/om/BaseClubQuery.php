<?php


/**
 * Base class that represents a query for the 'Club' table.
 *
 * 
 *
 * @method     ClubQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method     ClubQuery orderByClubname($order = Criteria::ASC) Order by the ClubName column
 * @method     ClubQuery orderByAddress($order = Criteria::ASC) Order by the Address column
 * @method     ClubQuery orderByCity($order = Criteria::ASC) Order by the City column
 * @method     ClubQuery orderByProvince($order = Criteria::ASC) Order by the Province column
 * @method     ClubQuery orderByPostalcode($order = Criteria::ASC) Order by the PostalCode column
 * @method     ClubQuery orderByMapitaddress($order = Criteria::ASC) Order by the MapItAddress column
 * @method     ClubQuery orderByPhone($order = Criteria::ASC) Order by the Phone column
 * @method     ClubQuery orderByTollfree($order = Criteria::ASC) Order by the TollFree column
 * @method     ClubQuery orderByContactemail($order = Criteria::ASC) Order by the ContactEmail column
 * @method     ClubQuery orderByApplicationemail($order = Criteria::ASC) Order by the ApplicationEmail column
 * @method     ClubQuery orderByPersonaltrainingemail($order = Criteria::ASC) Order by the PersonalTrainingEmail column
 * @method     ClubQuery orderByPromotions($order = Criteria::ASC) Order by the Promotions column
 * @method     ClubQuery orderByScheduleurl($order = Criteria::ASC) Order by the ScheduleURL column
 * @method     ClubQuery orderByPoolscheduleurl($order = Criteria::ASC) Order by the PoolScheduleURL column
 * @method     ClubQuery orderByUseinternal($order = Criteria::ASC) Order by the UseInternal column
 * @method     ClubQuery orderBySundayhours($order = Criteria::ASC) Order by the SundayHours column
 * @method     ClubQuery orderByMondayhours($order = Criteria::ASC) Order by the MondayHours column
 * @method     ClubQuery orderByTuesdayhours($order = Criteria::ASC) Order by the TuesdayHours column
 * @method     ClubQuery orderByWednesdayhours($order = Criteria::ASC) Order by the WednesdayHours column
 * @method     ClubQuery orderByThursdayhours($order = Criteria::ASC) Order by the ThursdayHours column
 * @method     ClubQuery orderByFridayhours($order = Criteria::ASC) Order by the FridayHours column
 * @method     ClubQuery orderBySaturdayhours($order = Criteria::ASC) Order by the SaturdayHours column
 * @method     ClubQuery orderByActive($order = Criteria::ASC) Order by the Active column
 * @method     ClubQuery orderByFreetrial($order = Criteria::ASC) Order by the FreeTrial column
 * @method     ClubQuery orderBySalessundayhours($order = Criteria::ASC) Order by the SalesSundayHours column
 * @method     ClubQuery orderBySalesmondayhours($order = Criteria::ASC) Order by the SalesMondayHours column
 * @method     ClubQuery orderBySalestuesdayhours($order = Criteria::ASC) Order by the SalesTuesdayHours column
 * @method     ClubQuery orderBySaleswednesdayhours($order = Criteria::ASC) Order by the SalesWednesdayHours column
 * @method     ClubQuery orderBySalesthursdayhours($order = Criteria::ASC) Order by the SalesThursdayHours column
 * @method     ClubQuery orderBySalesfridayhours($order = Criteria::ASC) Order by the SalesFridayHours column
 * @method     ClubQuery orderBySalessaturdayhours($order = Criteria::ASC) Order by the SalesSaturdayHours column
 * @method     ClubQuery orderBySalestrailer($order = Criteria::ASC) Order by the SalesTrailer column
 * @method     ClubQuery orderByOnlinepromo($order = Criteria::ASC) Order by the OnlinePromo column
 * @method     ClubQuery orderByOnlinepromotion($order = Criteria::ASC) Order by the OnlinePromotion column
 * @method     ClubQuery orderByNewsletterurl($order = Criteria::ASC) Order by the NewsletterURL column
 * @method     ClubQuery orderByFacebookUrl($order = Criteria::ASC) Order by the facebook_url column
 * @method     ClubQuery orderByMobileImage($order = Criteria::ASC) Order by the mobile_image column
 *
 * @method     ClubQuery groupById() Group by the ID column
 * @method     ClubQuery groupByClubname() Group by the ClubName column
 * @method     ClubQuery groupByAddress() Group by the Address column
 * @method     ClubQuery groupByCity() Group by the City column
 * @method     ClubQuery groupByProvince() Group by the Province column
 * @method     ClubQuery groupByPostalcode() Group by the PostalCode column
 * @method     ClubQuery groupByMapitaddress() Group by the MapItAddress column
 * @method     ClubQuery groupByPhone() Group by the Phone column
 * @method     ClubQuery groupByTollfree() Group by the TollFree column
 * @method     ClubQuery groupByContactemail() Group by the ContactEmail column
 * @method     ClubQuery groupByApplicationemail() Group by the ApplicationEmail column
 * @method     ClubQuery groupByPersonaltrainingemail() Group by the PersonalTrainingEmail column
 * @method     ClubQuery groupByPromotions() Group by the Promotions column
 * @method     ClubQuery groupByScheduleurl() Group by the ScheduleURL column
 * @method     ClubQuery groupByPoolscheduleurl() Group by the PoolScheduleURL column
 * @method     ClubQuery groupByUseinternal() Group by the UseInternal column
 * @method     ClubQuery groupBySundayhours() Group by the SundayHours column
 * @method     ClubQuery groupByMondayhours() Group by the MondayHours column
 * @method     ClubQuery groupByTuesdayhours() Group by the TuesdayHours column
 * @method     ClubQuery groupByWednesdayhours() Group by the WednesdayHours column
 * @method     ClubQuery groupByThursdayhours() Group by the ThursdayHours column
 * @method     ClubQuery groupByFridayhours() Group by the FridayHours column
 * @method     ClubQuery groupBySaturdayhours() Group by the SaturdayHours column
 * @method     ClubQuery groupByActive() Group by the Active column
 * @method     ClubQuery groupByFreetrial() Group by the FreeTrial column
 * @method     ClubQuery groupBySalessundayhours() Group by the SalesSundayHours column
 * @method     ClubQuery groupBySalesmondayhours() Group by the SalesMondayHours column
 * @method     ClubQuery groupBySalestuesdayhours() Group by the SalesTuesdayHours column
 * @method     ClubQuery groupBySaleswednesdayhours() Group by the SalesWednesdayHours column
 * @method     ClubQuery groupBySalesthursdayhours() Group by the SalesThursdayHours column
 * @method     ClubQuery groupBySalesfridayhours() Group by the SalesFridayHours column
 * @method     ClubQuery groupBySalessaturdayhours() Group by the SalesSaturdayHours column
 * @method     ClubQuery groupBySalestrailer() Group by the SalesTrailer column
 * @method     ClubQuery groupByOnlinepromo() Group by the OnlinePromo column
 * @method     ClubQuery groupByOnlinepromotion() Group by the OnlinePromotion column
 * @method     ClubQuery groupByNewsletterurl() Group by the NewsletterURL column
 * @method     ClubQuery groupByFacebookUrl() Group by the facebook_url column
 * @method     ClubQuery groupByMobileImage() Group by the mobile_image column
 *
 * @method     ClubQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ClubQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ClubQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ClubQuery leftJoinClassresult($relationAlias = null) Adds a LEFT JOIN clause to the query using the Classresult relation
 * @method     ClubQuery rightJoinClassresult($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Classresult relation
 * @method     ClubQuery innerJoinClassresult($relationAlias = null) Adds a INNER JOIN clause to the query using the Classresult relation
 *
 * @method     ClubQuery leftJoinInstructor($relationAlias = null) Adds a LEFT JOIN clause to the query using the Instructor relation
 * @method     ClubQuery rightJoinInstructor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Instructor relation
 * @method     ClubQuery innerJoinInstructor($relationAlias = null) Adds a INNER JOIN clause to the query using the Instructor relation
 *
 * @method     Club findOne(PropelPDO $con = null) Return the first Club matching the query
 * @method     Club findOneOrCreate(PropelPDO $con = null) Return the first Club matching the query, or a new Club object populated from the query conditions when no match is found
 *
 * @method     Club findOneById(int $ID) Return the first Club filtered by the ID column
 * @method     Club findOneByClubname(string $ClubName) Return the first Club filtered by the ClubName column
 * @method     Club findOneByAddress(string $Address) Return the first Club filtered by the Address column
 * @method     Club findOneByCity(string $City) Return the first Club filtered by the City column
 * @method     Club findOneByProvince(string $Province) Return the first Club filtered by the Province column
 * @method     Club findOneByPostalcode(string $PostalCode) Return the first Club filtered by the PostalCode column
 * @method     Club findOneByMapitaddress(string $MapItAddress) Return the first Club filtered by the MapItAddress column
 * @method     Club findOneByPhone(string $Phone) Return the first Club filtered by the Phone column
 * @method     Club findOneByTollfree(string $TollFree) Return the first Club filtered by the TollFree column
 * @method     Club findOneByContactemail(string $ContactEmail) Return the first Club filtered by the ContactEmail column
 * @method     Club findOneByApplicationemail(string $ApplicationEmail) Return the first Club filtered by the ApplicationEmail column
 * @method     Club findOneByPersonaltrainingemail(string $PersonalTrainingEmail) Return the first Club filtered by the PersonalTrainingEmail column
 * @method     Club findOneByPromotions(string $Promotions) Return the first Club filtered by the Promotions column
 * @method     Club findOneByScheduleurl(string $ScheduleURL) Return the first Club filtered by the ScheduleURL column
 * @method     Club findOneByPoolscheduleurl(string $PoolScheduleURL) Return the first Club filtered by the PoolScheduleURL column
 * @method     Club findOneByUseinternal(boolean $UseInternal) Return the first Club filtered by the UseInternal column
 * @method     Club findOneBySundayhours(string $SundayHours) Return the first Club filtered by the SundayHours column
 * @method     Club findOneByMondayhours(string $MondayHours) Return the first Club filtered by the MondayHours column
 * @method     Club findOneByTuesdayhours(string $TuesdayHours) Return the first Club filtered by the TuesdayHours column
 * @method     Club findOneByWednesdayhours(string $WednesdayHours) Return the first Club filtered by the WednesdayHours column
 * @method     Club findOneByThursdayhours(string $ThursdayHours) Return the first Club filtered by the ThursdayHours column
 * @method     Club findOneByFridayhours(string $FridayHours) Return the first Club filtered by the FridayHours column
 * @method     Club findOneBySaturdayhours(string $SaturdayHours) Return the first Club filtered by the SaturdayHours column
 * @method     Club findOneByActive(boolean $Active) Return the first Club filtered by the Active column
 * @method     Club findOneByFreetrial(boolean $FreeTrial) Return the first Club filtered by the FreeTrial column
 * @method     Club findOneBySalessundayhours(string $SalesSundayHours) Return the first Club filtered by the SalesSundayHours column
 * @method     Club findOneBySalesmondayhours(string $SalesMondayHours) Return the first Club filtered by the SalesMondayHours column
 * @method     Club findOneBySalestuesdayhours(string $SalesTuesdayHours) Return the first Club filtered by the SalesTuesdayHours column
 * @method     Club findOneBySaleswednesdayhours(string $SalesWednesdayHours) Return the first Club filtered by the SalesWednesdayHours column
 * @method     Club findOneBySalesthursdayhours(string $SalesThursdayHours) Return the first Club filtered by the SalesThursdayHours column
 * @method     Club findOneBySalesfridayhours(string $SalesFridayHours) Return the first Club filtered by the SalesFridayHours column
 * @method     Club findOneBySalessaturdayhours(string $SalesSaturdayHours) Return the first Club filtered by the SalesSaturdayHours column
 * @method     Club findOneBySalestrailer(boolean $SalesTrailer) Return the first Club filtered by the SalesTrailer column
 * @method     Club findOneByOnlinepromo(string $OnlinePromo) Return the first Club filtered by the OnlinePromo column
 * @method     Club findOneByOnlinepromotion(string $OnlinePromotion) Return the first Club filtered by the OnlinePromotion column
 * @method     Club findOneByNewsletterurl(string $NewsletterURL) Return the first Club filtered by the NewsletterURL column
 * @method     Club findOneByFacebookUrl(string $facebook_url) Return the first Club filtered by the facebook_url column
 * @method     Club findOneByMobileImage(string $mobile_image) Return the first Club filtered by the mobile_image column
 *
 * @method     array findById(int $ID) Return Club objects filtered by the ID column
 * @method     array findByClubname(string $ClubName) Return Club objects filtered by the ClubName column
 * @method     array findByAddress(string $Address) Return Club objects filtered by the Address column
 * @method     array findByCity(string $City) Return Club objects filtered by the City column
 * @method     array findByProvince(string $Province) Return Club objects filtered by the Province column
 * @method     array findByPostalcode(string $PostalCode) Return Club objects filtered by the PostalCode column
 * @method     array findByMapitaddress(string $MapItAddress) Return Club objects filtered by the MapItAddress column
 * @method     array findByPhone(string $Phone) Return Club objects filtered by the Phone column
 * @method     array findByTollfree(string $TollFree) Return Club objects filtered by the TollFree column
 * @method     array findByContactemail(string $ContactEmail) Return Club objects filtered by the ContactEmail column
 * @method     array findByApplicationemail(string $ApplicationEmail) Return Club objects filtered by the ApplicationEmail column
 * @method     array findByPersonaltrainingemail(string $PersonalTrainingEmail) Return Club objects filtered by the PersonalTrainingEmail column
 * @method     array findByPromotions(string $Promotions) Return Club objects filtered by the Promotions column
 * @method     array findByScheduleurl(string $ScheduleURL) Return Club objects filtered by the ScheduleURL column
 * @method     array findByPoolscheduleurl(string $PoolScheduleURL) Return Club objects filtered by the PoolScheduleURL column
 * @method     array findByUseinternal(boolean $UseInternal) Return Club objects filtered by the UseInternal column
 * @method     array findBySundayhours(string $SundayHours) Return Club objects filtered by the SundayHours column
 * @method     array findByMondayhours(string $MondayHours) Return Club objects filtered by the MondayHours column
 * @method     array findByTuesdayhours(string $TuesdayHours) Return Club objects filtered by the TuesdayHours column
 * @method     array findByWednesdayhours(string $WednesdayHours) Return Club objects filtered by the WednesdayHours column
 * @method     array findByThursdayhours(string $ThursdayHours) Return Club objects filtered by the ThursdayHours column
 * @method     array findByFridayhours(string $FridayHours) Return Club objects filtered by the FridayHours column
 * @method     array findBySaturdayhours(string $SaturdayHours) Return Club objects filtered by the SaturdayHours column
 * @method     array findByActive(boolean $Active) Return Club objects filtered by the Active column
 * @method     array findByFreetrial(boolean $FreeTrial) Return Club objects filtered by the FreeTrial column
 * @method     array findBySalessundayhours(string $SalesSundayHours) Return Club objects filtered by the SalesSundayHours column
 * @method     array findBySalesmondayhours(string $SalesMondayHours) Return Club objects filtered by the SalesMondayHours column
 * @method     array findBySalestuesdayhours(string $SalesTuesdayHours) Return Club objects filtered by the SalesTuesdayHours column
 * @method     array findBySaleswednesdayhours(string $SalesWednesdayHours) Return Club objects filtered by the SalesWednesdayHours column
 * @method     array findBySalesthursdayhours(string $SalesThursdayHours) Return Club objects filtered by the SalesThursdayHours column
 * @method     array findBySalesfridayhours(string $SalesFridayHours) Return Club objects filtered by the SalesFridayHours column
 * @method     array findBySalessaturdayhours(string $SalesSaturdayHours) Return Club objects filtered by the SalesSaturdayHours column
 * @method     array findBySalestrailer(boolean $SalesTrailer) Return Club objects filtered by the SalesTrailer column
 * @method     array findByOnlinepromo(string $OnlinePromo) Return Club objects filtered by the OnlinePromo column
 * @method     array findByOnlinepromotion(string $OnlinePromotion) Return Club objects filtered by the OnlinePromotion column
 * @method     array findByNewsletterurl(string $NewsletterURL) Return Club objects filtered by the NewsletterURL column
 * @method     array findByFacebookUrl(string $facebook_url) Return Club objects filtered by the facebook_url column
 * @method     array findByMobileImage(string $mobile_image) Return Club objects filtered by the mobile_image column
 *
 * @package    propel.generator.site.om
 */
abstract class BaseClubQuery extends ModelCriteria
{
    
    /**
     * Initializes internal state of BaseClubQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'site', $modelName = 'Club', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ClubQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     ClubQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ClubQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ClubQuery) {
            return $criteria;
        }
        $query = new ClubQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Club|Club[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ClubPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ClubPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   Club A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `CLUBNAME`, `ADDRESS`, `CITY`, `PROVINCE`, `POSTALCODE`, `MAPITADDRESS`, `PHONE`, `TOLLFREE`, `CONTACTEMAIL`, `APPLICATIONEMAIL`, `PERSONALTRAININGEMAIL`, `PROMOTIONS`, `SCHEDULEURL`, `POOLSCHEDULEURL`, `USEINTERNAL`, `SUNDAYHOURS`, `MONDAYHOURS`, `TUESDAYHOURS`, `WEDNESDAYHOURS`, `THURSDAYHOURS`, `FRIDAYHOURS`, `SATURDAYHOURS`, `ACTIVE`, `FREETRIAL`, `SALESSUNDAYHOURS`, `SALESMONDAYHOURS`, `SALESTUESDAYHOURS`, `SALESWEDNESDAYHOURS`, `SALESTHURSDAYHOURS`, `SALESFRIDAYHOURS`, `SALESSATURDAYHOURS`, `SALESTRAILER`, `ONLINEPROMO`, `ONLINEPROMOTION`, `NEWSLETTERURL`, `FACEBOOK_URL`, `MOBILE_IMAGE` FROM `Club` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Club();
            $obj->hydrate($row);
            ClubPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Club|Club[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Club[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ClubPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ClubPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(ClubPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the ClubName column
     *
     * Example usage:
     * <code>
     * $query->filterByClubname('fooValue');   // WHERE ClubName = 'fooValue'
     * $query->filterByClubname('%fooValue%'); // WHERE ClubName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $clubname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByClubname($clubname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($clubname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $clubname)) {
                $clubname = str_replace('*', '%', $clubname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::CLUBNAME, $clubname, $comparison);
    }

    /**
     * Filter the query on the Address column
     *
     * Example usage:
     * <code>
     * $query->filterByAddress('fooValue');   // WHERE Address = 'fooValue'
     * $query->filterByAddress('%fooValue%'); // WHERE Address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $address The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByAddress($address = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($address)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $address)) {
                $address = str_replace('*', '%', $address);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::ADDRESS, $address, $comparison);
    }

    /**
     * Filter the query on the City column
     *
     * Example usage:
     * <code>
     * $query->filterByCity('fooValue');   // WHERE City = 'fooValue'
     * $query->filterByCity('%fooValue%'); // WHERE City LIKE '%fooValue%'
     * </code>
     *
     * @param     string $city The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByCity($city = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($city)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $city)) {
                $city = str_replace('*', '%', $city);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::CITY, $city, $comparison);
    }

    /**
     * Filter the query on the Province column
     *
     * Example usage:
     * <code>
     * $query->filterByProvince('fooValue');   // WHERE Province = 'fooValue'
     * $query->filterByProvince('%fooValue%'); // WHERE Province LIKE '%fooValue%'
     * </code>
     *
     * @param     string $province The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByProvince($province = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($province)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $province)) {
                $province = str_replace('*', '%', $province);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::PROVINCE, $province, $comparison);
    }

    /**
     * Filter the query on the PostalCode column
     *
     * Example usage:
     * <code>
     * $query->filterByPostalcode('fooValue');   // WHERE PostalCode = 'fooValue'
     * $query->filterByPostalcode('%fooValue%'); // WHERE PostalCode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $postalcode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByPostalcode($postalcode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($postalcode)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $postalcode)) {
                $postalcode = str_replace('*', '%', $postalcode);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::POSTALCODE, $postalcode, $comparison);
    }

    /**
     * Filter the query on the MapItAddress column
     *
     * Example usage:
     * <code>
     * $query->filterByMapitaddress('fooValue');   // WHERE MapItAddress = 'fooValue'
     * $query->filterByMapitaddress('%fooValue%'); // WHERE MapItAddress LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mapitaddress The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByMapitaddress($mapitaddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mapitaddress)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mapitaddress)) {
                $mapitaddress = str_replace('*', '%', $mapitaddress);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::MAPITADDRESS, $mapitaddress, $comparison);
    }

    /**
     * Filter the query on the Phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE Phone = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE Phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the TollFree column
     *
     * Example usage:
     * <code>
     * $query->filterByTollfree('fooValue');   // WHERE TollFree = 'fooValue'
     * $query->filterByTollfree('%fooValue%'); // WHERE TollFree LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tollfree The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByTollfree($tollfree = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tollfree)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tollfree)) {
                $tollfree = str_replace('*', '%', $tollfree);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::TOLLFREE, $tollfree, $comparison);
    }

    /**
     * Filter the query on the ContactEmail column
     *
     * Example usage:
     * <code>
     * $query->filterByContactemail('fooValue');   // WHERE ContactEmail = 'fooValue'
     * $query->filterByContactemail('%fooValue%'); // WHERE ContactEmail LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contactemail The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByContactemail($contactemail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contactemail)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $contactemail)) {
                $contactemail = str_replace('*', '%', $contactemail);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::CONTACTEMAIL, $contactemail, $comparison);
    }

    /**
     * Filter the query on the ApplicationEmail column
     *
     * Example usage:
     * <code>
     * $query->filterByApplicationemail('fooValue');   // WHERE ApplicationEmail = 'fooValue'
     * $query->filterByApplicationemail('%fooValue%'); // WHERE ApplicationEmail LIKE '%fooValue%'
     * </code>
     *
     * @param     string $applicationemail The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByApplicationemail($applicationemail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($applicationemail)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $applicationemail)) {
                $applicationemail = str_replace('*', '%', $applicationemail);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::APPLICATIONEMAIL, $applicationemail, $comparison);
    }

    /**
     * Filter the query on the PersonalTrainingEmail column
     *
     * Example usage:
     * <code>
     * $query->filterByPersonaltrainingemail('fooValue');   // WHERE PersonalTrainingEmail = 'fooValue'
     * $query->filterByPersonaltrainingemail('%fooValue%'); // WHERE PersonalTrainingEmail LIKE '%fooValue%'
     * </code>
     *
     * @param     string $personaltrainingemail The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByPersonaltrainingemail($personaltrainingemail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($personaltrainingemail)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $personaltrainingemail)) {
                $personaltrainingemail = str_replace('*', '%', $personaltrainingemail);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::PERSONALTRAININGEMAIL, $personaltrainingemail, $comparison);
    }

    /**
     * Filter the query on the Promotions column
     *
     * Example usage:
     * <code>
     * $query->filterByPromotions('fooValue');   // WHERE Promotions = 'fooValue'
     * $query->filterByPromotions('%fooValue%'); // WHERE Promotions LIKE '%fooValue%'
     * </code>
     *
     * @param     string $promotions The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByPromotions($promotions = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($promotions)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $promotions)) {
                $promotions = str_replace('*', '%', $promotions);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::PROMOTIONS, $promotions, $comparison);
    }

    /**
     * Filter the query on the ScheduleURL column
     *
     * Example usage:
     * <code>
     * $query->filterByScheduleurl('fooValue');   // WHERE ScheduleURL = 'fooValue'
     * $query->filterByScheduleurl('%fooValue%'); // WHERE ScheduleURL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $scheduleurl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByScheduleurl($scheduleurl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($scheduleurl)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $scheduleurl)) {
                $scheduleurl = str_replace('*', '%', $scheduleurl);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::SCHEDULEURL, $scheduleurl, $comparison);
    }

    /**
     * Filter the query on the PoolScheduleURL column
     *
     * Example usage:
     * <code>
     * $query->filterByPoolscheduleurl('fooValue');   // WHERE PoolScheduleURL = 'fooValue'
     * $query->filterByPoolscheduleurl('%fooValue%'); // WHERE PoolScheduleURL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $poolscheduleurl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByPoolscheduleurl($poolscheduleurl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($poolscheduleurl)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $poolscheduleurl)) {
                $poolscheduleurl = str_replace('*', '%', $poolscheduleurl);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::POOLSCHEDULEURL, $poolscheduleurl, $comparison);
    }

    /**
     * Filter the query on the UseInternal column
     *
     * Example usage:
     * <code>
     * $query->filterByUseinternal(true); // WHERE UseInternal = true
     * $query->filterByUseinternal('yes'); // WHERE UseInternal = true
     * </code>
     *
     * @param     boolean|string $useinternal The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByUseinternal($useinternal = null, $comparison = null)
    {
        if (is_string($useinternal)) {
            $UseInternal = in_array(strtolower($useinternal), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ClubPeer::USEINTERNAL, $useinternal, $comparison);
    }

    /**
     * Filter the query on the SundayHours column
     *
     * Example usage:
     * <code>
     * $query->filterBySundayhours('fooValue');   // WHERE SundayHours = 'fooValue'
     * $query->filterBySundayhours('%fooValue%'); // WHERE SundayHours LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sundayhours The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterBySundayhours($sundayhours = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sundayhours)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sundayhours)) {
                $sundayhours = str_replace('*', '%', $sundayhours);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::SUNDAYHOURS, $sundayhours, $comparison);
    }

    /**
     * Filter the query on the MondayHours column
     *
     * Example usage:
     * <code>
     * $query->filterByMondayhours('fooValue');   // WHERE MondayHours = 'fooValue'
     * $query->filterByMondayhours('%fooValue%'); // WHERE MondayHours LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mondayhours The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByMondayhours($mondayhours = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mondayhours)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mondayhours)) {
                $mondayhours = str_replace('*', '%', $mondayhours);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::MONDAYHOURS, $mondayhours, $comparison);
    }

    /**
     * Filter the query on the TuesdayHours column
     *
     * Example usage:
     * <code>
     * $query->filterByTuesdayhours('fooValue');   // WHERE TuesdayHours = 'fooValue'
     * $query->filterByTuesdayhours('%fooValue%'); // WHERE TuesdayHours LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tuesdayhours The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByTuesdayhours($tuesdayhours = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tuesdayhours)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tuesdayhours)) {
                $tuesdayhours = str_replace('*', '%', $tuesdayhours);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::TUESDAYHOURS, $tuesdayhours, $comparison);
    }

    /**
     * Filter the query on the WednesdayHours column
     *
     * Example usage:
     * <code>
     * $query->filterByWednesdayhours('fooValue');   // WHERE WednesdayHours = 'fooValue'
     * $query->filterByWednesdayhours('%fooValue%'); // WHERE WednesdayHours LIKE '%fooValue%'
     * </code>
     *
     * @param     string $wednesdayhours The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByWednesdayhours($wednesdayhours = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($wednesdayhours)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $wednesdayhours)) {
                $wednesdayhours = str_replace('*', '%', $wednesdayhours);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::WEDNESDAYHOURS, $wednesdayhours, $comparison);
    }

    /**
     * Filter the query on the ThursdayHours column
     *
     * Example usage:
     * <code>
     * $query->filterByThursdayhours('fooValue');   // WHERE ThursdayHours = 'fooValue'
     * $query->filterByThursdayhours('%fooValue%'); // WHERE ThursdayHours LIKE '%fooValue%'
     * </code>
     *
     * @param     string $thursdayhours The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByThursdayhours($thursdayhours = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($thursdayhours)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $thursdayhours)) {
                $thursdayhours = str_replace('*', '%', $thursdayhours);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::THURSDAYHOURS, $thursdayhours, $comparison);
    }

    /**
     * Filter the query on the FridayHours column
     *
     * Example usage:
     * <code>
     * $query->filterByFridayhours('fooValue');   // WHERE FridayHours = 'fooValue'
     * $query->filterByFridayhours('%fooValue%'); // WHERE FridayHours LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fridayhours The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByFridayhours($fridayhours = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fridayhours)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $fridayhours)) {
                $fridayhours = str_replace('*', '%', $fridayhours);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::FRIDAYHOURS, $fridayhours, $comparison);
    }

    /**
     * Filter the query on the SaturdayHours column
     *
     * Example usage:
     * <code>
     * $query->filterBySaturdayhours('fooValue');   // WHERE SaturdayHours = 'fooValue'
     * $query->filterBySaturdayhours('%fooValue%'); // WHERE SaturdayHours LIKE '%fooValue%'
     * </code>
     *
     * @param     string $saturdayhours The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterBySaturdayhours($saturdayhours = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($saturdayhours)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $saturdayhours)) {
                $saturdayhours = str_replace('*', '%', $saturdayhours);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::SATURDAYHOURS, $saturdayhours, $comparison);
    }

    /**
     * Filter the query on the Active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(true); // WHERE Active = true
     * $query->filterByActive('yes'); // WHERE Active = true
     * </code>
     *
     * @param     boolean|string $active The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (is_string($active)) {
            $Active = in_array(strtolower($active), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ClubPeer::ACTIVE, $active, $comparison);
    }

    /**
     * Filter the query on the FreeTrial column
     *
     * Example usage:
     * <code>
     * $query->filterByFreetrial(true); // WHERE FreeTrial = true
     * $query->filterByFreetrial('yes'); // WHERE FreeTrial = true
     * </code>
     *
     * @param     boolean|string $freetrial The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByFreetrial($freetrial = null, $comparison = null)
    {
        if (is_string($freetrial)) {
            $FreeTrial = in_array(strtolower($freetrial), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ClubPeer::FREETRIAL, $freetrial, $comparison);
    }

    /**
     * Filter the query on the SalesSundayHours column
     *
     * Example usage:
     * <code>
     * $query->filterBySalessundayhours('fooValue');   // WHERE SalesSundayHours = 'fooValue'
     * $query->filterBySalessundayhours('%fooValue%'); // WHERE SalesSundayHours LIKE '%fooValue%'
     * </code>
     *
     * @param     string $salessundayhours The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterBySalessundayhours($salessundayhours = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($salessundayhours)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $salessundayhours)) {
                $salessundayhours = str_replace('*', '%', $salessundayhours);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::SALESSUNDAYHOURS, $salessundayhours, $comparison);
    }

    /**
     * Filter the query on the SalesMondayHours column
     *
     * Example usage:
     * <code>
     * $query->filterBySalesmondayhours('fooValue');   // WHERE SalesMondayHours = 'fooValue'
     * $query->filterBySalesmondayhours('%fooValue%'); // WHERE SalesMondayHours LIKE '%fooValue%'
     * </code>
     *
     * @param     string $salesmondayhours The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterBySalesmondayhours($salesmondayhours = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($salesmondayhours)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $salesmondayhours)) {
                $salesmondayhours = str_replace('*', '%', $salesmondayhours);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::SALESMONDAYHOURS, $salesmondayhours, $comparison);
    }

    /**
     * Filter the query on the SalesTuesdayHours column
     *
     * Example usage:
     * <code>
     * $query->filterBySalestuesdayhours('fooValue');   // WHERE SalesTuesdayHours = 'fooValue'
     * $query->filterBySalestuesdayhours('%fooValue%'); // WHERE SalesTuesdayHours LIKE '%fooValue%'
     * </code>
     *
     * @param     string $salestuesdayhours The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterBySalestuesdayhours($salestuesdayhours = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($salestuesdayhours)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $salestuesdayhours)) {
                $salestuesdayhours = str_replace('*', '%', $salestuesdayhours);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::SALESTUESDAYHOURS, $salestuesdayhours, $comparison);
    }

    /**
     * Filter the query on the SalesWednesdayHours column
     *
     * Example usage:
     * <code>
     * $query->filterBySaleswednesdayhours('fooValue');   // WHERE SalesWednesdayHours = 'fooValue'
     * $query->filterBySaleswednesdayhours('%fooValue%'); // WHERE SalesWednesdayHours LIKE '%fooValue%'
     * </code>
     *
     * @param     string $saleswednesdayhours The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterBySaleswednesdayhours($saleswednesdayhours = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($saleswednesdayhours)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $saleswednesdayhours)) {
                $saleswednesdayhours = str_replace('*', '%', $saleswednesdayhours);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::SALESWEDNESDAYHOURS, $saleswednesdayhours, $comparison);
    }

    /**
     * Filter the query on the SalesThursdayHours column
     *
     * Example usage:
     * <code>
     * $query->filterBySalesthursdayhours('fooValue');   // WHERE SalesThursdayHours = 'fooValue'
     * $query->filterBySalesthursdayhours('%fooValue%'); // WHERE SalesThursdayHours LIKE '%fooValue%'
     * </code>
     *
     * @param     string $salesthursdayhours The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterBySalesthursdayhours($salesthursdayhours = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($salesthursdayhours)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $salesthursdayhours)) {
                $salesthursdayhours = str_replace('*', '%', $salesthursdayhours);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::SALESTHURSDAYHOURS, $salesthursdayhours, $comparison);
    }

    /**
     * Filter the query on the SalesFridayHours column
     *
     * Example usage:
     * <code>
     * $query->filterBySalesfridayhours('fooValue');   // WHERE SalesFridayHours = 'fooValue'
     * $query->filterBySalesfridayhours('%fooValue%'); // WHERE SalesFridayHours LIKE '%fooValue%'
     * </code>
     *
     * @param     string $salesfridayhours The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterBySalesfridayhours($salesfridayhours = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($salesfridayhours)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $salesfridayhours)) {
                $salesfridayhours = str_replace('*', '%', $salesfridayhours);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::SALESFRIDAYHOURS, $salesfridayhours, $comparison);
    }

    /**
     * Filter the query on the SalesSaturdayHours column
     *
     * Example usage:
     * <code>
     * $query->filterBySalessaturdayhours('fooValue');   // WHERE SalesSaturdayHours = 'fooValue'
     * $query->filterBySalessaturdayhours('%fooValue%'); // WHERE SalesSaturdayHours LIKE '%fooValue%'
     * </code>
     *
     * @param     string $salessaturdayhours The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterBySalessaturdayhours($salessaturdayhours = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($salessaturdayhours)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $salessaturdayhours)) {
                $salessaturdayhours = str_replace('*', '%', $salessaturdayhours);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::SALESSATURDAYHOURS, $salessaturdayhours, $comparison);
    }

    /**
     * Filter the query on the SalesTrailer column
     *
     * Example usage:
     * <code>
     * $query->filterBySalestrailer(true); // WHERE SalesTrailer = true
     * $query->filterBySalestrailer('yes'); // WHERE SalesTrailer = true
     * </code>
     *
     * @param     boolean|string $salestrailer The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterBySalestrailer($salestrailer = null, $comparison = null)
    {
        if (is_string($salestrailer)) {
            $SalesTrailer = in_array(strtolower($salestrailer), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ClubPeer::SALESTRAILER, $salestrailer, $comparison);
    }

    /**
     * Filter the query on the OnlinePromo column
     *
     * Example usage:
     * <code>
     * $query->filterByOnlinepromo('fooValue');   // WHERE OnlinePromo = 'fooValue'
     * $query->filterByOnlinepromo('%fooValue%'); // WHERE OnlinePromo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $onlinepromo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByOnlinepromo($onlinepromo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($onlinepromo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $onlinepromo)) {
                $onlinepromo = str_replace('*', '%', $onlinepromo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::ONLINEPROMO, $onlinepromo, $comparison);
    }

    /**
     * Filter the query on the OnlinePromotion column
     *
     * Example usage:
     * <code>
     * $query->filterByOnlinepromotion('fooValue');   // WHERE OnlinePromotion = 'fooValue'
     * $query->filterByOnlinepromotion('%fooValue%'); // WHERE OnlinePromotion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $onlinepromotion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByOnlinepromotion($onlinepromotion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($onlinepromotion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $onlinepromotion)) {
                $onlinepromotion = str_replace('*', '%', $onlinepromotion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::ONLINEPROMOTION, $onlinepromotion, $comparison);
    }

    /**
     * Filter the query on the NewsletterURL column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsletterurl('fooValue');   // WHERE NewsletterURL = 'fooValue'
     * $query->filterByNewsletterurl('%fooValue%'); // WHERE NewsletterURL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $newsletterurl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByNewsletterurl($newsletterurl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($newsletterurl)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $newsletterurl)) {
                $newsletterurl = str_replace('*', '%', $newsletterurl);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::NEWSLETTERURL, $newsletterurl, $comparison);
    }

    /**
     * Filter the query on the facebook_url column
     *
     * Example usage:
     * <code>
     * $query->filterByFacebookUrl('fooValue');   // WHERE facebook_url = 'fooValue'
     * $query->filterByFacebookUrl('%fooValue%'); // WHERE facebook_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $facebookUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByFacebookUrl($facebookUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($facebookUrl)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $facebookUrl)) {
                $facebookUrl = str_replace('*', '%', $facebookUrl);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::FACEBOOK_URL, $facebookUrl, $comparison);
    }

    /**
     * Filter the query on the mobile_image column
     *
     * Example usage:
     * <code>
     * $query->filterByMobileImage('fooValue');   // WHERE mobile_image = 'fooValue'
     * $query->filterByMobileImage('%fooValue%'); // WHERE mobile_image LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mobileImage The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function filterByMobileImage($mobileImage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mobileImage)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mobileImage)) {
                $mobileImage = str_replace('*', '%', $mobileImage);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClubPeer::MOBILE_IMAGE, $mobileImage, $comparison);
    }

    /**
     * Filter the query by a related Classresult object
     *
     * @param   Classresult|PropelObjectCollection $classresult  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   ClubQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByClassresult($classresult, $comparison = null)
    {
        if ($classresult instanceof Classresult) {
            return $this
                ->addUsingAlias(ClubPeer::ID, $classresult->getClubid(), $comparison);
        } elseif ($classresult instanceof PropelObjectCollection) {
            return $this
                ->useClassresultQuery()
                ->filterByPrimaryKeys($classresult->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByClassresult() only accepts arguments of type Classresult or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Classresult relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function joinClassresult($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Classresult');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Classresult');
        }

        return $this;
    }

    /**
     * Use the Classresult relation Classresult object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ClassresultQuery A secondary query class using the current class as primary query
     */
    public function useClassresultQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinClassresult($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Classresult', 'ClassresultQuery');
    }

    /**
     * Filter the query by a related Instructor object
     *
     * @param   Instructor|PropelObjectCollection $instructor  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   ClubQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByInstructor($instructor, $comparison = null)
    {
        if ($instructor instanceof Instructor) {
            return $this
                ->addUsingAlias(ClubPeer::ID, $instructor->getClubid(), $comparison);
        } elseif ($instructor instanceof PropelObjectCollection) {
            return $this
                ->useInstructorQuery()
                ->filterByPrimaryKeys($instructor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByInstructor() only accepts arguments of type Instructor or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Instructor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function joinInstructor($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Instructor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Instructor');
        }

        return $this;
    }

    /**
     * Use the Instructor relation Instructor object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   InstructorQuery A secondary query class using the current class as primary query
     */
    public function useInstructorQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinInstructor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Instructor', 'InstructorQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Club $club Object to remove from the list of results
     *
     * @return ClubQuery The current query, for fluid interface
     */
    public function prune($club = null)
    {
        if ($club) {
            $this->addUsingAlias(ClubPeer::ID, $club->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

} // BaseClubQuery