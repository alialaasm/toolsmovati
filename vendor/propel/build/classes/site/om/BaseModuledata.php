<?php


/**
 * Base class that represents a row from the 'ModuleData' table.
 *
 * 
 *
 * @package    propel.generator.site.om
 */
abstract class BaseModuledata extends BaseObject 
{

    /**
     * Peer class name
     */
    const PEER = 'ModuledataPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        ModuledataPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the modulename field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $modulename;

    /**
     * The value for the var1 field.
     * @var        string
     */
    protected $var1;

    /**
     * The value for the var2 field.
     * @var        string
     */
    protected $var2;

    /**
     * The value for the var3 field.
     * @var        string
     */
    protected $var3;

    /**
     * The value for the var4 field.
     * @var        string
     */
    protected $var4;

    /**
     * The value for the var5 field.
     * @var        string
     */
    protected $var5;

    /**
     * The value for the var6 field.
     * @var        string
     */
    protected $var6;

    /**
     * The value for the var7 field.
     * @var        string
     */
    protected $var7;

    /**
     * The value for the var8 field.
     * @var        string
     */
    protected $var8;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->modulename = '';
    }

    /**
     * Initializes internal state of BaseModuledata object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [modulename] column value.
     * 
     * @return   string
     */
    public function getModulename()
    {

        return $this->modulename;
    }

    /**
     * Get the [var1] column value.
     * 
     * @return   string
     */
    public function getVar1()
    {

        return $this->var1;
    }

    /**
     * Get the [var2] column value.
     * 
     * @return   string
     */
    public function getVar2()
    {

        return $this->var2;
    }

    /**
     * Get the [var3] column value.
     * 
     * @return   string
     */
    public function getVar3()
    {

        return $this->var3;
    }

    /**
     * Get the [var4] column value.
     * 
     * @return   string
     */
    public function getVar4()
    {

        return $this->var4;
    }

    /**
     * Get the [var5] column value.
     * 
     * @return   string
     */
    public function getVar5()
    {

        return $this->var5;
    }

    /**
     * Get the [var6] column value.
     * 
     * @return   string
     */
    public function getVar6()
    {

        return $this->var6;
    }

    /**
     * Get the [var7] column value.
     * 
     * @return   string
     */
    public function getVar7()
    {

        return $this->var7;
    }

    /**
     * Get the [var8] column value.
     * 
     * @return   string
     */
    public function getVar8()
    {

        return $this->var8;
    }

    /**
     * Set the value of [modulename] column.
     * 
     * @param      string $v new value
     * @return   Moduledata The current object (for fluent API support)
     */
    public function setModulename($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->modulename !== $v) {
            $this->modulename = $v;
            $this->modifiedColumns[] = ModuledataPeer::MODULENAME;
        }


        return $this;
    } // setModulename()

    /**
     * Set the value of [var1] column.
     * 
     * @param      string $v new value
     * @return   Moduledata The current object (for fluent API support)
     */
    public function setVar1($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->var1 !== $v) {
            $this->var1 = $v;
            $this->modifiedColumns[] = ModuledataPeer::VAR1;
        }


        return $this;
    } // setVar1()

    /**
     * Set the value of [var2] column.
     * 
     * @param      string $v new value
     * @return   Moduledata The current object (for fluent API support)
     */
    public function setVar2($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->var2 !== $v) {
            $this->var2 = $v;
            $this->modifiedColumns[] = ModuledataPeer::VAR2;
        }


        return $this;
    } // setVar2()

    /**
     * Set the value of [var3] column.
     * 
     * @param      string $v new value
     * @return   Moduledata The current object (for fluent API support)
     */
    public function setVar3($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->var3 !== $v) {
            $this->var3 = $v;
            $this->modifiedColumns[] = ModuledataPeer::VAR3;
        }


        return $this;
    } // setVar3()

    /**
     * Set the value of [var4] column.
     * 
     * @param      string $v new value
     * @return   Moduledata The current object (for fluent API support)
     */
    public function setVar4($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->var4 !== $v) {
            $this->var4 = $v;
            $this->modifiedColumns[] = ModuledataPeer::VAR4;
        }


        return $this;
    } // setVar4()

    /**
     * Set the value of [var5] column.
     * 
     * @param      string $v new value
     * @return   Moduledata The current object (for fluent API support)
     */
    public function setVar5($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->var5 !== $v) {
            $this->var5 = $v;
            $this->modifiedColumns[] = ModuledataPeer::VAR5;
        }


        return $this;
    } // setVar5()

    /**
     * Set the value of [var6] column.
     * 
     * @param      string $v new value
     * @return   Moduledata The current object (for fluent API support)
     */
    public function setVar6($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->var6 !== $v) {
            $this->var6 = $v;
            $this->modifiedColumns[] = ModuledataPeer::VAR6;
        }


        return $this;
    } // setVar6()

    /**
     * Set the value of [var7] column.
     * 
     * @param      string $v new value
     * @return   Moduledata The current object (for fluent API support)
     */
    public function setVar7($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->var7 !== $v) {
            $this->var7 = $v;
            $this->modifiedColumns[] = ModuledataPeer::VAR7;
        }


        return $this;
    } // setVar7()

    /**
     * Set the value of [var8] column.
     * 
     * @param      string $v new value
     * @return   Moduledata The current object (for fluent API support)
     */
    public function setVar8($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->var8 !== $v) {
            $this->var8 = $v;
            $this->modifiedColumns[] = ModuledataPeer::VAR8;
        }


        return $this;
    } // setVar8()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->modulename !== '') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->modulename = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->var1 = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->var2 = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->var3 = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->var4 = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->var5 = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->var6 = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->var7 = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->var8 = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 9; // 9 = ModuledataPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Moduledata object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ModuledataPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ModuledataPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ModuledataPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ModuledataQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ModuledataPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ModuledataPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ModuledataPeer::MODULENAME)) {
            $modifiedColumns[':p' . $index++]  = '`MODULENAME`';
        }
        if ($this->isColumnModified(ModuledataPeer::VAR1)) {
            $modifiedColumns[':p' . $index++]  = '`VAR1`';
        }
        if ($this->isColumnModified(ModuledataPeer::VAR2)) {
            $modifiedColumns[':p' . $index++]  = '`VAR2`';
        }
        if ($this->isColumnModified(ModuledataPeer::VAR3)) {
            $modifiedColumns[':p' . $index++]  = '`VAR3`';
        }
        if ($this->isColumnModified(ModuledataPeer::VAR4)) {
            $modifiedColumns[':p' . $index++]  = '`VAR4`';
        }
        if ($this->isColumnModified(ModuledataPeer::VAR5)) {
            $modifiedColumns[':p' . $index++]  = '`VAR5`';
        }
        if ($this->isColumnModified(ModuledataPeer::VAR6)) {
            $modifiedColumns[':p' . $index++]  = '`VAR6`';
        }
        if ($this->isColumnModified(ModuledataPeer::VAR7)) {
            $modifiedColumns[':p' . $index++]  = '`VAR7`';
        }
        if ($this->isColumnModified(ModuledataPeer::VAR8)) {
            $modifiedColumns[':p' . $index++]  = '`VAR8`';
        }

        $sql = sprintf(
            'INSERT INTO `ModuleData` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`MODULENAME`':
						$stmt->bindValue($identifier, $this->modulename, PDO::PARAM_STR);
                        break;
                    case '`VAR1`':
						$stmt->bindValue($identifier, $this->var1, PDO::PARAM_STR);
                        break;
                    case '`VAR2`':
						$stmt->bindValue($identifier, $this->var2, PDO::PARAM_STR);
                        break;
                    case '`VAR3`':
						$stmt->bindValue($identifier, $this->var3, PDO::PARAM_STR);
                        break;
                    case '`VAR4`':
						$stmt->bindValue($identifier, $this->var4, PDO::PARAM_STR);
                        break;
                    case '`VAR5`':
						$stmt->bindValue($identifier, $this->var5, PDO::PARAM_STR);
                        break;
                    case '`VAR6`':
						$stmt->bindValue($identifier, $this->var6, PDO::PARAM_STR);
                        break;
                    case '`VAR7`':
						$stmt->bindValue($identifier, $this->var7, PDO::PARAM_STR);
                        break;
                    case '`VAR8`':
						$stmt->bindValue($identifier, $this->var8, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param      mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        } else {
            $this->validationFailures = $res;

            return false;
        }
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param      array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = ModuledataPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ModuledataPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getModulename();
                break;
            case 1:
                return $this->getVar1();
                break;
            case 2:
                return $this->getVar2();
                break;
            case 3:
                return $this->getVar3();
                break;
            case 4:
                return $this->getVar4();
                break;
            case 5:
                return $this->getVar5();
                break;
            case 6:
                return $this->getVar6();
                break;
            case 7:
                return $this->getVar7();
                break;
            case 8:
                return $this->getVar8();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['Moduledata'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Moduledata'][$this->getPrimaryKey()] = true;
        $keys = ModuledataPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getModulename(),
            $keys[1] => $this->getVar1(),
            $keys[2] => $this->getVar2(),
            $keys[3] => $this->getVar3(),
            $keys[4] => $this->getVar4(),
            $keys[5] => $this->getVar5(),
            $keys[6] => $this->getVar6(),
            $keys[7] => $this->getVar7(),
            $keys[8] => $this->getVar8(),
        );

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param      string $name peer name
     * @param      mixed $value field value
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ModuledataPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @param      mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setModulename($value);
                break;
            case 1:
                $this->setVar1($value);
                break;
            case 2:
                $this->setVar2($value);
                break;
            case 3:
                $this->setVar3($value);
                break;
            case 4:
                $this->setVar4($value);
                break;
            case 5:
                $this->setVar5($value);
                break;
            case 6:
                $this->setVar6($value);
                break;
            case 7:
                $this->setVar7($value);
                break;
            case 8:
                $this->setVar8($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ModuledataPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setModulename($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setVar1($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setVar2($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setVar3($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setVar4($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setVar5($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setVar6($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setVar7($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setVar8($arr[$keys[8]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ModuledataPeer::DATABASE_NAME);

        if ($this->isColumnModified(ModuledataPeer::MODULENAME)) $criteria->add(ModuledataPeer::MODULENAME, $this->modulename);
        if ($this->isColumnModified(ModuledataPeer::VAR1)) $criteria->add(ModuledataPeer::VAR1, $this->var1);
        if ($this->isColumnModified(ModuledataPeer::VAR2)) $criteria->add(ModuledataPeer::VAR2, $this->var2);
        if ($this->isColumnModified(ModuledataPeer::VAR3)) $criteria->add(ModuledataPeer::VAR3, $this->var3);
        if ($this->isColumnModified(ModuledataPeer::VAR4)) $criteria->add(ModuledataPeer::VAR4, $this->var4);
        if ($this->isColumnModified(ModuledataPeer::VAR5)) $criteria->add(ModuledataPeer::VAR5, $this->var5);
        if ($this->isColumnModified(ModuledataPeer::VAR6)) $criteria->add(ModuledataPeer::VAR6, $this->var6);
        if ($this->isColumnModified(ModuledataPeer::VAR7)) $criteria->add(ModuledataPeer::VAR7, $this->var7);
        if ($this->isColumnModified(ModuledataPeer::VAR8)) $criteria->add(ModuledataPeer::VAR8, $this->var8);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ModuledataPeer::DATABASE_NAME);
        $criteria->add(ModuledataPeer::MODULENAME, $this->modulename);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return   string
     */
    public function getPrimaryKey()
    {
        return $this->getModulename();
    }

    /**
     * Generic method to set the primary key (modulename column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setModulename($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getModulename();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of Moduledata (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setVar1($this->getVar1());
        $copyObj->setVar2($this->getVar2());
        $copyObj->setVar3($this->getVar3());
        $copyObj->setVar4($this->getVar4());
        $copyObj->setVar5($this->getVar5());
        $copyObj->setVar6($this->getVar6());
        $copyObj->setVar7($this->getVar7());
        $copyObj->setVar8($this->getVar8());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setModulename(''); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return                 Moduledata Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return   ModuledataPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ModuledataPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->modulename = null;
        $this->var1 = null;
        $this->var2 = null;
        $this->var3 = null;
        $this->var4 = null;
        $this->var5 = null;
        $this->var6 = null;
        $this->var7 = null;
        $this->var8 = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ModuledataPeer::DEFAULT_STRING_FORMAT);
    }

} // BaseModuledata
