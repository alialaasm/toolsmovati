<?php


/**
 * Base class that represents a query for the 'Studio' table.
 *
 * 
 *
 * @method     StudioQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method     StudioQuery orderByClubid($order = Criteria::ASC) Order by the ClubId column
 * @method     StudioQuery orderByStudioname($order = Criteria::ASC) Order by the StudioName column
 * @method     StudioQuery orderByStudiostyle($order = Criteria::ASC) Order by the StudioStyle column
 *
 * @method     StudioQuery groupById() Group by the ID column
 * @method     StudioQuery groupByClubid() Group by the ClubId column
 * @method     StudioQuery groupByStudioname() Group by the StudioName column
 * @method     StudioQuery groupByStudiostyle() Group by the StudioStyle column
 *
 * @method     StudioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     StudioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     StudioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     Studio findOne(PropelPDO $con = null) Return the first Studio matching the query
 * @method     Studio findOneOrCreate(PropelPDO $con = null) Return the first Studio matching the query, or a new Studio object populated from the query conditions when no match is found
 *
 * @method     Studio findOneById(int $ID) Return the first Studio filtered by the ID column
 * @method     Studio findOneByClubid(int $ClubId) Return the first Studio filtered by the ClubId column
 * @method     Studio findOneByStudioname(string $StudioName) Return the first Studio filtered by the StudioName column
 * @method     Studio findOneByStudiostyle(string $StudioStyle) Return the first Studio filtered by the StudioStyle column
 *
 * @method     array findById(int $ID) Return Studio objects filtered by the ID column
 * @method     array findByClubid(int $ClubId) Return Studio objects filtered by the ClubId column
 * @method     array findByStudioname(string $StudioName) Return Studio objects filtered by the StudioName column
 * @method     array findByStudiostyle(string $StudioStyle) Return Studio objects filtered by the StudioStyle column
 *
 * @package    propel.generator.site.om
 */
abstract class BaseStudioQuery extends ModelCriteria
{
    
    /**
     * Initializes internal state of BaseStudioQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'site', $modelName = 'Studio', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new StudioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     StudioQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return StudioQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof StudioQuery) {
            return $criteria;
        }
        $query = new StudioQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Studio|Studio[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = StudioPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(StudioPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   Studio A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `CLUBID`, `STUDIONAME`, `STUDIOSTYLE` FROM `Studio` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Studio();
            $obj->hydrate($row);
            StudioPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Studio|Studio[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Studio[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return StudioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(StudioPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return StudioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(StudioPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StudioQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(StudioPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the ClubId column
     *
     * Example usage:
     * <code>
     * $query->filterByClubid(1234); // WHERE ClubId = 1234
     * $query->filterByClubid(array(12, 34)); // WHERE ClubId IN (12, 34)
     * $query->filterByClubid(array('min' => 12)); // WHERE ClubId > 12
     * </code>
     *
     * @param     mixed $clubid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StudioQuery The current query, for fluid interface
     */
    public function filterByClubid($clubid = null, $comparison = null)
    {
        if (is_array($clubid)) {
            $useMinMax = false;
            if (isset($clubid['min'])) {
                $this->addUsingAlias(StudioPeer::CLUBID, $clubid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clubid['max'])) {
                $this->addUsingAlias(StudioPeer::CLUBID, $clubid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StudioPeer::CLUBID, $clubid, $comparison);
    }

    /**
     * Filter the query on the StudioName column
     *
     * Example usage:
     * <code>
     * $query->filterByStudioname('fooValue');   // WHERE StudioName = 'fooValue'
     * $query->filterByStudioname('%fooValue%'); // WHERE StudioName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $studioname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StudioQuery The current query, for fluid interface
     */
    public function filterByStudioname($studioname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($studioname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $studioname)) {
                $studioname = str_replace('*', '%', $studioname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(StudioPeer::STUDIONAME, $studioname, $comparison);
    }

    /**
     * Filter the query on the StudioStyle column
     *
     * Example usage:
     * <code>
     * $query->filterByStudiostyle('fooValue');   // WHERE StudioStyle = 'fooValue'
     * $query->filterByStudiostyle('%fooValue%'); // WHERE StudioStyle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $studiostyle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StudioQuery The current query, for fluid interface
     */
    public function filterByStudiostyle($studiostyle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($studiostyle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $studiostyle)) {
                $studiostyle = str_replace('*', '%', $studiostyle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(StudioPeer::STUDIOSTYLE, $studiostyle, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Studio $studio Object to remove from the list of results
     *
     * @return StudioQuery The current query, for fluid interface
     */
    public function prune($studio = null)
    {
        if ($studio) {
            $this->addUsingAlias(StudioPeer::ID, $studio->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

} // BaseStudioQuery