<?php


/**
 * Base class that represents a query for the 'UserAccess' table.
 *
 * 
 *
 * @method     UseraccessQuery orderByUserid($order = Criteria::ASC) Order by the UserID column
 * @method     UseraccessQuery orderByMenuid($order = Criteria::ASC) Order by the MenuID column
 * @method     UseraccessQuery orderByClubid($order = Criteria::ASC) Order by the ClubID column
 * @method     UseraccessQuery orderByStudioid($order = Criteria::ASC) Order by the StudioID column
 * @method     UseraccessQuery orderById($order = Criteria::ASC) Order by the ID column
 *
 * @method     UseraccessQuery groupByUserid() Group by the UserID column
 * @method     UseraccessQuery groupByMenuid() Group by the MenuID column
 * @method     UseraccessQuery groupByClubid() Group by the ClubID column
 * @method     UseraccessQuery groupByStudioid() Group by the StudioID column
 * @method     UseraccessQuery groupById() Group by the ID column
 *
 * @method     UseraccessQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     UseraccessQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     UseraccessQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     Useraccess findOne(PropelPDO $con = null) Return the first Useraccess matching the query
 * @method     Useraccess findOneOrCreate(PropelPDO $con = null) Return the first Useraccess matching the query, or a new Useraccess object populated from the query conditions when no match is found
 *
 * @method     Useraccess findOneByUserid(int $UserID) Return the first Useraccess filtered by the UserID column
 * @method     Useraccess findOneByMenuid(int $MenuID) Return the first Useraccess filtered by the MenuID column
 * @method     Useraccess findOneByClubid(int $ClubID) Return the first Useraccess filtered by the ClubID column
 * @method     Useraccess findOneByStudioid(int $StudioID) Return the first Useraccess filtered by the StudioID column
 * @method     Useraccess findOneById(int $ID) Return the first Useraccess filtered by the ID column
 *
 * @method     array findByUserid(int $UserID) Return Useraccess objects filtered by the UserID column
 * @method     array findByMenuid(int $MenuID) Return Useraccess objects filtered by the MenuID column
 * @method     array findByClubid(int $ClubID) Return Useraccess objects filtered by the ClubID column
 * @method     array findByStudioid(int $StudioID) Return Useraccess objects filtered by the StudioID column
 * @method     array findById(int $ID) Return Useraccess objects filtered by the ID column
 *
 * @package    propel.generator.site.om
 */
abstract class BaseUseraccessQuery extends ModelCriteria
{
    
    /**
     * Initializes internal state of BaseUseraccessQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'site', $modelName = 'Useraccess', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new UseraccessQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     UseraccessQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return UseraccessQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof UseraccessQuery) {
            return $criteria;
        }
        $query = new UseraccessQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Useraccess|Useraccess[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = UseraccessPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(UseraccessPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   Useraccess A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `USERID`, `MENUID`, `CLUBID`, `STUDIOID`, `ID` FROM `UserAccess` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Useraccess();
            $obj->hydrate($row);
            UseraccessPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Useraccess|Useraccess[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Useraccess[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return UseraccessQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UseraccessPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return UseraccessQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UseraccessPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the UserID column
     *
     * Example usage:
     * <code>
     * $query->filterByUserid(1234); // WHERE UserID = 1234
     * $query->filterByUserid(array(12, 34)); // WHERE UserID IN (12, 34)
     * $query->filterByUserid(array('min' => 12)); // WHERE UserID > 12
     * </code>
     *
     * @param     mixed $userid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UseraccessQuery The current query, for fluid interface
     */
    public function filterByUserid($userid = null, $comparison = null)
    {
        if (is_array($userid)) {
            $useMinMax = false;
            if (isset($userid['min'])) {
                $this->addUsingAlias(UseraccessPeer::USERID, $userid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userid['max'])) {
                $this->addUsingAlias(UseraccessPeer::USERID, $userid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UseraccessPeer::USERID, $userid, $comparison);
    }

    /**
     * Filter the query on the MenuID column
     *
     * Example usage:
     * <code>
     * $query->filterByMenuid(1234); // WHERE MenuID = 1234
     * $query->filterByMenuid(array(12, 34)); // WHERE MenuID IN (12, 34)
     * $query->filterByMenuid(array('min' => 12)); // WHERE MenuID > 12
     * </code>
     *
     * @param     mixed $menuid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UseraccessQuery The current query, for fluid interface
     */
    public function filterByMenuid($menuid = null, $comparison = null)
    {
        if (is_array($menuid)) {
            $useMinMax = false;
            if (isset($menuid['min'])) {
                $this->addUsingAlias(UseraccessPeer::MENUID, $menuid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($menuid['max'])) {
                $this->addUsingAlias(UseraccessPeer::MENUID, $menuid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UseraccessPeer::MENUID, $menuid, $comparison);
    }

    /**
     * Filter the query on the ClubID column
     *
     * Example usage:
     * <code>
     * $query->filterByClubid(1234); // WHERE ClubID = 1234
     * $query->filterByClubid(array(12, 34)); // WHERE ClubID IN (12, 34)
     * $query->filterByClubid(array('min' => 12)); // WHERE ClubID > 12
     * </code>
     *
     * @param     mixed $clubid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UseraccessQuery The current query, for fluid interface
     */
    public function filterByClubid($clubid = null, $comparison = null)
    {
        if (is_array($clubid)) {
            $useMinMax = false;
            if (isset($clubid['min'])) {
                $this->addUsingAlias(UseraccessPeer::CLUBID, $clubid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clubid['max'])) {
                $this->addUsingAlias(UseraccessPeer::CLUBID, $clubid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UseraccessPeer::CLUBID, $clubid, $comparison);
    }

    /**
     * Filter the query on the StudioID column
     *
     * Example usage:
     * <code>
     * $query->filterByStudioid(1234); // WHERE StudioID = 1234
     * $query->filterByStudioid(array(12, 34)); // WHERE StudioID IN (12, 34)
     * $query->filterByStudioid(array('min' => 12)); // WHERE StudioID > 12
     * </code>
     *
     * @param     mixed $studioid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UseraccessQuery The current query, for fluid interface
     */
    public function filterByStudioid($studioid = null, $comparison = null)
    {
        if (is_array($studioid)) {
            $useMinMax = false;
            if (isset($studioid['min'])) {
                $this->addUsingAlias(UseraccessPeer::STUDIOID, $studioid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($studioid['max'])) {
                $this->addUsingAlias(UseraccessPeer::STUDIOID, $studioid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UseraccessPeer::STUDIOID, $studioid, $comparison);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UseraccessQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(UseraccessPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Useraccess $useraccess Object to remove from the list of results
     *
     * @return UseraccessQuery The current query, for fluid interface
     */
    public function prune($useraccess = null)
    {
        if ($useraccess) {
            $this->addUsingAlias(UseraccessPeer::ID, $useraccess->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

} // BaseUseraccessQuery