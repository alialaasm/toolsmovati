<?php


/**
 * Base class that represents a query for the 'ActionButton' table.
 *
 * 
 *
 * @method     ActionbuttonQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method     ActionbuttonQuery orderByColour($order = Criteria::ASC) Order by the Colour column
 * @method     ActionbuttonQuery orderBySubtitle($order = Criteria::ASC) Order by the SubTitle column
 * @method     ActionbuttonQuery orderByTitle($order = Criteria::ASC) Order by the Title column
 * @method     ActionbuttonQuery orderByDisplayorder($order = Criteria::ASC) Order by the DisplayOrder column
 * @method     ActionbuttonQuery orderByUrl($order = Criteria::ASC) Order by the URL column
 * @method     ActionbuttonQuery orderByActive($order = Criteria::ASC) Order by the Active column
 *
 * @method     ActionbuttonQuery groupById() Group by the ID column
 * @method     ActionbuttonQuery groupByColour() Group by the Colour column
 * @method     ActionbuttonQuery groupBySubtitle() Group by the SubTitle column
 * @method     ActionbuttonQuery groupByTitle() Group by the Title column
 * @method     ActionbuttonQuery groupByDisplayorder() Group by the DisplayOrder column
 * @method     ActionbuttonQuery groupByUrl() Group by the URL column
 * @method     ActionbuttonQuery groupByActive() Group by the Active column
 *
 * @method     ActionbuttonQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ActionbuttonQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ActionbuttonQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     Actionbutton findOne(PropelPDO $con = null) Return the first Actionbutton matching the query
 * @method     Actionbutton findOneOrCreate(PropelPDO $con = null) Return the first Actionbutton matching the query, or a new Actionbutton object populated from the query conditions when no match is found
 *
 * @method     Actionbutton findOneById(int $ID) Return the first Actionbutton filtered by the ID column
 * @method     Actionbutton findOneByColour(string $Colour) Return the first Actionbutton filtered by the Colour column
 * @method     Actionbutton findOneBySubtitle(string $SubTitle) Return the first Actionbutton filtered by the SubTitle column
 * @method     Actionbutton findOneByTitle(string $Title) Return the first Actionbutton filtered by the Title column
 * @method     Actionbutton findOneByDisplayorder(int $DisplayOrder) Return the first Actionbutton filtered by the DisplayOrder column
 * @method     Actionbutton findOneByUrl(string $URL) Return the first Actionbutton filtered by the URL column
 * @method     Actionbutton findOneByActive(boolean $Active) Return the first Actionbutton filtered by the Active column
 *
 * @method     array findById(int $ID) Return Actionbutton objects filtered by the ID column
 * @method     array findByColour(string $Colour) Return Actionbutton objects filtered by the Colour column
 * @method     array findBySubtitle(string $SubTitle) Return Actionbutton objects filtered by the SubTitle column
 * @method     array findByTitle(string $Title) Return Actionbutton objects filtered by the Title column
 * @method     array findByDisplayorder(int $DisplayOrder) Return Actionbutton objects filtered by the DisplayOrder column
 * @method     array findByUrl(string $URL) Return Actionbutton objects filtered by the URL column
 * @method     array findByActive(boolean $Active) Return Actionbutton objects filtered by the Active column
 *
 * @package    propel.generator.site.om
 */
abstract class BaseActionbuttonQuery extends ModelCriteria
{
    
    /**
     * Initializes internal state of BaseActionbuttonQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'site', $modelName = 'Actionbutton', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ActionbuttonQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     ActionbuttonQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ActionbuttonQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ActionbuttonQuery) {
            return $criteria;
        }
        $query = new ActionbuttonQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Actionbutton|Actionbutton[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ActionbuttonPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ActionbuttonPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   Actionbutton A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `COLOUR`, `SUBTITLE`, `TITLE`, `DISPLAYORDER`, `URL`, `ACTIVE` FROM `ActionButton` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Actionbutton();
            $obj->hydrate($row);
            ActionbuttonPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Actionbutton|Actionbutton[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Actionbutton[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ActionbuttonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ActionbuttonPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ActionbuttonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ActionbuttonPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ActionbuttonQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(ActionbuttonPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the Colour column
     *
     * Example usage:
     * <code>
     * $query->filterByColour('fooValue');   // WHERE Colour = 'fooValue'
     * $query->filterByColour('%fooValue%'); // WHERE Colour LIKE '%fooValue%'
     * </code>
     *
     * @param     string $colour The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ActionbuttonQuery The current query, for fluid interface
     */
    public function filterByColour($colour = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($colour)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $colour)) {
                $colour = str_replace('*', '%', $colour);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ActionbuttonPeer::COLOUR, $colour, $comparison);
    }

    /**
     * Filter the query on the SubTitle column
     *
     * Example usage:
     * <code>
     * $query->filterBySubtitle('fooValue');   // WHERE SubTitle = 'fooValue'
     * $query->filterBySubtitle('%fooValue%'); // WHERE SubTitle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $subtitle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ActionbuttonQuery The current query, for fluid interface
     */
    public function filterBySubtitle($subtitle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($subtitle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $subtitle)) {
                $subtitle = str_replace('*', '%', $subtitle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ActionbuttonPeer::SUBTITLE, $subtitle, $comparison);
    }

    /**
     * Filter the query on the Title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE Title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE Title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ActionbuttonQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ActionbuttonPeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the DisplayOrder column
     *
     * Example usage:
     * <code>
     * $query->filterByDisplayorder(1234); // WHERE DisplayOrder = 1234
     * $query->filterByDisplayorder(array(12, 34)); // WHERE DisplayOrder IN (12, 34)
     * $query->filterByDisplayorder(array('min' => 12)); // WHERE DisplayOrder > 12
     * </code>
     *
     * @param     mixed $displayorder The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ActionbuttonQuery The current query, for fluid interface
     */
    public function filterByDisplayorder($displayorder = null, $comparison = null)
    {
        if (is_array($displayorder)) {
            $useMinMax = false;
            if (isset($displayorder['min'])) {
                $this->addUsingAlias(ActionbuttonPeer::DISPLAYORDER, $displayorder['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($displayorder['max'])) {
                $this->addUsingAlias(ActionbuttonPeer::DISPLAYORDER, $displayorder['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ActionbuttonPeer::DISPLAYORDER, $displayorder, $comparison);
    }

    /**
     * Filter the query on the URL column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE URL = 'fooValue'
     * $query->filterByUrl('%fooValue%'); // WHERE URL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ActionbuttonQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $url)) {
                $url = str_replace('*', '%', $url);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ActionbuttonPeer::URL, $url, $comparison);
    }

    /**
     * Filter the query on the Active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(true); // WHERE Active = true
     * $query->filterByActive('yes'); // WHERE Active = true
     * </code>
     *
     * @param     boolean|string $active The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ActionbuttonQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (is_string($active)) {
            $Active = in_array(strtolower($active), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ActionbuttonPeer::ACTIVE, $active, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Actionbutton $actionbutton Object to remove from the list of results
     *
     * @return ActionbuttonQuery The current query, for fluid interface
     */
    public function prune($actionbutton = null)
    {
        if ($actionbutton) {
            $this->addUsingAlias(ActionbuttonPeer::ID, $actionbutton->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

} // BaseActionbuttonQuery