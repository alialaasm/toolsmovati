<?php


/**
 * Base class that represents a query for the 'Menu' table.
 *
 * 
 *
 * @method     MenuQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method     MenuQuery orderByLeftval($order = Criteria::ASC) Order by the LeftVal column
 * @method     MenuQuery orderByRightval($order = Criteria::ASC) Order by the RightVal column
 * @method     MenuQuery orderByTemplate($order = Criteria::ASC) Order by the Template column
 * @method     MenuQuery orderBySectiontype($order = Criteria::ASC) Order by the SectionType column
 * @method     MenuQuery orderBySectiontypeid($order = Criteria::ASC) Order by the SectionTypeID column
 * @method     MenuQuery orderByTitle($order = Criteria::ASC) Order by the Title column
 * @method     MenuQuery orderByEncodedtitle($order = Criteria::ASC) Order by the EncodedTitle column
 * @method     MenuQuery orderByShowinmenu($order = Criteria::ASC) Order by the ShowInMenu column
 * @method     MenuQuery orderByBannerid($order = Criteria::ASC) Order by the BannerID column
 * @method     MenuQuery orderByUrl($order = Criteria::ASC) Order by the URL column
 * @method     MenuQuery orderByTarget($order = Criteria::ASC) Order by the Target column
 * @method     MenuQuery orderByKeywords($order = Criteria::ASC) Order by the Keywords column
 * @method     MenuQuery orderByDescription($order = Criteria::ASC) Order by the Description column
 * @method     MenuQuery orderByActive($order = Criteria::ASC) Order by the Active column
 * @method     MenuQuery orderByRowmark($order = Criteria::ASC) Order by the RowMark column
 *
 * @method     MenuQuery groupById() Group by the ID column
 * @method     MenuQuery groupByLeftval() Group by the LeftVal column
 * @method     MenuQuery groupByRightval() Group by the RightVal column
 * @method     MenuQuery groupByTemplate() Group by the Template column
 * @method     MenuQuery groupBySectiontype() Group by the SectionType column
 * @method     MenuQuery groupBySectiontypeid() Group by the SectionTypeID column
 * @method     MenuQuery groupByTitle() Group by the Title column
 * @method     MenuQuery groupByEncodedtitle() Group by the EncodedTitle column
 * @method     MenuQuery groupByShowinmenu() Group by the ShowInMenu column
 * @method     MenuQuery groupByBannerid() Group by the BannerID column
 * @method     MenuQuery groupByUrl() Group by the URL column
 * @method     MenuQuery groupByTarget() Group by the Target column
 * @method     MenuQuery groupByKeywords() Group by the Keywords column
 * @method     MenuQuery groupByDescription() Group by the Description column
 * @method     MenuQuery groupByActive() Group by the Active column
 * @method     MenuQuery groupByRowmark() Group by the RowMark column
 *
 * @method     MenuQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     MenuQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     MenuQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     Menu findOne(PropelPDO $con = null) Return the first Menu matching the query
 * @method     Menu findOneOrCreate(PropelPDO $con = null) Return the first Menu matching the query, or a new Menu object populated from the query conditions when no match is found
 *
 * @method     Menu findOneById(int $ID) Return the first Menu filtered by the ID column
 * @method     Menu findOneByLeftval(int $LeftVal) Return the first Menu filtered by the LeftVal column
 * @method     Menu findOneByRightval(int $RightVal) Return the first Menu filtered by the RightVal column
 * @method     Menu findOneByTemplate(string $Template) Return the first Menu filtered by the Template column
 * @method     Menu findOneBySectiontype(string $SectionType) Return the first Menu filtered by the SectionType column
 * @method     Menu findOneBySectiontypeid(int $SectionTypeID) Return the first Menu filtered by the SectionTypeID column
 * @method     Menu findOneByTitle(string $Title) Return the first Menu filtered by the Title column
 * @method     Menu findOneByEncodedtitle(string $EncodedTitle) Return the first Menu filtered by the EncodedTitle column
 * @method     Menu findOneByShowinmenu(int $ShowInMenu) Return the first Menu filtered by the ShowInMenu column
 * @method     Menu findOneByBannerid(int $BannerID) Return the first Menu filtered by the BannerID column
 * @method     Menu findOneByUrl(string $URL) Return the first Menu filtered by the URL column
 * @method     Menu findOneByTarget(string $Target) Return the first Menu filtered by the Target column
 * @method     Menu findOneByKeywords(string $Keywords) Return the first Menu filtered by the Keywords column
 * @method     Menu findOneByDescription(string $Description) Return the first Menu filtered by the Description column
 * @method     Menu findOneByActive(int $Active) Return the first Menu filtered by the Active column
 * @method     Menu findOneByRowmark(int $RowMark) Return the first Menu filtered by the RowMark column
 *
 * @method     array findById(int $ID) Return Menu objects filtered by the ID column
 * @method     array findByLeftval(int $LeftVal) Return Menu objects filtered by the LeftVal column
 * @method     array findByRightval(int $RightVal) Return Menu objects filtered by the RightVal column
 * @method     array findByTemplate(string $Template) Return Menu objects filtered by the Template column
 * @method     array findBySectiontype(string $SectionType) Return Menu objects filtered by the SectionType column
 * @method     array findBySectiontypeid(int $SectionTypeID) Return Menu objects filtered by the SectionTypeID column
 * @method     array findByTitle(string $Title) Return Menu objects filtered by the Title column
 * @method     array findByEncodedtitle(string $EncodedTitle) Return Menu objects filtered by the EncodedTitle column
 * @method     array findByShowinmenu(int $ShowInMenu) Return Menu objects filtered by the ShowInMenu column
 * @method     array findByBannerid(int $BannerID) Return Menu objects filtered by the BannerID column
 * @method     array findByUrl(string $URL) Return Menu objects filtered by the URL column
 * @method     array findByTarget(string $Target) Return Menu objects filtered by the Target column
 * @method     array findByKeywords(string $Keywords) Return Menu objects filtered by the Keywords column
 * @method     array findByDescription(string $Description) Return Menu objects filtered by the Description column
 * @method     array findByActive(int $Active) Return Menu objects filtered by the Active column
 * @method     array findByRowmark(int $RowMark) Return Menu objects filtered by the RowMark column
 *
 * @package    propel.generator.site.om
 */
abstract class BaseMenuQuery extends ModelCriteria
{
    
    /**
     * Initializes internal state of BaseMenuQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'site', $modelName = 'Menu', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new MenuQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     MenuQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return MenuQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof MenuQuery) {
            return $criteria;
        }
        $query = new MenuQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Menu|Menu[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = MenuPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(MenuPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   Menu A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `LEFTVAL`, `RIGHTVAL`, `TEMPLATE`, `SECTIONTYPE`, `SECTIONTYPEID`, `TITLE`, `ENCODEDTITLE`, `SHOWINMENU`, `BANNERID`, `URL`, `TARGET`, `KEYWORDS`, `DESCRIPTION`, `ACTIVE`, `ROWMARK` FROM `Menu` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Menu();
            $obj->hydrate($row);
            MenuPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Menu|Menu[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Menu[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MenuPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MenuPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(MenuPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the LeftVal column
     *
     * Example usage:
     * <code>
     * $query->filterByLeftval(1234); // WHERE LeftVal = 1234
     * $query->filterByLeftval(array(12, 34)); // WHERE LeftVal IN (12, 34)
     * $query->filterByLeftval(array('min' => 12)); // WHERE LeftVal > 12
     * </code>
     *
     * @param     mixed $leftval The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterByLeftval($leftval = null, $comparison = null)
    {
        if (is_array($leftval)) {
            $useMinMax = false;
            if (isset($leftval['min'])) {
                $this->addUsingAlias(MenuPeer::LEFTVAL, $leftval['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($leftval['max'])) {
                $this->addUsingAlias(MenuPeer::LEFTVAL, $leftval['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuPeer::LEFTVAL, $leftval, $comparison);
    }

    /**
     * Filter the query on the RightVal column
     *
     * Example usage:
     * <code>
     * $query->filterByRightval(1234); // WHERE RightVal = 1234
     * $query->filterByRightval(array(12, 34)); // WHERE RightVal IN (12, 34)
     * $query->filterByRightval(array('min' => 12)); // WHERE RightVal > 12
     * </code>
     *
     * @param     mixed $rightval The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterByRightval($rightval = null, $comparison = null)
    {
        if (is_array($rightval)) {
            $useMinMax = false;
            if (isset($rightval['min'])) {
                $this->addUsingAlias(MenuPeer::RIGHTVAL, $rightval['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rightval['max'])) {
                $this->addUsingAlias(MenuPeer::RIGHTVAL, $rightval['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuPeer::RIGHTVAL, $rightval, $comparison);
    }

    /**
     * Filter the query on the Template column
     *
     * Example usage:
     * <code>
     * $query->filterByTemplate('fooValue');   // WHERE Template = 'fooValue'
     * $query->filterByTemplate('%fooValue%'); // WHERE Template LIKE '%fooValue%'
     * </code>
     *
     * @param     string $template The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterByTemplate($template = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($template)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $template)) {
                $template = str_replace('*', '%', $template);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MenuPeer::TEMPLATE, $template, $comparison);
    }

    /**
     * Filter the query on the SectionType column
     *
     * Example usage:
     * <code>
     * $query->filterBySectiontype('fooValue');   // WHERE SectionType = 'fooValue'
     * $query->filterBySectiontype('%fooValue%'); // WHERE SectionType LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sectiontype The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterBySectiontype($sectiontype = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sectiontype)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sectiontype)) {
                $sectiontype = str_replace('*', '%', $sectiontype);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MenuPeer::SECTIONTYPE, $sectiontype, $comparison);
    }

    /**
     * Filter the query on the SectionTypeID column
     *
     * Example usage:
     * <code>
     * $query->filterBySectiontypeid(1234); // WHERE SectionTypeID = 1234
     * $query->filterBySectiontypeid(array(12, 34)); // WHERE SectionTypeID IN (12, 34)
     * $query->filterBySectiontypeid(array('min' => 12)); // WHERE SectionTypeID > 12
     * </code>
     *
     * @param     mixed $sectiontypeid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterBySectiontypeid($sectiontypeid = null, $comparison = null)
    {
        if (is_array($sectiontypeid)) {
            $useMinMax = false;
            if (isset($sectiontypeid['min'])) {
                $this->addUsingAlias(MenuPeer::SECTIONTYPEID, $sectiontypeid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sectiontypeid['max'])) {
                $this->addUsingAlias(MenuPeer::SECTIONTYPEID, $sectiontypeid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuPeer::SECTIONTYPEID, $sectiontypeid, $comparison);
    }

    /**
     * Filter the query on the Title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE Title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE Title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MenuPeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the EncodedTitle column
     *
     * Example usage:
     * <code>
     * $query->filterByEncodedtitle('fooValue');   // WHERE EncodedTitle = 'fooValue'
     * $query->filterByEncodedtitle('%fooValue%'); // WHERE EncodedTitle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $encodedtitle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterByEncodedtitle($encodedtitle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($encodedtitle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $encodedtitle)) {
                $encodedtitle = str_replace('*', '%', $encodedtitle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MenuPeer::ENCODEDTITLE, $encodedtitle, $comparison);
    }

    /**
     * Filter the query on the ShowInMenu column
     *
     * Example usage:
     * <code>
     * $query->filterByShowinmenu(1234); // WHERE ShowInMenu = 1234
     * $query->filterByShowinmenu(array(12, 34)); // WHERE ShowInMenu IN (12, 34)
     * $query->filterByShowinmenu(array('min' => 12)); // WHERE ShowInMenu > 12
     * </code>
     *
     * @param     mixed $showinmenu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterByShowinmenu($showinmenu = null, $comparison = null)
    {
        if (is_array($showinmenu)) {
            $useMinMax = false;
            if (isset($showinmenu['min'])) {
                $this->addUsingAlias(MenuPeer::SHOWINMENU, $showinmenu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($showinmenu['max'])) {
                $this->addUsingAlias(MenuPeer::SHOWINMENU, $showinmenu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuPeer::SHOWINMENU, $showinmenu, $comparison);
    }

    /**
     * Filter the query on the BannerID column
     *
     * Example usage:
     * <code>
     * $query->filterByBannerid(1234); // WHERE BannerID = 1234
     * $query->filterByBannerid(array(12, 34)); // WHERE BannerID IN (12, 34)
     * $query->filterByBannerid(array('min' => 12)); // WHERE BannerID > 12
     * </code>
     *
     * @param     mixed $bannerid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterByBannerid($bannerid = null, $comparison = null)
    {
        if (is_array($bannerid)) {
            $useMinMax = false;
            if (isset($bannerid['min'])) {
                $this->addUsingAlias(MenuPeer::BANNERID, $bannerid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bannerid['max'])) {
                $this->addUsingAlias(MenuPeer::BANNERID, $bannerid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuPeer::BANNERID, $bannerid, $comparison);
    }

    /**
     * Filter the query on the URL column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE URL = 'fooValue'
     * $query->filterByUrl('%fooValue%'); // WHERE URL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $url)) {
                $url = str_replace('*', '%', $url);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MenuPeer::URL, $url, $comparison);
    }

    /**
     * Filter the query on the Target column
     *
     * Example usage:
     * <code>
     * $query->filterByTarget('fooValue');   // WHERE Target = 'fooValue'
     * $query->filterByTarget('%fooValue%'); // WHERE Target LIKE '%fooValue%'
     * </code>
     *
     * @param     string $target The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterByTarget($target = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($target)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $target)) {
                $target = str_replace('*', '%', $target);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MenuPeer::TARGET, $target, $comparison);
    }

    /**
     * Filter the query on the Keywords column
     *
     * Example usage:
     * <code>
     * $query->filterByKeywords('fooValue');   // WHERE Keywords = 'fooValue'
     * $query->filterByKeywords('%fooValue%'); // WHERE Keywords LIKE '%fooValue%'
     * </code>
     *
     * @param     string $keywords The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterByKeywords($keywords = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($keywords)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $keywords)) {
                $keywords = str_replace('*', '%', $keywords);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MenuPeer::KEYWORDS, $keywords, $comparison);
    }

    /**
     * Filter the query on the Description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE Description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE Description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MenuPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the Active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(1234); // WHERE Active = 1234
     * $query->filterByActive(array(12, 34)); // WHERE Active IN (12, 34)
     * $query->filterByActive(array('min' => 12)); // WHERE Active > 12
     * </code>
     *
     * @param     mixed $active The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (is_array($active)) {
            $useMinMax = false;
            if (isset($active['min'])) {
                $this->addUsingAlias(MenuPeer::ACTIVE, $active['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($active['max'])) {
                $this->addUsingAlias(MenuPeer::ACTIVE, $active['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuPeer::ACTIVE, $active, $comparison);
    }

    /**
     * Filter the query on the RowMark column
     *
     * Example usage:
     * <code>
     * $query->filterByRowmark(1234); // WHERE RowMark = 1234
     * $query->filterByRowmark(array(12, 34)); // WHERE RowMark IN (12, 34)
     * $query->filterByRowmark(array('min' => 12)); // WHERE RowMark > 12
     * </code>
     *
     * @param     mixed $rowmark The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function filterByRowmark($rowmark = null, $comparison = null)
    {
        if (is_array($rowmark)) {
            $useMinMax = false;
            if (isset($rowmark['min'])) {
                $this->addUsingAlias(MenuPeer::ROWMARK, $rowmark['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rowmark['max'])) {
                $this->addUsingAlias(MenuPeer::ROWMARK, $rowmark['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuPeer::ROWMARK, $rowmark, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Menu $menu Object to remove from the list of results
     *
     * @return MenuQuery The current query, for fluid interface
     */
    public function prune($menu = null)
    {
        if ($menu) {
            $this->addUsingAlias(MenuPeer::ID, $menu->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

} // BaseMenuQuery