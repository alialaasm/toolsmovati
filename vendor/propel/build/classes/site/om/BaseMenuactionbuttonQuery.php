<?php


/**
 * Base class that represents a query for the 'MenuActionButton' table.
 *
 * 
 *
 * @method     MenuactionbuttonQuery orderByMenuid($order = Criteria::ASC) Order by the MenuID column
 * @method     MenuactionbuttonQuery orderByActionbuttonid($order = Criteria::ASC) Order by the ActionButtonID column
 *
 * @method     MenuactionbuttonQuery groupByMenuid() Group by the MenuID column
 * @method     MenuactionbuttonQuery groupByActionbuttonid() Group by the ActionButtonID column
 *
 * @method     MenuactionbuttonQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     MenuactionbuttonQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     MenuactionbuttonQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     Menuactionbutton findOne(PropelPDO $con = null) Return the first Menuactionbutton matching the query
 * @method     Menuactionbutton findOneOrCreate(PropelPDO $con = null) Return the first Menuactionbutton matching the query, or a new Menuactionbutton object populated from the query conditions when no match is found
 *
 * @method     Menuactionbutton findOneByMenuid(int $MenuID) Return the first Menuactionbutton filtered by the MenuID column
 * @method     Menuactionbutton findOneByActionbuttonid(int $ActionButtonID) Return the first Menuactionbutton filtered by the ActionButtonID column
 *
 * @method     array findByMenuid(int $MenuID) Return Menuactionbutton objects filtered by the MenuID column
 * @method     array findByActionbuttonid(int $ActionButtonID) Return Menuactionbutton objects filtered by the ActionButtonID column
 *
 * @package    propel.generator.site.om
 */
abstract class BaseMenuactionbuttonQuery extends ModelCriteria
{
    
    /**
     * Initializes internal state of BaseMenuactionbuttonQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'site', $modelName = 'Menuactionbutton', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new MenuactionbuttonQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     MenuactionbuttonQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return MenuactionbuttonQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof MenuactionbuttonQuery) {
            return $criteria;
        }
        $query = new MenuactionbuttonQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query 
                         A Primary key composition: [$MenuID, $ActionButtonID]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Menuactionbutton|Menuactionbutton[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = MenuactionbuttonPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(MenuactionbuttonPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   Menuactionbutton A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `MENUID`, `ACTIONBUTTONID` FROM `MenuActionButton` WHERE `MENUID` = :p0 AND `ACTIONBUTTONID` = :p1';
        try {
            $stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
			$stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Menuactionbutton();
            $obj->hydrate($row);
            MenuactionbuttonPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Menuactionbutton|Menuactionbutton[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Menuactionbutton[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return MenuactionbuttonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(MenuactionbuttonPeer::MENUID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(MenuactionbuttonPeer::ACTIONBUTTONID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return MenuactionbuttonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(MenuactionbuttonPeer::MENUID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(MenuactionbuttonPeer::ACTIONBUTTONID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the MenuID column
     *
     * Example usage:
     * <code>
     * $query->filterByMenuid(1234); // WHERE MenuID = 1234
     * $query->filterByMenuid(array(12, 34)); // WHERE MenuID IN (12, 34)
     * $query->filterByMenuid(array('min' => 12)); // WHERE MenuID > 12
     * </code>
     *
     * @param     mixed $menuid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuactionbuttonQuery The current query, for fluid interface
     */
    public function filterByMenuid($menuid = null, $comparison = null)
    {
        if (is_array($menuid) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(MenuactionbuttonPeer::MENUID, $menuid, $comparison);
    }

    /**
     * Filter the query on the ActionButtonID column
     *
     * Example usage:
     * <code>
     * $query->filterByActionbuttonid(1234); // WHERE ActionButtonID = 1234
     * $query->filterByActionbuttonid(array(12, 34)); // WHERE ActionButtonID IN (12, 34)
     * $query->filterByActionbuttonid(array('min' => 12)); // WHERE ActionButtonID > 12
     * </code>
     *
     * @param     mixed $actionbuttonid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MenuactionbuttonQuery The current query, for fluid interface
     */
    public function filterByActionbuttonid($actionbuttonid = null, $comparison = null)
    {
        if (is_array($actionbuttonid) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(MenuactionbuttonPeer::ACTIONBUTTONID, $actionbuttonid, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Menuactionbutton $menuactionbutton Object to remove from the list of results
     *
     * @return MenuactionbuttonQuery The current query, for fluid interface
     */
    public function prune($menuactionbutton = null)
    {
        if ($menuactionbutton) {
            $this->addCond('pruneCond0', $this->getAliasedColName(MenuactionbuttonPeer::MENUID), $menuactionbutton->getMenuid(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(MenuactionbuttonPeer::ACTIONBUTTONID), $menuactionbutton->getActionbuttonid(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

} // BaseMenuactionbuttonQuery