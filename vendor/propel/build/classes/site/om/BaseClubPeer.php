<?php


/**
 * Base static class for performing query and update operations on the 'Club' table.
 *
 * 
 *
 * @package    propel.generator.site.om
 */
abstract class BaseClubPeer {

    /** the default database name for this class */
    const DATABASE_NAME = 'site';

    /** the table name for this class */
    const TABLE_NAME = 'Club';

    /** the related Propel class for this table */
    const OM_CLASS = 'Club';

    /** the related TableMap class for this table */
    const TM_CLASS = 'ClubTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 38;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 38;

    /** the column name for the ID field */
    const ID = 'Club.ID';

    /** the column name for the CLUBNAME field */
    const CLUBNAME = 'Club.CLUBNAME';

    /** the column name for the ADDRESS field */
    const ADDRESS = 'Club.ADDRESS';

    /** the column name for the CITY field */
    const CITY = 'Club.CITY';

    /** the column name for the PROVINCE field */
    const PROVINCE = 'Club.PROVINCE';

    /** the column name for the POSTALCODE field */
    const POSTALCODE = 'Club.POSTALCODE';

    /** the column name for the MAPITADDRESS field */
    const MAPITADDRESS = 'Club.MAPITADDRESS';

    /** the column name for the PHONE field */
    const PHONE = 'Club.PHONE';

    /** the column name for the TOLLFREE field */
    const TOLLFREE = 'Club.TOLLFREE';

    /** the column name for the CONTACTEMAIL field */
    const CONTACTEMAIL = 'Club.CONTACTEMAIL';

    /** the column name for the APPLICATIONEMAIL field */
    const APPLICATIONEMAIL = 'Club.APPLICATIONEMAIL';

    /** the column name for the PERSONALTRAININGEMAIL field */
    const PERSONALTRAININGEMAIL = 'Club.PERSONALTRAININGEMAIL';

    /** the column name for the PROMOTIONS field */
    const PROMOTIONS = 'Club.PROMOTIONS';

    /** the column name for the SCHEDULEURL field */
    const SCHEDULEURL = 'Club.SCHEDULEURL';

    /** the column name for the POOLSCHEDULEURL field */
    const POOLSCHEDULEURL = 'Club.POOLSCHEDULEURL';

    /** the column name for the USEINTERNAL field */
    const USEINTERNAL = 'Club.USEINTERNAL';

    /** the column name for the SUNDAYHOURS field */
    const SUNDAYHOURS = 'Club.SUNDAYHOURS';

    /** the column name for the MONDAYHOURS field */
    const MONDAYHOURS = 'Club.MONDAYHOURS';

    /** the column name for the TUESDAYHOURS field */
    const TUESDAYHOURS = 'Club.TUESDAYHOURS';

    /** the column name for the WEDNESDAYHOURS field */
    const WEDNESDAYHOURS = 'Club.WEDNESDAYHOURS';

    /** the column name for the THURSDAYHOURS field */
    const THURSDAYHOURS = 'Club.THURSDAYHOURS';

    /** the column name for the FRIDAYHOURS field */
    const FRIDAYHOURS = 'Club.FRIDAYHOURS';

    /** the column name for the SATURDAYHOURS field */
    const SATURDAYHOURS = 'Club.SATURDAYHOURS';

    /** the column name for the ACTIVE field */
    const ACTIVE = 'Club.ACTIVE';

    /** the column name for the FREETRIAL field */
    const FREETRIAL = 'Club.FREETRIAL';

    /** the column name for the SALESSUNDAYHOURS field */
    const SALESSUNDAYHOURS = 'Club.SALESSUNDAYHOURS';

    /** the column name for the SALESMONDAYHOURS field */
    const SALESMONDAYHOURS = 'Club.SALESMONDAYHOURS';

    /** the column name for the SALESTUESDAYHOURS field */
    const SALESTUESDAYHOURS = 'Club.SALESTUESDAYHOURS';

    /** the column name for the SALESWEDNESDAYHOURS field */
    const SALESWEDNESDAYHOURS = 'Club.SALESWEDNESDAYHOURS';

    /** the column name for the SALESTHURSDAYHOURS field */
    const SALESTHURSDAYHOURS = 'Club.SALESTHURSDAYHOURS';

    /** the column name for the SALESFRIDAYHOURS field */
    const SALESFRIDAYHOURS = 'Club.SALESFRIDAYHOURS';

    /** the column name for the SALESSATURDAYHOURS field */
    const SALESSATURDAYHOURS = 'Club.SALESSATURDAYHOURS';

    /** the column name for the SALESTRAILER field */
    const SALESTRAILER = 'Club.SALESTRAILER';

    /** the column name for the ONLINEPROMO field */
    const ONLINEPROMO = 'Club.ONLINEPROMO';

    /** the column name for the ONLINEPROMOTION field */
    const ONLINEPROMOTION = 'Club.ONLINEPROMOTION';

    /** the column name for the NEWSLETTERURL field */
    const NEWSLETTERURL = 'Club.NEWSLETTERURL';

    /** the column name for the FACEBOOK_URL field */
    const FACEBOOK_URL = 'Club.FACEBOOK_URL';

    /** the column name for the MOBILE_IMAGE field */
    const MOBILE_IMAGE = 'Club.MOBILE_IMAGE';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of Club objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array Club[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'Clubname', 'Address', 'City', 'Province', 'Postalcode', 'Mapitaddress', 'Phone', 'Tollfree', 'Contactemail', 'Applicationemail', 'Personaltrainingemail', 'Promotions', 'Scheduleurl', 'Poolscheduleurl', 'Useinternal', 'Sundayhours', 'Mondayhours', 'Tuesdayhours', 'Wednesdayhours', 'Thursdayhours', 'Fridayhours', 'Saturdayhours', 'Active', 'Freetrial', 'Salessundayhours', 'Salesmondayhours', 'Salestuesdayhours', 'Saleswednesdayhours', 'Salesthursdayhours', 'Salesfridayhours', 'Salessaturdayhours', 'Salestrailer', 'Onlinepromo', 'Onlinepromotion', 'Newsletterurl', 'FacebookUrl', 'MobileImage', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'clubname', 'address', 'city', 'province', 'postalcode', 'mapitaddress', 'phone', 'tollfree', 'contactemail', 'applicationemail', 'personaltrainingemail', 'promotions', 'scheduleurl', 'poolscheduleurl', 'useinternal', 'sundayhours', 'mondayhours', 'tuesdayhours', 'wednesdayhours', 'thursdayhours', 'fridayhours', 'saturdayhours', 'active', 'freetrial', 'salessundayhours', 'salesmondayhours', 'salestuesdayhours', 'saleswednesdayhours', 'salesthursdayhours', 'salesfridayhours', 'salessaturdayhours', 'salestrailer', 'onlinepromo', 'onlinepromotion', 'newsletterurl', 'facebookUrl', 'mobileImage', ),
        BasePeer::TYPE_COLNAME => array (self::ID, self::CLUBNAME, self::ADDRESS, self::CITY, self::PROVINCE, self::POSTALCODE, self::MAPITADDRESS, self::PHONE, self::TOLLFREE, self::CONTACTEMAIL, self::APPLICATIONEMAIL, self::PERSONALTRAININGEMAIL, self::PROMOTIONS, self::SCHEDULEURL, self::POOLSCHEDULEURL, self::USEINTERNAL, self::SUNDAYHOURS, self::MONDAYHOURS, self::TUESDAYHOURS, self::WEDNESDAYHOURS, self::THURSDAYHOURS, self::FRIDAYHOURS, self::SATURDAYHOURS, self::ACTIVE, self::FREETRIAL, self::SALESSUNDAYHOURS, self::SALESMONDAYHOURS, self::SALESTUESDAYHOURS, self::SALESWEDNESDAYHOURS, self::SALESTHURSDAYHOURS, self::SALESFRIDAYHOURS, self::SALESSATURDAYHOURS, self::SALESTRAILER, self::ONLINEPROMO, self::ONLINEPROMOTION, self::NEWSLETTERURL, self::FACEBOOK_URL, self::MOBILE_IMAGE, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'CLUBNAME', 'ADDRESS', 'CITY', 'PROVINCE', 'POSTALCODE', 'MAPITADDRESS', 'PHONE', 'TOLLFREE', 'CONTACTEMAIL', 'APPLICATIONEMAIL', 'PERSONALTRAININGEMAIL', 'PROMOTIONS', 'SCHEDULEURL', 'POOLSCHEDULEURL', 'USEINTERNAL', 'SUNDAYHOURS', 'MONDAYHOURS', 'TUESDAYHOURS', 'WEDNESDAYHOURS', 'THURSDAYHOURS', 'FRIDAYHOURS', 'SATURDAYHOURS', 'ACTIVE', 'FREETRIAL', 'SALESSUNDAYHOURS', 'SALESMONDAYHOURS', 'SALESTUESDAYHOURS', 'SALESWEDNESDAYHOURS', 'SALESTHURSDAYHOURS', 'SALESFRIDAYHOURS', 'SALESSATURDAYHOURS', 'SALESTRAILER', 'ONLINEPROMO', 'ONLINEPROMOTION', 'NEWSLETTERURL', 'FACEBOOK_URL', 'MOBILE_IMAGE', ),
        BasePeer::TYPE_FIELDNAME => array ('ID', 'ClubName', 'Address', 'City', 'Province', 'PostalCode', 'MapItAddress', 'Phone', 'TollFree', 'ContactEmail', 'ApplicationEmail', 'PersonalTrainingEmail', 'Promotions', 'ScheduleURL', 'PoolScheduleURL', 'UseInternal', 'SundayHours', 'MondayHours', 'TuesdayHours', 'WednesdayHours', 'ThursdayHours', 'FridayHours', 'SaturdayHours', 'Active', 'FreeTrial', 'SalesSundayHours', 'SalesMondayHours', 'SalesTuesdayHours', 'SalesWednesdayHours', 'SalesThursdayHours', 'SalesFridayHours', 'SalesSaturdayHours', 'SalesTrailer', 'OnlinePromo', 'OnlinePromotion', 'NewsletterURL', 'facebook_url', 'mobile_image', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Clubname' => 1, 'Address' => 2, 'City' => 3, 'Province' => 4, 'Postalcode' => 5, 'Mapitaddress' => 6, 'Phone' => 7, 'Tollfree' => 8, 'Contactemail' => 9, 'Applicationemail' => 10, 'Personaltrainingemail' => 11, 'Promotions' => 12, 'Scheduleurl' => 13, 'Poolscheduleurl' => 14, 'Useinternal' => 15, 'Sundayhours' => 16, 'Mondayhours' => 17, 'Tuesdayhours' => 18, 'Wednesdayhours' => 19, 'Thursdayhours' => 20, 'Fridayhours' => 21, 'Saturdayhours' => 22, 'Active' => 23, 'Freetrial' => 24, 'Salessundayhours' => 25, 'Salesmondayhours' => 26, 'Salestuesdayhours' => 27, 'Saleswednesdayhours' => 28, 'Salesthursdayhours' => 29, 'Salesfridayhours' => 30, 'Salessaturdayhours' => 31, 'Salestrailer' => 32, 'Onlinepromo' => 33, 'Onlinepromotion' => 34, 'Newsletterurl' => 35, 'FacebookUrl' => 36, 'MobileImage' => 37, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'clubname' => 1, 'address' => 2, 'city' => 3, 'province' => 4, 'postalcode' => 5, 'mapitaddress' => 6, 'phone' => 7, 'tollfree' => 8, 'contactemail' => 9, 'applicationemail' => 10, 'personaltrainingemail' => 11, 'promotions' => 12, 'scheduleurl' => 13, 'poolscheduleurl' => 14, 'useinternal' => 15, 'sundayhours' => 16, 'mondayhours' => 17, 'tuesdayhours' => 18, 'wednesdayhours' => 19, 'thursdayhours' => 20, 'fridayhours' => 21, 'saturdayhours' => 22, 'active' => 23, 'freetrial' => 24, 'salessundayhours' => 25, 'salesmondayhours' => 26, 'salestuesdayhours' => 27, 'saleswednesdayhours' => 28, 'salesthursdayhours' => 29, 'salesfridayhours' => 30, 'salessaturdayhours' => 31, 'salestrailer' => 32, 'onlinepromo' => 33, 'onlinepromotion' => 34, 'newsletterurl' => 35, 'facebookUrl' => 36, 'mobileImage' => 37, ),
        BasePeer::TYPE_COLNAME => array (self::ID => 0, self::CLUBNAME => 1, self::ADDRESS => 2, self::CITY => 3, self::PROVINCE => 4, self::POSTALCODE => 5, self::MAPITADDRESS => 6, self::PHONE => 7, self::TOLLFREE => 8, self::CONTACTEMAIL => 9, self::APPLICATIONEMAIL => 10, self::PERSONALTRAININGEMAIL => 11, self::PROMOTIONS => 12, self::SCHEDULEURL => 13, self::POOLSCHEDULEURL => 14, self::USEINTERNAL => 15, self::SUNDAYHOURS => 16, self::MONDAYHOURS => 17, self::TUESDAYHOURS => 18, self::WEDNESDAYHOURS => 19, self::THURSDAYHOURS => 20, self::FRIDAYHOURS => 21, self::SATURDAYHOURS => 22, self::ACTIVE => 23, self::FREETRIAL => 24, self::SALESSUNDAYHOURS => 25, self::SALESMONDAYHOURS => 26, self::SALESTUESDAYHOURS => 27, self::SALESWEDNESDAYHOURS => 28, self::SALESTHURSDAYHOURS => 29, self::SALESFRIDAYHOURS => 30, self::SALESSATURDAYHOURS => 31, self::SALESTRAILER => 32, self::ONLINEPROMO => 33, self::ONLINEPROMOTION => 34, self::NEWSLETTERURL => 35, self::FACEBOOK_URL => 36, self::MOBILE_IMAGE => 37, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'CLUBNAME' => 1, 'ADDRESS' => 2, 'CITY' => 3, 'PROVINCE' => 4, 'POSTALCODE' => 5, 'MAPITADDRESS' => 6, 'PHONE' => 7, 'TOLLFREE' => 8, 'CONTACTEMAIL' => 9, 'APPLICATIONEMAIL' => 10, 'PERSONALTRAININGEMAIL' => 11, 'PROMOTIONS' => 12, 'SCHEDULEURL' => 13, 'POOLSCHEDULEURL' => 14, 'USEINTERNAL' => 15, 'SUNDAYHOURS' => 16, 'MONDAYHOURS' => 17, 'TUESDAYHOURS' => 18, 'WEDNESDAYHOURS' => 19, 'THURSDAYHOURS' => 20, 'FRIDAYHOURS' => 21, 'SATURDAYHOURS' => 22, 'ACTIVE' => 23, 'FREETRIAL' => 24, 'SALESSUNDAYHOURS' => 25, 'SALESMONDAYHOURS' => 26, 'SALESTUESDAYHOURS' => 27, 'SALESWEDNESDAYHOURS' => 28, 'SALESTHURSDAYHOURS' => 29, 'SALESFRIDAYHOURS' => 30, 'SALESSATURDAYHOURS' => 31, 'SALESTRAILER' => 32, 'ONLINEPROMO' => 33, 'ONLINEPROMOTION' => 34, 'NEWSLETTERURL' => 35, 'FACEBOOK_URL' => 36, 'MOBILE_IMAGE' => 37, ),
        BasePeer::TYPE_FIELDNAME => array ('ID' => 0, 'ClubName' => 1, 'Address' => 2, 'City' => 3, 'Province' => 4, 'PostalCode' => 5, 'MapItAddress' => 6, 'Phone' => 7, 'TollFree' => 8, 'ContactEmail' => 9, 'ApplicationEmail' => 10, 'PersonalTrainingEmail' => 11, 'Promotions' => 12, 'ScheduleURL' => 13, 'PoolScheduleURL' => 14, 'UseInternal' => 15, 'SundayHours' => 16, 'MondayHours' => 17, 'TuesdayHours' => 18, 'WednesdayHours' => 19, 'ThursdayHours' => 20, 'FridayHours' => 21, 'SaturdayHours' => 22, 'Active' => 23, 'FreeTrial' => 24, 'SalesSundayHours' => 25, 'SalesMondayHours' => 26, 'SalesTuesdayHours' => 27, 'SalesWednesdayHours' => 28, 'SalesThursdayHours' => 29, 'SalesFridayHours' => 30, 'SalesSaturdayHours' => 31, 'SalesTrailer' => 32, 'OnlinePromo' => 33, 'OnlinePromotion' => 34, 'NewsletterURL' => 35, 'facebook_url' => 36, 'mobile_image' => 37, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = self::getFieldNames($toType);
        $key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, self::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return self::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. ClubPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(ClubPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ClubPeer::ID);
            $criteria->addSelectColumn(ClubPeer::CLUBNAME);
            $criteria->addSelectColumn(ClubPeer::ADDRESS);
            $criteria->addSelectColumn(ClubPeer::CITY);
            $criteria->addSelectColumn(ClubPeer::PROVINCE);
            $criteria->addSelectColumn(ClubPeer::POSTALCODE);
            $criteria->addSelectColumn(ClubPeer::MAPITADDRESS);
            $criteria->addSelectColumn(ClubPeer::PHONE);
            $criteria->addSelectColumn(ClubPeer::TOLLFREE);
            $criteria->addSelectColumn(ClubPeer::CONTACTEMAIL);
            $criteria->addSelectColumn(ClubPeer::APPLICATIONEMAIL);
            $criteria->addSelectColumn(ClubPeer::PERSONALTRAININGEMAIL);
            $criteria->addSelectColumn(ClubPeer::PROMOTIONS);
            $criteria->addSelectColumn(ClubPeer::SCHEDULEURL);
            $criteria->addSelectColumn(ClubPeer::POOLSCHEDULEURL);
            $criteria->addSelectColumn(ClubPeer::USEINTERNAL);
            $criteria->addSelectColumn(ClubPeer::SUNDAYHOURS);
            $criteria->addSelectColumn(ClubPeer::MONDAYHOURS);
            $criteria->addSelectColumn(ClubPeer::TUESDAYHOURS);
            $criteria->addSelectColumn(ClubPeer::WEDNESDAYHOURS);
            $criteria->addSelectColumn(ClubPeer::THURSDAYHOURS);
            $criteria->addSelectColumn(ClubPeer::FRIDAYHOURS);
            $criteria->addSelectColumn(ClubPeer::SATURDAYHOURS);
            $criteria->addSelectColumn(ClubPeer::ACTIVE);
            $criteria->addSelectColumn(ClubPeer::FREETRIAL);
            $criteria->addSelectColumn(ClubPeer::SALESSUNDAYHOURS);
            $criteria->addSelectColumn(ClubPeer::SALESMONDAYHOURS);
            $criteria->addSelectColumn(ClubPeer::SALESTUESDAYHOURS);
            $criteria->addSelectColumn(ClubPeer::SALESWEDNESDAYHOURS);
            $criteria->addSelectColumn(ClubPeer::SALESTHURSDAYHOURS);
            $criteria->addSelectColumn(ClubPeer::SALESFRIDAYHOURS);
            $criteria->addSelectColumn(ClubPeer::SALESSATURDAYHOURS);
            $criteria->addSelectColumn(ClubPeer::SALESTRAILER);
            $criteria->addSelectColumn(ClubPeer::ONLINEPROMO);
            $criteria->addSelectColumn(ClubPeer::ONLINEPROMOTION);
            $criteria->addSelectColumn(ClubPeer::NEWSLETTERURL);
            $criteria->addSelectColumn(ClubPeer::FACEBOOK_URL);
            $criteria->addSelectColumn(ClubPeer::MOBILE_IMAGE);
        } else {
            $criteria->addSelectColumn($alias . '.ID');
            $criteria->addSelectColumn($alias . '.CLUBNAME');
            $criteria->addSelectColumn($alias . '.ADDRESS');
            $criteria->addSelectColumn($alias . '.CITY');
            $criteria->addSelectColumn($alias . '.PROVINCE');
            $criteria->addSelectColumn($alias . '.POSTALCODE');
            $criteria->addSelectColumn($alias . '.MAPITADDRESS');
            $criteria->addSelectColumn($alias . '.PHONE');
            $criteria->addSelectColumn($alias . '.TOLLFREE');
            $criteria->addSelectColumn($alias . '.CONTACTEMAIL');
            $criteria->addSelectColumn($alias . '.APPLICATIONEMAIL');
            $criteria->addSelectColumn($alias . '.PERSONALTRAININGEMAIL');
            $criteria->addSelectColumn($alias . '.PROMOTIONS');
            $criteria->addSelectColumn($alias . '.SCHEDULEURL');
            $criteria->addSelectColumn($alias . '.POOLSCHEDULEURL');
            $criteria->addSelectColumn($alias . '.USEINTERNAL');
            $criteria->addSelectColumn($alias . '.SUNDAYHOURS');
            $criteria->addSelectColumn($alias . '.MONDAYHOURS');
            $criteria->addSelectColumn($alias . '.TUESDAYHOURS');
            $criteria->addSelectColumn($alias . '.WEDNESDAYHOURS');
            $criteria->addSelectColumn($alias . '.THURSDAYHOURS');
            $criteria->addSelectColumn($alias . '.FRIDAYHOURS');
            $criteria->addSelectColumn($alias . '.SATURDAYHOURS');
            $criteria->addSelectColumn($alias . '.ACTIVE');
            $criteria->addSelectColumn($alias . '.FREETRIAL');
            $criteria->addSelectColumn($alias . '.SALESSUNDAYHOURS');
            $criteria->addSelectColumn($alias . '.SALESMONDAYHOURS');
            $criteria->addSelectColumn($alias . '.SALESTUESDAYHOURS');
            $criteria->addSelectColumn($alias . '.SALESWEDNESDAYHOURS');
            $criteria->addSelectColumn($alias . '.SALESTHURSDAYHOURS');
            $criteria->addSelectColumn($alias . '.SALESFRIDAYHOURS');
            $criteria->addSelectColumn($alias . '.SALESSATURDAYHOURS');
            $criteria->addSelectColumn($alias . '.SALESTRAILER');
            $criteria->addSelectColumn($alias . '.ONLINEPROMO');
            $criteria->addSelectColumn($alias . '.ONLINEPROMOTION');
            $criteria->addSelectColumn($alias . '.NEWSLETTERURL');
            $criteria->addSelectColumn($alias . '.FACEBOOK_URL');
            $criteria->addSelectColumn($alias . '.MOBILE_IMAGE');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(ClubPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            ClubPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(self::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(ClubPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 Club
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = ClubPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return ClubPeer::populateObjects(ClubPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement durirectly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ClubPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            ClubPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(self::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      Club $obj A Club object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            self::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A Club object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof Club) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Club object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(self::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   Club Found object or NULL if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(self::$instances[$key])) {
                return self::$instances[$key];
            }
        }

        return null; // just to be explicit
    }
    
    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool()
    {
        self::$instances = array();
    }
    
    /**
     * Method to invalidate the instance pool of all tables related to Club
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or NULL if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }
    
    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = ClubPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = ClubPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = ClubPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ClubPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (Club object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = ClubPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = ClubPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + ClubPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ClubPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            ClubPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseClubPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseClubPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new ClubTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass()
    {
        return ClubPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a Club or Criteria object.
     *
     * @param      mixed $values Criteria or Club object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ClubPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from Club object
        }

        if ($criteria->containsKey(ClubPeer::ID) && $criteria->keyContainsValue(ClubPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ClubPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(self::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a Club or Criteria object.
     *
     * @param      mixed $values Criteria or Club object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ClubPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(self::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(ClubPeer::ID);
            $value = $criteria->remove(ClubPeer::ID);
            if ($value) {
                $selectCriteria->add(ClubPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(ClubPeer::TABLE_NAME);
            }

        } else { // $values is Club object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(self::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the Club table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ClubPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(ClubPeer::TABLE_NAME, $con, ClubPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ClubPeer::clearInstancePool();
            ClubPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a Club or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or Club object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(ClubPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            ClubPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof Club) { // it's a model object
            // invalidate the cache for this single object
            ClubPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(self::DATABASE_NAME);
            $criteria->add(ClubPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                ClubPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(self::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            
            $affectedRows += BasePeer::doDelete($criteria, $con);
            ClubPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given Club object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      Club $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(ClubPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(ClubPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(ClubPeer::DATABASE_NAME, ClubPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return Club
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = ClubPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(ClubPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(ClubPeer::DATABASE_NAME);
        $criteria->add(ClubPeer::ID, $pk);

        $v = ClubPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return Club[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ClubPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(ClubPeer::DATABASE_NAME);
            $criteria->add(ClubPeer::ID, $pks, Criteria::IN);
            $objs = ClubPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseClubPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseClubPeer::buildTableMap();

