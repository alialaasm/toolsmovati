<?php



/**
 * This class defines the structure of the 'Instructor' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.site.map
 */
class InstructorTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'site.map.InstructorTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Instructor');
        $this->setPhpName('Instructor');
        $this->setClassname('Instructor');
        $this->setPackage('site');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('EMAIL', 'Email', 'VARCHAR', true, 45, '');
        $this->addColumn('PASSWORD', 'Password', 'VARCHAR', true, 45, '');
        $this->addColumn('FIRSTNAME', 'Firstname', 'VARCHAR', true, 45, '');
        $this->addColumn('LASTNAME', 'Lastname', 'VARCHAR', true, 45, '');
        $this->addForeignKey('CLUBID', 'Clubid', 'INTEGER', 'Club', 'ID', false, 10, null);
        $this->addColumn('IS_ADMIN', 'IsAdmin', 'INTEGER', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Club', 'Club', RelationMap::MANY_TO_ONE, array('ClubId' => 'ID', ), null, null);
        $this->addRelation('Classresult', 'Classresult', RelationMap::ONE_TO_MANY, array('ID' => 'InstructorID', ), null, null, 'Classresults');
    } // buildRelations()

} // InstructorTableMap
