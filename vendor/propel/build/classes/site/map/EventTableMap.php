<?php



/**
 * This class defines the structure of the 'Event' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.site.map
 */
class EventTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'site.map.EventTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Event');
        $this->setPhpName('Event');
        $this->setClassname('Event');
        $this->setPackage('site');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, 12, null);
        $this->addColumn('CATEGORYID', 'Categoryid', 'INTEGER', false, 12, null);
        $this->addForeignKey('CLASSID', 'Classid', 'INTEGER', 'Class', 'ID', false, 10, null);
        $this->addColumn('CLUBID', 'Clubid', 'INTEGER', false, 12, null);
        $this->addColumn('STUDIOID', 'Studioid', 'INTEGER', false, 12, null);
        $this->addColumn('INSTRUCTOR', 'Instructor', 'VARCHAR', true, 255, '');
        $this->addColumn('STARTEVENTHOUR', 'Starteventhour', 'TINYINT', false, null, null);
        $this->addColumn('STARTEVENTMIN', 'Starteventmin', 'TINYINT', false, null, null);
        $this->addColumn('STARTEVENTAP', 'Starteventap', 'CHAR', false, 2, null);
        $this->addColumn('ENDEVENTHOUR', 'Endeventhour', 'TINYINT', false, null, null);
        $this->addColumn('ENDEVENTMIN', 'Endeventmin', 'TINYINT', false, null, null);
        $this->addColumn('ENDEVENTAP', 'Endeventap', 'CHAR', false, 2, null);
        $this->addColumn('DAY', 'Day', 'TINYINT', false, 2, null);
        $this->addColumn('MONTH', 'Month', 'TINYINT', false, 2, null);
        $this->addColumn('YEAR', 'Year', 'INTEGER', false, 4, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('_Class', '_Class', RelationMap::MANY_TO_ONE, array('ClassId' => 'ID', ), null, null);
    } // buildRelations()

} // EventTableMap
