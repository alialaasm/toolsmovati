<?php
/**
 * Skeleton subclass for performing query and update operations on the 'Class' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.site
 */
class _ClassQuery extends Base_ClassQuery {

	public function _ClassQuery()
	 {
        parent::__construct();
     }

	public function GetClassByClub($key,$con = null)
	{
		$sql=" select id, ClassName from Class where id in (select distinct ClassId from Event join Class on Class.id = Event.ClassId where ClubID = :p0 ) order by ClassName";
		try 
		{
			if ($con === null) {
				$con = Propel::getConnection(InstructorPeer::DATABASE_NAME, Propel::CONNECTION_READ);
			}
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} 
		catch (Exception $e)
		{
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
        $obj = null;
        $obj=$stmt->fetchAll();
        $stmt->closeCursor();
        return $obj;
	}

} // _ClassQuery

?>
