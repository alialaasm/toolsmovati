<?php
// This file generated by Propel 1.6.6-dev convert-conf target
// from XML runtime conf file /home/ian/projects/athleticclub/site/runtime-conf.xml
$conf = array (
  'datasources' => 
  array (
    'site' => 
    array (
      'adapter' => 'mysql',
      'connection' => 
      array (
        'classname' => 'DebugPDO',
        'dsn' => 'mysql:dbname=tac',
        'user' => 'root',
        'password' => 'simple',
        'options' => 
        array (
          'ATTR_PERSISTENT' => 
          array (
            'value' => false,
          ),
        ),
        'attributes' => 
        array (
          'ATTR_EMULATE_PREPARES' => 
          array (
            'value' => true,
          ),
        ),
        'settings' => 
        array (
          'charset' => 
          array (
            'value' => 'utf8',
          ),
        ),
      ),
    ),
    'default' => 'site',
  ),
  'generator_version' => '1.6.6-dev',
);
$conf['classmap'] = include(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'classmap-site-conf.php');
return $conf;