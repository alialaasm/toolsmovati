<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/DALC.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/Banner.php");

class BannerManager extends DALC
{
	public function SelectAll($OrderBy = "Title")
	{
		$this->SetQuery("SELECT * FROM Banner ORDER BY @OrderBy");

		$this->AddParameter("@OrderBy", $OrderBy, SQLTYPE_VARCHAR);

		return $this->LoadList($this->Execute());
	}


	public function SelectActive($Active = 1, $OrderBy = "Titlex")
	{
		$this->SetQuery("SELECT * FROM Banner WHERE Active = @Active ORDER BY @OrderBy");

		$this->AddParameter("@Active", $Active, SQLTYPE_BOOL);
		$this->AddParameter("@OrderBy", $OrderBy, SQLTYPE_VARCHAR);

		return $this->LoadList($this->Execute());
	}

	public function SelectByMenuNodeID($MenuNodeID)
	{
		$query = "	SELECT
						Banner.*
					FROM
						Banner
						INNER JOIN Menu ON Menu.BannerID = Banner.ID
					WHERE
						Menu.ID = @MenuNodeID
						AND Banner.Active = 1";

		$this->SetQuery($query);

		$this->AddParameter("@MenuNodeID", $MenuNodeID, SQLTYPE_INT);

		$result = $this->Execute();

		if ($row = $result->fetch_object())
		{
			$banner = new Banner();

			$banner->LoadValues($row);

			return $banner;
		}

		return false;
	}


	private function LoadList(MySqlResult $items)
	{
		$banners = new ItemList();


		while ($item = $items->fetch_object())
		{
			$banner = new Banner();
			$banner->LoadValues($item);

			$banners->AddItem($banner);
		}

		return $banners;
	}
}

?>
