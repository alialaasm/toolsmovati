<?php

require_once("DALC.php");

class Department extends DALC
{
	// Database Attributes
	public $ID = 0;
	public $Name = "";
	public $Email = "";
	public $Active  = true;

	// Class Attributes
	public $LoadError = false;
	public $IsNewRow = false;

	public function Department($ID = 0)
	{
		DALC::DALC();

		if ((!is_numeric($ID)) || $ID < 0) { $ID = 0; }

		if ($ID > 0)
		{
			$query = "SELECT * FROM Department WHERE ID = @ID";

			$this->SetQuery($query);

			$this->AddParameter("@ID", $ID, SQLTYPE_INT);

			$result = $this->Execute();

			$row = $result->fetch_object();

			$vals = ($row === false) ? NULL : $row;

			$this->LoadError = ($row === false);

			$this->LoadValues($vals);
		}
		else
		{
			$this->IsNewRow = true;
		}
	}


	/**
	 *
	 */
	public function Update()
	{
		if ($this->IsNewRow)
		{
			$query = "	INSERT INTO
							`Department`
							( Name, Email, Active )
						VALUES
							( @Name, @Email, @Active )";
		}
		else
		{
			$query = "	UPDATE
							`Department`
						SET
							Name = @Name, Email = @Email, Active = @Active
						WHERE
							ID = @ID";
		}

		$this->SetQuery($query);

		$this->AddParameter("@Name", $this->Name, SQLTYPE_VARCHAR);
		$this->AddParameter("@Email", $this->Email, SQLTYPE_VARCHAR);
		$this->AddParameter("@Active", $this->Active, SQLTYPE_BOOL);

		if ($this->IsNewRow)
		{
			$this->Execute();
			$this->ID = $this->InsertID();
			$this->IsNewRow = false;
		}
		else
		{
			$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
			$this->Execute();
		}
	}


	public function Delete()
	{
		$this->SetQuery("DELETE FROM Department WHERE ID = @ID");

		$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);

		$this->Execute();
	}


	public function LoadValues($vals)
	{
		if ($vals != NULL)
		{
			if (isset($vals->ID)) $this->ID = intval($vals->ID);

			if (isset($vals->Name)) $this->Name = $vals->Name;
			if (isset($vals->Email)) $this->Email = $vals->Email;
			if (isset($vals->Active)) $this->Active = (intval($vals->Active) > 0);
		}
		else
		{

		}
	}
}

?>