<?


include_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/MySql.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/ItemList.php");

if(class_exists('DALC') != true){

abstract class DALC
{

	public $db = null;
	public $command = null;
	public $transactionError;
	public $transactionInProgress = false;


	public function DALC()
	{
		global $db;
		$this->db = $db;
	}


	/**
	 * Creates a new command and sets the query.
	 *
	 * @param string - $query the parameterized query to be executed.
	 */
	protected function SetQuery($query)	{ $this->command = $this->db->create_command($query); }


	/**
	 * Sets the value of a parameter in the query.
	 *
	 * @param string $parameter - the name of the parameter starting with @.
	 * @param mixed $value - the value of the parameter.
	 * @param int $type - the sql parameter type.
	 **********************************************************************/
	protected function AddParameter($parameter, $value, $type)
	{
		if ($this->command == null)
		{
			trigger_error("Parameter added without setting query.");
			echo "<br />";
			exit();
		}

		$this->command->add_parameter($parameter, $value, $type);
	}


	/**
	 * Executes a query and returns the result.
	 *
	 * @param string $query - a query to be executed.  If query is null,
	 *                        the currently set query will be executed.
	 *
	 * @return resource - the result of the query.
	 **********************************************************************/
	protected function Execute($query = null)
	{
		if ($query == null)
		{
			if ($this->command == null) trigger_error("Parameter added without setting query.", E_ERROR);
			$result = $this->command->execute();
		}
		else
		{
			$result = $this->db->query($query);

			if ($this->IsError() && $this->transactionInProgress)
				$this->transactionError = true;

		}

		return $result;
	}


	/**
	 * Executes a query and returns the result.
	 *
	 * @param string $query - a query to be executed.  If query is null,
	 *                        the currently set query will be executed.
	 *
	 * @return resource - the result of the query.
	 */
	protected function ExecuteScalar($query = null)
	{
		if ($query == null)
		{
			if ($this->command == null) trigger_error("Parameter added without setting query.", E_ERROR);
			$result = $this->command->execute();

		}
		else
		{
			$result = $this->db->query($query);

			if ($this->IsError() && $this->transactionInProgress)
				$this->transactionError = true;
		}

		$row = $result->fetch_array();

		return (($row !== false) ? $row[0] : false);
	}


	/**
	 * Retrieves the ID from the last insert to a table that has an auto
	 * increment primary key.
	 *
	 * @return int - the ID of the last insert.
	 */
	protected function InsertID()
	{
		return $this->db->insert_id();
	}


	/**
	 * Allows a user to determine if the last query executed encountered
	 * an error.
	 *
	 * @return boolean - true iff the last transaction encountered an
	 *                   error.
	 */
	protected function IsError() { return ($this->db->errno() != 0); }


	/**
	 * Gets the error message from the last transaction.
	 *
	 * @return string - the error message from the last transaction.
	 */
	protected function Error() { return $this->db->error(); }



/*********************************************************************/
/***** TRANSACT-SQL **************************************************/
/*********************************************************************/
	protected function BeginTransaction()
	{
		$this->transactionError = false;
		$this->Execute("START TRANSACTION");

		$this->transactionInProgress = true;
	}

	protected function CommitTransaction() { $this->Execute("COMMIT"); }
	protected function RollBackTransaction() { $this->Execute("ROLLBACK"); }

	protected function EndTransaction()
	{
		if ($this->transactionError)
			$this->RollBackTransaction();
		else
			$this->CommitTransaction();

		$this->transactionInProgress = false;

		// return true if there were no errors.
		return (!($this->transactionError));
	}
/*********************************************************************/
/***** END: TRANSACT-SQL *********************************************/
/*********************************************************************/



	function BusinessObject() { }

	// A real number greater than or equal to 0.
	public function IsMoney($value)	{ return (is_numeric($value) && ($value >= 0)); }

	// 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ...
	public function IsNaturalNumber($value)
	{
		if (is_numeric($value))
		{
			$int_value = intval($value);

			return (($value == $int_value) && ($int_value >= 0));
		}
		else
		{
			return false;
		}
	}

	// 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ...
	public function IsRealNumber($value)
	{
		return is_numeric($value);
	}


	public function __get($name)
	{
        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;
    }


	public function __set($name, $value)
	{
        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __SET(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;
    }
}
}
