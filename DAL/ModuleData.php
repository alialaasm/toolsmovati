<?php

require_once("DALC.php");

class ModuleData extends DALC
{
	public $ModuleName = "";
	public $Var1 = "";
	public $Var2 = "";
	public $LoadError = false;


	public function ModuleData($ModuleName)
	{
		DALC::DALC();

		$query = "SELECT * FROM ModuleData WHERE ModuleName = @ModuleName";

		$this->SetQuery($query);

		$this->AddParameter("@ModuleName", $ModuleName, SQLTYPE_VARCHAR);

		$result = $this->Execute();

		$row = $result->fetch_object();

		$vals = ($row === false) ? NULL : $row;

		$this->LoadError = ($row === false);

		$this->LoadValues($vals);
	}


	public function Update()
	{
		$query = "UPDATE ModuleData SET Var1 = @Var1, Var2 = @Var2 WHERE `ModuleName` = @ModuleName";

		$this->SetQuery($query);

		$this->AddParameter("@ModuleName", $this->ModuleName, SQLTYPE_VARCHAR);
		$this->AddParameter("@Var1", $this->Var1, SQLTYPE_TEXT);
		$this->AddParameter("@Var2", $this->Var2, SQLTYPE_TEXT);

		$this->Execute();

	}


	public function LoadValues($vals)
	{
		if ($vals != NULL)
		{
			if (isset($vals->ModuleName)) $this->ModuleName = $vals->ModuleName;
			if (isset($vals->Var1)) $this->Var1 = $vals->Var1;
			if (isset($vals->Var2)) $this->Var2 = $vals->Var2;
		}
		else
		{
		}
	}
}

?>