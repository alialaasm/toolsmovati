<?php

require_once("DALC.php");

class Career extends DALC
{

	public $ID = 0;
	public $Position = "";
	public $Description = "";
	public $Active  = true;

	// Club Attribures
	public $ClubName = "";
	public $ApplicationEmail = "";

	public $LoadError = false;
	public $IsNewRow = false;

	public function Career($ID = 0)
	{
		DALC::DALC();

		if ((!is_numeric($ID)) || $ID < 0) { $ID = 0; }

		if ($ID > 0)
		{
			$query = "SELECT * FROM Career WHERE ID = @ID";

			$this->SetQuery($query);

			$this->AddParameter("@ID", $ID, SQLTYPE_INT);

			$result = $this->Execute();

			$row = $result->fetch_object();

			$vals = ($row === false) ? NULL : $row;

			$this->LoadError = ($row === false);

			$this->LoadValues($vals);
		}
		else
		{
			$this->IsNewRow = true;
		}
	}


	public function UpdateClubAvailability($ClubIDs)
	{
		if (is_array($ClubIDs))
		{
			$this->BeginTransaction();

			$query = "DELETE FROM ClubCareer WHERE CareerID = @CareerID";
			$this->SetQuery($query);
			$this->AddParameter("@CareerID", $this->ID, SQLTYPE_INT);
			$this->Execute();

			$numClubIDs = count($ClubIDs);

			if ($numClubIDs > 0)
			{
				$query = "INSERT INTO ClubCareer (ClubID, CareerID) VALUES ";

				for ($i = 0; $i < $numClubIDs; $i++)
				{
					if ($i > 0) { $query .= ", "; }
					$query .= "(" . $ClubIDs[$i] . ", $this->ID)";
				}
				$this->SetQuery($query);

				$this->Execute();
			}

			$this->EndTransaction();
		}
	}


	public function Update()
	{
		if ($this->IsNewRow)
		{
			$query = "	INSERT INTO
							`Career`
							( Position, Description, Active )
						VALUES
							( @Position, @Description, @Active )";
		}
		else
		{
			$query = "	UPDATE
							`Career`
						SET
							Position = @Position,
							Description = @Description,
							Active = @Active
						WHERE
							ID = @ID";
		}

		$this->SetQuery($query);

		$this->AddParameter("@Position", $this->Position, SQLTYPE_VARCHAR);
		$this->AddParameter("@Description", $this->Description, SQLTYPE_TEXT);
		$this->AddParameter("@Active", $this->Active, SQLTYPE_BOOL);

		if ($this->IsNewRow)
		{
			$this->Execute();
			$this->ID = $this->InsertID();
			$this->IsNewRow = false;
		}
		else
		{
			$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
			$this->Execute();
		}
	}


	public function Delete()
	{
		$this->BeginTransaction();

		// Delete relationships to the Club table.
		$this->SetQuery("DELETE FROM ClubCareer WHERE CareerID = @CareerID");
		$this->AddParameter("@CareerID", $this->ID, SQLTYPE_INT);
		$this->Execute();

		// Delete relationships to the Club table.
		$this->SetQuery("DELETE FROM Career WHERE ID = @ID");
		$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
		$this->Execute();

		return $this->EndTransaction();
	}


	public function LoadValues($vals)
	{
		if ($vals != NULL)
		{
			if (isset($vals->ID)) $this->ID = intval($vals->ID);

			if (isset($vals->Position)) $this->Position = $vals->Position;
			if (isset($vals->Description)) $this->Description = $vals->Description;
			if (isset($vals->Active)) $this->Active = (intval($vals->Active) > 0);

			// Club Attributes
			if (isset($vals->ClubName)) $this->ClubName = $vals->ClubName;
			if (isset($vals->ApplicationEmail)) $this->ApplicationEmail = $vals->ApplicationEmail;
		}
		else
		{

		}
	}
}

?>