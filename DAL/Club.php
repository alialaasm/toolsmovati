<?php

require_once("DALC.php");

class Club extends DALC
{

	public $ID = 0;
	public $ClubName = "";
	public $Address = "";
	public $City = "";
	public $Province  = "";
	public $PostalCode  = "";
	public $MapItAddress = "";
	public $Phone  = "";
	public $TollFree  = "";
	public $ContactEmail  = "";
	public $ApplicationEmail  = "";
	public $PersonalTrainingEmail = "";
	public $NewsletterURL = "";
	public $ScheduleURL = "";
	public $PoolScheduleURL = "";
	public $UseInternal  = 0;
	public $Promotions  = "";
	public $OnlinePromotion  = "";
	public $HolidayHours = "";
	public $NoticeTitle = "";
	public $NoticeBody = "";
	public $PersonalTraining = "";

	public $SundayHours  = "";
	public $MondayHours  = "";
	public $TuesdayHours  = "";
	public $WednesdayHours  = "";
	public $ThursdayHours  = "";
	public $FridayHours  = "";
	public $SaturdayHours  = "";

	public $SalesSundayHours  = "";
	public $SalesMondayHours  = "";
	public $SalesTuesdayHours  = "";
	public $SalesWednesdayHours  = "";
	public $SalesThursdayHours  = "";
	public $SalesFridayHours  = "";
	public $SalesSaturdayHours  = "";
	public $SalesTrailer  = "";

	public $Active = true;
	public $FreeTrial = true;
	public $OnlinePromo = true;

	// Optional Attributes
	public $PositionIsAvailable;

	public $LoadError = false;
	public $IsNewRow = false;



	public function Club($ID = 0)
	{
		DALC::DALC();

		if ($ID < 0 || (!is_numeric($ID))) { $ID = 0; }

		if ($ID > 0)
		{
			$query = "SELECT * FROM Club WHERE ID = @ID";

			$this->SetQuery($query);

			$this->AddParameter("@ID", $ID, SQLTYPE_INT);

			$result = $this->Execute();

			$row = $result->fetch_object();

			$vals = ($row === false) ? NULL : $row;

			$this->LoadError = ($row === false);

			$this->LoadValues($vals);
		}
		else
		{
			$this->IsNewRow = true;
		}
	}


	public function Update()
	{
		if ($this->IsNewRow)
		{
			$query = "	INSERT INTO
							`Club`
							(
								ClubName, Address, City, Province, PostalCode, MapItAddress, FreeTrial, OnlinePromo,
								TollFree, Phone,
								ContactEmail, ApplicationEmail, PersonalTrainingEmail,UseInternal,
								Promotions, OnlinePromotion, NewsletterURL, ScheduleURL, PoolScheduleURL,
								SundayHours, MondayHours, TuesdayHours, WednesdayHours,
								ThursdayHours, FridayHours, SaturdayHours, SalesSundayHours, SalesMondayHours, SalesTuesdayHours, SalesWednesdayHours,
								SalesThursdayHours, SalesFridayHours, SalesSaturdayHours, SalesTrailer, HolidayHours, NoticeBody, NoticeTitle, PersonalTraining
							)
						VALUES
							(
								@ClubName, @Address, @City, @Province, @PostalCode, @MapItAddress, @FreeTrial, @OnlinePromo,
								@TollFree, @Phone,
								@ContactEmail, @ApplicationEmail, @PersonalTrainingEmail,@UseInternal,
								@Promotions, @OnlinePromotion, @NewsletterURL, @ScheduleURL, @PoolScheduleURL,
								@SundayHours, @MondayHours, @TuesdayHours, @WednesdayHours,
								@ThursdayHours, @FridayHours, @SaturdayHours,@SalesSundayHours, @SalesMondayHours, @SalesTuesdayHours, @SalesWednesdayHours,
								@SalesThursdayHours, @SalesFridayHours, @SalesSaturdayHours, @SalesTrailer, @HolidayHours, @NoticeBody, @NoticeTitle, @PersonalTraining
							)";

		}
		else
		{
			$query = "	UPDATE
							`Club`
						SET
							ClubName = @ClubName,
							Address = @Address, City = @City, Province = @Province, PostalCode = @PostalCode,
							MapItAddress = @MapItAddress,
							FreeTrial = @FreeTrial,
							OnlinePromo = @OnlinePromo,
							Phone = @Phone, TollFree = @TollFree, ContactEmail = @ContactEmail,
							ApplicationEmail = @ApplicationEmail, PersonalTrainingEmail = @PersonalTrainingEmail, 
							Promotions = @Promotions, OnlinePromotion = @OnlinePromotion, NewsletterURL = @NewsletterURL, ScheduleURL = @ScheduleURL, PoolScheduleURL = @PoolScheduleURL,
							UseInternal = @UseInternal,
							SundayHours = @SundayHours, MondayHours = @MondayHours,
							TuesdayHours = @TuesdayHours, WednesdayHours = @WednesdayHours,
							ThursdayHours = @ThursdayHours, FridayHours = @FridayHours,
							SaturdayHours = @SaturdayHours,
							SalesSundayHours = @SalesSundayHours, SalesMondayHours = @SalesMondayHours,
							SalesTuesdayHours = @SalesTuesdayHours, SalesWednesdayHours = @SalesWednesdayHours,
							SalesThursdayHours = @SalesThursdayHours, SalesFridayHours = @SalesFridayHours,
							SalesSaturdayHours = @SalesSaturdayHours,
							SalesTrailer = @SalesTrailer, HolidayHours = @HolidayHours, NoticeTitle = @NoticeTitle, NoticeBody = @NoticeBody, PersonalTraining = @PersonalTraining
						WHERE
							ID = @ID";
		}

		$this->SetQuery($query);

		$this->AddParameter("@ClubName", $this->ClubName, SQLTYPE_VARCHAR);
		$this->AddParameter("@Address", $this->Address, SQLTYPE_VARCHAR);
		$this->AddParameter("@City", $this->City, SQLTYPE_VARCHAR);
		$this->AddParameter("@Province", $this->Province, SQLTYPE_VARCHAR);
		$this->AddParameter("@PostalCode", $this->PostalCode, SQLTYPE_VARCHAR);
		$this->AddParameter("@MapItAddress", $this->MapItAddress, SQLTYPE_VARCHAR);

		$this->AddParameter("@Phone", $this->Phone, SQLTYPE_VARCHAR);
		$this->AddParameter("@TollFree", $this->TollFree, SQLTYPE_VARCHAR);
		$this->AddParameter("@ContactEmail", $this->ContactEmail, SQLTYPE_VARCHAR);
		$this->AddParameter("@ApplicationEmail", $this->ApplicationEmail, SQLTYPE_VARCHAR);
		$this->AddParameter("@PersonalTrainingEmail", $this->PersonalTrainingEmail, SQLTYPE_VARCHAR);
		$this->AddParameter("@Promotions", $this->Promotions, SQLTYPE_TEXT);
		$this->AddParameter("@OnlinePromotion", $this->OnlinePromotion, SQLTYPE_TEXT);
		$this->AddParameter("@NewsletterURL", $this->NewsletterURL, SQLTYPE_VARCHAR);
		$this->AddParameter("@ScheduleURL", $this->ScheduleURL, SQLTYPE_VARCHAR);
		$this->AddParameter("@PoolScheduleURL", $this->PoolScheduleURL, SQLTYPE_VARCHAR);
		$this->AddParameter("@UseInternal", $this->UseInternal, SQLTYPE_VARCHAR);

		$this->AddParameter("@SundayHours", $this->SundayHours, SQLTYPE_VARCHAR);
		$this->AddParameter("@MondayHours", $this->MondayHours, SQLTYPE_VARCHAR);
		$this->AddParameter("@TuesdayHours", $this->TuesdayHours, SQLTYPE_VARCHAR);
		$this->AddParameter("@WednesdayHours", $this->WednesdayHours, SQLTYPE_VARCHAR);
		$this->AddParameter("@ThursdayHours", $this->ThursdayHours, SQLTYPE_VARCHAR);
		$this->AddParameter("@FridayHours", $this->FridayHours, SQLTYPE_VARCHAR);
		$this->AddParameter("@SaturdayHours", $this->SaturdayHours, SQLTYPE_VARCHAR);

		$this->AddParameter("@SalesSundayHours", $this->SalesSundayHours, SQLTYPE_VARCHAR);
		$this->AddParameter("@SalesMondayHours", $this->SalesMondayHours, SQLTYPE_VARCHAR);
		$this->AddParameter("@SalesTuesdayHours", $this->SalesTuesdayHours, SQLTYPE_VARCHAR);
		$this->AddParameter("@SalesWednesdayHours", $this->SalesWednesdayHours, SQLTYPE_VARCHAR);
		$this->AddParameter("@SalesThursdayHours", $this->SalesThursdayHours, SQLTYPE_VARCHAR);
		$this->AddParameter("@SalesFridayHours", $this->SalesFridayHours, SQLTYPE_VARCHAR);
		$this->AddParameter("@SalesSaturdayHours", $this->SalesSaturdayHours, SQLTYPE_VARCHAR);
		$this->AddParameter("@SalesTrailer", $this->SalesTrailer, SQLTYPE_VARCHAR);
		$this->AddParameter("@Active", $this->Active, SQLTYPE_VARCHAR);
		$this->AddParameter("@FreeTrial", $this->FreeTrial, SQLTYPE_VARCHAR);
		$this->AddParameter("@OnlinePromo", $this->OnlinePromo, SQLTYPE_VARCHAR);

		$this->AddParameter("@HolidayHours", $this->HolidayHours, SQLTYPE_VARCHAR);
		$this->AddParameter("@NoticeTitle", $this->NoticeTitle, SQLTYPE_VARCHAR);
		$this->AddParameter("@NoticeBody", $this->NoticeBody, SQLTYPE_VARCHAR);
		
		$this->AddParameter("@PersonalTraining", $this->PersonalTraining, SQLTYPE_VARCHAR);
		

		if ($this->IsNewRow)
		{
			$this->Execute();
			$this->ID = $this->InsertID();
			$this->IsNewRow = false;
		}
		else
		{
			$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
			$this->Execute();
		}
	}



	/**
	 * Updates the list of users that can edit this club.
	 *
	 * @param array $UserIDs An array of user IDs.
	 *
	 * @return bool true iff there are no errors, else false;
	 */
	public function UpdateUserAccess($UserIDs)
	{
		if (is_array($UserIDs))
		{
			$this->BeginTransaction();

			$query = "DELETE FROM UserAccess WHERE ClubID = @ID";
			$this->SetQuery($query);
			$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
			$this->Execute();

			$numUserIDs = count($UserIDs);

			if ($numUserIDs > 0)
			{
				$query = "INSERT INTO UserAccess (UserID, ClubID) VALUES ";

				for ($i = 0; $i < $numUserIDs; $i++)
				{
					if ($i > 0) { $query .= ", "; }
					$query .= "(" . $UserIDs[$i] . ", $this->ID)";
				}
				$this->SetQuery($query);

				$this->Execute();
			}

			return $this->EndTransaction();
		}

		return true;
	}


	public function LoadValues($vals)
	{
		if ($vals != NULL)
		{
			if (isset($vals->ID)) $this->ID = intval($vals->ID);

			if (isset($vals->ClubName)) $this->ClubName = $vals->ClubName;
			if (isset($vals->Address)) $this->Address = $vals->Address;
			if (isset($vals->City)) $this->City = $vals->City;
			if (isset($vals->Province)) $this->Province = $vals->Province;
			if (isset($vals->PostalCode)) $this->PostalCode = $vals->PostalCode;
			if (isset($vals->MapItAddress)) $this->MapItAddress = $vals->MapItAddress;

			if (isset($vals->Phone)) $this->Phone = $vals->Phone;
			if (isset($vals->TollFree)) $this->TollFree = $vals->TollFree;
			if (isset($vals->ContactEmail)) $this->ContactEmail = $vals->ContactEmail;
			if (isset($vals->ApplicationEmail)) $this->ApplicationEmail = $vals->ApplicationEmail;
			if (isset($vals->PersonalTrainingEmail)) $this->PersonalTrainingEmail = $vals->PersonalTrainingEmail;

			if (isset($vals->Promotions)) $this->Promotions = $vals->Promotions;
			if (isset($vals->OnlinePromotion)) $this->OnlinePromotion = $vals->OnlinePromotion;
			if (isset($vals->HolidayHours)) $this->HolidayHours = $vals->HolidayHours;
			if (isset($vals->NoticeBody)) $this->NoticeBody = $vals->NoticeBody;
			if (isset($vals->PersonalTraining)) $this->PersonalTraining = $vals->PersonalTraining;


                        if (isset($vals->NoticeTitle)) $this->NoticeTitle = $vals->NoticeTitle;
			if (isset($vals->NewsletterURL)) $this->NewsletterURL = $vals->NewsletterURL;
			if (isset($vals->ScheduleURL)) $this->ScheduleURL = $vals->ScheduleURL;
			if (isset($vals->PoolScheduleURL)) $this->PoolScheduleURL = $vals->PoolScheduleURL;
			if (isset($vals->UseInternal)) $this->UseInternal = $vals->UseInternal;
			
			if (isset($vals->SundayHours)) $this->SundayHours = $vals->SundayHours;
			if (isset($vals->MondayHours)) $this->MondayHours = $vals->MondayHours;
			if (isset($vals->TuesdayHours)) $this->TuesdayHours = $vals->TuesdayHours;
			if (isset($vals->WednesdayHours)) $this->WednesdayHours = $vals->WednesdayHours;
			if (isset($vals->ThursdayHours)) $this->ThursdayHours = $vals->ThursdayHours;
			if (isset($vals->FridayHours)) $this->FridayHours = $vals->FridayHours;
			if (isset($vals->SaturdayHours)) $this->SaturdayHours = $vals->SaturdayHours;
			
			if (isset($vals->SalesSundayHours)) $this->SalesSundayHours = $vals->SalesSundayHours;
			if (isset($vals->SalesMondayHours)) $this->SalesMondayHours = $vals->SalesMondayHours;
			if (isset($vals->SalesTuesdayHours)) $this->SalesTuesdayHours = $vals->SalesTuesdayHours;
			if (isset($vals->SalesWednesdayHours)) $this->SalesWednesdayHours = $vals->SalesWednesdayHours;
			if (isset($vals->SalesThursdayHours)) $this->SalesThursdayHours = $vals->SalesThursdayHours;
			if (isset($vals->SalesFridayHours)) $this->SalesFridayHours = $vals->SalesFridayHours;
			if (isset($vals->SalesSaturdayHours)) $this->SalesSaturdayHours = $vals->SalesSaturdayHours;
			if (isset($vals->SalesTrailer)) $this->SalesTrailer = $vals->SalesSaturdayHours;

			if (isset($vals->SalesTrailer)) $this->SalesTrailer = $vals->SalesTrailer;
			if (isset($vals->FreeTrial)) $this->FreeTrial = $vals->FreeTrial;
			if (isset($vals->OnlinePromo)) $this->OnlinePromo = $vals->OnlinePromo;
			if (isset($vals->Active)) $this->Active = $vals->Active;

			// Optional Attribues
			if (isset($vals->PositionIsAvailable)) $this->PositionIsAvailable = (intval($vals->PositionIsAvailable) > 0);

		}
		else
		{
		}
	}
}

?>
