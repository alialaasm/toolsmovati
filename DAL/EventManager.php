<?php


require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/DALC.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/Event.php");


class EventManager extends DALC
{
	public function SelectAll()
	{
		$this->SetQuery("SELECT * FROM Event ORDER BY ClubId, Day");

		return $this->LoadList($this->Execute());
	}


	public function SelectByDayCurrentMonth($Day, $ClubId, $ClassId)
	{
		$this->SetQuery("SELECT *, Event.ID as EID FROM Event LEFT JOIN Class ON Event.classid=Class.id LEFT JOIN ClassCategory ON Class.catId=ClassCategory.ID WHERE Day = @Day AND ClubId = @ClubId AND StudioId = @ClassId AND Month = @Month ORDER BY ClubId, StudioId, Day, StartEventAP, StartEventHour");

		$this->AddParameter("@Day", $Day, SQLTYPE_INT);
		$this->AddParameter("@ClubId", $ClubId, SQLTYPE_INT);
		$this->AddParameter("@ClassId", $ClassId, SQLTYPE_INT);		
		$this->AddParameter("@Month", 5, SQLTYPE_INT);		
		
		return $this->LoadList($this->Execute());
	}

	public function SelectByDayAndMonth($Day, $Month, $ClubId, $StudioId)
	{
		$this->SetQuery("SELECT * FROM Event INNER JOIN Class ON Event.classid=Class.id WHERE Day = @Day AND Month = @Month AND ClubId = @ClubId AND StudioID = @StudioId ORDER BY ClubId, Day");

		$this->AddParameter("@Day", $Day, SQLTYPE_INT);
		$this->AddParameter("@Month", $Day, SQLTYPE_INT);
		$this->AddParameter("@ClubId", $ClubId, SQLTYPE_INT);
		$this->AddParameter("@StudioId", $ClubId, SQLTYPE_INT);
		
		return $this->LoadList($this->Execute());
	}	

	public function SelectStudioByLocationMonthDayAll($Day, $ClubId, $ClassId, $Month, $Year)
	{
		$this->SetQuery("SELECT *, Event.ID as EID FROM Event LEFT JOIN Class ON Event.classid=Class.id WHERE Day = @Day AND ClubId = @ClubId AND StudioId = @ClassId AND Month = @Month AND Year = @Year ORDER BY ClubId, StudioId, Day, StartEventAP, StartEventHour");

		$this->AddParameter("@Day", $Day, SQLTYPE_INT);
		$this->AddParameter("@ClubId", $ClubId, SQLTYPE_INT);
		$this->AddParameter("@ClassId", $ClassId, SQLTYPE_INT);		
		$this->AddParameter("@Month", $Month, SQLTYPE_INT);	
		$this->AddParameter("@Year", $Year, SQLTYPE_INT);			
		
		return $this->LoadList($this->Execute());
	}

	public function SelectStudioByLocationMonthDayCat($Day, $ClubId, $StudioId, $CatId, $Month, $Year)
	{
		$this->SetQuery("SELECT *, Event.ID as EID, ClassCategory.Colour as CCC FROM Event LEFT JOIN Class ON Event.classid=Class.id LEFT JOIN ClassCategory ON Class.catId=ClassCategory.ID WHERE Day = @Day AND ClubId = @ClubId AND Event.StudioId = @StudioId AND Class.catId = @CatId AND Month = @Month AND Year = @Year ORDER BY ClubId, StudioId, Day, StartEventAP, StartEventHour");

		$this->AddParameter("@Day", $Day, SQLTYPE_INT);
		$this->AddParameter("@ClubId", $ClubId, SQLTYPE_INT);
		$this->AddParameter("@StudioId", $StudioId, SQLTYPE_INT);
		$this->AddParameter("@CatId", $CatId, SQLTYPE_INT);
		$this->AddParameter("@Month", $Month, SQLTYPE_INT);	
		$this->AddParameter("@Year", $Year, SQLTYPE_INT);			
		
		return $this->LoadList($this->Execute());
	}
	
	public function SelectByMonthAndDay($Day, $Month, $ClubId)
	{
		$this->SetQuery("SELECT * FROM Event WHERE Day = @Day AND ClubId = @ClubId AND Month = @Month ORDER BY ClubId, Day");

		$results = $this->Execute();

		$categories = array();

		while ($row = $results->fetch_object()) { $clubs[] = $row->ClubId; }

		return $clubs;
	}

	private function LoadList(MySqlResult $items)
	{
		$event = new ItemList();

		while ($item = $items->fetch_object())
		{
			$event_item = new Event();
			$event_item->LoadValues($item);

			$event->AddItem($event_item);
		}

		return $event;
	}
}

?>
