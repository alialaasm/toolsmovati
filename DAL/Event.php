<?php

require_once("DALC.php");


class Event extends DALC
{

	public $ID = 0;
	public $ClassId = 0;
	public $ClubId = 0;
	public $CatId = 0;
	public $StudioId = 0;
	public $Instructor = "";
	public $StartEventHour = 0;
	public $StartEventMin = 0;
	public $StartEventAP = 0;
	public $EndEventHour = 0;
	public $EndEventMin = 0;
	public $EndEventAP = 0;
	public $Day = 0;
	public $Month = 0;
	public $Year = 0;	
	public $Category = "";
	public $ClassName = "";
	public $ClassCode = "";
	public $Colour = 1;
	public $EID = 0;	


	// Optional Attributes
	public $HasAccess = false;

	public $IsNewRow = false;
	public $LoadError = false;


	public function Event($ID = 0)
	{
		DALC::DALC();

		if ((!is_numeric($ID)) || $ID < 0) { $ID = 0; }

		if ($ID > 0)
		{
			$query = "SELECT * FROM `Event` WHERE ID = @ID";

			$this->SetQuery($query);

			$this->AddParameter("@ID", $ID, SQLTYPE_INT);

			$result = $this->Execute();

			$row = $result->fetch_object();

			$vals = ($row === false) ? NULL : $row;

			$this->LoadError = ($row === false);

			$this->LoadValues($vals);
		}
		else
		{
			$this->IsNewRow = true;
		}
	}


	public function Update()
	{
		 if ($this->IsNewRow)
		 {
			$query = "INSERT INTO `Event` (ClassId, ClubId, StudioId, Instructor, StartEventHour, StartEventMin, StartEventAP, EndEventHour, EndEventMin, EndEventAP, Day, Month, Year) VALUES (@ClassId, @ClubId, @StudioId, @Instructor, @StartEventHour, @StartEventMin, @StartEventAP, @EndEventHour, @EndEventMin, @EndEventAP, @Day, @Month, @Year)";
		 }
		 else
		 {
			$query = "UPDATE `Event` SET ClassId = @ClassId, ClubId = @ClubId, StudioId = @StudioId, Instructor = @Instructor, StartEventHour = @StartEventHour, StartEventMin = @StartEventMin,  StartEventAP = @StartEventAP, EndEventHour = @EndEventHour, EndEventMin = @EndEventMin, EndEventAP = @EndEventAP, Day = @Day, Month = @Month WHERE id = @ID";
		 }


		$this->SetQuery($query);

		$this->AddParameter("@ClassId", $this->ClassId, SQLTYPE_INT);
		$this->AddParameter("@ClubId", $this->ClubId, SQLTYPE_INT);
		$this->AddParameter("@StudioId", $this->StudioId, SQLTYPE_INT);
		$this->AddParameter("@Instructor", $this->Instructor, SQLTYPE_VARCHAR);		
		$this->AddParameter("@StartEventHour", $this->StartEventHour, SQLTYPE_INT);
		$this->AddParameter("@StartEventMin", $this->StartEventMin, SQLTYPE_INT);
		$this->AddParameter("@StartEventAP", $this->StartEventAP, SQLTYPE_VARCHAR);
		$this->AddParameter("@EndEventHour", $this->EndEventHour, SQLTYPE_INT);
		$this->AddParameter("@EndEventMin", $this->EndEventMin, SQLTYPE_INT);
		$this->AddParameter("@EndEventAP", $this->EndEventAP, SQLTYPE_VARCHAR);
		$this->AddParameter("@Month", $this->Month, SQLTYPE_INT);
		$this->AddParameter("@Day", $this->Day, SQLTYPE_INT);
		$this->AddParameter("@Year", $this->Year, SQLTYPE_INT);		
		
		if ($this->IsNewRow)
		{	  
			$this->Execute();
			$this->ID = $this->InsertID();
			$this->IsNewRow = false;
		}
		else
		{
			$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);	
			$this->Execute();
		}
	}



    public function Delete($ID)
	{
		$this->SetQuery("DELETE FROM `Event` WHERE id = $ID");
		
		$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);

		$this->Execute();
	}


	public function LoadValues($vals)
	{
		if ($vals != NULL)
		{
			if (isset($vals->id)) $this->ID = intval($vals->id);
			if (isset($vals->EID)) $this->EID = intval($vals->EID);			
			if (isset($vals->ClassId)) $this->ClassId = $vals->ClassId;
			if (isset($vals->ClubId)) $this->ClubId = $vals->ClubId;
			if (isset($vals->CatId)) $this->CatId = $vals->CatId;
			if (isset($vals->StudioId)) $this->StudioId = $vals->StudioId;
			if (isset($vals->Instructor)) $this->Instructor = $vals->Instructor;
			if (isset($vals->StartEventHour)) $this->StartEventHour = $vals->StartEventHour;			
			if (isset($vals->StartEventMin)) $this->StartEventMin = $vals->StartEventMin;
			if (isset($vals->StartEventAP)) $this->StartEventAP = $vals->StartEventAP;			
			if (isset($vals->EndEventHour)) $this->EndEventHour = $vals->EndEventHour;
			if (isset($vals->EndEventMin)) $this->EndEventMin = $vals->EndEventMin;			
			if (isset($vals->EndEventAP)) $this->EndEventAP = $vals->EndEventAP;
			if (isset($vals->Month)) $this->Month = $vals->Month;
			if (isset($vals->Day)) $this->Day = $vals->Day;
			if (isset($vals->Year)) $this->Year = $vals->Year;			
			if (isset($vals->Category)) $this->Category = $vals->Category;			
			if (isset($vals->ClassName)) $this->ClassName = $vals->ClassName;
			if (isset($vals->ClassCode)) $this->ClassCode = $vals->ClassCode;
			if (isset($vals->Colour)) $this->Colour = $vals->Colour;		

			// Optional Attributes
			if (isset($vals->HasAccess)) $this->HasAccess = (intval($vals->HasAccess) > 0);
		}
		else
		{
		}
	}
}

?>