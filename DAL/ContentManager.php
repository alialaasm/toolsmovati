<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/DALC.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/Content.php");

class ContentManager extends DALC
{
	public function SelectAll()
	{
		$this->SetQuery("SELECT * FROM Content ORDER BY Title");

		$results = $this->Execute();

		$contents = array();

		while ($row = $results->fetch_object())
		{
			$content = new Content();
			$content->LoadValues($row);

			$contents[] = $content;
		}

		return  $contents;
	}
}

?>
