<?php


require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/DALC.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/Class.php");


class ClassManager extends DALC
{

	public function SelectAll()
	{
		$this->SetQuery("SELECT * FROM Class ORDER BY CatId, `ClassName");

		return $this->LoadList($this->Execute());
	}


	public function SelectByClassCode($ClassCode)
	{
		$this->SetQuery("SELECT * FROM Class WHERE ClassCode = @ClassCode  AND Active = 1 ORDER BY CatId, `ClassName`");

		$this->AddParameter("@ClassCode", $ClassCode, SQLTYPE_VARCHAR);

		return $this->LoadList($this->Execute());
	}

	public function SelectByClassID($ID)
	{
		$this->SetQuery("SELECT * FROM Class WHERE ID = @ID  AND Active = 1 ORDER BY ID");

		$this->AddParameter("@ID", $ID, SQLTYPE_VARCHAR);

		return $this->LoadList($this->Execute());
	}


	public function SelectCategories()
	{
		$this->SetQuery("SELECT DISTINCT Category FROM Class ORDER BY CatId");

		$results = $this->Execute();

		$categories = array();

		while ($row = $results->fetch_object()) { $categories[] = $row->Category; }

		return $categories;
	}

	public function SelectClassCategories()
	{
		$this->SetQuery("SELECT * FROM ClassCategory ORDER BY CategoryName");

		$results = $this->Execute();

		$categories = array();

		while ($row = $results->fetch_object()) { 
					$i++;
					$categories[$i] = array(1=>$row->CategoryName,2=>$row->ID);							
		}		

		return $categories;
	}
	
	public function SelectClassName()
	{
		$this->SetQuery("SELECT ID, ClassName FROM Class WHERE Active = 1 ORDER BY ClassName");

		$results = $this->Execute();

		$categories = array();

		while ($row = $results->fetch_object()) { 
					$i++;
					$categories[$i] = array(1=>$row->ClassName,2=>$row->ID);			 
		}

		return $categories;
	}	


	public function SelectByCategory($Category)
	{
		$this->SetQuery("SELECT * FROM Class WHERE Category = @Category AND Active = 1 ORDER BY `ClassName`");

		$this->AddParameter("@Category", $Category, SQLTYPE_VARCHAR);

		return $this->LoadList($this->Execute());
	}

	public function LoadCats()
	{
		$this->SetQuery("SELECT CategoryName FROM ClassCategory ORDER BY ID");

		$results = $this->Execute();

		$classCats = array();

		while ($row = $results->fetch_object()) { $classCats[] = $row->CategoryName; }

		return $classCats;
	}

	private function LoadList(MySqlResult $items)
	{
		$class = new ItemList();

		while ($item = $items->fetch_object())
		{
			$class_item = new ClassDescription();
			$class_item->LoadValues($item);

			$class->AddItem($class_item);
		}

		return $class;
	}
}

?>
