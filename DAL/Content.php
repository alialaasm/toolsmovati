<?php

require_once("DALC.php");

class Content extends DALC
{
	public $ID = 0;
	public $Title = "";
	public $Description = "";
	public $IsNewRow = false;
	public $LoadError = false;


	public function Content($ID = 0)
	{
		DALC::DALC();

		if ($ID < 0 || (!is_numeric($ID))) { $ID = 0; }

		if ($ID > 0)
		{
			$query = "SELECT * FROM Content WHERE ID = @ID";

			$this->SetQuery($query);

			$this->AddParameter("@ID", $ID, SQLTYPE_INT);

			$result = $this->Execute();

			$row = $result->fetch_object();

			$vals = ($row === false) ? NULL : $row;

			$this->LoadError = ($row === false);

			$this->LoadValues($vals);
		}
		else
		{
			$this->IsNewRow = true;
		}
	}


	public function Update()
	{
		if ($this->IsNewRow)
		{
			$query = "INSERT INTO `Content` (Title, Description) VALUES (@Title, @Description)";
		}
		else
		{
			$query = "UPDATE `Content` SET Title = @Title, Description = @Description WHERE ID = @ID";
		}

		$this->SetQuery($query);

		$this->AddParameter("@Title", $this->Title, SQLTYPE_VARCHAR);
		$this->AddParameter("@Description", $this->Description, SQLTYPE_VARCHAR);

		if ($this->IsNewRow)
		{
			$this->Execute();
			$this->ID = $this->InsertID();
			$this->IsNewRow = false;
		}
		else
		{
			$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
			$this->Execute();
		}
	}


	public function LoadValues($vals)
	{
		if ($vals != NULL)
		{
			if (isset($vals->ID)) $this->ID = intval($vals->ID);
			if (isset($vals->Title)) $this->Title = $vals->Title;
			if (isset($vals->Description)) $this->Description = $vals->Description;
		}
		else
		{
		}
	}
}

?>