<?php

require_once("DALC.php");

class Press extends DALC
{

	public $ID = 0;
	public $ReleaseDate = "";
	public $Title = "";
	public $Description = "";
	public $Active  = true;
	public $LoadError = false;
	public $IsNewRow = false;

	public function Press($ID = 0)
	{
		DALC::DALC();

		if ((!is_numeric($ID)) || $ID < 0) { $ID = 0; }

		if ($ID > 0)
		{
			$query = "SELECT * FROM Press WHERE ID = @ID";

			$this->SetQuery($query);

			$this->AddParameter("@ID", $ID, SQLTYPE_INT);

			$result = $this->Execute();

			$row = $result->fetch_object();

			$vals = ($row === false) ? NULL : $row;

			$this->LoadError = ($row === false);

			$this->LoadValues($vals);
		}
		else
		{
			$this->IsNewRow = true;
		}
	}


	public function Update()
	{
		if ($this->IsNewRow)
		{
			$query = "	INSERT INTO
							`Press`
							( ReleaseDate, Title, Description, Active )
						VALUES
							( @ReleaseDate, @Title, @Description, @Active )";
		}
		else
		{
			$query = "	UPDATE
							`Press`
						SET
							ReleaseDate = @ReleaseDate,
							Title = @Title, Description = @Description,
							Active = @Active
						WHERE
							ID = @ID";
		}

		$this->SetQuery($query);

		$this->AddParameter("@ReleaseDate", $this->ReleaseDate, SQLTYPE_DATE);
		$this->AddParameter("@Title", $this->Title, SQLTYPE_VARCHAR);
		$this->AddParameter("@Description", $this->Description, SQLTYPE_TEXT);
		$this->AddParameter("@Active", $this->Active, SQLTYPE_BOOL);

		if ($this->IsNewRow)
		{
			$this->Execute();
			$this->ID = $this->InsertID();
			$this->IsNewRow = false;
		}
		else
		{
			$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
			$this->Execute();
		}
	}


	public function Delete()
	{
		$this->SetQuery("DELETE FROM Press WHERE ID = @ID");

		$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);

		$this->Execute();
	}


	public function LoadValues($vals)
	{
		if ($vals != NULL)
		{
			if (isset($vals->ID)) $this->ID = intval($vals->ID);

			if (isset($vals->ReleaseDate)) $this->ReleaseDate = date("Y-m-d", strtotime($vals->ReleaseDate));
			if (isset($vals->Title)) $this->Title = $vals->Title;
			if (isset($vals->Description)) $this->Description = $vals->Description;
			if (isset($vals->Active)) $this->Active = (intval($vals->Active) > 0);
		}
		else
		{
			$this->ReleaseDate = date("Y-m-d");
		}
	}
}

?>