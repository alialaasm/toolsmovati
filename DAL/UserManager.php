<?php


require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/DALC.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/User.php");


class UserManager extends DALC
{


	public function SelectAll()
	{
		$this->SetQuery("SELECT * FROM User ORDER BY `Name`");

		return $this->LoadList($this->Execute());
	}


	public function SelectActiveByUsername($Username)
	{
		$this->SetQuery("SELECT * FROM User WHERE Username = @Username AND Active = 1");

		$this->AddParameter("@Username", $Username, SQLTYPE_VARCHAR);

		$result = $this->Execute();

		$row = $result->fetch_object();

		if ($row === false)
		{
			$user = null;
		}
		else
		{
			$user = new User();
			$user->LoadValues($row);
		}

		return $user;
	}


	public function SelectWithPageAccess($MenuNodeID, $OrderBy = "`Name`")
	{
		$OrderBy = $this->db->escape_str($OrderBy);

		$query = "	SELECT
						User.*,
						IF (ISNULL(UserAccess.MenuID), 0, 1) AS HasAccess
					FROM
						User
						LEFT JOIN UserAccess ON User.ID = UserAccess.UserID AND UserAccess.MenuID = @MenuNodeID
					WHERE
						User.SuperUser = 0
					ORDER BY ". $OrderBy;

		$this->SetQuery($query);

		$this->AddParameter("@MenuNodeID", $MenuNodeID, SQLTYPE_INT);


		return $this->LoadList($this->Execute());
	}


	private function LoadList(MySqlResult $items)
	{
		$users = new ItemList();

		while ($item = $items->fetch_object())
		{
			$user = new User();
			$user->LoadValues($item);

			$users->AddItem($user);
		}

		return $users;
	}

}

?>
