<?php


require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/DALC.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/Club.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ModuleData.php");

class ClubManager extends DALC
{

	/**
	 * @param OrderBy The ORDER BY clause used to sort the list.
	 *
	 * @return ItemList The list of Clubs.
	 */
	public function SelectAll($OrderBy = "Province, City, ClubName")
	{
		$OrderBy = $this->db->escape_str($OrderBy);

		$this->SetQuery("SELECT * FROM Club ORDER BY ". $OrderBy);

		return $this->LoadList($this->Execute());
	}
	
	
	/**
	 * @
	 *
	 * 
	 */
				
		public function SelectAllwithAccess($OrderBy = "Province, City, ClubName, ClubID, UserID")
	{
		$OrderBy = $this->db->escape_str($OrderBy);

		$this->SetQuery("SELECT Club.ID, Club.Province, Club.City, Club.ClubName
						FROM Club,
						UserAccess
						WHERE Club.ID=UserAccess.ClubID AND ".$_SESSION["USER_ID"]."=UserAccess.UserID
						
						ORDER BY ". $OrderBy);

		

		return $this->LoadList($this->Execute());
	}


	/**
	 * @param $City The city to select clubs from.
	 *
	 * @return ItemList The list of Clubs.
	 */
	public function SelectByCity($City)
	{
		$this->SetQuery("SELECT * FROM Club WHERE City = @City ORDER BY ClubName");

		$this->AddParameter("@City", $City, SQLTYPE_VARCHAR);

		return $this->LoadList($this->Execute());
	}


	public function SelectAllWithGroupExNewsletters($OrderBy = "Province, City, ClubName")
	{
		$OrderBy = $this->db->escape_str($OrderBy);

		$this->SetQuery("SELECT * FROM Club WHERE NewsletterURL <> '' ORDER BY ". $OrderBy);

		return $this->LoadList($this->Execute());
	}


	public function SelectAllWithGroupExSchedules($OrderBy = "Province, City, ClubName")
	{
		$OrderBy = $this->db->escape_str($OrderBy);

		$this->SetQuery("SELECT * FROM Club WHERE ScheduleURL <> '' ORDER BY ". $OrderBy);

		return $this->LoadList($this->Execute());
	}


	public function SelectAllWithPoolSchedules($OrderBy = "Province, City, ClubName")
	{
		$OrderBy = $this->db->escape_str($OrderBy);

		$this->SetQuery("SELECT * FROM Club WHERE PoolScheduleURL <> '' ORDER BY ". $OrderBy);

		return $this->LoadList($this->Execute());
	}


	public function SelectAllWithCareers()
	{
		$this->SetQuery("SELECT * FROM Club WHERE ID IN (SELECT DISTINCT ClubID FROM ClubCareer) ORDER BY ClubName");

		return $this->LoadList($this->Execute());
	}


	public function SelectWithCareerAvailability($CareerID, $OrderBy = "Province, City, ClubName")
	{
		$OrderBy = $this->db->escape_str($OrderBy);

		$query = "	SELECT
						Club.*,
						IF (ISNULL(ClubCareer.ClubID), 0, 1) AS PositionIsAvailable
					FROM
						Club
						LEFT JOIN ClubCareer ON Club.ID = ClubCareer.ClubID AND ClubCareer.CareerID = @CareerID
					ORDER BY ". $OrderBy;

		$this->SetQuery($query);

		$this->AddParameter("@CareerID", $CareerID, SQLTYPE_INT);


		return $this->LoadList($this->Execute());
	}


	public function SelectCities()
	{
		$this->SetQuery("SELECT ID, City, ClubName FROM Club ORDER BY ID");

		$results = $this->Execute();

		$cities = array();

		while ($row = $results->fetch_object()) { $cities[] = $row->ClubName; }

		return $cities;
	}

	public function LoadClubs()
	{
		$this->SetQuery("SELECT ID, ClubName FROM Club ORDER BY ID");

		$results = $this->Execute();

		$clubs = array();

		while ($row = $results->fetch_object()) { $clubs[ $row->ID ] = $row->ClubName; }

		return $clubs;
	}

	private function LoadList(MySqlResult $items)
	{
		$clubs = new ItemList();


		while ($item = $items->fetch_object())
		{
			$club = new Club();
			$club->LoadValues($item);

			$clubs->AddItem($club);
		}

		return $clubs;
	}
}

?>
