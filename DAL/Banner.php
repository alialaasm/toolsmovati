<?php

require_once("DALC.php");

class Banner extends DALC
{

	public $ID = 0;
	public $Filename = "";
	public $Title = "";
	public $URL = "";
	public $Active  = true;
	public $LoadError = false;
	public $IsNewRow = false;

	public function Banner($ID = 0)
	{
		DALC::DALC();

		if ((!is_numeric($ID)) || $ID < 0) { $ID = 0; }

		if ($ID > 0)
		{
			$query = "SELECT * FROM Banner WHERE ID = @ID";

			$this->SetQuery($query);

			$this->AddParameter("@ID", $ID, SQLTYPE_INT);

			$result = $this->Execute();

			$row = $result->fetch_object();

			$vals = ($row === false) ? NULL : $row;

			$this->LoadError = ($row === false);

			$this->LoadValues($vals);
		}
		else
		{
			$this->IsNewRow = true;
		}
	}


	public function Update()
	{
		if ($this->IsNewRow)
		{
			$query = "	INSERT INTO
							`Banner`
							( Filename, Title, URL, Active )
						VALUES
							( @Filename, @Title, @URL, @Active )";
		}
		else
		{
			$query = "	UPDATE
							`Banner`
						SET
							Filename = @Filename,
							Title = @Title, URL = @URL,
							Active = @Active
						WHERE
							ID = @ID";
		}

		$this->SetQuery($query);

		$this->AddParameter("@Filename", $this->Filename, SQLTYPE_VARCHAR);
		$this->AddParameter("@Title", $this->Title, SQLTYPE_VARCHAR);
		$this->AddParameter("@URL", $this->URL, SQLTYPE_VARCHAR);
		$this->AddParameter("@Active", $this->Active, SQLTYPE_BOOL);

		if ($this->IsNewRow)
		{
			$this->Execute();
			$this->ID = $this->InsertID();
			$this->IsNewRow = false;
		}
		else
		{
			$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
			$this->Execute();
		}
	}


	public function Delete()
	{
		$this->SetQuery("DELETE FROM Banner WHERE ID = @ID");

		$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);

		$this->Execute();
	}


	public function LoadValues($vals)
	{
		if ($vals != NULL)
		{
			if (isset($vals->ID)) $this->ID = intval($vals->ID);

			if (isset($vals->Filename)) $this->Filename = $vals->Filename;
			if (isset($vals->Title)) $this->Title = $vals->Title;
			if (isset($vals->URL)) $this->URL = $vals->URL;
			if (isset($vals->Active)) $this->Active = (intval($vals->Active) > 0);
		}
		else
		{
			
		}
	}
}

?>