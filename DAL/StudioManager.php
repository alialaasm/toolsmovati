<?php


require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/DALC.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/Studio.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ModuleData.php");


class StudioManager extends DALC
{
	public function SelectAll()
	{
		$this->SetQuery("SELECT *, Studio.ID as SID FROM Studio LEFT JOIN Club ON Studio.ClubId=Club.ID ORDER BY `ClubId`");

		return $this->LoadList($this->Execute());
	}

	public function SelectStudioByLocation($Studios)
	{
		$this->SetQuery("SELECT ID, StudioName FROM Studio WHERE ClubId = @Studios");
		
		$this->AddParameter("@Studios", $Studios, SQLTYPE_VARCHAR);		

		$results = $this->Execute();

		$studios = array();

		while ($row = $results->fetch_object()) { 
					$i++;
					$studios[$i] = array(1=>$row->StudioName,2=>$row->ID);							
		}

		return $studios;
	}

	public function SelectClassesByClub( $club ){
		$sql = "SELECT Class.ClassName as Name, Instructor, StudioId, Day, concat(Event.StartEventHour, ':', Event.StartEventMin, Event.StartEventAP, ' - ', Event.EndEventHour, ':', Event.EndEventMin, Event.EndEventAP) as Time, Category, Description, Event.ID as EventID FROM Event LEFT JOIN Class ON Event.classid=Class.id WHERE ClubId = @club ORDER BY ClubId, StudioId, Day, StartEventAP, StartEventHour <> 12, StartEventHour";
		$this->SetQuery( $sql );
		
		$this->AddParameter("@club", $club , SQLTYPE_VARCHAR);		

		$results = $this->Execute();

		$classes = array();

		while ($row = $results->fetch_object()) { 
					$i++;
					$classes[$i] = $row;
		}

		return $classes;

	}

        public function SelectClassesByMember( $member_id ){
#                $sql = "SELECT Class.ClassName as Name, Instructor, StudioId, Day, concat(Event.StartEventHour, ':', Event.StartEventMin, Event.StartEventAP, ' - ', Event.EndEventHour, ':', Event.EndEventMin, Event.EndEventAP) as Time, Category, Description, Event.ID as EventID, Studio.Name as StudioName, Club.ClubName as ClubName FROM Event LEFT JOIN Class ON Event.classid=Class.id JOIN MemberClass mc on mc.ClassId = Event.ID and mc.MemberId = @member LEFT JOIN Club on Club.ID = ClubId JOIN Studio.ID = StudioId ORDER BY ClubId, StudioId, Day, StartEventAP, StartEventHour";
		$sql = "SELECT Class.ClassName as Name, Instructor, StudioId, Day, concat(Event.StartEventHour, ':', Event.StartEventMin, Event.StartEventAP, ' - ', Event.EndEventHour, ':', Event.EndEventMin, Event.EndEventAP) as Time, Category, Description, Event.ID as EventID, Studio.StudioName as StudioName, Club.ClubName as ClubName FROM Event LEFT JOIN Class ON Event.classid=Class.id JOIN MemberClass mc on mc.ClassId = Event.ID and mc.MemberId = @member JOIN Studio on Studio.ID = StudioId JOIN Club on Club.ID = Studio.ClubId ORDER BY Studio.ClubId, StudioId, Day, StartEventAP, StartEventHour";

                $this->SetQuery( $sql );

                $this->AddParameter("@member", $member_id , SQLTYPE_VARCHAR);

                $results = $this->Execute();

                $classes = array();

                while ($row = $results->fetch_object()) {
                                        $i++;
                                        $classes[$i] = $row;
                }

                return $classes;

        }
	

	public function SelectStudioByLocationAll($Studios) {
		$this->SetQuery("SELECT * FROM Studio WHERE ClubId = @Studios");
		
		$this->AddParameter("@Studios", $Studios, SQLTYPE_VARCHAR);		

		return $this->LoadList($this->Execute());
	}

	public function SelectStudioByID($ID)
	{
		$this->SetQuery("SELECT * FROM Studio WHERE ID = @ID");
		
		$this->AddParameter("@ID", $ID, SQLTYPE_VARCHAR);		
		$this->AddParameter("@StudioName", $StudioName, SQLTYPE_VARCHAR);		
		$this->AddParameter("@ClubId", $ClubId, SQLTYPE_VARCHAR);				

		return $this->LoadList($this->Execute());
	}


	private function LoadList(MySqlResult $items)
	{
		$studio = new ItemList();

		while ($item = $items->fetch_object())
		{
			$studio_item = new Studio();
			$studio_item->LoadValues($item);

			$studio->AddItem($studio_item);
		}

		return $studio;
	}
}

?>
