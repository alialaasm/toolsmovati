<?php

require_once("DALC.php");

class ActionButton extends DALC
{

	public $ID = 0;
	public $Colour = "";
	public $Title = "";
	public $SubTitle = "";
	public $DisplayOrder = "";
	public $URL = "";
	public $Active  = true;

	// Optional Attributes
	public $HasPageAssociation = false;


	public $LoadError = false;
	public $IsNewRow = false;

	public function ActionButton($ID = 0)
	{
		DALC::DALC();

		if ((!is_numeric($ID)) || $ID < 0) { $ID = 0; }

		if ($ID > 0)
		{
			$query = "SELECT * FROM ActionButton WHERE ID = @ID";

			$this->SetQuery($query);

			$this->AddParameter("@ID", $ID, SQLTYPE_INT);

			$result = $this->Execute();

			$row = $result->fetch_object();

			$vals = ($row === false) ? NULL : $row;

			$this->LoadError = ($row === false);

			$this->LoadValues($vals);
		}
		else
		{
			$this->IsNewRow = true;
		}
	}


	public function Update()
	{
		if ($this->IsNewRow)
		{
			$query = "	INSERT INTO
							`ActionButton`
							( Colour, Title, SubTitle, DisplayOrder, URL, Active )
						VALUES
							( @Colour, @Title, @SubTitle, @DisplayOrder, @URL, @Active )";

			$this->SetQuery("SELECT MAX(DisplayOrder) FROM ActionButton");
			$returnVal = $this->ExecuteScalar();

			$maxOrder = intval($returnVal);

			$this->DisplayOrder = $maxOrder + 1;
		}
		else
		{
			$query = "	UPDATE
							`ActionButton`
						SET
							Colour = @Colour,
							Title = @Title, SubTitle = @SubTitle,
							DisplayOrder = @DisplayOrder, URL = @URL,
							Active = @Active
						WHERE
							ID = @ID";
		}

		$this->SetQuery($query);

		$this->AddParameter("@Colour", $this->Colour, SQLTYPE_VARCHAR);
		$this->AddParameter("@Title", $this->Title, SQLTYPE_VARCHAR);
		$this->AddParameter("@SubTitle", $this->SubTitle, SQLTYPE_VARCHAR);
		$this->AddParameter("@DisplayOrder", $this->DisplayOrder, SQLTYPE_INT);
		$this->AddParameter("@URL", $this->URL, SQLTYPE_VARCHAR);
		$this->AddParameter("@Active", $this->Active, SQLTYPE_BOOL);

		if ($this->IsNewRow)
		{
			$this->Execute();
			$this->ID = $this->InsertID();
			$this->IsNewRow = false;
		}
		else
		{
			$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
			$this->Execute();
		}
	}

	public function MoveUp()
	{
		//check current order, if 1 do not moveup
		if($this->DisplayOrder > 1)
		{
			$this->BeginTransaction();

			$previousOrder = $this->DisplayOrder - 1;

			$this->SetQuery("UPDATE ActionButton SET DisplayOrder = DisplayOrder + 1 WHERE DisplayOrder = $previousOrder");
			$this->Execute();

			$this->SetQuery("UPDATE ActionButton SET DisplayOrder = $previousOrder WHERE ID = @ID");
			$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
			$this->Execute();

			return $this->EndTransaction();
		}

		return true;
	}

	public function MoveDown()
	{
		$this->SetQuery("SELECT MAX(DisplayOrder) FROM ActionButton");

		$returnVal = $this->ExecuteScalar();

		$maxOrder = intval($returnVal);

		//check current order, if 1 do not moveup
		if($this->DisplayOrder < $maxOrder)
		{

			$this->BeginTransaction();

			$nextOrder = $this->DisplayOrder + 1;

			$this->SetQuery("UPDATE ActionButton SET DisplayOrder = DisplayOrder - 1 WHERE DisplayOrder = $nextOrder");
			$this->Execute();

			$this->SetQuery("UPDATE ActionButton SET DisplayOrder = $nextOrder WHERE ID = @ID");
			$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
			$this->Execute();

			return $this->EndTransaction();
		}

		return true;
	}


	public function Delete()
	{
		$this->BeginTransaction();

		$this->SetQuery("UPDATE ActionButton SET DisplayOrder = DisplayOrder - 1 WHERE DisplayOrder > " . $this->DisplayOrder);
		$this->Execute();

		$this->SetQuery("DELETE FROM ActionButton WHERE ID = @ID");
		$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
		$this->Execute();

		return $this->EndTransaction();
	}


	public function LoadValues($vals)
	{
		if ($vals != NULL)
		{
			if (isset($vals->ID)) $this->ID = intval($vals->ID);

			if (isset($vals->Colour)) $this->Colour = $vals->Colour;
			if (isset($vals->Title)) $this->Title = $vals->Title;
			if (isset($vals->SubTitle)) $this->SubTitle = $vals->SubTitle;
			if (isset($vals->DisplayOrder)) $this->DisplayOrder = $vals->DisplayOrder;
			if (isset($vals->URL)) $this->URL = $vals->URL;
			if (isset($vals->Active)) $this->Active = (intval($vals->Active) > 0);

			// Optional Attributes
			if (isset($vals->HasPageAssociation)) $this->HasPageAssociation = (intval($vals->HasPageAssociation) > 0);
		}
		else
		{

		}
	}
}

?>