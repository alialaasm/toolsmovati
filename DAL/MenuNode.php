<?php


require_once("DALC.php");



class MenuNode extends DALC
{

	public $ID = 0;
	public $LeftVal = 0;
	public $RightVal = 0;
	public $Template = "";
	public $SectionType = "";
	public $SectionTypeID = 0;
	public $Title = "";
	public $EncodedTitle = "";
	public $ShowInMenu = true;
	public $BannerID = 0;
	public $URL = "";
	public $Target = "";
	public $Keywords = "";
	public $Description = "";
	public $Active  = true;


	public $LoadError = false;
	public $IsNewRow = false;


	/**
	 *
	 */
	public function MenuNode($ID = 0)
	{
		DALC::DALC();

		if ((!is_numeric($ID)) || $ID < 0) { $ID = 0; }

		if ($ID > 0)
		{
			$query = "SELECT * FROM Menu WHERE ID = @ID";

			$this->SetQuery($query);

			$this->AddParameter("@ID", $ID, SQLTYPE_INT);

			$result = $this->Execute();

			$row = $result->fetch_object();

			$vals = ($row === false) ? NULL : $row;

			$this->LoadError = ($row === false);

			$this->LoadValues($vals);
		}
		else
		{
			$this->IsNewRow = true;
		}

	}


	/**
	 * Inserts or updates this node in the database.
	 *
	 * @param int $parentNodeID If this is a new node then the parent node ID
	 *                          is the ID of the node this node will be placed
	 *                          under.
	 *
	 * @return bool true iff the transactions have completed successfully.
	 */
	public function Update($parentNodeID = 0)
	{

		$this->BeginTransaction();


		if ($this->IsNewRow)
		{

			// Get the RightVal of the parent so that space can be added to the tree.
			$this->SetQuery("SELECT RightVal FROM Menu WHERE ID = $parentNodeID");
			$parentRightVal = $this->ExecuteScalar();


			// Update the Left and Right vals by two, since each node takes 2 spaces.
			$this->SetQuery("UPDATE Menu SET LeftVal = LeftVal + 2 WHERE LeftVal >= $parentRightVal");
			$this->Execute();

			$this->SetQuery("UPDATE Menu SET RightVal = RightVal + 2  WHERE RightVal >= $parentRightVal");
			$this->Execute();


			$this->LeftVal = $parentRightVal;
			$this->RightVal = $this->LeftVal + 1;


			$query = "	INSERT INTO
							`Menu`
							(
								LeftVal, RightVal,
								Template, SectionType, SectionTypeID,
								Title, EncodedTitle,
								ShowInMenu, BannerID, URL, Target,
								Keywords, Description,
								Active
							)
						VALUES
							(
								@LeftVal, @RightVal,
								@Template, @SectionType, @SectionTypeID,
								@Title, @EncodedTitle,
								@ShowInMenu, @BannerID, @URL, @Target,
								@Keywords, @Description,
								@Active
							)";
		}
		else
		{
			$query = "	UPDATE
							`Menu`
						SET
							Template = @Template, SectionType = @SectionType, SectionTypeID = @SectionTypeID,
							Title = @Title, EncodedTitle = @EncodedTitle,
							ShowInMenu = @ShowInMenu, BannerID = @BannerID, URL = @URL, Target = @Target,
							Keywords = @Keywords, Description = @Description,
							Active = @Active
						WHERE
							ID = @ID";
		}

		$this->SetQuery($query);

		$this->AddParameter("@Template", $this->Template, SQLTYPE_VARCHAR);
		$this->AddParameter("@SectionType", $this->SectionType, SQLTYPE_VARCHAR);
		$this->AddParameter("@SectionTypeID", $this->SectionTypeID, SQLTYPE_INT);
		$this->AddParameter("@Title", $this->Title, SQLTYPE_VARCHAR);
		$this->AddParameter("@EncodedTitle", $this->EncodedTitle, SQLTYPE_VARCHAR);
		$this->AddParameter("@ShowInMenu", $this->ShowInMenu, SQLTYPE_BOOL);
		$this->AddParameter("@BannerID", $this->BannerID, SQLTYPE_INT);
		$this->AddParameter("@URL", $this->URL, SQLTYPE_VARCHAR);
		$this->AddParameter("@Target", $this->Target, SQLTYPE_VARCHAR);
		$this->AddParameter("@Keywords", $this->Keywords, SQLTYPE_VARCHAR);
		$this->AddParameter("@Description", $this->Description, SQLTYPE_VARCHAR);
		$this->AddParameter("@Active", $this->Active, SQLTYPE_BOOL);

		if ($this->IsNewRow)
		{
			$this->AddParameter("@LeftVal", $this->LeftVal, SQLTYPE_INT);
			$this->AddParameter("@RightVal", $this->RightVal, SQLTYPE_INT);

			$this->Execute();

			$this->ID = $this->InsertID();

			$this->IsNewRow = false;
		}
		else
		{
			$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
			$this->Execute();
		}

		return $this->EndTransaction();
	}



	/**
	 * Updates the list of users that can edit this menu item.
	 *
	 * @param array $UserIDs An array of user IDs.
	 *
	 * @return bool true iff there are no errors, else false;
	 */
	public function UpdateUserAccess($UserIDs)
	{
		if (is_array($UserIDs))
		{
			$this->BeginTransaction();

			$query = "DELETE FROM UserAccess WHERE MenuID = @MenuNodeID";
			$this->SetQuery($query);
			$this->AddParameter("@MenuNodeID", $this->ID, SQLTYPE_INT);
			$this->Execute();

			$numUserIDs = count($UserIDs);

			if ($numUserIDs > 0)
			{
				$query = "INSERT INTO UserAccess (UserID, MenuID) VALUES ";

				for ($i = 0; $i < $numUserIDs; $i++)
				{
					if ($i > 0) { $query .= ", "; }
					$query .= "(" . $UserIDs[$i] . ", $this->ID)";
				}
				$this->SetQuery($query);

				$this->Execute();
			}

			return $this->EndTransaction();
		}

		return true;
	}

	/**
	 * Updates the list of quick action buttons for this menu item.
	 *
	 * @param array $ActionButtonIDs An array of action button IDs.
	 *
	 * @return bool true iff there are no errors, else false;
	 */
	public function UpdateActionButtons($ActionButtonIDs)
	{
		if (is_array($ActionButtonIDs))
		{
			$this->BeginTransaction();

			$query = "DELETE FROM MenuActionButton WHERE MenuID = @MenuNodeID";
			$this->SetQuery($query);
			$this->AddParameter("@MenuNodeID", $this->ID, SQLTYPE_INT);
			$this->Execute();

			$numActionButtonIDs = count($ActionButtonIDs);

			if ($numActionButtonIDs > 0)
			{
				$query = "INSERT INTO MenuActionButton (ActionButtonID, MenuID) VALUES ";

				for ($i = 0; $i < $numActionButtonIDs; $i++)
				{
					if ($i > 0) { $query .= ", "; }
					$query .= "(" . $ActionButtonIDs[$i] . ", $this->ID)";
				}
				$this->SetQuery($query);

				$this->Execute();
			}

			return $this->EndTransaction();
		}

		return true;
	}


	/**
	 * Move a menu node left in the tree.  Nodes can only be moved left and right
	 * at their current level, they cannot be moved up and down in the tree.
	 *
	 * @return bool True iff all actions executed successfully.
	 */
	public function MoveUp()
	{

		// Select the left sibling to change places.
		$this->SetQuery("SELECT * FROM Menu WHERE RightVal = @RightVal");
		$this->AddParameter("@RightVal", ($this->LeftVal - 1), SQLTYPE_INT);
		$result = $this->Execute();
		$leftSibling = $result->fetch_object();

		// If there wasn't a row returned then the left sibling does not exist,
		// so this node is the left most child, so do nothing.  This is not an error,
		// there is no change.

		if ($leftSibling !== false)
		{

			$this->BeginTransaction();



			// Mark the rows about to be moved so they can be differentiated later.
			$this->SetQuery("UPDATE Menu SET RowMark = 1 WHERE LeftVal >= @LeftVal AND RightVal <= @RightVal");
			$this->AddParameter("@LeftVal", $this->LeftVal, SQLTYPE_INT);
			$this->AddParameter("@RightVal", $this->RightVal, SQLTYPE_INT);
			$this->Execute();



			// Move this node left.
			$query = "	UPDATE Menu
						SET LeftVal = LeftVal - @MovementSize1, RightVal = RightVal - @MovementSize2
						WHERE LeftVal >= @LeftVal AND RightVal <= @RightVal";

			$this->SetQuery($query);
			$this->AddParameter("@MovementSize1", ($this->LeftVal - $leftSibling->LeftVal), SQLTYPE_INT);
			$this->AddParameter("@MovementSize2", ($this->LeftVal - $leftSibling->LeftVal), SQLTYPE_INT);
			$this->AddParameter("@LeftVal", $this->LeftVal, SQLTYPE_INT);
			$this->AddParameter("@RightVal", $this->RightVal, SQLTYPE_INT);

			$this->Execute();



			// Move the left sibling node right.
			$query = "	UPDATE Menu
						SET LeftVal = LeftVal + @MovementSize1, RightVal = RightVal + @MovementSize2
						WHERE LeftVal >= @LeftVal AND RightVal <= @RightVal AND RowMark = 0";

			$this->SetQuery($query);
			$this->AddParameter("@MovementSize1", ($this->RightVal - $leftSibling->RightVal), SQLTYPE_INT);
			$this->AddParameter("@MovementSize2", ($this->RightVal - $leftSibling->RightVal), SQLTYPE_INT);
			$this->AddParameter("@LeftVal", $leftSibling->LeftVal, SQLTYPE_INT);
			$this->AddParameter("@RightVal", $leftSibling->RightVal, SQLTYPE_INT);

			$this->Execute();



			// Clear the row marks.
			$this->SetQuery("UPDATE Menu SET RowMark = 0");
			$this->Execute();


			$this->RightVal = $leftSibling->LeftVal + ($this->RightVal - $this->LeftVal);
			$this->LeftVal = $leftSibling->LeftVal;

			$success = $this->EndTransaction();

		}
		else
		{
			$success = true;
		}


		return $success;
	}



	/**
	 * Move a menu node right in the tree.  Nodes can only be moved left and right
	 * at their current level, they cannot be moved up and down in the tree.
	 *
	 * @return bool True iff all actions executed successfully.
	 */
	public function MoveDown()
	{

		// Select the right sibling to change places.
		$this->SetQuery("SELECT * FROM Menu WHERE LeftVal = @LeftVal");
		$this->AddParameter("@LeftVal", ($this->RightVal + 1), SQLTYPE_INT);
		$result = $this->Execute();
		$rightSibling = $result->fetch_object();


		// If there wasn't a row returned then the right sibling does not exist,
		// so this node is the right most child, so do nothing.  This is not an error,
		// there is no change.
		if ($rightSibling !== false)
		{

			$this->BeginTransaction();



			// Mark the rows about to be moved so they can be differentiated later.
			$this->SetQuery("UPDATE Menu SET RowMark = 1 WHERE LeftVal >= @LeftVal AND RightVal <= @RightVal");
			$this->AddParameter("@LeftVal", $this->LeftVal, SQLTYPE_INT);
			$this->AddParameter("@RightVal", $this->RightVal, SQLTYPE_INT);
			$this->Execute();



			// Move this node down.
			$query = "	UPDATE Menu
						SET LeftVal = LeftVal + @MovementSize1, RightVal = RightVal + @MovementSize2
						WHERE LeftVal >= @LeftVal AND RightVal <= @RightVal";

			$this->SetQuery($query);
			$this->AddParameter("@MovementSize1", ($rightSibling->RightVal - $this->RightVal), SQLTYPE_INT);
			$this->AddParameter("@MovementSize2", ($rightSibling->RightVal - $this->RightVal), SQLTYPE_INT);
			$this->AddParameter("@LeftVal", $this->LeftVal, SQLTYPE_INT);
			$this->AddParameter("@RightVal", $this->RightVal, SQLTYPE_INT);

			$this->Execute();



			// Move the right sibling node up.
			$query = "	UPDATE Menu
						SET LeftVal = LeftVal - @MovementSize1, RightVal = RightVal - @MovementSize2
						WHERE LeftVal >= @LeftVal AND RightVal <= @RightVal AND RowMark = 0";

			$this->SetQuery($query);
			$this->AddParameter("@MovementSize1", ($rightSibling->LeftVal - $this->LeftVal), SQLTYPE_INT);
			$this->AddParameter("@MovementSize2", ($rightSibling->LeftVal - $this->LeftVal), SQLTYPE_INT);
			$this->AddParameter("@LeftVal", $rightSibling->LeftVal, SQLTYPE_INT);
			$this->AddParameter("@RightVal", $rightSibling->RightVal, SQLTYPE_INT);

			$this->Execute();



			// Clear the row marks.
			$this->SetQuery("UPDATE Menu SET RowMark = 0");
			$this->Execute();

			$this->RightVal = $rightSibling->LeftVal + ($this->RightVal - $this->LeftVal);
			$this->LeftVal = $rightSibling->LeftVal;

			$success = $this->EndTransaction();
		}
		else
		{
			$success = true;
		}


		return $success;
	}


	/**
	 *
	 */
	public function Delete()
	{

		$this->BeginTransaction();


		// Delete this menu node and any child nodes.
		$this->SetQuery("DELETE FROM Menu WHERE LeftVal >= @LeftVal AND RightVal <= @RightVal");
		$this->AddParameter("@LeftVal", $this->LeftVal, SQLTYPE_INT);
		$this->AddParameter("@RightVal", $this->RightVal, SQLTYPE_INT);
		$this->Execute();

		$movementSize = ($this->RightVal - $this->LeftVal) + 1;

		// Update the Left and Right vals by the number of nodes deleted.
		$this->SetQuery("UPDATE Menu SET LeftVal = LeftVal - @MovementSize WHERE LeftVal >= @RightVal");
		$this->AddParameter("@RightVal", $this->RightVal, SQLTYPE_INT);
		$this->AddParameter("@MovementSize", $movementSize, SQLTYPE_INT);
		$this->Execute();

		$this->SetQuery("UPDATE Menu SET RightVal = RightVal - @MovementSize WHERE RightVal >= @RightVal");
		$this->AddParameter("@RightVal", $this->RightVal, SQLTYPE_INT);
		$this->AddParameter("@MovementSize", $movementSize, SQLTYPE_INT);
		$this->Execute();


		return $this->EndTransaction();
	}


	/**
	 *
	 */
	public function LoadValues($vals)
	{
		if ($vals != NULL)
		{
			if (isset($vals->ID)) $this->ID = intval($vals->ID);

			if (isset($vals->LeftVal)) $this->LeftVal = intval($vals->LeftVal);
			if (isset($vals->RightVal)) $this->RightVal = intval($vals->RightVal);

			if (isset($vals->Template)) $this->Template = $vals->Template;
			if (isset($vals->SectionType)) $this->SectionType = $vals->SectionType;
			if (isset($vals->SectionTypeID)) $this->SectionTypeID = intval($vals->SectionTypeID);
			if (isset($vals->Title)) $this->Title = $vals->Title;
			if (isset($vals->EncodedTitle)) $this->EncodedTitle = $vals->EncodedTitle;
			if (isset($vals->ShowInMenu)) $this->ShowInMenu = (intval($vals->ShowInMenu) > 0);
			if (isset($vals->BannerID)) $this->BannerID = intval($vals->BannerID);
			if (isset($vals->URL)) $this->URL = $vals->URL;
			if (isset($vals->Target)) $this->Target = $vals->Target;
			if (isset($vals->Keywords)) $this->Keywords = $vals->Keywords;
			if (isset($vals->Description)) $this->Description = $vals->Description;
			if (isset($vals->Active)) $this->Active = (intval($vals->Active) > 0);

		}
	}
}

?>