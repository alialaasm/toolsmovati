<?php


require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/DALC.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/Press.php");


class PressManager extends DALC
{

	public function SelectAll()
	{
		$this->SetQuery("SELECT * FROM Press ORDER BY ReleaseDate DESC");

		return $this->LoadList($this->Execute());
	}


	public function SelectByYear($Year)
	{
		$this->SetQuery("SELECT * FROM Press WHERE ReleaseDate >= @StartDate AND ReleaseDate <= @EndDate ORDER BY ReleaseDate DESC");

		$this->AddParameter("@StartDate", "$Year-01-01", SQLTYPE_DATE);
		$this->AddParameter("@EndDate", "$Year-12-31", SQLTYPE_DATE);

		return $this->LoadList($this->Execute());
	}


	public function SelectYears()
	{
		$this->SetQuery("SELECT DISTINCT Year(ReleaseDate) AS Year FROM Press ORDER BY Year DESC");

		$results = $this->Execute();

		$years = array();

		while ($row = $results->fetch_object()) { $years[] = $row->Year; }

		return $years;
	}


	private function LoadList(MySqlResult $items)
	{
		$press = new ItemList();

		while ($item = $items->fetch_object())
		{
			$press_item = new Press();
			$press_item->LoadValues($item);

			$press->AddItem($press_item);
		}

		return $press;
	}
}

?>
