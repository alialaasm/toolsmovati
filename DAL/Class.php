<?php

require_once("DALC.php");

class ClassDescription extends DALC
{

	public $ID = 0;
	public $Category = "";
	public $CatId = 0;
	public $ClassName = "";
	public $Description = "";
	public $VideoLink = "";
	public $ClassCode = "";
	public $Colour = "#CCFF00";	
	public $Active  = true;
	public $LoadError = false;
	public $IsNewRow = false;

	public function ClassDescription($ID = 0)
	{
		DALC::DALC();

		if ((!is_numeric($ID)) || $ID < 0) { $ID = 0; }

		if ($ID > 0)
		{
			$query = "SELECT * FROM Class WHERE ID = @ID";

			$this->SetQuery($query);

			$this->AddParameter("@ID", $ID, SQLTYPE_INT);

			$result = $this->Execute();

			$row = $result->fetch_object();

			$vals = ($row === false) ? NULL : $row;

			$this->LoadError = ($row === false);

			$this->LoadValues($vals);
		}
		else
		{
			$this->IsNewRow = true;
		}
	}


	public function Update()
	{
		if ($this->IsNewRow)
		{
			$query = "	INSERT INTO
							`Class`
							( Category, CatId, ClassName, Description, VideoLink, ClassCode, Active )
						VALUES
							( @Category, @CatId, @ClassName, @Description, @VideoLink, @ClassCode, @Active )";
		}
		else
		{
			$query = "	UPDATE
							`Class`
						SET
							Category = @Category, CatId = @CatId,
							ClassName = @ClassName, Description = @Description, 
							VideoLink = @VideoLink, ClassCode = @ClassCode,
							Active = @Active
						WHERE
							ID = @ID";
		}

		$this->SetQuery($query);

		$this->AddParameter("@Category", $this->Category, SQLTYPE_VARCHAR);
		$this->AddParameter("@CatId", $this->CatId, SQLTYPE_INT);
		$this->AddParameter("@ClassName", $this->ClassName, SQLTYPE_VARCHAR);
		$this->AddParameter("@Description", $this->Description, SQLTYPE_TEXT);
		$this->AddParameter("@VideoLink", $this->VideoLink, SQLTYPE_VARCHAR);
		$this->AddParameter("@ClassCode", $this->ClassCode, SQLTYPE_VARCHAR);
		$this->AddParameter("@Active", $this->Active, SQLTYPE_BOOL);
		$this->AddParameter("@Colour", $this->Active, SQLTYPE_TEXT);		

		if ($this->IsNewRow)
		{
			$this->Execute();
			$this->ID = $this->InsertID();
			$this->IsNewRow = false;
		}
		else
		{
			$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
			$this->Execute();
		}
	}


	public function Delete()
	{
		$this->SetQuery("DELETE FROM Class WHERE ID = @ID");

		$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);

		$this->Execute();
	}


	public function LoadValues($vals)
	{
		if ($vals != NULL)
		{
			if (isset($vals->ID)) $this->ID = intval($vals->ID);

			if (isset($vals->Category)) $this->Category = $vals->Category;
			if (isset($vals->CatId)) $this->CatId = $vals->CatId;
			if (isset($vals->ClassName)) $this->ClassName = $vals->ClassName;
			if (isset($vals->Description)) $this->Description = $vals->Description;
			if (isset($vals->VideoLink)) $this->VideoLink = $vals->VideoLink;			
			if (isset($vals->ClassCode)) $this->ClassCode = $vals->ClassCode;
			if (isset($vals->Active)) $this->Active = (intval($vals->Active) > 0);
			if (isset($vals->Colour)) $this->Colour = $vals->Colour;			
		}
		else
		{
		}
	}
}

?>