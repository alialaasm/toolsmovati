<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/DALC.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ActionButton.php");


/**
 *
 */
class ActionButtonManager extends DALC
{

	/**
	 *
	 */
	public function SelectAll($OrderBy = "DisplayOrder")
	{
		$OrderBy = $this->db->escape_str($OrderBy);

		$this->SetQuery("SELECT * FROM ActionButton ORDER BY " . $OrderBy);

		return $this->LoadList($this->Execute());
	}


	/**
	 *
	 */
	public function SelectActive($Active = 1, $OrderBy = "DisplayOrder")
	{
		$OrderBy = $this->db->escape_str($OrderBy);

		$this->SetQuery("SELECT * FROM ActionButton WHERE Active = @Active ORDER BY " . $OrderBy);

		$this->AddParameter("@Active", $Active, SQLTYPE_BOOL);

		return $this->LoadList($this->Execute());
	}

	/**
	 *
	 */
	public function SelectByMenuNodeID($MenuNodeID, $OrderBy = "DisplayOrder")
	{
		$OrderBy = $this->db->escape_str($OrderBy);

		$query = "	SELECT
						ActionButton.*
					 FROM
					 	ActionButton
					 	INNER JOIN MenuActionButton ON ActionButton.ID = MenuActionButton.ActionButtonID
					 WHERE
					 	ActionButton.Active
					 	AND MenuActionButton.MenuID = @MenuNodeID
					 ORDER BY " . $OrderBy;

		$this->SetQuery($query);

		$this->AddParameter("@MenuNodeID", $MenuNodeID, SQLTYPE_BOOL);

		return $this->LoadList($this->Execute());
	}


	/**
	 *
	 */
	public function SelectMaxOrderIndex()
	{
		$this->SetQuery("SELECT MAX(DisplayOrder) FROM ActionButton");

		$returnVal = $this->ExecuteScalar();

		return intval($returnVal);
	}


	/**
	 *
	 */
	public function SelectWithPageAssociation($MenuNodeID, $OrderBy = "`Title`")
	{
		$OrderBy = $this->db->escape_str($OrderBy);

		$query = "	SELECT
						ActionButton.*,
						IF (ISNULL(MenuActionButton.ActionButtonID), 0, 1) AS HasPageAssociation
					FROM
						ActionButton
						LEFT JOIN MenuActionButton ON
							ActionButton.ID = MenuActionButton.ActionButtonID AND MenuActionButton.MenuID = @MenuNodeID
					ORDER BY ". $OrderBy;

		$this->SetQuery($query);

		$this->AddParameter("@MenuNodeID", $MenuNodeID, SQLTYPE_INT);


		return $this->LoadList($this->Execute());
	}


	/**
	 *
	 */
	private function LoadList(MySqlResult $items)
	{
		$actionButtons = new ItemList();


		while ($item = $items->fetch_object())
		{
			$actionButton = new ActionButton();
			$actionButton->LoadValues($item);

			$actionButtons->AddItem($actionButton);
		}

		return $actionButtons;
	}
}

?>
