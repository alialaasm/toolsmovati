<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/DALC.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/Department.php");

class DepartmentManager extends DALC
{
	public function SelectAll($OrderBy = "Name")
	{
		$this->SetQuery("SELECT * FROM Department ORDER BY @OrderBy");

		$this->AddParameter("@OrderBy", $OrderBy, SQLTYPE_VARCHAR);

		return $this->LoadList($this->Execute());
	}


	public function SelectActive($Active = 1, $OrderBy = "Name")
	{
		$this->SetQuery("SELECT * FROM Department WHERE Active = @Active ORDER BY @OrderBy");

		$this->AddParameter("@Active", $Active, SQLTYPE_BOOL);
		$this->AddParameter("@OrderBy", $OrderBy, SQLTYPE_VARCHAR);

		return $this->LoadList($this->Execute());
	}


	private function LoadList(MySqlResult $items)
	{
		$Departments = new ItemList();


		while ($item = $items->fetch_object())
		{
			$Department = new Department();
			$Department->LoadValues($item);

			$Departments->AddItem($Department);
		}

		return $Departments;
	}
}

?>
