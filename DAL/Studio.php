<?php

require_once("DALC.php");


class Studio extends DALC
{

	public $ID = 0;
	public $SID = 0;	
	public $ClubId = "";
	public $StudioName = "";
	public $ClubName = "";
	

	// Optional Attributes
	public $HasAccess = false;

	public $IsNewRow = false;
	public $LoadError = false;


	public function Studio($ID = 0)
	{
		DALC::DALC();

		if ((!is_numeric($ID)) || $ID < 0) { $ID = 0; }

		if ($ID > 0)
		{
			$query = "SELECT * FROM `Studio` WHERE ID = @ID";

			$this->SetQuery($query);

			$this->AddParameter("@ID", $ID, SQLTYPE_INT);

			$result = $this->Execute();

			$row = $result->fetch_object();

			$vals = ($row === false) ? NULL : $row;

			$this->LoadError = ($row === false);

			$this->LoadValues($vals);
		}
		else
		{
			$this->IsNewRow = true;
		}
	}


	public function Update()
	{
		if ($this->IsNewRow)
		{
			$query = "INSERT INTO `Studio` (ClubId, StudioName) VALUES (@ClubId, @StudioName)";
		}
		else
		{
			$query = "UPDATE `Studio` SET ClubId = @ClubId, StudioName = @StudioName WHERE ID = @ID";
		}

		$this->SetQuery($query);

		$this->AddParameter("@ClubId", $this->ClubId, SQLTYPE_VARCHAR);
		$this->AddParameter("@StudioName", $this->StudioName, SQLTYPE_VARCHAR);


		if ($this->IsNewRow)
		{
			$this->Execute();
			$this->ID = $this->InsertID();
			$this->IsNewRow = false;
		}
		else
		{
			$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
			$this->Execute();
		}
	}



    public function Delete()
	{
		$this->SetQuery("DELETE FROM Studio WHERE ID = @ID");

		$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);

		$this->Execute();
	}


	public function LoadValues($vals)
	{
		if ($vals != NULL)
		{
			if (isset($vals->ID)) $this->ID = intval($vals->ID);
			if (isset($vals->SID)) $this->SID = intval($vals->SID);			
			if (isset($vals->ClubId)) $this->ClubId = $vals->ClubId;
			if (isset($vals->StudioName)) $this->StudioName = $vals->StudioName;
			if (isset($vals->ClubName)) $this->ClubName = $vals->ClubName;
			
			// Optional Attributes
			if (isset($vals->HasAccess)) $this->HasAccess = (intval($vals->HasAccess) > 0);
		}
		else
		{
		}
	}
}

?>