<?php


require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/DALC.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/Career.php");


class CareerManager extends DALC
{

	public function SelectAll()
	{
		$this->SetQuery("SELECT * FROM Career ORDER BY Position DESC");

		return $this->LoadList($this->Execute());
	}


	public function SelectByClubID($ClubID)
	{
		$query = "	SELECT
						Career.*, Club.ClubName, Club.ApplicationEmail
					FROM
						Career
						INNER JOIN ClubCareer ON Career.ID = ClubCareer.CareerID
						INNER JOIN Club ON ClubCareer.ClubID = Club.ID
					WHERE
						ClubCareer.ClubID = @ClubID
					ORDER BY
						Position DESC";

		$this->SetQuery($query);

		$this->AddParameter("@ClubID", $ClubID, SQLTYPE_INT);

		return $this->LoadList($this->Execute());
	}


	public function SelectByPosition($Position)
	{
		$query = "	SELECT
						Career.*, Club.ClubName, Club.ApplicationEmail
					FROM
						Career
						INNER JOIN Club ON Career.ClubID = Club.ID
					WHERE
						Position = @Position
					ORDER BY
						ClubID, Position DESC";

		$this->SetQuery($query);

		$this->AddParameter("@Position", $Position, SQLTYPE_VARCHAR);

		return $this->LoadList($this->Execute());
	}



	public function SelectPositionsByClubID($ClubID, $Active = 1)
	{
		$query = "	SELECT
						Career.*
					FROM
						Career
						INNER JOIN ClubCareer ON Career.ID = ClubCareer.CareerID AND ClubCareer.ClubID = @ClubID
					ORDER BY
						Career.Position DESC";

		$this->SetQuery($query);

		$this->AddParameter("@ClubID", $ClubID, SQLTYPE_INT);

		return $this->LoadList($this->Execute());
	}


	public function SelectPositions($Type = "")
	{
		$Type = $this->db->escape_str($Type);

		$this->SetQuery("SELECT Name FROM CareerPosition WHERE Type LIKE '%" . $Type . "%' ORDER BY Type, Name");

		$results = $this->Execute();

		$positions = array();

		while ($row = $results->fetch_object()) { $positions[] = $row->Name; }

		return $positions;
	}


	public function SelectAvailable($Type = "")
	{
		$Type = $this->db->escape_str($Type);

		$this->SetQuery("SELECT * FROM Career WHERE ID IN (SELECT DISTINCT CareerID FROM ClubCareer)");

		return $this->LoadList($this->Execute());
	}


	private function LoadList(MySqlResult $items)
	{
		$career = new ItemList();

		while ($item = $items->fetch_object())
		{
			$career_item = new Career();
			$career_item->LoadValues($item);

			$career->AddItem($career_item);
		}

		return $career;
	}
}

?>
