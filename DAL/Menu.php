<?php

include_once("DALC.php");
include_once("MenuNode.php");

class Menu extends DALC
{


	public function Menu()
	{
		parent::DALC();
	}



	public function Render($view = "view", $rootMenuID = "1", $levelsToDisplay = -1, $includeRoot = false)
	{
		$output = "";

		/* ----- Retrieve the Root Node ----- */
		// Retrieve the root node so that the numeric bounds of the child
		// nodes can be determined.
		$this->SetQuery("SELECT * FROM Menu WHERE ID = @ID AND Active = 1");
		$this->AddParameter("@ID", $rootMenuID, SQLTYPE_VARCHAR);

		// retrieve the left and right value of the $root node
		$result = $this->Execute();

		$rootNode = $result->fetch_object();

		if ($rootNode === false) { return ""; }

		/* ----- Retrieve the Menu ----- */
		// Retrieve the descendants of the root node.
		$this->SetQuery("SELECT * FROM Menu WHERE LeftVal BETWEEN " . $rootNode->LeftVal . " AND " .
								$rootNode->RightVal . " ORDER BY LeftVal ASC");

		$result = $this->Execute();

		// Create an empty stack to hold the RightNode values.
		$right = array();

		$depth = -1;

		$currentNode = $result->fetch_object();
		$nextNode = $result->fetch_object();

		// Display each node in the menu.
		do
		{
			// The stack size is equal to the tree depth starting with row 0.
			$stack_size = count($right);

			// Only check stack if there is one.
			if ($stack_size > 0)
			{
				$temp_size = $stack_size;
				// Check if we should remove a node from the stack.
				while ($right[$stack_size - 1]->RightVal < $currentNode->RightVal)
				{
					array_pop($right);
					$stack_size--;

					if (($temp_size - 2) >= $stack_size) { $output .= "</div></ul>\n</li>\n"; }
				}
			}


			if ($stack_size > $depth)
			{
				if ($depth >= 0 || ($depth < 0 && $includeRoot))
				{
					$output .= "\n<ul class=\"level_" . ($depth + 1) ." ". $currentNode->EncodedTitle ."\" id=\"". $currentNode->EncodedTitle ."" . ($depth + 1) ."\"><div id=\"bgimg".$currentNode->EncodedTitle."\">";
				}
			}


			if ($depth >= 0 || ($depth < 0 && $includeRoot))
			{
					if ($currentNode->ShowInMenu && $currentNode->Active) { 
						$output .= "<li>"; 
					}
					else { $output .= "<li style=\"display:none;\">"; }



				if ($view == "view")
				{
					$href = "";
					$startIndex = ($rootMenuID > 1) ? 0 : 1;
					for($i = $startIndex; $i < $stack_size; $i++)
					{
						$href .= $right[$i]->EncodedTitle . "-" . $right[$i]->ID . "/";
					}

					$output .= "<a href=\"" . SITE_URL . $href . $currentNode->EncodedTitle . "-" . $currentNode->ID . "\">";
					$output .= htmlentities($currentNode->Title);
					$output .= "</a>";
				}
				else
				{
					$output .= $currentNode->Title;
					$output .= "<span style=\"float:right;\">";
					$output .= "<a href=\"menuEdit.php?parentNodeID=" . $currentNode->ID . "'\">Add</a>";
					$output .= "<a href=\"menuEdit.php?nodeID=" . $currentNode->ID . "'\">Edit</a>";
					$output .= "<a href=\"delete-menu-item\" onclick=\"DeleteMenuItem(" . $currentNode->ID . "); return false;\">Delete</a>";
					$output .= "</span>";
				}

				if ($nextNode !== false && ($nextNode->LeftVal > $currentNode->RightVal))
				{
					$output .= "</li>\n";
				}
			}


			// Add this node to the stack.
			$right[] = $currentNode;

			$depth = count($right) - 1;

			$currentNode = $nextNode;
			$nextNode = $result->fetch_object();

		} while (!($currentNode === false && $nextNode === false));

		// Pop one item off the stack so that the root
		// element is not closed if it is not included.
		if (!$includeRoot) { array_pop($right); }

		// Close any remaining lists.
		while (array_pop($right) != NULL)
		{
			$output .= "</li>\n</div></ul>\n";
		}


		return $output;
	}



	public function RenderForViewing($rootMenuID = "1", $levelsToDisplay = -1, $includeRoot = false)
	{
		return $this->Render("view", $rootMenuID, $levelsToDisplay, $includeRoot);
	}



	public function RenderForEditing($includeRoot = false)
	{
		return $this->Render("edit", 1, -1, $includeRoot);
	}



	public function RenderToArray($includeRoot = false)
	{
		$output = "";

		/* ----- Retrieve the Root Node ----- */
		// Retrieve the root node so that the numeric bounds of the child
		// nodescan be determined.
		$this->SetQuery("SELECT * FROM Menu WHERE ID = @ID");
		$this->AddParameter("@ID", "1", SQLTYPE_VARCHAR);

		// retrieve the left and right value of the $root node
		$result = $this->Execute();

		$rootNode = $result->fetch_object();

		if ($rootNode === false) { return; }

		/* ----- Retrieve the Menu ----- */
		// Retrieve the descendants of the root node.
		$this->SetQuery("SELECT * FROM Menu WHERE LeftVal BETWEEN " . $rootNode->LeftVal . " AND " .
								$rootNode->RightVal . " ORDER BY LeftVal ASC");

		// retrieve the left and right value of the $root node
		$result = $this->Execute();

		if ($result === false) { return; }

		$rootNode = $result->fetch_object();


		/* ----- Retrieve the Menu ----- */

		// create an empty stack to hold the RightNode values
		$right = array();

		// Retrieve the descendants of the root node.
		$this->SetQuery("SELECT * FROM Menu WHERE LeftVal BETWEEN " . $rootNode->LeftVal . " AND " .
								$rootNode->RightVal . " ORDER BY LeftVal ASC");

		$result = $this->Execute();

		$menuTitle = 0;
		$menuURL = 1;
		$menuArray = array(array(), array());

		// display each row
		while ($currentNode = $result->fetch_object())
		{
			// The stack size is equal to the tree depth starting with row 0.
			$stack_size = count($right);

			// Only check stack if there is one.
			if ($stack_size > 0)
			{
				$temp_size = $stack_size;

				// Check if we should remove a node from the stack.
				while ($right[$stack_size - 1]->RightVal < $currentNode->RightVal)
				{
					array_pop($right);
					$stack_size--;
				}
			}

			if ($currentNode->ID > 1)
			{
				$href = "";
				for($i = 1; $i < $stack_size; $i++)
				{
					$href .= $right[$i]->EncodedTitle . "-" . $right[$i]->ID . "/";
				}

				$href .= $currentNode->EncodedTitle . "-" . $currentNode->ID;

				$menuArray[$menuTitle][] = $currentNode->Title;
				$menuArray[$menuURL][] = $href;
			}

			// add this node to the stack
			$right[] = $currentNode;
		}

		/* ----- Sort Array By Title ----- */
		$numItems = count($menuArray[$menuTitle]);
		for ($i = 0; $i < ($numItems - 1); $i++)
		{
			for ($j = $i + 1; $j < $numItems; $j++)
			{
				if (strcmp($menuArray[$menuTitle][$i], $menuArray[$menuTitle][$j]) > 0)
				{
					$tempTitle = $menuArray[$menuTitle][$i];
					$tempURL = $menuArray[$menuURL][$i];

					$menuArray[$menuTitle][$i] = $menuArray[$menuTitle][$j];
					$menuArray[$menuURL][$i] = $menuArray[$menuURL][$j];

					$menuArray[$menuTitle][$j] = $tempTitle;
					$menuArray[$menuURL][$j] = $tempURL;
				}
			}
		}

		return $menuArray;
	}

}

?>
