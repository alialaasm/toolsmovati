<?php

require_once("DALC.php");


class User extends DALC
{

	public $ID = 0;
	public $Username = "";
	public $Name = "";
	public $Password = "";
	public $Active = true;
	public $SuperUser = false;
	public $UserLevel = "test";	
	public $ClubId = 1;	

	// Optional Attributes
	public $HasAccess = false;

	public $IsNewRow = false;
	public $LoadError = false;


	public function User($ID = 0)
	{
		DALC::DALC();

		if ((!is_numeric($ID)) || $ID < 0) { $ID = 0; }

		if ($ID > 0)
		{
			$query = "SELECT * FROM `User` WHERE ID = @ID";

			$this->SetQuery($query);

			$this->AddParameter("@ID", $ID, SQLTYPE_INT);

			$result = $this->Execute();

			$row = $result->fetch_object();

			$vals = ($row === false) ? NULL : $row;

			$this->LoadError = ($row === false);

			$this->LoadValues($vals);
		}
		else
		{
			$this->IsNewRow = true;
		}
	}


	public function Update()
	{
		if ($this->IsNewRow)
		{
			$query = "INSERT INTO `User` (Username, Name, Password, Active, SuperUser, UserLevel, ClubId) VALUES (@Username, @Name, @Password, @Active, @SuperUser, @UserLevel, @ClubId)";
		}
		else
		{
			$query = "UPDATE `User` SET Username = @Username, Name = @Name, Password = @Password, Active = @Active, SuperUser = @SuperUser, UserLevel = @UserLevel, ClubId = @ClubId WHERE ID = @ID";
		}

		$this->SetQuery($query);

		$this->AddParameter("@Username", $this->Username, SQLTYPE_VARCHAR);
		$this->AddParameter("@Name", $this->Name, SQLTYPE_VARCHAR);
		$this->AddParameter("@Password", $this->Password, SQLTYPE_VARCHAR);
		$this->AddParameter("@Active", $this->Active, SQLTYPE_BOOL);
		$this->AddParameter("@SuperUser", $this->SuperUser, SQLTYPE_BOOL);
		$this->AddParameter("@UserLevel", $this->UserLevel, SQLTYPE_VARCHAR);
		$this->AddParameter("@ClubId", $this->ClubId, SQLTYPE_INT);		


		if ($this->IsNewRow)
		{
			$this->Execute();
			$this->ID = $this->InsertID();
			$this->IsNewRow = false;
		}
		else
		{
			$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);
			$this->Execute();
		}
	}



    public function Delete()
	{
		$this->SetQuery("DELETE FROM User WHERE ID = @ID");

		$this->AddParameter("@ID", $this->ID, SQLTYPE_INT);

		$this->Execute();
	}


	public function LoadValues($vals)
	{
		if ($vals != NULL)
		{
			if (isset($vals->ID)) $this->ID = intval($vals->ID);
			if (isset($vals->Username)) $this->Username = $vals->Username;
			if (isset($vals->Name)) $this->Name = $vals->Name;
			if (isset($vals->Password)) $this->Password = $vals->Password;
			if (isset($vals->Active)) $this->Active = (intval($vals->Active) > 0);
			if (isset($vals->SuperUser)) $this->SuperUser = (intval($vals->SuperUser) > 0);
			if (isset($vals->UserLevel)) $this->UserLevel = $vals->UserLevel;	
			if (isset($vals->ClubId)) $this->ClubId = $vals->ClubId;

			// Optional Attributes
			if (isset($vals->HasAccess)) $this->HasAccess = (intval($vals->HasAccess) > 0);
		}
		else
		{
		}
	}
}

?>