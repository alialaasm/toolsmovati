<?php

require_once("DALC.php");


class HomePageButton
{
	public $Title = "";
	public $Description = "";
	public $NavigateText = "";
	public $NavigateURL = "";
}


class HomePage extends DALC
{

	public $ModuleName = "HomePage";
	public $Button1;
	public $Button2;
	public $LoadError = false;


	public function HomePage()
	{
		DALC::DALC();

		$query = "SELECT * FROM ModuleData WHERE ModuleName = @ModuleName";

		$this->SetQuery($query);

		$this->AddParameter("@ModuleName", "HomePage", SQLTYPE_VARCHAR);

		$result = $this->Execute();

		$row = $result->fetch_object();

		$vals = ($row === false) ? NULL : $row;

		$this->LoadError = ($row === false);

		$this->LoadValues($vals);
	}


	public function Update()
	{
		$query = "	UPDATE
						`ModuleData`
					SET
						Var1 = @Var1, Var2 = @Var2, Var3 = @Var3, Var4 = @Var4,
						Var5 = @Var5, Var6 = @Var6, Var7 = @Var7, Var8 = @Var8
					WHERE
						ModuleName = @ModuleName";

		$this->SetQuery($query);

		$this->AddParameter("@Var1", $this->Button1->Title, SQLTYPE_TEXT);
		$this->AddParameter("@Var2", $this->Button1->Description, SQLTYPE_TEXT);
		$this->AddParameter("@Var3", $this->Button1->NavigateText, SQLTYPE_TEXT);
		$this->AddParameter("@Var4", $this->Button1->NavigateURL, SQLTYPE_TEXT);

		$this->AddParameter("@Var5", $this->Button2->Title, SQLTYPE_TEXT);
		$this->AddParameter("@Var6", $this->Button2->Description, SQLTYPE_TEXT);
		$this->AddParameter("@Var7", $this->Button2->NavigateText, SQLTYPE_TEXT);
		$this->AddParameter("@Var8", $this->Button2->NavigateURL, SQLTYPE_TEXT);

		$this->AddParameter("@ModuleName", $this->ModuleName, SQLTYPE_TEXT);

		$this->Execute();
	}


	public function LoadValues($vals)
	{
		if ($vals != NULL)
		{
			$this->Button1 = new HomePageButton();
			if (isset($vals->Var1)) $this->Button1->Title = $vals->Var1;
			if (isset($vals->Var2)) $this->Button1->Description = $vals->Var2;
			if (isset($vals->Var3)) $this->Button1->NavigateText = $vals->Var3;
			if (isset($vals->Var4)) $this->Button1->NavigateURL = $vals->Var4;

			$this->Button2 = new HomePageButton();
			if (isset($vals->Var5)) $this->Button2->Title = $vals->Var5;
			if (isset($vals->Var6)) $this->Button2->Description = $vals->Var6;
			if (isset($vals->Var7)) $this->Button2->NavigateText = $vals->Var7;
			if (isset($vals->Var8)) $this->Button2->NavigateURL = $vals->Var8;
		}
	}
}

?>