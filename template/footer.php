		<? if ($t_template == Template::$Primary) { ?>
		<? } else if ($t_template == Template::$Secondary) { ?>
        <div class="break"></div>
        </div>
        <div id="btmimg"></div>
		<? } ?>

 
        <div id="imagetitle">
        	Preview Our New Flagship Club Design!
        2 Amazing New Clubs Opening Soon in Ottawa and Waterloo!</div>
        <div id="imagescroll">
            <!-- "previous page" action --> 
            <a class="prevPage browse left"></a> 
            <!-- root element for scrollable --> 
			<div class="scrollable" id="infinite">     
                <!-- root element for the items --> 
                <div class="items" > 
                  <a href="<?=SITE_URL ?>images/template/preview/lg-cardio.jpg" title="Co-ed Cardio Centre with Personalized Televisions (Over 140 Pieces of Equiped)"><img src="<?=SITE_URL ?>images/template/preview/thumb-cardio.png" rel="#lg-cardio" /></a>
                  <a href="<?=SITE_URL ?>images/template/preview/lg-cafe.jpg" title="Healthy and Comfortable Members Only Lounge (Wi-Fi Enabled Cafe, Nutrition and Pro Shop, Complimentary Billiards)"><img src="<?=SITE_URL ?>images/template/preview/thumb-lounge.png" rel="#lg-lounge" /></a>
                  <a href="<?=SITE_URL ?>images/template/preview/lg-pool.jpg" title="Women's Only Salt Water Pool and Spa Centre - Seperate Family Pool & Kids Aquatic Programs!"><img src="<?=SITE_URL ?>images/template/preview/thumb-pool.png" rel="#lg-pool" /></a>
                  <a href="<?=SITE_URL ?>images/template/preview/lg-washroom.jpg" title="Luxurious Change Rooms and Private Showers (Steam and Dry Saunas)"><img src="<?=SITE_URL ?>images/template/preview/thumb-washroom.png" rel="#lg-washroom" /></a>
                  <a href="<?=SITE_URL ?>images/template/preview/lg-yoga.jpg" title="Over 150 Group Exercise Classes Each Week (Anti-Gravity Yoga, Hot Yoga, Outdoor Yoga & More)"><img src="<?=SITE_URL ?>images/template/preview/thumb-yoga.png" rel="#lg-yoga" /></a>
                  <a href="<?=SITE_URL ?>images/template/preview/lg-skystudio.jpg" title="Canada's First Sky Studio (Glass Sky Light Covered Studio with Open Glass Wall)"><img src="<?=SITE_URL ?>images/template/preview/thumb-skystudio.png" rel="#lg-skystudio" /></a>
                  <a href="<?=SITE_URL ?>images/template/preview/lg-gym.jpg" title="Multi-Sport Gymnasium (Adults & Kids Sports Activities)"><img src="<?=SITE_URL ?>images/template/preview/thumb-gym.png" rel="#lg-gym" /></a>
                  <a href="<?=SITE_URL ?>images/template/preview/lg-lobby.jpg" title="Welcoming Club Lobby (Plasma TV Wall and Water Fall"><img src="<?=SITE_URL ?>images/template/preview/thumb-lobby.png" rel="#lg-lobby" /></a>
                  <a href="<?=SITE_URL ?>images/template/preview/lg-cardio.jpg" title="Co-ed Cardio Centre with Personalized Televisions (Over 140 Pieces of Equiped)"><img src="<?=SITE_URL ?>images/template/preview/thumb-cardio.png" rel="#lg-cardio" /></a>
                  <a href="<?=SITE_URL ?>images/template/preview/lg-cafe.jpg" title="Healthy and Comfortable Members Only Lounge (Wi-Fi Enabled Cafe, Nutrition and Pro Shop, Complimentary Billiards)"><img src="<?=SITE_URL ?>images/template/preview/thumb-lounge.png" rel="#lg-lounge" /></a>
                  <a href="<?=SITE_URL ?>images/template/preview/lg-pool.jpg" title="Women's Only Salt Water Pool and Spa Centre - Seperate Family Pool & Kids Aquatic Programs!"><img src="<?=SITE_URL ?>images/template/preview/thumb-pool.png" rel="#lg-pool" /></a>
                  <a href="<?=SITE_URL ?>images/template/preview/lg-washroom.jpg" title="Luxurious Change Rooms and Private Showers (Steam and Dry Saunas)"><img src="<?=SITE_URL ?>images/template/preview/thumb-washroom.png" rel="#lg-washroom" /></a>
                  <a href="<?=SITE_URL ?>images/template/preview/lg-yoga.jpg" title="Over 150 Group Exercise Classes Each Week (Anti-Gravity Yoga, Hot Yoga, Outdoor Yoga & More)"><img src="<?=SITE_URL ?>images/template/preview/thumb-yoga.png" rel="#lg-yoga" /></a>
                  <a href="<?=SITE_URL ?>images/template/preview/lg-skystudio.jpg" title="Canada's First Sky Studio (Glass Sky Light Covered Studio with Open Glass Wall)"><img src="<?=SITE_URL ?>images/template/preview/thumb-skystudio.png" rel="#lg-skystudio" /></a>
                  <a href="<?=SITE_URL ?>images/template/preview/lg-gym.jpg" title="Multi-Sport Gymnasium (Adults & Kids Sports Activities)"><img src="<?=SITE_URL ?>images/template/preview/thumb-gym.png" rel="#lg-gym" /></a>
                  <a href="<?=SITE_URL ?>images/template/preview/lg-lobby.jpg" title="Welcoming Club Lobby (Plasma TV Wall and Water Fall"><img src="<?=SITE_URL ?>images/template/preview/thumb-lobby.png" rel="#lg-lobby" /></a>
				</div>
            </div>
            <div id="tooltip"></div>

            <!-- "next page" action --> 
            <a class="nextPage browse right"></a>
            <!-- overlay element -->
            <div class="simple_overlay" id="gallery">
            
                <!-- "previous image" action -->
                <a class="prev">prev</a>
            
                <!-- "next image" action -->
                <a class="next">next</a>
            
                <!-- image information -->
                <div class="info"></div>
            
                <!-- load indicator (animated gif) -->
                <img class="progress" src="http://static.flowplayer.org/tools/img/overlay/loading.gif" />
            </div>
            
      	</div>
        <div id="footerhomemain">
                <div class="cta red" id="action01">
                                    <div class="actionimg">
                                        <img src="<?=SITE_URL ?>images/template/cta/thumb-lobby.png" width="70" height="61" />
                                    </div>
                                    <div class="actionmain" onclick="window.location = SITE_URL + 'our-clubs-20/new-locations-31'; return false;">
                                        <p class="actionhead">New Clubs<br />
                                          Opening Soon!
                                        </p>
                                        <p class="actiontext">Preview The Clubs!</p>
                                    </div>
                                </div>
                <div class="cta black" id="action02">
                                    <div class="actionimg">
                                        <img src="<?=SITE_URL ?>images/template/cta/thumb-group.png" width="70" height="61" />
                                    </div>
                                    <div class="actionmain" onclick="window.location = SITE_URL + 'activities-21/group-fitness-schedules-32'; return false;">
                                        <p class="actionhead">Group Class<br />
                                          Schedules!
                                        </p>
                                        <p class="actiontext">Class Schedules</p>
                                    </div>
                                </div>
        		<div class="cta black" id="action03">
                	<div class="actionimg">
               	    	<img src="<?=SITE_URL ?>images/template/cta/thumb-testi.png" width="70" height="61" />
                    </div>
                	<div class="actionmain" onclick="window.location = SITE_URL + 'member-testimonials-93'; return false;">
						<p class="actionhead">Real Life,<br />
						  Real Stories!
						</p>
						<p class="actiontext">Member Testimonials</p>
					</div>
                </div>
        		<div class="cta black" id="action04">
                	<div class="actionimg">
               	    	<img src="<?=SITE_URL ?>images/template/cta/thumb-location.png" width="70" height="61" />
                    </div>
                	<div class="actionmain" onclick="window.location = SITE_URL + 'contact-5'; return false;">
						<p class="actionhead">Find a Club<br />
						  Near You!
						</p>
						<p class="actiontext">Maps &amp; Locations</p>
					</div>
                </div>
        </div>
    </div>

    <div class="simple_overlay" id="gallery">
        <div class="info"></div>
        <img class="progress" src="<?=SITE_URL ?>images/loading2.gif" />
    </div>
    <? if ($t_template == Template::$Primary) { ?>
    <? $video01=1;?>
	<? $video02=2;?>
        <div class="overlay" id="mies1">
                <a 
                    href="http://www.theathleticclubs.ca/media/videos/video<?=rand($video01, $video02)?>.flv"  
                     style="display:block;width:720px;height:396px;margin-left:auto;margin-right:auto; padding-top: 9px; padding-bottom: 9px; background-color:#000;"  
                     id="player"> 
                </a> 
                <script>
                    flowplayer("player", "http://www.theathleticclubs.ca/videos/flowplayer-3.2.0.swf",  {
                        clip: {
                            // these two configuration variables does the trick
                            autoPlay: true, 
                            autoBuffering: true,
							forceStop: false,
							//When video play is finished
							onFinish: function()
							{ 
								//hide the controlbar
								$f().getPlugin("play").hide();
								$("div[id*=mies1]").data("overlay").close();
							}

                        }
                    });
                </script>
        </div>
 <script>
        // What is $(document).ready ? See: http://flowplayer.org/tools/documentation/basics.html#document_ready
        $(function() {
                  
		  setTimeout(function () {
		  	$("div[id*=mies1]").overlay({api: true, expose: '#000', closeOnClick: true}).load();
		  	}, 1000);
		});

</script>
<? } ?>

<script>
$(document).ready(function() {
	  $(".scrollable")
		.scrollable({clickable: false})
		.mousewheel(50)
		.circular().find("a")
		.overlay({target: '#gallery',expose: '#000',closeOnClick: true})
		.gallery({disabledClass: 'inactive',autohide: false, template: '<strong>${title}</strong>'});
});
</script>

<ul id="copyfoot">
	<li class="left">
    	<a href="<?=SITE_URL ?>about-2/privacy-policy-86" class="legallink">PRIVACY POLICY</a>
    </li>
    
    <li class="right">
    	2008 - <? print date("Y")?> THE ATHLETIC CLUB. All rights reserved.
    </li>
</ul>
<div style="clear:both;">&nbsp;</div>
</td></tr>
</table>
<script>
	flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.3.swf", {
			
		// this is the player configuration. You'll learn on upcoming demos.
		plugins:  {
			controls:  {
				volume: false		
			}
		}
	});
</script>
</body>
</html>