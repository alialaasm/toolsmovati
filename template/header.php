<?

require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/Menu.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ActionButtonManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/BannerManager.php");

$menu = new Menu();

$actionButtonManager = new ActionButtonManager();
$actionButtons = $actionButtonManager->SelectByMenuNodeID($selectedMenuID);

$bannerManager = new BannerManager();
$banner = $bannerManager->SelectByMenuNodeID($selectedMenuID);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<base href="<?=SITE_URL ?>" />
<title>The Athletic Club</title>
<link rel="stylesheet" type="text/css" href="<?=SITE_URL ?>css/tacglobal.css" />
<link rel="stylesheet" type="text/css" href="<?=SITE_URL ?>css/scrollable-horizontal.css" />
<link rel="stylesheet" type="text/css" href="<?=SITE_URL ?>css/scrollable-buttons.css" />
<link rel="stylesheet" type="text/css" href="<?=SITE_URL ?>css/overlay-basic.css"/>
<link rel="stylesheet" type="text/css" href="<?=SITE_URL ?>css/navigation.css"/>

<? if ($template == "blue") { ?>
	<link rel="stylesheet" type="text/css" href="<?=SITE_URL ?>css/blue.css"/>
<? } ?>
<script type="text/javascript" src="/js/flowplayer-3.2.0.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/flowplayerstyle.css">
<?
	$ccbg = array('bee','ladybug','greenhills','farm','leafs','field');
	
?>
	<link rel="stylesheet" type="text/css" href="<?=SITE_URL ?>css/<?=$ccbg[rand(0,5)] ?>.css"/>
<script src="<?=SITE_URL ?>js/jquery.tools.min.js"></script>
<script src="<?=SITE_URL ?>js/navigation.js" type="text/javascript"></script>
	    <script type="text/javascript" src="<?=SITE_URL ?>js/jquery.cookie.js"></script>
		<script type="text/javascript" src="<?=SITE_URL ?>js/swfobject.js"></script>
		<script type="text/javascript" src="<?=SITE_URL ?>js/rollover.js"></script>
		<script type="text/javascript" src="<?=SITE_URL ?>js/popup.js"></script>
<!-- <script src="<?//=SITE_URL ?>js/time.js" type="text/javascript"></script>-->
<script type="text/javascript">
	var SITE_URL = "<?=SITE_URL ?>";
	var MODULE_URL = "<?=MODULE_URL ?>";
</script>
<style>

/* styling of the container. */
a.myPlayer {
	display:block;
	width: 364px;
	height:200px; 
	text-align:center;
	margin:0 15px 15px 0;
	float:left;
	border: none;
}

/* play button */
a.myPlayer img {
	margin-top:5px;
	border:0px;
}

/* when container is hovered we alter the border color */
a.myPlayer:hover {
}
</style>

</head>

	
    	<?php
			function curPageURL() {
			 $pageURL = 'http';
			 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
			 $pageURL .= "://";
			 if ($_SERVER["SERVER_PORT"] != "80") {
			  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
			 } else {
			  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
			 }
			 return $pageURL;
			}
		?>
		<body>
	<table id="bg02" cellpadding="0" cellspacing="0"><tr><td>
	<div class="wrapper">
    	<div id="header">
       	  <div id="ad01" onclick="window.location = SITE_URL + 'activities-21/whats-new-90'; return false;">
            </div>
            <h1 id="logo"><a href="http://www.theathleticclubs.ca">Fitness Club - The Athletic Club<span></span></a></h1>
        	<div id="ad03" onclick="window.location = SITE_URL + 'membership-13/lose-it-weight-loss-fitness-challenge-99'; return false;">
            </div>
        </div>
	</div>
    <div id="navwrap">
        <div class="leftnav">
        </div>
        <div id="navmain">
            <?=$menu->RenderForViewing(1, 1); ?>
        </div>
        <div class="rightnav">
        </div>
	</div>
	<div class="wrapper">

			<? if ($template == Template::$Primary) { ?>
              <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="955" height="300" id="FlashID" title="Home Animation">
                          <param name="movie" value="/home_animationv4.swf" />
                          <param name="quality" value="high" />
                          <param name="wmode" value="transparent" />
                          <param name="swfversion" value="9.0.45.0" />
                          <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
                          <param name="expressinstall" value="../Scripts/expressInstall.swf" />
                          <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
                          <!--[if !IE]>-->
                          <object type="application/x-shockwave-flash" data="/home_animationv4.swf" width="955" height="300">
                            <!--<![endif]-->
                            <param name="quality" value="high" />
                            <param name="wmode" value="transparent" />
                            <param name="swfversion" value="9.0.45.0" />
                            <param name="expressinstall" value="/Scripts/expressInstall.swf" />
                            <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
                            <div>
                              <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
                              <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>
                            </div>
                            <!--[if !IE]>-->
                </object>
                          <!--<![endif]-->
              </object>
<? } else if ($template == Template::$Secondary) { ?>

				<div class="content">
					<div id="columnleft">
						<? if ($showSubMenu) { ?>
                            <div id="subnav">
                                <div id="subnavtop"><img src="../images/subNav-top.png" width="218" height="17" /></div>
                                    <div id="subnavcontent">
                                    <?=$menu->RenderForViewing($mainMenuID, 2); ?></div>
                                <div id="subnavbottom"><img src="../images/subNav-btm.png" width="218" height="20" alt="" /></div>
                            </div>
						<? } ?>

						<?  while ($actionButton = $actionButtons->NextItem()) { ?>
                        				<? if (curPageURL() == SITE_URL.$actionButton->URL) { ?>
            								<link rel="stylesheet" type="text/css" href="<?=SITE_URL ?>css/<?=$actionButton->Colour ?>.css"/>
                                        <a id="activeCallToAction" class="callToAction <?=$actionButton->Colour ?>CallToAction" href="quick-action" onclick="window.location = SITE_URL + '<?=$actionButton->URL ?>'; return false;">
										<? } else {?>
								<a class="callToAction <?=$actionButton->Colour ?>CallToAction" href="quick-action" onclick="window.location = SITE_URL + '<?=$actionButton->URL ?>'; return false;">
                                	<? } ?>
									<?=$actionButton->Title ?><br />
									<div class="subTitle">&diams; <?=$actionButton->SubTitle ?></div>
								</a>
						<? } ?>
						<? if ($showSubMenu) { ?>
                      </div>
						<? } ?>
						<? if ($banner) { ?>
						<div id="columnright">
                          <div id="anchorimage">
                              <div id="anchortop"><img src="../images/anchor_t.png" width="215" height="5" alt="" /></div>
                                  <div id="anchorcontent">
                                      <div id="imagehoder">
                                            <img src="<?=BANNER_GALLERY . $banner->Filename ?>" alt="<?=$banner->Title ?>" /></div>
                                  </div>
                              <div id="anchorbottom"><img src="../images/anchor_b.png" width="215" height="9" alt="" /></div>
                             </div>
                             <div style="clear:both;"></div>
                        </div>
						<? } ?>
							
			<? } ?>    
	<script type="text/javascript">
<!--
swfobject.registerObject("FlashID");
//-->
    </script>
