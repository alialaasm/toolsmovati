<?php



class Template
{
	public static $Primary = "primary";
	public static $Secondary = "secondary";
	public static $Admin = "admin";
}


function InsertHeader($template = Template::Primary, $mainMenuID = 0, $selectedMenuID = 0, $showSubMenu = true)
{
	global $t_template;

	$t_template = $template;

	if ($t_template == Template::$Admin)
	{
		include_once($_SERVER["DOCUMENT_ROOT"] . "/template/admin_header.php");
	}
	else
	{
		include_once($_SERVER["DOCUMENT_ROOT"] . "/template/header.php");
	}
}

function InsertFooter()
{
	global $t_template;

	if ($t_template == Template::$Admin)
	{
		include_once($_SERVER["DOCUMENT_ROOT"] . "/template/admin_footer.php");
	}
	else
	{
		include_once($_SERVER["DOCUMENT_ROOT"] . "/template/footer.php");
	}
}

?>
