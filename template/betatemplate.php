<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../css/tacglobal.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../css/scrollable-horizontal.css" />
<link rel="stylesheet" type="text/css" href="../css/scrollable-buttons.css" />
<link rel="stylesheet" type="text/css" href="../css/overlay-basic.css"/>
<link rel="stylesheet" type="text/css" href="../css/navigation.css"/>


<script src="../js/jquery.tools.min.js"></script>
<script src="../js/navigation.js" type="text/javascript"></script>
<script src="../js/time.js" type="text/javascript"></script>

</head>

<body>
	<table id="bg02" cellpadding="0" cellspacing="0"><tr><td>
	<div class="wrapper">
    	<div id="header">
       	  <div id="ad01">
            </div>
            <h1 id="logo"><a href="http://www.theathleticclubs.ca">Fitness Club - The Athletic Club<span></span></a></h1>
        	<div id="ad03">
            </div>
        </div>
	</div>
    <div id="navwrap">
	<div class="leftnav">
    </div>
        <div id="navmain">
    <ul class="level_1">
                <li><a href="#" class="nav_item">Menu</a>
                	<ul class="level_2">
                    	<li><a href="#">Menu Types</a></li>
                    </ul>
            </li>
                <li><a href="#" class="nav_item">About</a>
                	<ul class="level_2">
                    	<li><a href="#">Menu About</a></li>
                    </ul>
              </li>
                <li><a href="#" class="nav_item">Membership</a>
                	<ul class="level_2">
                    	<li><a href="#">Menu Membership</a></li>
                    </ul>
            </li>
                <li><a href="#" class="nav_item">Our Club</a></li>
                <li><a href="#" class="nav_item">Activities</a></li>
                <li><a href="#" class="nav_item">Services</a></li>
                <li><a href="#" class="nav_item">Health & Wellness</a></li>
                <li><a href="#" class="nav_item">Careers</a></li>
                <li><a href="#" class="nav_item">Contact</a></li>
            </ul>
        </div>
	<div class="rightnav">
    </div>
	</div>
	<div class="wrapper">
        <div id="main">
          <p><img src="../images/template/slides/temp-slide01.png" width="943" height="272" /></p>
</div>
        <div id="imagetitle">
        	Preview Images
        </div>
        <div id="imagescroll">
            <!-- "previous page" action --> 
            <a class="prevPage browse left"></a> 
            <!-- root element for scrollable --> 
			<div class="scrollable" id="infinite">     
                <!-- root element for the items --> 
                <div class="items" > 
                    <!-- 1-5 -->
                  <a href="../images/template/preview/lg-cardio.jpg" title="Co-ed Cardio Centre with Personalized Televisions (Over 140 Pieces of Equiped)"><img src="../images/template/preview/thumb-cardio.png" rel="#lg-cardio" /></a>
                  <img src="../images/template/preview/thumb-lounge.png" rel="#lg-lounge" />
                  <img src="../images/template/preview/thumb-pool.png" rel="#lg-pool" />
                  <img src="../images/template/preview/thumb-washroom.png" rel="#lg-washroom" />
        		  <img src="../images/template/preview/thumb-yoga.png" rel="#lg-yoga" />
                    <!-- 5-10 -->
                  <img src="../images/template/preview/thumb-cardio.png" rel="#lg-cardio" />
                  <img src="../images/template/preview/thumb-lounge.png" rel="#lg-lounge" />
                  <img src="../images/template/preview/thumb-pool.png" rel="#lg-pool" />
                  <img src="../images/template/preview/thumb-washroom.png" rel="#lg-washroom" />
                  <img src="../images/template/preview/thumb-yoga.png" rel="#lg-yoga" />
                    <!-- 10-15 --> 
                  <img src="../images/template/preview/thumb-cardio.png" rel="#lg-cardio" />
                  <img src="../images/template/preview/thumb-lounge.png" rel="#lg-lounge" />
                  <img src="../images/template/preview/thumb-pool.png" rel="#lg-pool" />
                  <img src="../images/template/preview/thumb-washroom.png" rel="#lg-washroom" />
        <img src="../images/template/preview/thumb-yoga.png" rel="#lg-yoga" />
                    <!-- 15-20 --> 
                  <img src="../images/template/preview/thumb-cardio.png" rel="#lg-cardio" />
                  <img src="../images/template/preview/thumb-lounge.png" rel="#lg-lounge" />
                  <img src="../images/template/preview/thumb-pool.png" rel="#lg-pool" />
                  <img src="../images/template/preview/thumb-washroom.png" rel="#lg-washroom" />
                  <img src="../images/template/preview/thumb-yoga.png" rel="#lg-yoga" />
				</div>
            </div>
            <div id="tooltip"></div>

            <!-- "next page" action --> 
            <a class="nextPage browse right"></a>
      	</div>
        <div id="footerhomemain">
<div class="cta red" id="action01">
                	<div class="actionimg">
               	    	<img src="../images/template/cta/thumb-lobby.png" width="70" height="61" />
                    </div>
                	<div class="actionmain">
						<p class="actionhead">New Clubs<br />
						  Opening Soon!
						</p>
						<p class="actiontext">Preview The Clubs!</p>
					</div>
                </div>
<div class="cta black" id="action02">
                	<div class="actionimg">
               	    	<img src="../images/template/cta/thumb-lobby.png" width="70" height="61" />
                    </div>
                	<div class="actionmain">
						<p class="actionhead">Group Class<br />
						  Schedules!
						</p>
						<p class="actiontext">Class Schedules</p>
					</div>
                </div>
        		<div class="cta black" id="action03">
                	<div class="actionimg">
               	    	<img src="../images/template/cta/thumb-lobby.png" width="70" height="61" />
                    </div>
                	<div class="actionmain">
						<p class="actionhead">Real Life,<br />
						  Real Stories!
						</p>
						<p class="actiontext">Member Testimonials</p>
					</div>
                </div>
        		<div class="cta black" id="action04">
                	<div class="actionimg">
               	    	<img src="../images/template/cta/thumb-lobby.png" width="70" height="61" />
                    </div>
                	<div class="actionmain">
						<p class="actionhead">Find a Club<br />
						  Near You!
						</p>
						<p class="actiontext">Maps &amp; Locations</p>
					</div>
                </div>
        </div>
    </div>

<div class="simple_overlay" id="gallery">
	<div class="info"></div>

	<img class="progress" src="http://static.flowplayer.org/tools/img/overlay/loading.gif" />
</div>
 
<script>
$(document).ready(function() {
	$(".scrollable").scrollable({clickable: false}).mousewheel(50).circular().find("a").overlay({target: '#gallery',expose: '#111',closeOnClick: true}).gallery({disabledClass: 'inactive',autohide: false,template:'<strong>${title}</strong>'});
});
</script>
<div style="clear:both;">&nbsp;</div>
</td></tr>
</table>
</body>
</html>