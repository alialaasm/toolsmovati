<?
	/*
	Silentum RndImg v1.0.0
	Modified June 2, 2008
	rndimg.php copyright 2008 "HyperSilence"
	*/

	// If you want more than five images, add more $img_# variables

	$img[] = "images/anchorimages/ballladies.jpg"; // 0
	$img[] = "images/anchorimages/boxingbreak.jpg"; // 1
	$img[] = "images/anchorimages/coach.jpg";
	$img[] = "images/anchorimages/couple.jpg";
	$img[] = "images/anchorimages/girlonball.jpg";
	$img[] = "images/anchorimages/greenshirtworkout.jpg";
	$img[] = "images/anchorimages/guycrunches.jpg";
	$img[] = "images/anchorimages/happywomen.jpg";
	$img[] = "images/anchorimages/kickboxing.jpg";
	$img[] = "images/anchorimages/massage.jpg";
	$img[] = "images/anchorimages/momandchild.jpg"; // 10
	$img[] = "images/anchorimages/momanddaughter.jpg";
	$img[] = "images/anchorimages/momandsonflying.jpg";
	$img[] = "images/anchorimages/partners.jpg";
	$img[] = "images/anchorimages/piggyback.jpg";
	$img[] = "images/anchorimages/pilates.jpg";
	$img[] = "images/anchorimages/pumpclass.jpg";
	$img[] = "images/anchorimages/squash.jpg";
	$img[] = "images/anchorimages/swimminggirl.jpg";
	$img[] = "images/anchorimages/swimmingguy.jpg";
	$img[] = "images/anchorimages/trainer.jpg"; // 20
	$img[] = "images/anchorimages/trainerandgirl.jpg";
	$img[] = "images/anchorimages/treadmillgirl.jpg";
	$img[] = "images/anchorimages/treadmillguy.jpg";
	$img[] = "images/anchorimages/weightgirl.jpg";
	$img[] = "images/anchorimages/yogaclass.jpg";
	$img[] = "images/anchorimages/yogaclass2.jpg";
	$img[] = "images/anchorimages/yogacouple.jpg";
	$img[] = "images/anchorimages/yogagirl.jpg";
	$img[] = "images/anchorimages/yogapose.jpg"; // 29

	$total_images = rand(0,(count($img) - 1)); // Change the 5 to the maximum number of images you want displayed

	echo "<img src=\"".$img[$total_images]."\" alt=\"\" />";
?>