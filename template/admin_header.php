<?php

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head runat="server">

	    <title>The Athletic Club - Content Management System</title>
	    <base href="<?=SITE_URL ?>" />
	    <link href="<?=SITE_URL ?>css/admin.css" type="text/css" rel="stylesheet" />
        <link href="<?=SITE_URL ?>css/adminschedule.css" rel="stylesheet" type="text/css" />

	    <script type="text/javascript" src="js/jquery-1.2.3.js"></script>
	    <script type="text/javascript">
	    	var SITE_URL = "<?=SITE_URL ?>";
	    	var ADMIN_URL = SITE_URL + "<?=ADMIN_DIR ?>";
	    </script>

	</head>

	<body>
	    <form id="frmAdmin" name="frmAdmin" action="<?=$_SERVER["REQUEST_URI"] ?>" enctype="multipart/form-data" method="post">
			<input type="hidden" id="txtAction" name="txtAction" value="" />

            <div class="header">
                <div class="top">
                    <div class="logo" style="text-align: center; width: 290px; margin: 0px auto;">
                      <img src="<?=SITE_URL ?>images/template/logo.png" alt="" width="290px"/><br />
                        Content Management System
                  </div>
                  <div class="logout"><a id="hLogOut" href="logout.php" runat="server"><img src="../rhinoflow/images/logoutButton.jpg" width="89" height="21" /></a></div>
                </div>

<table class="list" cellspacing="0" style="clear: both;">
            <tr>
                <th class="left"></th>
                <th class="adminNav" style="text-align:left;">
					<a href="rhinoflow/index.php">Home</a>
				<? if ($_SESSION["USER_LEVEL2"] == 'AD' || $_SESSION["USER_LEVEL2"] == 'SU' || $_SESSION["USER_LEVEL2"] == 'SA') { ?>
					<a href="rhinoflow/menu.php">Menu</a>
                    <? } ?>
				<? if ($_SESSION["USER_LEVEL2"] == 'SU') { ?>
					<a href="rhinoflow/actionButton.php">Action Buttons</a>
					<a href="rhinoflow/banner.php">Images</a>
					<a href="rhinoflow/user.php">Users</a>
					<a href="rhinoflow/studios.php">Studios</a>  
					<a href="rhinoflow/schedules.php">Schedules</a>                                      
                <? } ?>
				<? if ($_SESSION["USER_LEVEL2"] == 'SA') { ?>
					<a href="rhinoflow/studios.php">Studios</a>
                <? } ?>
				<? if ($_SESSION["USER_LEVEL2"] == 'SA' || $_SESSION["USER_LEVEL2"] == 'SAL') { ?>
					<a href="rhinoflow/schedules.php">Schedules</a>
                <? } ?>
					<a href="rhinoflow/userselfEdit.php">Change Password</a></th>
                <th class="right"></th>
            </tr>
    </table>
                
            </div>
                <div class="wrap">
                    <div class="height_spacer"></div>
                    <div class="content">