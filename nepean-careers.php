<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>MOVATI ATHLETIC NEPEAN</title>
	<link rel="stylesheet" type="text/css" href="normalize.css">
	<style>

	/*===============================
	=            General            =
	===============================*/
	* { 
		margin:0; 
		padding:0; 
		text-shadow: none !important;
		font-family: "Gotham SSm 5r", "Gotham SSm A", "Gotham SSm B", sans-serif;
		font-size: 14px;
		-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;
	}
	body{ background-color: #292929; }
	/*=====  End of General  ======*/
	
	
	/*==============================
	=            Header            =
	==============================*/
	.header{
		background-image: url(http://tools.movatiathletic.com/mobile/images/logo_small.png);
		background-repeat: no-repeat;
		background-position: 1% 50%;
		background-color: #2D8790;
		border-bottom: 1px solid rgba(255, 255, 255, 0.4);
		height: 45px;
	}
	.back *, .back {
		background-color: transparent!important;
		border: none!important;
	}
	.header a.back{
		position: absolute;
		border: none;
		top: 5px;
		right: 5px;
		display: block;
		width: 100%;
		background-image: none!important;
		text-align:right;
		height: 1em;
	}

	.header a.back {
		display: block;
		border: none!important;
	}
	/*=====  End of Header  ======*/

	/*===============================
	=            Content            =
	===============================*/
	.heading{
		color:#fff;
	    font-size: 16px;
	    text-transform: uppercase;
	    font-weight: 500;
	    margin:0;
	    padding:20px 15px;
	}
	.content{
		padding: 0px;
		background-color: #292929;
	}
	p{
		color: #333333;
		margin:0 0 15px 0;
		line-height:1.2;
	}
	p strong{
		font-weight:bold;
		text-transform:uppercase;
		display:block;
		margin:0 auto 5px auto;
	}
	a { color:#0574d9; }
	.link-black { color:#000; }
	.views {
		text-align:center;
		margin:20px auto;
	}
	* {
		text-shadow: none !important;
		font-family: "Gotham SSm 5r", "Gotham SSm A", "Gotham SSm B", sans-serif;
		font-size: 14px;
	}
	.views a {
		border: 1px solid red;
		display: inline-block;
		width: 25%;
		border: 1px solid white;
		color: #444442 !important;
		text-align: center;
		background-color: #f0f0f0;
		padding: 10px 0 10px 0;
		vertical-align: middle;
		font-weight: normal !important;
		font-size: 12px;
		text-transform: uppercase;
		text-decoration:none;
		margin:0 5px 10px 5px;
	}	
	.views a:hover,
	.views a.active{
		background: #444442;
		color:#fff !important;
	}
	.message{
		color:#fff;
	    font-size: 14px;
	    font-weight: 500;
	    margin:0;
	    padding:0 15px;
	}
	/*=====  End of Content  ======*/
	
	

	/*=================================
	=            Accordian            =
	=================================*/
	.accordion-content p:last-of-type{ margin:0; }
	.accordion-toggle {
		font-weight: normal !important;
	    border: none;
	    border-top: 1px solid rgba(255, 255, 255, 0.4);
	    background-color: rgb(45, 135, 144);
	    height: 45px;
	    line-height: 45px;
	    padding: 0 56px 0 12px;
	    text-align: left;
	    position: relative;
	    font-size:18px;
	    color:#fff;
	    text-transform:initial;
	    cursor:pointer;
	}
  	.accordion-content {
  		display: none;
  		background: #f0f0f0 /*{c-body-background-color}*/;
		background-image: none !important;
		margin:0;
		padding: .7em 15px .7em 15px;
  	}
  	.accordion-content.default {display: block;}
  	.arrow { 
  		position: relative;
  		top:10%;
  		padding-left:5px;
  	}
	/*=====  End of Accordian  ======*/
	@media all and (max-width: 1000px) {
  		.views a { width:46%; margin:0 1% 10px 1%;}
  	}
	@media all and (max-width: 515px) {
		.accordion-toggle{ font-size: 14px;}
		.arrow { 
  		position: absolute; top:15%; right:10px; padding-left:0; }
  		.views a { width:80%}
  	}
	

	
	</style>
</head>
<body>
<div class="header">
	<? if(isset($_GET["m"])){ 
		$mobile = $_GET["m"];
	?>
		<? if ($mobile == "yes") { ?>
			<a href="#" class="back" onclick="history.go(-1); return false;"><img src="mobile/back.png"></a>
		<? } ?>
	<? } ?>
	</div>
</div>	

<? if(isset($_GET["position-type"])){ 
	$position_type = $_GET["position-type"];
?>

<?
	// set default timezone
	date_default_timezone_set('America/Detroit');
	$today = date("Y-m-d"); 

?>

<div class="content">

	<? if ($position_type == "leadership-admin") { ?>
	<!-- LEADERSHIP & ADMIN POSITIONS -->
		<section id="leadership-admin">
			<h2 class="heading">MOVATI ATHLETIC NEPEAN - LEADERSHIP &amp; ADMIN POSITIONS</h2>
			<div id="accordion">
				<? 
					$posting_date = "2016-03-26"; 
					if ($posting_date <= $today) {
				?>
				<div class="accordian-item">
				  <h4 class="accordion-toggle">Club General Manager <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
				  <div class="accordion-content">
				    <p>The Club General Manager is a seasoned leader that is responsible for the Clubâ€™s overall production, leadership, coordination, operation and success. The Club General Manager is responsible for ensuring the Club meets or exceeds financial goals, creates and sustains a high performance work culture and provides strong leadership in areas of customer service, team management, operations management and sales performance.  The Club General Manager acts as a brand ambassador and exemplifies the organizationâ€™s mission, vision, philosophy and values.</p>
				    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
				    <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
				    <p><strong>AVAILABLE POSITIONS:</strong> 1</p>
				    <p><strong>ACCEPTING APPLICATIONS:</strong> March 26, 2016</p>
				  </div>
			  </div>
			  <? } ?>
			  <? 
					$posting_date = "2016-02-17"; 
					if ($posting_date <= $today) {
			  ?>
			  <div class="accordian-item">
				  <h4 class="accordion-toggle">Sales Manager <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
				  <div class="accordion-content">
				    <p>The Club Membership Sales Manager is an experienced salesâ€™ professional who reports directly to the Club General Manager and has accountability to the Corporate Membership Sales Director.  This self-motivated and detailed oriented individual is focused on delivering membership sales results and providing leadership to the Club sales team. The Sales Manager is a people-oriented leader who effectively coaches, manages and motivates staff to achieve sales objectives and create a great customer experience.</p>
				    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
				    <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
				    <p><strong>AVAILABLE POSITIONS:</strong> 1</p>
				    <p><strong>ACCEPTING APPLICATIONS:</strong>  February 17, 2016</p>
				  </div>
			  </div>
			  <? } ?>
			  <? 
					$posting_date = "2016-8-01"; 
					if ($posting_date <= $today) {
			  ?>
			  <div class="accordian-item">
				  <h4 class="accordion-toggle">Concierge Services Manager <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
				  <div class="accordion-content">
				    <p>The Concierge Services Manager is an experienced professional who reports directly to the Club General Manager. This self-motivated and detailed oriented individual is responsible for continuously improve membership retention and the customer experience. The Concierge Services Manager is a people-oriented leader who effectively coaches, manages and motivates staff.</p>
				    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
				     <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
				    <p><strong>AVAILABLE POSITIONS:</strong> 1</p>
				    <p><strong>ACCEPTING APPLICATIONS:</strong> August 1, 2016</p>
				  </div>
			  </div>
			  <? } ?>
			  <? 
					$posting_date = "2016-07-25"; 
					if ($posting_date <= $today) {
			  ?>
			  <div class="accordian-item">
				  <h4 class="accordion-toggle">Facilities Operations Manager <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
				  <div class="accordion-content">
				    <p>The Facilities Operations Manager is an experienced professional who reports directly to the Club General Manager and Corporate VP of Facilities. The Facilities Operations Manager is responsible for executing facilities improvements, assisting in the design and implementation of work orders and project planning associated with maintaining exceptional facilities and ensuring a safe, clean and welcoming environment for members and visitors. The Facilities Operations Manager is a people-oriented leader who effectively coaches, manages and motivates staff.</p>
				    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
				     <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
				    <p><strong>AVAILABLE POSITIONS:</strong> 1</p>
				    <p><strong>ACCEPTING APPLICATIONS:</strong>July 25, 2016</p>
				  </div>
			  </div>
			  <? } ?>
			  <? 
					$posting_date = "2016-07-02"; 
					if ($posting_date <= $today) {
			  ?>
			  <div class="accordian-item">
				  <h4 class="accordion-toggle">Personal Training Manager <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
				  <div class="accordion-content">
				    <p>Reporting directly to the Club General Manager, the Personal Training Manager is an experienced, self-motivated and detail-oriented fitness and sales professional who is responsible for building a strong Personal Training team and managing the Personal Training department by delivering member experience and results, operational efficiency, and financial performance. The Personal Training Manager is a people-oriented leader who effectively coaches, manages and motivates staff.</p>
				    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
				     <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
				    <p><strong>AVAILABLE POSITIONS:</strong> 1</p>
				    <p><strong>ACCEPTING APPLICATIONS:</strong> July 2, 2016</p>
				  </div>
			  </div>
			  <? } ?>
			  <? 
					$posting_date = "2016-08-08"; 
					if ($posting_date <= $today) {
			  ?>
			  <div class="accordian-item">
				  <h4 class="accordion-toggle">Assistant Personal Training Manager <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
				  <div class="accordion-content">
				    <p>Reporting directly to the Club Personal Training Manager the Assistant PTM  is an experienced, self-motivated and sales-oriented fitness professional who is responsible for mentoring a team of Personal Trainers with the Personal Training Manager to  deliver upon the member experience driving  results, operational efficiencies, and financial performance.  The incumbent work week will have 15-20 hours dedicated to servicing Personal Training sessions.  In addition s/he will be required to conduct 10 hours per week of Fitness Assessments or Demos and 10 hours per week of Employee/team development.</p>
				    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
				     <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
				    <p><strong>AVAILABLE POSITIONS:</strong></p>
				    <p><strong>ACCEPTING APPLICATIONS:</strong> August 8, 2016</p>
				  </div>
			  </div>
			  <? }  ?>
			</div>
		</section>
		<!-- End of LEADERSHIP & ADMIN POSITIONS -->
	<? } ?>



	<? if ($position_type == "supervisory") { ?>
	<!-- SUPERVISORY POSITIONS -->
		<section id="supervisory">
			<h2 class="heading">MOVATI ATHLETIC NEPEAN - SUPERVISORY POSITIONS</h2>
			<div id="accordion">
			  <? 
					$posting_date = "2016-09-20"; 
					if ($posting_date <= $today) {
			  ?>
				<div class="accordian-item">
				  <h4 class="accordion-toggle">Group Fitness Coordinator <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
				  <div class="accordion-content">
				    <p>Reporting to the General Manager and, responsible to the Group Fitness organization. Creating and maintaining a great team of instructors that will live up to Movati Athletic high standard of excellence! The incumbent will go above and beyond to uphold our mission statement for all members.</p>
				    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
				    <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
				    <p><strong>AVAILABLE POSITIONS:</strong> 1</p>
				    <p><strong>ACCEPTING APPLICATIONS:</strong> September 20, 2016 </p>
				  </div>
				</div>
				<? } ?>
				 <? 
					$posting_date = "2016-08-08"; 
					if ($posting_date <= $today) {
				 ?>
				<div class="accordian-item">
			  	  <h4 class="accordion-toggle">Concierge Services Supervisor <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
				  <div class="accordion-content">
				    <p>Under the direction of the Concierge Services Manager, the Concierge Services Supervisor is responsible for the coordination, direction and supervision of Concierge Services' Associates, Squash &amp; Fitness staff.  The Concierge Services Supervisor is responsible for promoting exceptional customer service practices throughout the club and for continuously improving membership retention and the customer experience. The Concierge Services Supervisor is a people-oriented supervisor who effectively coaches, mentors and motivates staff, and acts as a delegate to the Concierge Services Manager.</p>
				    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
				     <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
				    <p><strong>AVAILABLE POSITIONS:</strong> 1</p>
				    <p><strong>ACCEPTING APPLICATIONS:</strong> August 8, 2016</p>
				  </div>
			  </div>
			  <? } ?>
			  <? 
					$posting_date = "2016-08-08"; 
					if ($posting_date <= $today) {
			  ?>
			  <div class="accordian-item">
			  	  <h4 class="accordion-toggle">Aquatics Supervisor  (Lifeguard & Aquatics Classes) <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
				  <div class="accordion-content">
				    <p>Under the direction of the General Manager, the Youth Programs Supervisor is responsible for the coordination, direction and supervision of all Youth Programs (Youth and Aquatics Programs). The Youth Programs Supervisor is responsible for promoting and ensuring that Youth Programs provide fun, enriching, and safe programs that encourage member participation through a variety of interactive, educational and/or fitness based programs. The Youth Programs Supervisor is a people-oriented supervisor who effectively coaches, mentors and motivates staff.</p>
				    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
				     <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
				    <p><strong>AVAILABLE POSITIONS:</strong> 1</p>
				    <p><strong>ACCEPTING APPLICATIONS:</strong> August 8, 2016</p>
				  </div>
			  </div>
			  <? } ?>
			  <? 
					$posting_date = "2016-08-08"; 
					if ($posting_date <= $today) {
			  ?>
			  <div class="accordian-item">
			  	  <h4 class="accordion-toggle">Child Care Supervisor <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
				  <div class="accordion-content">
				    <p>Reporting to the General Manager, the Child Care Supervisor is an experienced, self-motivated and detail-oriented professional who is responsible for overseeing the day-to-day administration and functions of the Playroom.  The Child Care Supervisor is responsible for overseeing and providing a safe, engaging and entertaining environment for children of all ages.</p>
				    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
				     <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
				    <p><strong>AVAILABLE POSITIONS:</strong> 1</p>
				    <p><strong>ACCEPTING APPLICATIONS:</strong> August 8, 2016</p>
				  </div>
			  </div>
			  <? } ?>
			</div>
		</section>
		<!-- End of SUPERVISORY POSITIONS -->
	<? } ?>



	<? if ($position_type == "building-operations") { ?>
	<!-- BUILDING OPERATIONS -->
		<section id="building-operations">
			<h2 class="heading">MOVATI ATHLETIC NEPEAN - BUILDING OPERATIONS</h2>
			<div id="accordion">
			  <? 
					$posting_date = "2016-08-23"; 
					if ($posting_date <= $today) {
			  ?>
			  <div class="accordian-item">
			  	  <h4 class="accordion-toggle">Building Operator <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
				  <div class="accordion-content">
				    <p>Reporting to the Facilities Operations Manager, Building Operator is responsible for organizing, prioritizing and providing day-to-day assignment and direction to Maintenance Technicians to ensure the prompt and safe maintenance of all Club assets and equipment.  The Building Operator is responsible for the preventative maintenance; repair of all fitness equipment and facilities, ensuring a safe work environment and safe work practices.</p>
				    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
				    <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com">jobs@movatiathletic.com</a></p>
				    <p><strong>AVAILABLE POSITIONS:</strong> 1</p>
				    <p><strong>ACCEPTING APPLICATIONS:</strong> August 23, 2016</p>
				  </div>
			  </div>
			  <? } ?>
			  <? 
					$posting_date = "2016-08-23"; 
					if ($posting_date <= $today) {
			  ?>
			  <div class="accordian-item">
			  	  <h4 class="accordion-toggle">Maintenance Technician <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
				  <div class="accordion-content">
				    <p>Reporting to the Facilities Operations Manager, the Maintenance Technician is responsible for ensuring the prompt and safe maintenance of all Club assets and equipment.  The Maintenance Technician is responsible for the preventative maintenance; repair of all fitness equipment and facilities, ensuring a safe work environment and safe work practices.</p>
				    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
				     <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
				    <p><strong>AVAILABLE POSITIONS:</strong> 1</p>
				    <p><strong>ACCEPTING APPLICATIONS:</strong> August 23, 2016</p>
				  </div>
			  </div>
			  <? } ?>
			  <? 
					$posting_date = "2016-09-27"; 
					if ($posting_date <= $today) {
			  ?>
			  <div class="accordian-item">
			  	  <h4 class="accordion-toggle">Custodians <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
				  <div class="accordion-content">
				    <p>Reporting to the Facilities Operations Manager, the Custodian is responsible for ensuring that the Club and facilities are maintained in a healthy, safe and sanitary manner.</p>
				    <p><strong>APPLICANTS:</strong> Please drop off your resume at our Movati Club, Barrhaven</p>
				    <p><strong>AVAILABLE POSITIONS:</strong> 4 Full Time, 4 Part Time, 2 On Call</p>
				    <p><strong>ACCEPTING APPLICATIONS:</strong> September 27, 2016</p>
				  </div>
			  </div>
			   <? } ?>
			  <? 
					$posting_date = "2016-09-20"; 
					if ($posting_date <= $today) {
			  ?>
			  <div class="accordian-item">
			  	  <h4 class="accordion-toggle">Customer Service Associate/Custodians <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
				  <div class="accordion-content">
				    <p>Reporting to the Facilities Operations Manager, and under the direction of the Concierge Services Manager, the Customer Service Associate/Custodian is a hybrid role that is responsible for promoting exceptional customer service practices throughout the Club and for ensuring that the Club and facilities are maintained in a healthy, safe and sanitary manner.</p>
				    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
				    <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com">jobs@movatiathletic.com</a></p>
				    <p><strong>AVAILABLE POSITIONS:</strong> 2 (11pm - 7:00 am shift)</p>
				    <p><strong>ACCEPTING APPLICATIONS:</strong> September 20, 2016</p>
				  </div>
			  </div>
			  <? } ?>
			</div>
		</section>
		<!-- End of BUILDING OPERATIONS -->
	<? } ?>


	<? if ($position_type == "administration-functions") { ?>
	<!-- ADMINISTRATION FUNCTIONS -->
	<section id="administration-functions">
		<h2 class="heading">MOVATI ATHLETIC NEPEAN - ADMINISTRATION FUNCTIONS</h2>
		<div id="accordion">
		  <? 
				$posting_date = "2016-08-01"; 
				if ($posting_date <= $today) {
		  ?>
		  <div class="accordian-item">
		  	  <h4 class="accordion-toggle">Club Human Resource Business Partner (HRBP) <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
			  <div class="accordion-content">
			    <p>Reporting to the Area HR Manager, the Human Resources (HR) Generalist plays a critical functional leadership and support role as part of the Movati Athletic corporate team. The HR Business Partner manages the <a href="http://humanresources.about.com/od/careerinhr/fl/how-much-money-does-an-hr-generalist-make.htm" target="_blank" class="link-black">administration of human resources</a> policies, procedures and programs and provides advice, guidance and support across HR disciplines including: recruitment and selection, compensation and total rewards, organizational and job design, talent management, and organizational development.  Provides support in the fulfillment of Movati Athleticâ€™s mission, vision and philosophy.</p>
			    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
			     <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
			    <p><strong>AVAILABLE POSITIONS:</strong> 1</p>
			    <p><strong>ACCEPTING APPLICATIONS:</strong> August 1, 2016</p>
			  </div>
		  </div>
		  <? } ?>
		  <? 
				$posting_date = "2016-09-20"; 
				if ($posting_date <= $today) {
		  ?>
		  <div class="accordian-item">
		  	  <h4 class="accordion-toggle">Membership Coordinators<span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
			  <div class="accordion-content">
			    <p>Reporting to the Sales Manager, the Membership Coordinator is responsible for delivering and continuously improving membership sales results and performance.</p>
			    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account</p>
			     <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
			    <p><strong>AVAILABLE POSITIONS:</strong> 12 Full Time</p>
			    <p><strong>ACCEPTING APPLICATIONS:</strong> September 20, 2016</p>
			  </div>
		  </div>
		  <? } ?>
		  <? 
				$posting_date = "2016-09-20"; 
				if ($posting_date <= $today) {
		  ?>
		  <div class="accordian-item">
		  	  <h4 class="accordion-toggle">Customer Service Associates <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
			  <div class="accordion-content">
			    <p>Reporting to the Concierge Services Manager, the Customer Service Associate is responsible for promoting exceptional customer service practices throughout the Club and for striving to continuously improving membership retention and the customer experience.</p>
			    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account</p>
			    <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com">jobs@movatiathletic.com</a></p>
			    <p><strong>AVAILABLE POSITIONS:</strong> 6 Full Time, 16 Part Time</p>
			    <p><strong>ACCEPTING APPLICATIONS:</strong> September 20, 2016</p>
			  </div>
		  </div>
		  <? } ?>
		  <? 
				$posting_date = "2016-09-27"; 
				if ($posting_date <= $today) {
		  ?>
		  <div class="accordian-item">
		  	  <h4 class="accordion-toggle">Playroom Associates <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
			  <div class="accordion-content">
			    <p>Reporting to the Child Care Supervisor, the Playroom Associate is responsible for providing a safe, engaging and entertaining experience for children of all ages.  The Playroom Associate is responsible for the supervision of children, ensuring the safety, health and well-being of children at all times.</p>
			    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
			     <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
			    <p><strong>AVAILABLE POSITIONS:</strong> 2 Full Time, 16 Part Time</p>
			    <p><strong>ACCEPTING APPLICATIONS:</strong> September 27, 2016</p>
			  </div>
		  </div>
		  <? } ?>
		</div>
	</section>
	<!-- End of  ADMINISTRATION FUNCTIONS -->
	<? } ?>


	<? if ($position_type == "fitness-trainers") { ?>
	<!-- GROUP FITNESS & PERSONAL TRAINERS -->
	<section id="fitness-trainers">
		<h2 class="heading">MOVATI ATHLETIC NEPEAN - GROUP FITNESS &amp; PERSONAL TRAINERS</h2>
		<div id="accordion">
		  <? 
				$posting_date = "2016-07-13"; 
				if ($posting_date <= $today) {
		  ?>
		  <div class="accordian-item">
		  	  <h4 class="accordion-toggle">Personal Trainers <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
			  <div class="accordion-content">
			    <p>Reporting to the Personal Training Manager, the Personal Trainer is responsible for providing members with customized Personal Training programs and knowledge, motivation and coaching to achieve their personal fitness goals.  The Personal Trainer promotes exceptional customer service practices throughout the Club and strives to continuously improving the Personal Training service offering.</p>
			    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
			     <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
			    <p><strong>AVAILABLE POSITIONS:</strong> 20</p>
			    <p><strong>ACCEPTING APPLICATIONS:</strong> July 13, 2016</p>
			  </div>
		  </div>
		  <? } ?>
		  <? 
				$posting_date = "2016-09-30"; 
				if ($posting_date <= $today) {
		  ?>
		  <div class="accordian-item">
		  	  <h4 class="accordion-toggle">Group Fitness Instructors <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
			  <div class="accordion-content">
			    <p>Reporting to the Group Fitness Coordinator, the Group Fitness Instructor is responsible for providing best in class fitness instruction, knowledge, coaching and motivation to Club members to help them achieve their personal fitness goals.  The Group Fitness Instructor promotes exceptional customer service practices throughout the Club and strives to continuously improving the Group Fitness service offering.</p>
			    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
			     <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com">jobs@movatiathletic.com</a></p>
			    <p><strong>AVAILABLE POSITIONS:</strong> 20</p>
			    <p><strong>ACCEPTING APPLICATIONS:</strong> September 30, 2016</p>
			  </div>
		  </div>
		  <? } ?>
		  <? 
				$posting_date = "2016-09-27"; 
				if ($posting_date <= $today) {
		  ?>
		  <div class="accordian-item">
		  	  <h4 class="accordion-toggle">Lifeguard/Swim Instructor <span class="arrow"><img src="list-arrow.png" alt=">"></span></h4>
			  <div class="accordion-content">
			    <p>Reporting to the Youth Programs Supervisor, the Lifeguard & Swim Instructor is responsible for maintaining a safe aquatic environment at all times and for providing best in class swim instruction, knowledge, coaching and motivation to participants.  The Lifeguard & Swim instructor conducts swimming lessons in a safe and enjoyable environment, while demonstrating good class control and safety supervision.  The Lifeguard & Swim Instructor will respond to emergencies in the pool and on the pool deck, and will act as a first responder, as needed.</p>
			    <p><strong>EXTERNAL APPLICANTS:</strong> Pre-register by following this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=TACFIT&ccId=911055788_5579&type=MP&lang=en_CA" target="_blank">link</a> to set up your application account </p>
			     <p><strong>INTERNAL APPLICANTS:</strong> <a href="mailto:jobs@movatiathletic.com" class="link-black">submit</a> your resume &amp; interest to your club HRBP</p>
			    <p><strong>AVAILABLE POSITIONS:</strong> 8</p>
			    <p><strong>ACCEPTING APPLICATIONS:</strong> September 27, 2016</p>
			  </div>
		  </div>
		  <? } ?>
		</div>
	</section>
	<!-- End of  GROUP FITNESS & PERSONAL TRAINERS -->
	<? } ?>
</div>

<div class="views">
		<? if ($mobile == "yes") { ?>
			<? 
				$posting_date = "2016-02-17"; 
				if ($posting_date <= $today) {
			?>
			<a href="/nepean-careers.php?position-type=leadership-admin&m=yes" <? if ($position_type == "leadership-admin") { ?> class="active" <? } ?>>LEADERSHIP &amp; ADMINISTRATION</a>
			<? } ?>
			<? 
				$posting_date = "2016-08-08"; 
				if ($posting_date <= $today) {
			?>
			<a href="/nepean-careers.php?position-type=supervisory&m=yes" <? if ($position_type == "supervisory") { ?> class="active" <? } ?>>SUPERVISORY</a>
			<? } ?>
			<? 
				$posting_date = "2016-08-23"; 
				if ($posting_date <= $today) {
			 ?>
			<a href="/nepean-careers.php?position-type=building-operations&m=yes" <? if ($position_type == "building-operations") { ?> class="active" <? } ?>>BUILDING OPERATIONS</a>
			<? } ?>
			<? 
				$posting_date = "2016-08-01"; 
				if ($posting_date <= $today) {
			?>
			<a href="/nepean-careers.php?position-type=administration-functions&m=yes" <? if ($position_type == "administration-functions") { ?> class="active" <? } ?>>ADMINISTRATION</a>
			<? } ?>
			<? 
				$posting_date = "2016-07-13"; 
				if ($posting_date <= $today) {
			?>
			<a href="/nepean-careers.php?position-type=fitness-trainers&m=yes" <? if ($position_type == "fitness-trainers") { ?> class="active" <? } ?>>GROUP FITNESS &amp; PERSONAL TRAINERS</a>
			<? } ?>
		<? } else { ?> <!-- DESKTOP -->
			<? 
				$posting_date = "2016-02-17"; 
				if ($posting_date <= $today) {
			?>
			<a href="/nepean-careers.php?position-type=leadership-admin" <? if ($position_type == "leadership-admin") { ?> class="active" <? } ?>>LEADERSHIP &amp; ADMINISTRATION</a>
			<? } ?>
			<? 
				$posting_date = "2016-08-08"; 
				if ($posting_date <= $today) {
			?>
			<a href="/nepean-careers.php?position-type=supervisory" <? if ($position_type == "supervisory") { ?> class="active" <? } ?>>SUPERVISORY</a>
			<? } ?>
			<? 
				$posting_date = "2016-08-23"; 
				if ($posting_date <= $today) {
			?>
			<a href="/nepean-careers.php?position-type=building-operations" <? if ($position_type == "building-operations") { ?> class="active" <? } ?>>BUILDING OPERATIONS</a>
			<? } ?>
			<? 
				$posting_date = "2016-08-01"; 
				if ($posting_date <= $today) {
			?>
			<a href="/nepean-careers.php?position-type=administration-functions" <? if ($position_type == "administration-functions") { ?> class="active" <? } ?>>ADMINISTRATION</a>
			<? } ?>
			<? 
				$posting_date = "2016-07-13"; 
				if ($posting_date <= $today) {
			?>
			<a href="/nepean-careers.php?position-type=fitness-trainers" <? if ($position_type == "fitness-trainers") { ?> class="active" <? } ?>>GROUP FITNESS &amp; PERSONAL TRAINERS</a>
			<? } ?>
		<? } ?>
	</div>
<? } //End of position type ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<script type="text/javascript">
	  $(document).ready(function($) {
	  	if ($('div #accordion').children().length == 0){
	      		$('#accordion').append( "<p class='message'>No current positions available.</p>" );
	      	}
	    $('#accordion').find('.accordion-toggle').click(function(){

	      //Expand or collapse this panel
	      $(this).next().slideToggle('fast');

	      //Hide the other panels
	      $(".accordion-content").not($(this).next()).slideUp('fast');

	    });
	  });
	</script>
</body>
</html>