<?php
/*********************************************************************
 * This file should be included at the top of every page before any
 * other include files.
 *********************************************************************/

ini_set('session.gc_maxlifetime', 86400);
session_start();


date_default_timezone_set('America/Toronto');


ini_set("upload_max_filesize", 50000000);
ini_set("memory_limit", 50000000);


if (stristr($_SERVER["HTTP_HOST"], "localhost"))
{
	define("SITE_URL", "http://localhost/TheAthleticClub/");
	define("APP_PATH", "c:\\InetPub\wwwroot\\TheAthleticClub\\");
	define("ERROR_LOG_PATH", APP_PATH . "\\error.log");

	$_SERVER["DOCUMENT_ROOT"] = APP_PATH;

	error_reporting(E_ALL);
	ini_set('display_errors', true);

	// Set the include paths.
	ini_set("include_path", ini_get("include_path") . ";" . APP_PATH . "App_Code;" . APP_PATH . "BLL;" . APP_PATH . "DAL");

	$slash = "\\";

	include_once($_SERVER["DOCUMENT_ROOT"] . "\\template\\template.php");
	include_once($_SERVER["DOCUMENT_ROOT"] . "\\App_Code\\functions.php");
	include_once($_SERVER["DOCUMENT_ROOT"] . "\\App_Code\\MySql.php");


	define("DB_HOST", "external-db.s56630.gridserver.com");
}
else
{
	define("SITE_URL", "http://" . $_SERVER["HTTP_HOST"] . "/");
	define("APP_PATH", "/nfs/c03/h04/mnt/56630/domains/tools.movatiathletic.com/html/");
	#define("ERROR_LOG_PATH", "/nfs/c03/h04/mnt/56630/domains/theathleticclubs.ca/error_log/error.log");

	ini_set("include_path", ini_get("include_path") . ":" . APP_PATH . "/App_Code:" . APP_PATH . "/BLL:" . APP_PATH . "/DAL");

	// we will do our own error handling
	error_reporting(E_ALL);
	ini_set('display_errors', true);

	include_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/error_handler.php");
	$old_error_handler = set_error_handler("userErrorHandler");

	$slash = "/";

	//define("DB_HOST", $_SERVER["DATABASE_SERVER"]);
	define("DB_HOST", "external-db.s56630.gridserver.com");
}


include_once($_SERVER["DOCUMENT_ROOT"] . $slash . "template" . $slash . "template.php");
include_once($_SERVER["DOCUMENT_ROOT"] . $slash . "App_Code" . $slash . "functions.php");
include_once($_SERVER["DOCUMENT_ROOT"] . $slash . "App_Code" . $slash . "MySql.php");
include_once($_SERVER["DOCUMENT_ROOT"] . $slash . "App_Code" . $slash . "request.php");


define("ADMIN", 0);

define("IsPostBack", (count($_POST) > 0));


/* ----- PageURL ----- */
$url_parts = explode("\?", $_SERVER["REQUEST_URI"], 2);

define("PageURL", $url_parts[0]);


/* ----- Directories ----- */
define("MODULE_DIR", "modules");
define("ADMIN_DIR", "rhinoflow");

/* ----- URLs ----- */
define("MODULE_URL", SITE_URL . MODULE_DIR . "/");
define("ADMIN_URL", SITE_URL . ADMIN_DIR . "/");


/* ----- Thumbnail Sizes -----
define("MAX_DIS_WIDTH", 100);
define("MAX_DIS_HEIGHT", 2000);

define("BANNER_GALLERY_THUMBNAIL_WIDTH", 100);
define("BANNER_GALLERY_THUMBNAIL_HEIGHT", 10000);
define("BANNER_GALLERY", SITE_URL . "media/banners/fullsize/");
define("BANNER_GALLERY_PATH", APP_PATH . "media/banners/fullsize/");
define("BANNER_GALLERY_THUMBNAIL", SITE_URL . "media/banners/thumbnails/");
define("BANNER_GALLERY_THUMBNAIL_PATH", APP_PATH . "media/banners/thumbnails/");

define("BUTTON_GALLERY", SITE_URL . "media/quickActions/fullsize/");
define("BUTTON_GALLERY_PATH", APP_PATH . "media/quickActions/fullsize/");
define("BUTTON_GALLERY_DISPLAY", SITE_URL . "media/quickActions/display/");
define("BUTTON_GALLERY_DISPLAY_PATH", APP_PATH . "media/quickActions/display/");
 */


/* ----- Text Images ----- 
define("TEXT_GALLERY", SITE_URL . "media/text/fullsize/");
define("TEXT_GALLERY_PATH", APP_PATH . "media/text/fullsize/");
define("TEXT_GALLERY_THUMBNAILS", SITE_URL . "media/text/thumbs/");
define("TEXT_GALLERY_THUMBNAILS_PATH", APP_PATH . "media/text/thumbs/");
define("TEXT_GALLERY_DISPLAY", SITE_URL . "media/text/display/");
define("TEXT_GALLERY_DISPLAY_PATH", APP_PATH . "media/text/display/");
*/

/* ----- Thumbnail Sizes ----- */
define("MAX_DIS_WIDTH", 100);
define("MAX_DIS_HEIGHT", 2000);

define("BANNER_GALLERY_THUMBNAIL_WIDTH", 100);
define("BANNER_GALLERY_THUMBNAIL_HEIGHT", 10000);
define("BANNER_GALLERY", "http://beta.theathleticclubs.ca/media/banners/fullsize/");
define("BANNER_GALLERY_PATH", "/nfs/c03/h04/mnt/56630/domains/beta.theathleticclubs.ca/html/media/banners/fullsize/");
define("BANNER_GALLERY_THUMBNAIL", "http://beta.theathleticclubs.ca/media/banners/thumbnails/");
define("BANNER_GALLERY_THUMBNAIL_PATH", "/nfs/c03/h04/mnt/56630/domains/beta.theathleticclubs.ca/html/media/banners/thumbnails/");

define("BUTTON_GALLERY", "http://beta.theathleticclubs.ca/media/quickActions/fullsize/");
define("BUTTON_GALLERY_PATH", "/nfs/c03/h04/mnt/56630/domains/beta.theathleticclubs.ca/html/media/quickActions/fullsize/");
define("BUTTON_GALLERY_DISPLAY", "http://beta.theathleticclubs.ca/media/quickActions/display/");
define("BUTTON_GALLERY_DISPLAY_PATH", "/nfs/c03/h04/mnt/56630/domains/beta.theathleticclubs.ca/html/media/quickActions/display/");



/* ----- Text Images ----- */
define("TEXT_GALLERY", "http://beta.theathleticclubs.ca/media/text/fullsize/");
define("TEXT_GALLERY_PATH", "/nfs/c03/h04/mnt/56630/domains/beta.theathleticclubs.ca/html/media/text/fullsize/");
define("TEXT_GALLERY_THUMBNAILS", "http://beta.theathleticclubs.ca/media/text/thumbs/");
define("TEXT_GALLERY_THUMBNAILS_PATH", "/nfs/c03/h04/mnt/56630/domains/beta.theathleticclubs.ca/html/media/text/thumbs/");
define("TEXT_GALLERY_DISPLAY", "http://beta.theathleticclubs.ca/media/text/display/");
define("TEXT_GALLERY_DISPLAY_PATH", "/nfs/c03/h04/mnt/56630/domains/beta.theathleticclubs.ca/html/media/text/display/");




/* ----- Database Settings ----- */
define("DB_USERNAME", "db56630_betaad");
define("DB_PASSWORD", "fitness123");
define("DB_NAME", "db56630_tacbeta");

$db = new MySql(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

error_reporting(E_ALL);

?>
