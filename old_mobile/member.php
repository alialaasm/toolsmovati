<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/config.php");
#echo ".";
include_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/Member.php");
#echo "?";

$member = get_session_member();

function get_session_member(){

	if ($_SESSION["member"]){
		return new Member($_SESSION["member"]);
	}
	
	return 0;
}

function set_session_member($id){
	$_SESSION["member"] = $id;	
}

#echo "A? ";

$result = "";

if (post_text("action") == "register"){
	$member = new Member();

	$member->Email = post_text('email');
	$member->ClubId = post_int('club');
	$member->Password = post_text('password');
	
	$problem = "";
	
	if ($member->Email == ''){
		$problem = "Please enter your email address below.";	
	}

	if ($member->Password == ''){
		$problem = "Please enter your password.";
	}
					

	if ($member->Password != post_text('password2')){
		$problem = "Your passwords don't match";
	}


	if ($problem == ""){
		$member->Update();
		set_session_member( $member->ID );

		$result = 'OK';
	}else{
		$result = $problem;
	}
}

if (post_text("action") == "add_class"){
	$result = "test";
	$member->AddClass( post_int("class") );
}

if (post_text("action") == "remove_class"){
        $member->RemoveClass( post_int("class") );
}

if (post_text("action") == "login"){
	$_member = new Member();
	$_member->Login( post_text("email"), post_text("password"));

	if ($_member->ID > 0){
		set_session_member( $_member->ID );
		$result = 'OK';
	}else{
		$result = 'Login Failed';
	}
}

if (get_text("action") == "logout"){
	set_session_member( 0 );
	
	header("Location: /mobile/");
}

?>
<?= $result ?>
