var data = {
		
	
	journal_plan: {
		protein: 3,
		starch: 3,
		fruit: 3,
		vegetable: 3,
		dairy: 2,
		fat: 1,
		free_exchange: 1,
		meal_replacement : 0
	},

"menu_options":{"6":["Cereal - Cheerios","starch",{"s":"1.00"}],"11":["Pasta","starch",{"s":"0.50"}],"3":["Eggs","protein",{"p":"1.00"}],"7":["Cereal - Corn Flakes","starch",{"s":"1.00"}],"9":["Melba Toast","starch",{"s":"1.00"}],"12":["Health Choice Pasta Sauce","vegetable",{"v":"0.50"}],"2":["Chicken Breast","protein",{"p":"1.00"}],"14":["Wine","beverage",{"x":"1.00"}],"8":["English Muffin","starch",{"s":"1.00"}],"1":["Beef, lean","protein",{"p":"1.00"}],"4":["Poulty, dark meat","protein",{"p":"1.00"}],"13":["Light Beer","beverage",{"x":"1.00"}],"10":["Lean Cuisine","frozen_food",{"v":"0.50","f":"0.50","s":"0.50"}],"5":["Diet Bread","starch",{"s":"1.00"}]}

};
