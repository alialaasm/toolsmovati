<?php
/*********************************************************************
 * FILE: class.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Displays a content section of the site.
 * *****************************************************************
 *
 */
session_start();

/*touch screen mod: 1 of 2*/
require_once("config.php");

if (isset($_GET['LOCATION']) && !isset($_GET['STUDIO'])){
	
	mysql_connect(DB_HOST, DB_USERNAME, DB_PASSWORD) or
		die("Could not connect: " . mysql_error());
	mysql_select_db(DB_NAME);
	
        #quick and dirty fix by ian barton (ian@opid.ca - 2016-05-26)
	# all mysql_functions are dangerous though

	$result = mysql_query("SELECT ID FROM Studio WHERE ClubId = ".intval($_GET['LOCATION']));
	#$result = mysql_query("SELECT ID FROM Studio WHERE ClubId = ".$_GET['LOCATION']);
	
	$row = mysql_fetch_row($result);

	$_GET['STUDIO'] = $row[0];
}

/*end touch screen mod: 1 of 2*/

if(!isset($_GET['LOCATION'])) $_GET['LOCATION']=1;
if(!isset($_GET['STUDIO'])) $_GET['STUDIO']=1;
if(!isset($_GET['MONTH'])) $_GET['MONTH']=5;
if(!isset($_GET['YEAR'])) $_GET['YEAR']=2010;

$_GET['LOCATION'] = intval($_GET['LOCATION']);
$_GET['STUDIO'] = intval($_GET['STUDIO']);
$_GET['MONTH'] = intval($_GET['MONTH']);
$_GET['YEAR'] = intval($_GET['YEAR']);


/*touch screen mod: 2 of 2*/
//require_once("config.php");
/*end touch screen mod: 2 of 2*/
require_once("rhinoflow/fckeditor/fckeditor.php");
require_once("DAL/EventManager.php");
require_once("DAL/StudioManager.php");
require_once("DAL/ClassManager.php");
require_once("DAL/ClubManager.php");

$eventManager = new EventManager();
$events = $eventManager->SelectAll();
$clubManager = new ClubManager();
$clubs = $clubManager->LoadClubs();
$studioManager = new StudioManager();
$studios = $studioManager->SelectStudioByID($_GET['STUDIO']);
$studiosNav = $studioManager->SelectStudioByLocationAll($_GET['LOCATION']);

?>
<!-- <link rel="stylesheet" type="text/css" href="http://static.flowplayer.org/tools/css/overlay-apple.css"/> -->
<link rel="stylesheet" type="text/css" href="/schedules/css/overlay-apple.css"/>
<!-- <link rel="stylesheet" type="text/css" href="http://static.flowplayer.org/tools/css/standalone.css"/>  -->
<link href="/schedules/css/example.css?refresh=233" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/schedules/css/print.css" type="text/css" media="print" />
<!-- <script src="http://cdn.jquerytools.org/1.1.2/full/jquery.tools.min.js"></script> -->
<!-- <script src="http://cdn.jquerytools.org/1.2.6/full/jquery.tools.min.js"></script> -->
<script src="/js/jquery.tools.min.js"></script>
<style> 
 
/* use a semi-transparent image for the overlay */
#overlay {
	background-image:url(/css/transparent.png);
	color:#efefef;
	height:450px;
}
 
/* container for external content. uses vertical scrollbar, if needed */
.contentWrap {
	height:441px;
	overflow-y:auto;
}

</style> 
<link href="/schedules/css/<?=$studio->StyleSheet?>.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	DeleteEvent = function(ID)
	{
		var confirmed;

		confirmed = confirm ("Are you sure you want to delete this event?");

		if (confirmed)
		{
			$("#txtEventID").val(ID);
			$("#txtAction").val("delete");

			$("#frmAdmin").submit();
		}

		return false;
	}
</script>

<input type="hidden" id="txtEventID" name="txtEventID" value="0" />

<div id="contentAdmin">

	<!--h1>Scheduled Classes [<?=$clubs[$_GET['LOCATION']]?>]</h1-->


<? 
while ($studio = $studios->NextItem()) {
	
	//echo "<h2>".$studio->StudioName."</h2>";
	//echo $studio->ID;
if(!isset($_GET['CATID'])){
	$monday = $eventManager->SelectByDayCurrentMonth(1,$_GET['LOCATION'],$_GET['STUDIO']);
	$tuesday = $eventManager->SelectByDayCurrentMonth(2,$_GET['LOCATION'],$_GET['STUDIO']);
	$wednesday = $eventManager->SelectByDayCurrentMonth(3,$_GET['LOCATION'],$_GET['STUDIO']);
	$thursday = $eventManager->SelectByDayCurrentMonth(4,$_GET['LOCATION'],$_GET['STUDIO']);
	$friday = $eventManager->SelectByDayCurrentMonth(5,$_GET['LOCATION'],$_GET['STUDIO']);
	$saturday = $eventManager->SelectByDayCurrentMonth(6,$_GET['LOCATION'],$_GET['STUDIO']);
	$sunday = $eventManager->SelectByDayCurrentMonth(7,$_GET['LOCATION'],$_GET['STUDIO']);

} else {
	$monday = $eventManager->SelectStudioByLocationMonthDayCat(1,$_GET['LOCATION'],$_GET['STUDIO'],$_GET['CATID'],$_GET['MONTH'],$_GET['YEAR']);
	$tuesday = $eventManager->SelectStudioByLocationMonthDayCat(2,$_GET['LOCATION'],$_GET['STUDIO'],$_GET['CATID'],$_GET['MONTH'],$_GET['YEAR']);
	$wednesday = $eventManager->SelectStudioByLocationMonthDayCat(3,$_GET['LOCATION'],$_GET['STUDIO'],$_GET['CATID'],$_GET['MONTH'],$_GET['YEAR']);
	$thursday = $eventManager->SelectStudioByLocationMonthDayCat(4,$_GET['LOCATION'],$_GET['STUDIO'],$_GET['CATID'],$_GET['MONTH'],$_GET['YEAR']);
	$friday = $eventManager->SelectStudioByLocationMonthDayCat(5,$_GET['LOCATION'],$_GET['STUDIO'],$_GET['CATID'],$_GET['MONTH'],$_GET['YEAR']);
	$saturday = $eventManager->SelectStudioByLocationMonthDayCat(6,$_GET['LOCATION'],$_GET['STUDIO'],$_GET['CATID'],$_GET['MONTH'],$_GET['YEAR']);
	$sunday = $eventManager->SelectStudioByLocationMonthDayCat(7,$_GET['LOCATION'],$_GET['STUDIO'],$_GET['CATID'],$_GET['MONTH'],$_GET['YEAR']);
}
$colours = array('pink', 'pink', 'blue', 'green', 'yellow', 'red', 'purple', 'orange', 'beige', 'aqua');
?>

	<div id="schedulewrapper">
    	<div id="heading"><img src="/schedules/logo2.png"/> <?=$clubs[$_GET['LOCATION']]?> | <strong><?=$studio->StudioName?></strong><!-- | <?//=date('M Y')?> --></div>

        <ul id="studionav">
	    <? while ($studioNav = $studiosNav->NextItem()) { ?>    
        	<li><a href="http://tools.movatiathletic.com/schedule.php?LOCATION=<?=$_GET['LOCATION']?>&STUDIO=<?=$studioNav->ID?><? if(isset($_GET['CATID'])){ echo "&CATID=".$_GET['CATID']; }?>&MONTH=<?=$_GET['MONTH']?>&YEAR=<?=$_GET['YEAR']?>" <? if($studioNav->ID == $_GET['STUDIO']){ ?>class="active"<? } ?>><?=$studioNav->StudioName?></a></li>
       <? } ?>
	</ul>



        <div id="weekwrapper">
       	  <div class="doyofweek">
            	<div class="dowtitle">MONDAY</div>
				 <? 
					while ($me = $monday->NextItem()) {
					$titleStudio = checkPoolClosed($me->ClassName, $me->ClassCode, $me->Instructor);
					$starthour = $me->StartEventHour;
					if($starthour == 0) $starthour = 12;
					
					echo "<a href='external-pop.php?CID=$me->ClassId' rel='#overlay'><div class='event ".$colours[$me->Colour]."'>";
					echo "<div class='time'>".$starthour.":".$me->StartEventMin." ".$me->StartEventAP." - ".$me->EndEventHour.":".$me->EndEventMin." ".$me->EndEventAP."</div>";
					echo "<div class='title'>".$titleStudio['title']."</div>";				
					echo "<div class='studio'>".$titleStudio['instructor']."</div></div></a>"; ?>
 					<!-- make all links with the 'rel' attribute open overlays --> 			
			  <? } ?>                
            </div>
        	<div class="doyofweek">
            	<div class="dowtitle">TUESDAY</div>
				 <? 
					while ($me = $tuesday->NextItem()) {
					$titleStudio = checkPoolClosed($me->ClassName, $me->ClassCode, $me->Instructor);
					$starthour = $me->StartEventHour;
					if($starthour == 0) $starthour = 12;
					echo "<a href='external-pop.php?CID=$me->ClassId' rel='#overlay'><div class='event ".$colours[$me->Colour]."'>";
					echo "<div class='time'>".$starthour.":".$me->StartEventMin." ".$me->StartEventAP." - ".$me->EndEventHour.":".$me->EndEventMin." ".$me->EndEventAP."</div>";
					echo "<div class='title'>".$titleStudio['title']."</div>";				
					echo "<div class='studio'>".$titleStudio['instructor']."</div></div></a>";
				 } ?>     
            </div>
            
        	<div class="doyofweek">
            	<div class="dowtitle">WEDNESDAY</div>
				 <? 
					while ($me = $wednesday->NextItem()) {
					$titleStudio = checkPoolClosed($me->ClassName, $me->ClassCode, $me->Instructor);
					$starthour = $me->StartEventHour;
					if($starthour == 0) $starthour = 12;						
					echo "<a href='external-pop.php?CID=$me->ClassId' rel='#overlay'><div class='event ".$colours[$me->Colour]."'>";
					echo "<div class='time'>".$starthour.":".$me->StartEventMin." ".$me->StartEventAP." - ".$me->EndEventHour.":".$me->EndEventMin." ".$me->EndEventAP."</div>";
					echo "<div class='title'>".$titleStudio['title']."</div>";				
					echo "<div class='studio'>".$titleStudio['instructor']."</div></div></a>";
				 } ?>                     
            </div>
        	<div class="doyofweek">
            	<div class="dowtitle">THURSDAY</div>
				 <? 
					while ($me = $thursday->NextItem()) {
					$titleStudio = checkPoolClosed($me->ClassName, $me->ClassCode, $me->Instructor);
					$starthour = $me->StartEventHour;
					if($starthour == 0) $starthour = 12;						
					//print_r($me);
					echo "<a href='external-pop.php?CID=$me->ClassId' rel='#overlay'><div class='event ".$colours[$me->Colour]."'>";
					echo "<div class='time'>".$starthour.":".$me->StartEventMin." ".$me->StartEventAP." - ".$me->EndEventHour.":".$me->EndEventMin." ".$me->EndEventAP."</div>";
					echo "<div class='title'>".$titleStudio['title']."</div>";				
					echo "<div class='studio'>".$titleStudio['instructor']."</div></div></a>";
				 } ?>  
            </div>
            
        	<div class="doyofweek">
            	<div class="dowtitle">FRIDAY</div>
				 <? 
					while ($me = $friday->NextItem()) {
					$titleStudio = checkPoolClosed($me->ClassName, $me->ClassCode, $me->Instructor);
					$starthour = $me->StartEventHour;
					if($starthour == 0) $starthour = 12;						
					echo "<a href='external-pop.php?CID=$me->ClassId' rel='#overlay'><div class='event ".$colours[$me->Colour]."'>";
					echo "<div class='time'>".$starthour.":".$me->StartEventMin." ".$me->StartEventAP." - ".$me->EndEventHour.":".$me->EndEventMin." ".$me->EndEventAP."</div>";
					echo "<div class='title'>".$titleStudio['title']."</div>";				
					echo "<div class='studio'>".$titleStudio['instructor']."</div></div></a>";
				 } ?> 
            </div>
            
        	<div class="doyofweek">
            	<div class="dowtitle">SATURDAY</div>
 				 <? 
					while ($me = $saturday->NextItem()) {
					$titleStudio = checkPoolClosed($me->ClassName, $me->ClassCode, $me->Instructor);	
					$starthour = $me->StartEventHour;
					if($starthour == 0) $starthour = 12;						
					echo "<a href='external-pop.php?CID=$me->ClassId' rel='#overlay'><div class='event ".$colours[$me->Colour]."'>";
					echo "<div class='time'>".$starthour.":".$me->StartEventMin." ".$me->StartEventAP." - ".$me->EndEventHour.":".$me->EndEventMin." ".$me->EndEventAP."</div>";
					echo "<div class='title'>".$titleStudio['title']."</div>";				
					echo "<div class='studio'>".$titleStudio['instructor']."</div></div></a>";	
				 } ?> 
            </div>
            
        	<div class="doyofweek">
            	<div class="dowtitle">SUNDAY</div>
				 <? 
					while ($me = $sunday->NextItem()) {
					$titleStudio = checkPoolClosed($me->ClassName, $me->ClassCode, $me->Instructor);
					$starthour = $me->StartEventHour;
					if($starthour == 0) $starthour = 12;						
					echo "<a href='external-pop.php?CID=$me->ClassId' rel='#overlay'><div class='event ".$colours[$me->Colour]."'>";
					echo "<div class='time'>".$starthour.":".$me->StartEventMin." ".$me->StartEventAP." - ".$me->EndEventHour.":".$me->EndEventMin." ".$me->EndEventAP."</div>";
					echo "<div class='title'>".$titleStudio['title']."</div>";				
					echo "<div class='studio'>".$titleStudio['instructor']."</div></div></a>";
				 } ?> 
            </div>
            <div class="clear"></div>
        </div>
    </div>


	<? } ?>
</div>


					<div class="apple_overlay" id="overlay" style=""> 
                        <!-- the external content is loaded inside this tag --> 
			                        <div class="contentWrap"></div> 
					</div>

<?php
function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}
function checkPoolClosed($classname, $classcode, $instructor){
  $title = "?"; 
  if(!preg_match('/iDISABLEDpool closed/i',$instructor)){
    $title = $classname." (".$classcode.")";
    $instructor = $instructor;
  }else{
    #$title = $instructor;
    #$instructor = '';
  }
  return array(
    'title' => $title,
    'instructor' => $instructor,
  );
}
?>
<div class="centerfoot">
<ul id="classCategories">
	<li class="pink"><a href="http://tools.movatiathletic.com/schedule.php?LOCATION=<?=$_GET['LOCATION']?>&STUDIO=<?=$_GET['STUDIO']?>&CATID=1">Yoga</a></li>
    <li class="blue"><a href="http://tools.movatiathletic.com/schedule.php?LOCATION=<?=$_GET['LOCATION']?>&STUDIO=<?=$_GET['STUDIO']?>&CATID=2">Step</a></li>
    <li class="green"><a href="http://tools.movatiathletic.com/schedule.php?LOCATION=<?=$_GET['LOCATION']?>&STUDIO=<?=$_GET['STUDIO']?>&CATID=3">Pilates</a></li>
    <li class="yellow"><a href="http://tools.movatiathletic.com/schedule.php?LOCATION=<?=$_GET['LOCATION']?>&STUDIO=<?=$_GET['STUDIO']?>&CATID=4">Cycle</a></li>
    <li class="red"><a href="http://tools.movatiathletic.com/schedule.php?LOCATION=<?=$_GET['LOCATION']?>&STUDIO=<?=$_GET['STUDIO']?>&CATID=5">Boxing</a></li>
    <li class="purple"><a href="http://tools.movatiathletic.com/schedule.php?LOCATION=<?=$_GET['LOCATION']?>&STUDIO=<?=$_GET['STUDIO']?>&CATID=6">Strength & Cardio</a></li>
    <li class="orange"><a href="http://tools.movatiathletic.com/schedule.php?LOCATION=<?=$_GET['LOCATION']?>&STUDIO=<?=$_GET['STUDIO']?>&CATID=7">Dance</a></li>
    <li class="beige"><a href="http://tools.movatiathletic.com/schedule.php?LOCATION=<?=$_GET['LOCATION']?>&STUDIO=<?=$_GET['STUDIO']?>&CATID=8">Core & Tone</a></li>
    <li class="aqua"><a href="http://tools.movatiathletic.com/schedule.php?LOCATION=<?=$_GET['LOCATION']?>&STUDIO=<?=$_GET['STUDIO']?>&CATID=9">Aqua</a></li>
    <li><a href="http://tools.movatiathletic.com/schedule.php?LOCATION=<?=$_GET['LOCATION']?>&STUDIO=<?=$_GET['STUDIO']?>" style="color: #000;">Show All</a></li>
</ul>
</div>
<script> 
 
$(function() {
 
	// if the function argument is given to overlay,
	// it is assumed to be the onBeforeLoad event listener
	$("a[rel]").overlay({
 
		expose: 'white',
		effect: 'apple',
 
		onBeforeLoad: function() {
 
			// grab wrapper element inside content
			var wrap = this.getContent().find(".contentWrap");
 
			// load the page specified in the trigger
			wrap.load(this.getTrigger().attr("href"));
		}
 
	});
});
</script> 
