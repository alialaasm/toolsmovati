<?php

function d($var, $exit = 0)
{
	echo "<hr />";
	echo "<span style=\"color:red;\">" . var_dump($var) . "</span>";
	echo "<hr />";

	if ($exit != 0) { exit(); }
}



/**
 * Provides a way to communicate a messaage to the user.  The message
 * is displayed in a JavaScript alert box.
 *
 * @param $message string The message to be displayed.
 * @param $url string The URL to be re-directed to.  No redirection if the URL is empty.
 * @param $fullHTML boolean true iff html headers should be displayed.  Useful if the only user response is the message.
 */
function messageBox($message = "", $url = "", $fullHTML = false)
{
	if ($message != "")
	{
		$message = str_replace("\"", "\\\"", $message);

		if ($fullHTML) { print("<html><head>"); }

		print("<script type=\"text/javascript\"> alert(\"$message\"); </script>");

		if ($url != "")
		{
			print("<script type=\"text/javascript\"> window.location = \"" . $url . "\"; </script>");
			exit();
		}

		if ($fullHTML)
		{
			print("<body></body></html>");
			exit();
		}
	}
}

?>
