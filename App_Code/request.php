<?php



/***** POST **********************************************************/
function post_text($var, $default = "")
{	
	$str = isset($_POST[$var]) ? $_POST[$var] : $default;
	if (get_magic_quotes_gpc())
		$str = stripslashes($str);
		
	return $str;
}

function post_int($var, $default = 0) {
	$value = post_text($var);

	return is_numeric($value) ? intval($value) : $default;
}

function post_positive_int($var, $default = 0)
{
	$value = post_int($var);

	return ($value < 0) ? 0 : $value;
}

function post_double($var, $default = 0.00) {
	$value = post_text($var);

	return is_numeric($value) ? doubleval($value) : $default;
}

function post_bool($var, $default = false) {
	if (!isset($_POST[$var])) return $default;

	return ((post_text($var) == "true") || (post_int($var) != 0));
}

function post_array($name)
{
	if (!isset($_POST[$name]))
		return array();

	if (is_array($_POST[$name]))
		$arr = $_POST[$name];
	else
		$arr = split(",", $_POST[$name]);

	$length = count($arr);
	if (get_magic_quotes_gpc())
		for ($i = 0; $i < $length; $i++)
				$arr[$i] = stripslashes($arr[$i]);

	return $arr;
}


/***** GET ***********************************************************/
function get_text($var, $default = "") {
	return isset($_GET[$var]) ? $_GET[$var] : $default;
}

function get_int($var, $default = 0) {
	$value = get_text($var);

	return is_numeric($value) ? intval($value) : $default;
}

function get_positive_int($var, $default = 0)
{
	$value = get_int($var);

	return ($value < 0) ? 0 : $value;
}

function get_double($var, $default = 0.00) {
	$value = get_text($var);

	return is_numeric($value) ? doubleval($value) : $default;
}

function get_bool($var, $default = false) {
	if (!isset($_GET[$var])) return $default;

	return (get_text($var) == "true" || get_int($var) != 0);
}





/***** SESSION FUNCTIONS *********************************************/
function session_text($var, $default = "") {
	return isset($_SESSION[$var]) ? $_SESSION[$var] : $default;
}

function session_int($var, $default = 0) {
	$value = session_text($var);

	return is_numeric($value) ? intval($value) : $default;
}


// Returns true iff the session variable
// contains bool(true), string(true) or NOT int(0).
function session_bool($var, $default = false)
{
	if (!isset($_SESSION[$var])) return $default;

	if (is_bool($_SESSION[$var]))
		return (bool)($_SESSION[$var]);

	return ((session_text($var) == "true") || (session_int($var) != 0));
}
?>