<?php

define("SQL_STORED_PROC", 0);
define("SQL_TEXT", 1);

/***** CLASS: MySqlResult *******************************************/
class MySql
{
	
	var $query = "";

	var $_db = NULL;

	var $_insert_id = -1;

	var $_host = NULL;
	var $_username = NULL;
	var $_password = NULL;
	var $_dbname = NULL;

	var $_new_link = NULL;
	var $_client_flags = NULL;


	function MySql($host = "localhost", $username = "" , $password = "" , $dbname = "", $new_link = true, $client_flags = "")
	{
		$this->_host = $host;
		$this->_username = $username;
		$this->_password = $password;
		$this->_dbname = $dbname;

		$this->_new_link = $new_link;
		$this->_client_flags = $client_flags;

		$this->connect();
	}


	function connect()
	{
		if (is_resource($this->_db)) mysql_close($this->_db);

		if ($this->_client_flags == "")
			$this->_db = mysql_connect($this->_host, $this->_username, $this->_password, $this->_new_link);
		else
			$this->_db = mysql_connect($this->_host, $this->_username, $this->_password, $this->_new_link, $this->_client_flags);

		if ($this->_dbname != "") { mysql_select_db($this->_dbname, $this->_db); }
	}


	function ping()
	{
		return mysql_ping($this->_db);
	}


	function escape_str($value)
	{
		// Stripslashes
		if (get_magic_quotes_gpc())
		{
			$value = stripslashes($value);
		}

		return mysql_real_escape_string($value, $this->_db);
	}


	function escape_int($value)
	{
		return $return_value = is_numeric($value) ? (string)((int)$value) : "0";
	}


	function insert_id()
	{
		return mysql_insert_id($this->_db);
	}


	function select_db($dbname)
	{
		mysql_select_db($dbname, $this->_db);
	}


	function errno()
	{
		return mysql_errno($this->_db);
	}


	function error()
	{
		return mysql_error($this->_db);
	}

	function query($query = "")
	{
		if ($query != "")
		{
			$result = mysql_query($query, $this->_db);
		}
		else
		{
			$result = mysql_query($this->query, $this->_db);
		}

		if (mysql_errno() > 0)
		{
			print_r("<span style=\"color:red;\">" . mysql_error() . "</span><hr />");
			print_r("<span style=\"color:red;\">" . $query . "</span><hr />");
			exit();
		}

		return (is_resource($result)) ? new MySqlResult($result) : true;
	}

	function query_scalar($query = NULL)
	{
		if ($query != NULL)
		{
			$result = mysql_query($query, $this->_db);
		}
		else
		{
			$result = mysql_query($this->query, $this->_db);
		}

		if (mysql_errno() > 0)
		{
			print_r("<span style=\"color:red;\">" . mysql_error() . "</span><hr />");
			print_r("<span style=\"color:red;\">" . $query . "</span><hr />");
			exit();
		}

		if ($row = mysql_fetch_array($result))
		{
			return $row[0];
		}

		return false;
	}

	function fetch_row()
	{

		if ((func_num_args() % 2) != 1) { return false; }

		$num_statements = ((func_num_args() - 1) / 2);

		$table_name = func_get_arg(0);

		$query = "SELECT * FROM $table_name";

		if ($num_statements > 0) $query .= " WHERE 1";

		for ($i = 1; $i <= $num_statements; $i += 2)
		{
			$key_name = func_get_arg($i);
			$key_value = func_get_arg($i + 1);

			$query .= " AND (`$key_name` = '$key_value')";
		}

		$query .= " LIMIT 0, 1";

		$result = $this->query($query, $this->_db);

		return $result->fetch_array();

	}

	function fetch_object()
	{
		if (func_num_args() == 0)
		{
			$query = $this->query;
		}
		else if ((func_num_args() % 2) == 1)
		{

			$num_statements = ((func_num_args() - 1) / 2);

			$table_name = func_get_arg(0);

			$query = "SELECT * FROM $table_name";

			if ($num_statements > 0) $query .= " WHERE 1";

			for ($i = 1; $i <= $num_statements; $i ++)
			{
				$arg_index = (2 * $i) - 1;
				$key_name = func_get_arg($arg_index);
				$key_value = func_get_arg($arg_index + 1);

				$query .= " AND (`$key_name` = '$key_value')";
			}

			$query .= " LIMIT 0, 1";
		}
		else
		{
			return false;
		}

		$result = $this->query($query);

		return $result->fetch_object();

	}


	function create_command($query, $type = SQL_TEXT)
	{
		return new MySqlCommand($this, $query, $type);
	}

}
/***** END MySql *****************************************************/




/***** MySqlResult ***************************************************/
class MySqlResult
{
	var $_result = NULL;

	var $_num_fields = 0;

	var $_row_index = 0;
	
	function MySqlResult($result = NULL)
	{
		if ($result != NULL)
		{
			$this->_result = $result;
			$this->_num_fields = mysql_num_fields($result);
		}
	}

	function fetch_assoc()
	{
		$row = mysql_fetch_assoc($this->_result);

		if ($row !== false) { $this->_row_index++; }

		return $row;
	}

	function fetch_array() 
	{
		$row = mysql_fetch_array($this->_result);

		if ($row !== false) { $this->_row_index++; }

		return $row;
	}

	function fetch_object()
	{
		$row = mysql_fetch_object($this->_result);

		if ($row !== false) { $this->_row_index++; }

		return $row;
	}

	function fetch_field($field_num)
	{

		$field = mysql_fetch_field($this->_result, $field_num);

		$flags = mysql_field_flags($this->_result, $field_num);
		$field->auto_increment = strstr($flags, "auto_increment") ? 1 : 0;
		
		$field->length = mysql_field_len($this->_result, $field_num);

		return $field;
	}
	
	function free_result()
	{
		mysql_free_result($this->_result);
	}

	function data_seek($row)
	{
		return mysql_data_seek($this->_result, $row);
	}

	function num_fields()
	{
		return $this->_num_fields;
	}

	function num_rows()
	{
		return mysql_num_rows($this->_result);
	}

	function has_rows()
	{
		return ($this->num_rows() > 0);
	}

	function row_index()
	{
		return $this->_row_index;
	}
	
	function odd_row()
	{
		return ($this->row_index() % 2 == 1);
	}
	
	function even_row()
	{
		return ($this->row_index() % 2 == 0);
	}

	function num_pages($pagesize)
	{
		$num_rows = $this->num_rows();

		$page_count = (int)($num_rows / $pagesize);

		if ($num_rows % $pagesize > 0)
		{
			$page_count = $page_count + 1;
		}

		return $page_count;
	}

	function set_page($page_num, $page_size)
	{
		if ($page_num < 1 || ($page_num > $this->num_pages($page_size)))
		{
			$this->data_seek(1);
		}
		
		// move the cursor to the current page.
		$this->data_seek(($page_num - 1) * $page_size);
	}
}
/***** END: MySqlResult **********************************************/



/***** CLASS: MySqlCommand *******************************************
       This class is used to generate MySql queries using a command
	style object.  This class takes care of building statements and
	prevents SQL injection by removing these duties from the programmer.

	NOTES:
	1) There is code left over from when this class was ported from
	   handling MS-Sql to MySql.  The code is left to be ported to MySql
		 once MySql is in common use.
 *********************************************************************/
define("SQLTYPE_VARCHAR", 0);
define("SQLTYPE_NVARCHAR", 1);
define("SQLTYPE_TEXT", 2);
define("SQLTYPE_NTEXT", 3);
define("SQLTYPE_INT", 4);
define("SQLTYPE_DOUBLE", 5);
define("SQLTYPE_DATE", 6);
define("SQLTYPE_BOOL", 7);

class MySqlCommand {
	var $_query = NULL;
	var $_output_query = NULL;
	var $_db = NULL;

	var $_type = NULL;

	var $_values = array();

	function MySqlCommand($db, $query, $type = SQL_TEXT)
	{

		//if ($type == SQL_STORED_PROC || $type == SQL_TEXT)
		if ($type == SQL_TEXT || $type == SQL_STORED_PROC)
		{
			$this->_type = $type;
		}
		else
		{
			die("$type is not a valid MySqlCommand type.");
		}

		$this->_db = $db;
		$this->_query = $query;

	}


	/********************************************************************
	  var add_parameter
	 ********************************************************************
	  parameters
		string - parameter - the name of the parameter starting with @.
		mixed  - value     - the value of the parameter.
		int    - type      - the sql parameter type.
	 ********************************************************************/
	function add_parameter($parameter, $value, $type)
	{
		$success = false;

		if ($type == SQLTYPE_TEXT || $type == SQLTYPE_VARCHAR)
		{
			$value = "'" . mysql_escape_string($value) . "'";
			$success = true;
		}
		else if ($type == SQLTYPE_INT)
		{
			if (is_numeric($value))
			{
				$value = (int)($value);
			}
			$success = true;
		}
		else if ($type == SQLTYPE_DOUBLE)
		{
			if (is_numeric($value))
			{
				$value = (double)($value);
			}
			$success = true;
		}
		else if ($type == SQLTYPE_DATE)
		{
			$value = "'" . mysql_escape_string($value) . "'";
			$success = true;
		}
		else if ($type == SQLTYPE_BOOL)
		{
			$success = true;
			if (is_bool($value) || $value === 0 || $value === 1)
			{
				if ($value === true)
					$value = 1;
				else if ($value === false)
					$value = 0;

				$success = true;
			}	
		}

		if ($success)
		{
			if (stristr($this->_query, $parameter . "," ))
				$this->_values[$parameter . ","] = $value . ",";
			else
				$this->_values[$parameter] = $value;
		}

		return $success;
	}


	/********************************************************************
	  var execute
	 ********************************************************************
	 ********************************************************************/
	function execute()
	{
		$this->_output_query = str_replace(array_keys($this->_values), $this->_values, $this->_query);
		
		$result = mysql_query($this->_output_query, $this->_db->_db);

		if (mysql_errno() != 0) {
			print(mysql_error() . "<hr />");
			exit();
		}

		if ($this->_type == SQL_STORED_PROC) {
			if (!($this->_db->ping())) $this->_db->connect();
		}

		
		if (is_resource($result)) {
			return new MySqlResult($result);
		}
		else {
			return $result;
		}
	}


	function execute_scalar()
	{
		$result = $this->execute();

		if ($result->errno() == 0 && @$result->num_rows() == 1)
		{
			$row = $result->fetch_array();

			return $row[0];
		}

		return false;
	}

	function free_statement() {
		mssql_free_statement($this->_command);
	}

}
/***** END: MySqlCommand *********************************************/

?>