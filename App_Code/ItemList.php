<?

class ItemList
{
	private $_itemCount;
	private $_items;
	private $_currentPosition;


	function ItemList()
	{
		$this->RemoveAll();
	}


	function AddItem($item)
	{
		$this->_items[] = $item;
		$this->_itemCount++;
	}


	function NextItem()
	{
		$item = $this->ItemAt($this->_currentPosition);

		if (!($item == NULL)) $this->_currentPosition++;

		return $item;
	}


	function ItemAt($index)
	{
		if ($index >= 0 && $index < $this->Count())
			return $this->_items[$index];
		else
			return NULL;
	}


	public function RemoveAll()
	{
		$this->_items = array();
		$this->_itemCount = 0;
		$this->_currentPosition = 0;
	}


	function Reset() { $this->_currentPosition = 0; }

	function Count() { return $this->_itemCount; }

	function EvenRow() { return ($this->_currentPosition % 2 == 0); }

	function OddRow() { return ($this->_currentPosition % 2 == 1); }

	function CurrentPosition() {  return $this->_currentPosition; }

	function HasRows() { return ($this->Count() > 0); }

}

?>