<?php

/*********************************************************************
	* This function authenticates a user.
	*
	* usage: authenticate()
	*
	* @access  public
	* @param   user_level - the types of users that are allowed.
	* @return	 void
	*/

function go_to_login() {
	$_SESSION["return_url"] = $_SERVER["PHP_SELF"];
	//print_r($_SESSION); //comment
	session_write_close();
	header("location: " . SITE_URL . "login.php?return_url=" . $_SERVER["PHP_SELF"]);
	exit();
}

function authenticate() {
	if (func_num_args() == 0)
		$auth_levels[] = 0;
	else
		$auth_levels = func_get_args();

	if (!isset($_SESSION["IP_ADDRESS"]))
		go_to_login();

	$user_levels = split(",", isset($_SESSION["USER_GROUPS"]) ? $_SESSION["USER_GROUPS"] : "");

	$valid_group = false;

	foreach ($user_levels as $user_level)
		foreach ($auth_levels as $auth_level)
			if ($user_level == $auth_level)
				$valid_group = true;

	if (!$valid_group)
		go_to_login();

}
?>