<?
/** Uploads a file from a post form. *********************************
	* Given the name of the file to be uploaded and a destination path,
	* this function will upload the file from the $_FILES collection.  If
	* the filename exists, this function will append (1), (2), ... until
	* an unused filename is found.
	*
	* @access			public
	* @param			file_object_name - the name of the file object found in the form.
	* @param			dest_path - location the file should be uploaded to, ending in "/".
	* @new_name	file_name - the new name of the uploaded file.
	* @returns		the filename iff the file was successfully uploaded, else "".
	*/
function upload_file($file_object_name, $dest_path, $filename = "") {
	// test the file object exists
	if (!isset($_FILES[$file_object_name]["name"]))
		return "";

	if ($filename == "")
		$filename =  strtolower(basename($_FILES[$file_object_name]["name"]));

	$new_filename = $filename;
	$i = 0;
	while (file_exists($dest_path . $new_filename)) {
		$file_parts = explode(".", $filename);
		$total_parts = count($file_parts);
		// error exploding file
		if ($total_parts == 0)
			return "";
		// the filename has no extension so tack on the added part
		else if ($total_parts == 1)
			$new_filename = "{$file_parts[0]}($i)";
		// the filename has an extension so rebuild the filename
		// with the added part before the extension.
		else {
			$new_filename = $file_parts[0];
			for ($j = 1; $j < $total_parts - 1; $j++)
				$new_filename .= ".{$file_parts[$j]}";
			$new_filename .= "($i).{$file_parts[$total_parts-1]}";
		}
		$i++;
	}

	$success = move_uploaded_file($_FILES[$file_object_name]["tmp_name"], $dest_path . $new_filename);
	
	return ($success) ? $new_filename : "";
}


/** Tests if a file is a given typee. ********************************
	* Given the name of the file, this functions tests if the extension
	* is one of the given file types.
	*
	* @access		public
	* @param		filename - the name of the file to be tested.
	* @param		$types - string containing types, separated by ","
	* @returns	true iff the $filename has an image extension, else false.
	*/
function is_type($filename, $types) {
	$types = split(",", $types);
	$fname =  strtolower($filename);
	$ext = substr((($t = strrchr($fname,'.')) !== false) ? $t : '', 1);

	for ($i = 0; $i < count($types); $i++)
		if ($ext == $types[$i])
			return true;

	return false;
}

/** Tests if a filename is an image. *********************************
	* Given the name of the file, this functions tests if the extension
	* is a JPEG/JPG, GIF, or PNG.  This function should be replaced by a
	* proper regular expression implementation.
	*
	* @access		public
	* @param		filename - the name of the file to be tested.
	* @returns	true iff the $filename has an image extension, else false.
	*/
function is_image($filename) {
	return is_type($filename, "jpg,jpeg,gif,png");
}

function is_document($filename) {
	return is_type($filename, "doc,pdf,txt");
}

/** Resizes an image and saves it. ***********************************
	* Given the name of the file to be resized and a destination path,
	* this function will resize the image to fit the width and height
	* specified.  The images dimensions are resized relative to each
	* other.
	*
	* @access		public
	* @param		old_path - 
	* @param		new_path - 
	* @param		max_width - 
	* @param		max_height - 
	* @returns	the filename iff the file was successfully uploaded, else "".
	*/
function resize_image($old_path, $new_path, $max_width, $max_height) {
	
	/* Get the dimensions of the source picture */
	list($old_width, $old_height, $type, $attr) = getImageSize($old_path);

	if (!$old_width)
		return false;

	switch ($type) {
	case 1: // GIF
		$source_id = imageCreateFromGif($old_path);
		break;
	case 2: // JPEG/JPG
		$source_id = imageCreateFromJpeg($old_path);
		break;
	case 3: // PNG
		$source_id = imageCreateFromPng($old_path);
		break;
	}

	if (($old_width / (double)($max_width)) >= ($old_height / (double)($max_height))) {
		$new_width = $max_width;
		$new_height = ($old_height * $max_width) / $old_width;
	}
	else {
		$new_width = ($old_width * $max_height) / $old_height;
		$new_height = $max_height;
	}

	/* Create a new image object (not neccessarily true colour). */
	$target_id = imageCreateTrueColor($new_width, $new_height);

	/* Resize the original picture and copy it into the just created image object. */
	$target_pic = imageCopyResampled($target_id, $source_id, 0,0,0,0, $new_width, $new_height, $old_width, $old_height);

	/* Save the image. */
	switch ($type) {
	case 1: // GIF
		$success = imageGif($target_id, $new_path);
		break;
	case 2: // JPG
		$success = imageJpeg($target_id, $new_path, 100);
		break;
	case 3: // PNG
		$success = imagePng($target_id, $new_path, 100);
		break;
	}

	return true;
}
?>