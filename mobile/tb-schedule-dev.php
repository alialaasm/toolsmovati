<!DOCTYPE HTML>
<html>
<head>
	<title>Movati Athletic</title>
	<base href="/mobile/"/>
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="http://www.movatiathletic.com/static/css/fonts.min.css?bec28ed3" media="all"/> 
	<!-- <script type="text/javascript" charset="utf-8" src="phonegap-0.9.3.js"></script>
	<script src="jquery.mobile-1.0rc2.min.js"></script> -->
	<style>
		body{
			margin:0;
		}
		
		.header{
			background-image: url(http://tools.movatiathletic.com/mobile/images/logo_small.png);
			background-repeat: no-repeat;
			background-position: 1% 50%;
			background-color: #2D8790;
			border-bottom: 1px solid rgba(255, 255, 255, 0.4);
			height: 45px;
		}
		.back *, .back {
			background-color: transparent!important;
			border: none!important;
		}
		.header a.back{
			position: absolute;
			border: none;
			top: 5px;
			right: 5px;
			display: block;
			width: 100%;
			background-image: none!important;
			text-align:right;
			height: 1em;
			cursor:pointer;
		}
	
		.header a.back {
			display: block;
			border: none!important;
		}
	</style>

  
</head>
<body>

  <div class="header">
	<!-- <a href="javascript:void()" class="back" onclick="history.go(-1); return false;"><img src="back.png"></a> -->
	<a class="back" onclick="surveyPopup()"><img src="back.png"></a>
  </div> 

  <div class="content">
  	<script>
		var acct = '544'; var loc = ''; var cat = ''; var stylesheet=''; var hideLastnames = true;
		var jsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
		document.write("<scr"+"ipt src='"+jsHost+"ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js' type='text/javascript'></scr"+"ipt>");
		document.write("<scr"+"ipt>var jQuery = jQuery.noConflict(true);</scr"+"ipt>");
		document.write("<scr"+"ipt src='"+jsHost+"www.groupexpro.com/schedule/embed/schedule_embed_responsive.js.php?a="+acct+"' type='text/javascript'></scr"+"ipt>");
	</script>
  </div>
 <script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21947500-1']);
  _gaq.push(['_setDomainName', '.theathleticclubs.ca']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js" type="text/javascript">`
</script>
<!-- TEMP. POPUP CODE -->
   <style>
	.js-open-survey { display:block; }
	.survey-box {
	  display: none;
	  position: fixed;
	  z-index: 1000;
	  width: 98%;
	  background: #2d8791;
	  background-clip: padding-box;
	}

	.survey-box .survey-header {
	  padding: 2rem 1.5rem 1.25rem 1.5rem;
	  background:#222;
	  text-align:center;
	}

	.survey-box .survey-header h3,
	.survey-box .survey-header h4 { 
		margin: 0; 
		color:#fff;
		text-align:center;
		font: 800 32px/42px "Gotham SSm 8r", "Gotham SSm A", "Gotham SSm B", sans-serif;
	}

	.survey-box footer,
	.survey-box .survey-footer {
	  text-align: left;
	  padding: 1rem;
	}

	.logo{
		margin:0 auto .5rem auto;
	}
	.logo img{
		margin:0 auto;
	}
	.survey-overlay {
	  opacity: 0;
	  filter: alpha(opacity=0);
	  position: fixed;
	  top: 0;
	  left: 0;
	  z-index: 900;
	  width: 100%;
	  height: 100%;
	  background: rgba(0, 0, 0, 0.4) !important;
	  display:none;
	}

	.survey-box p{
		color:#fff;
		text-align:center;
		font: 300 15px/20px "Gotham SSm 4r", "Gotham SSm A", "Gotham SSm B", sans-serif;
		margin:0;
	}

	.survey-box .survey-body { text-align:center; }

	.survey-button{
	    background: #222;
	    color: #ffffff;
	    font: 500 15px/45px "Gotham SSm 5r", "Gotham SSm A", "Gotham SSm B", sans-serif;
	    letter-spacing: 0;
	    display: inline-block;
	    margin: 1.25rem auto 0 auto;
	    padding: 0 25px;
	    position: relative;
	    text-transform: uppercase;
	    text-decoration: none;
	}
	.survey-button-no{
		color:#fff;
		text-decoration:none;
		font: 300 15px/20px "Gotham SSm 4r", "Gotham SSm A", "Gotham SSm B", sans-serif;
	}
	.survey-button:hover {
	    background-color: #eeebe1;
	    color: #222;
	}


	a.survey-close {
	  line-height: 1;
	  font-size: 1.5rem;
	  position: absolute;
	  top: 5%;
	  right: 2%;
	  text-decoration: none;
	  color: #bbb;
	  font: 300 20px "Gotham SSm 4r", "Gotham SSm A", "Gotham SSm B", sans-serif;
	}

	a.survey-close:hover {
	  color: #fff;
	  -webkit-transition: color 1s ease;
	  -moz-transition: color 1s ease;
	  transition: color 1s ease;
	}

	.survery-arrow{
		width: 0px;
		height: 0px;
		border-left: 20px solid transparent;
		border-right: 20px solid transparent;
		border-top: 20px solid #222;
		margin:0 auto;
	}

	@media (min-width: 1200px) {
	  .survey-box { width: 45%; }
	}
	@media (min-width: 600px) and (max-width: 1199px){
	  .survey-box { width: 85%; }
	}
	</style>
	<div class="survey survey-reg">
		<a class="js-open-survey" href="" data-survey-id="survey-modal"></a>

		<!-- survey-modal -->
	   <div id="survey-modal" class="survey-box"> 
	  	<div class="survey-header">
		    <a href="" class="js-survey-close survey-close">x</a>
		    <div class="logo">
		    	<img src="movati_athletic_logo_small.png" alt="Movati Athletics Logo">
		    </div>
		    <h3>Activities Schedule Survey</h3>
		    <p>To help us improve the activities schedule, please take a short anonymous survey (takes about 2 minutes to complete) to let us know your opinion.</p><br />
		    <p>Thank you for your time and consideration.</p>
		  </div>
		  <div class="survey-body">
		  	<div class="survery-arrow"></div>
		  	<a href="https://www.surveymonkey.com/r/KBSNJBL" class="survey-button" target="_blank">Take Survey</a>
		  </div>
		  <div class="survey-footer">
		  	<a href="" class="js-survey-close survey-button-no" class="js-survey-close">No Thanks</a>
		  </div>
		</div>
		<div class='survey-overlay'></div>
	</div>
	<div class="survey survey-back">
		<a class="js-open-survey" href="" data-survey-id="survey-modal"></a>

		<!-- survey-modal -->
	   <div id="survey-modal-back" class="survey-box"> 
	  	<div class="survey-header">
		    <a href="" class="js-survey-close survey-close">x</a>
		    <div class="logo">
		    	<img src="movati_athletic_logo_small.png" alt="Movati Athletics Logo">
		    </div>
		    <h3>Activities Schedule Survey</h3>
		    <p>To help us improve the activities schedule, please take a short anonymous survey (takes about 2 minutes to complete) to let us know your opinion.</p><br />
		    <p>Thank you for your time and consideration.</p>
		  </div>
		  <div class="survey-body">
		  	<div class="survery-arrow"></div>
		  	<a href="https://www.surveymonkey.com/r/2LJ3CYZ" class="survey-button" target="_blank">Take Survey</a>
		  </div>
		  <div class="survey-footer">
		  	<a href="" class="js-survey-close survey-button-no" class="js-survey-close">No Thanks</a>
		  </div>
		</div>
		<div class='survey-overlay'></div>
	</div>
	<script>
            function setCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays*24*60*60*1000));
                var expires = "expires="+d.toUTCString();
                document.cookie = cname + "=" + cvalue + "; " + expires;
            }

            function getCookie(cname) {
                var name = cname + "=";
                var ca = document.cookie.split(';');
                for(var i=0; i<ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0)==' ') c = c.substring(1);
                    if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
                }
                return "";
            }

            function checkCookie() {
                var bannerStatus=getCookie("hidebanner");
                if ((bannerStatus=="true") || ($('body').hasClass('nepean'))) {
                    $(".notification").css("display","none");
                } else {
                    $(".notification").css("display","block");	    
                }
            }

            var surveyStatus=getCookie("hidesurvey");

            function surveyPopup(){
              var surveyStatus=getCookie("hidesurvey");
              if (surveyStatus=="true"){
              	history.go(-1); 
              	return false;
              } else {
              	$(".survey-back .survey-overlay").fadeTo(500, 0.7);
				$('.survey-back #survey-modal-back').fadeIn($(this).data());
				setCookie('hidesurvey', 'true', 3);
              }
            }
	        
	        $(document).ready(function(){

            if (surveyStatus==""){
            
                    setTimeout( function() {$('a[data-survey-id]').trigger('click'); },10000);

                    $('a[data-survey-id]').click(function(e) {
						surveyStatus=getCookie("hidesurvey");
						if (surveyStatus==""){
							e.preventDefault();
							$(".survey-reg .survey-overlay").fadeTo(500, 0.7);
							var surveyBox = $(this).attr('data-survey-id');
							$('#'+surveyBox).fadeIn($(this).data());
							setCookie('hidesurvey', 'true', 3);
						} 
						return false;
                    }); 

                    $(".survey-back .js-survey-close, .survey-back .survey-overlay, .survey-back #overlay").click(function() {
                    	$(".survey-box, .survey-overlay").fadeOut(500, function() {
                            $(".survey-overlay").css("display:none;");
                            $(".js-open-survey").remove();
                            $(".survey").remove();
                        });
                        history.go(-1); 
              			return false;
                    });

                      
                    $(".survey-reg .js-survey-close,.survey-reg  .survey-overlay,.survey-reg  #overlay").click(function() {
                        $(".survey-box, .survey-overlay").fadeOut(500, function() {
                            $(".survey-overlay").css("display:none;");
                            $(".js-open-survey").remove();
                            $(".survey").remove();
                        });
                        return false;
                    });
                    $(".survey-reg  .survey-button").click(function() {
                        $(".survey-box, .survey-overlay").fadeOut(500, function() {
                            $(".survey-overlay").css("display:none;");
                            $(".js-open-survey").remove();
                            $(".survey").remove();
                        });
                    });
                    $(".survey-back .survey-button").click(function() {
                    	$(".survey-box, .survey-overlay").fadeOut(500, function() {
                            $(".survey-overlay").css("display:none;");
                            $(".js-open-survey").remove();
                            $(".survey").remove();
                        });
                        $history.go(-1); 
                    });

                     
                    $(window).resize(function() {
                      $(".survey-box").css({
                        top: ($(window).height() - $(".survey-box").outerHeight()) / 2,
                        left: ($(window).width() - $(".survey-box").outerWidth()) / 2
                      });
                    });
                     
                    $(window).resize();
              }

		    });

	    </script>	   
</body>
</html>