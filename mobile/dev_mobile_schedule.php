<!DOCTYPE HTML>
<html>
<head>
	<title>Movati Athletic</title>
	<base href="/mobile/"/>
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="http://www.movatiathletic.com/static/css/fonts.min.css?bec28ed3" media="all"/> 
	<script type="text/javascript" charset="utf-8" src="phonegap-0.9.3.js"></script>
	<script src="jquery.mobile-1.0rc2.min.js"></script>
	<style>
		body{
			margin:0;
		}
		
		.header{
			background-image: url(http://tools.movatiathletic.com/mobile/images/logo_small.png);
			background-repeat: no-repeat;
			background-position: 1% 50%;
			background-color: #2D8790;
			border-bottom: 1px solid rgba(255, 255, 255, 0.4);
			height: 45px;
		}
		.back *, .back {
			background-color: transparent!important;
			border: none!important;
		}
		.header a.back{
			position: absolute;
			border: none;
			top: 5px;
			right: 5px;
			display: block;
			width: 100%;
			background-image: none!important;
			text-align:right;
			height: 1em;
		}
	
		.header a.back {
			display: block;
			border: none!important;
		}
	</style>

  
</head>
<body>

  <div class="header">
	<a href="#" class="back" onclick="history.go(-1); return false;"><img src="back.png"></a>
  </div> 

  <div class="content">
  	<script>
		var acct = '544'; var loc = ''; var cat = ''; var stylesheet=''; var hideLastnames = true;
		var jsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
		document.write("<scr"+"ipt src='"+jsHost+"ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js' type='text/javascript'></scr"+"ipt>");
		document.write("<scr"+"ipt>var jQuery = jQuery.noConflict(true);</scr"+"ipt>");
		document.write("<scr"+"ipt src='"+jsHost+"www.groupexpro.com/schedule/embed/schedule_embed_responsive.js.php?a="+acct+"' type='text/javascript'></scr"+"ipt>");
	</script>
  </div>
 <script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21947500-1']);
  _gaq.push(['_setDomainName', '.theathleticclubs.ca']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

window.location.hash="<?= $_GET["view"]  ?>";

</script>	   
</body>
</html>