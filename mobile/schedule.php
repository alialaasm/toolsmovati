<? 

$APP_SUBDIR = "mobile";

$all_clubs = array (
	"amherstburg" => 6,
	"brantford" => 3,
	"guelph"=> 5,
	"kanata" => 14,
	"kingsville" => 7,
	"london-north"=>1,
	"london-south"=>2,
	"ottawa-orleans"=>8,
	"ottawa-trainyards"=>9,
	"thunder-bay"=>4,
	"waterloo"=>12,
	"barrhaven"=>15
);

#require_once($_SERVER["DOCUMENT_ROOT"] . "/config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/oldconfig.php");

include_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClubManager.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/StudioManager.php");

include_once($_SERVER["DOCUMENT_ROOT"] . "/" . $APP_SUBDIR . "/member.php");

error_reporting(E_ALL);
ini_set('display_errors', true);

$clubID = 0;
$clubManager = new ClubManager();
$clubs = $clubManager->SelectAll();
$studioManager = new StudioManager();

$member = get_session_member();
$myClub = 0;
if ($member){
	$myClub = new Club($member->ClubId);
}

function print_class($c,$member, $has_class=0){
?>
<li class="schedule_class class_item_<?= $c->EventID ?>">
<? $the_time = preg_replace('/00:/', '12:', $c->Time);   ?>
<h4><?= $c->Name ?><div class="small time nobreak"><?= $the_time ?></div>
<br/><div class="instructor small"><?= $c->Instructor ?></div>
</h4>
<? if ($has_class){ ?>
<p><?= $c->StudioName ?></p>
<? }
$desc = strip_tags( $c->Description );
?>
<p class="detail"><?= $desc ?><br/>

<? if ($member){
	if ( $has_class == 0 ){ ?>
        <br/><a href="#" class="add_class class_<?= $c->EventID ?>">Add this class to my schedule</a>
	<? }else{ ?>
        <br/><a href="#" class="remove_class class_<?= $c->EventID ?>">Remove this class from my schedule</a>

<?
	}
	}


}

function has_class( $member, $c ){
	return 0;	
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Movati Athletic</title>
	<base href="/mobile/"/>
	<meta name="viewport" content="width=default-width, initial-scale=1" />
	
	<script type="text/javascript" charset="utf-8" src="phonegap-0.9.3.js"></script>
	<link rel="stylesheet" type="text/css" href="gotham.css" />
	<link rel="stylesheet" type="text/css" href="http://www.movatiathletic.com/static/css/fonts.min.css?bec28ed3" media="all"/>
	<link rel="stylesheet" href="mov.css" /> 
	<link rel="stylesheet" href="default.css" /> 

	<style>
		li.divider { 
			background-color: #999999 !important;
		}

.ui-body-c {
	background: 			#f0f0f0 /*{c-body-background-color}*/;
	background-image: none !important;
}

                h4.ui-li-heading {
                padding-right: 3em !important;
                }


		.ui-bar-b{
			background-color: #999999 !important;
			background-image: none;
		}
		
		.small { font-weight: normal; } 
		.time, .instructor {	 margin-left: 2em; }
		.detail{ display: none; }
		.club_image{ float: left;  width: 250px; padding-right: 20px; }
		.fb { display: block; clear: left; }
		.ui-header{
			background-image: url(images/logo_small.png);
			background-repeat: no-repeat;
			background-position: 1% 50%;
			background-color: #2D8790;
			border-bottom: 1px solid rgba(255, 255, 255, 0.4);
			height: 45px;
		}
		.back *, .back {
			background-color: transparent!important;
			border: none!important;
		}
		.ui-header a.back{
			position: absolute;
			border: none;
			top: 5px;
			display: block;
			width: 100%;
			background-image: none!important;
			text-align:right;
			height: 1em;
		}
		.ui-header a.back .ui-btn-text {
                        background-image: none!important;
			background-color: none!important;
			font-weight: normal;
			font-size: 20px;
		}
		.ui-header .ui-btn {
			display: none;
		}		
	
		.ui-header a.back {
			display: block;
			border: none!important;
		}
		.ui-header h1 {
			display: none !important;
			text-align: left !important;			
			margin-left: 45px !important;
			color: #fff;
			text-shadow: none !important;
			text-transform: uppercase;
		}

		.classes li, .classes li p, .classes h4 {
			text-overflow: ellipsis !important;
			overflow: visible !important;
			white-space: normal !important;
		}

		.nobreak { 
			white-space: nowrap;
		}
		

		#home {
			background-image: url(/<?= $APP_SUBDIR?>/02.jpg);
			background-repeat: no-repeat;
			background-position: top center;
			padding-top: 10px;
		}

		.ui-bar-a {
			color: #000;
			font-weight: bold;
			text-shadow: 0 1px 1px white;
		}
		
		a {
			text-decoration: none;
						
		}
		.intro a {
                        color: #7fc251 !important;
		}
		
		.intro {
			text-align: center;
		}

		.ui-content {
			padding: 0px;
			background-color: #292929;
		}

		.ui-content h2 {
			padding-left: 15px;
			font-size: 18px;
			text-transform: uppercase;
			font-weight: 500;
		}

		.views {
			max-width: 50em;
			padding-left: 5%;
		}
		* {
			text-shadow: none !important;
			font-family: "Gotham SSm 5r", "Gotham SSm A", "Gotham SSm B", sans-serif;
			font-size: 14px;
		}
		.views a {
			border: 1px solid red;
			display: inline-block;
			width: 45%;
			border: 1px solid white;
			color: white !important;
			text-align: center;
			background-color: #FA413C;
			padding: 5px 0 5px 0;
			vertical-align: middle;
			font-weight: normal !important;
			font-size: 12px;
			text-transform: uppercase;
		}	

		.ui-btn-text {
			text-transform: uppercase;
		}

		li.divider {
			text-transform: uppercase;
		}
		.ui-collapsible-heading {
			font-size: 18px !important;
		}

		.ui-listview li {
			padding: 0 36px;
		}
		img.movlogo {
			width: 80%;
			max-width: 450px;
		}

		.intro h3 {
			margin-bottom: 2em;			
			text-transform: uppercase;
			font-size: 18px;
		}

		a[data-role="button"]{
			background-color: #FA413C;
			border: 1px solid white;	
		}

		.inner_content{
			padding: 15px;
		}

		.inner_content a {
			color: white !important;
		}

		.ui-btn {
			font-weight: normal !important;
			border: none;
			border-top: 1px solid rgba(255, 255, 255, 0.4);


                        background-color: rgb(45, 135, 144);

			height: 45px;
			line-height: 45px;
			padding: 0 96px 0 12px;
			text-align: left;
			width: 100%;
			position: relative;
		}
		.ui-btn-text {
			display: block;
			width: 95%;
                        background-image: url(images/arrow.png)!important;
                        background-repeat: no-repeat !important;
                        background-position: right center !important;
		}

		.schedule_class{
			position: relative;
		}
		.schedule_class::after {
			content: " ";
			display: block;
			position: absolute;
			width: 23px; 
			height: 23px;
			top: 20px;
			right: 4%;
			
                        background-image: url(images/plus.png)!important;
                        background-repeat: no-repeat !important;
		}

		.ui-btn .ui-icon{
			display: none;
		}

		.ui-btn-inner{
			padding: 0 12px !important;
			
		}
		#schedule h3{
			position: relative;
		}

		.ui-li-static .ui-li {
			padding: .7em 15px .7em 45px;
			display: block;
		}		

	</style>



	<script src="jquery-1.6.4.min.js"></script> 
	<script src="jquery.mobile-1.0rc2.min.js"></script>

	<script src="app.js"></script> 
	<script src="data.js"></script> 

	<script type="text/javascript">

		
	var App = new _App();
	
	$( function() {
		
		$('#searchButton').click(function() {
		  			
  			
		});


		$(".classes li").click(function(a, b){
			$(this).find(".detail").toggle();
		});

		$('.add_class').click(function(event,a) {
		 	event.stopPropagation();	
			$(event.target).html("Class Added!");
			class_id = event.target.className;

			var rex = new RegExp(' class_([0-9]*)');
			matches = rex.exec( class_id );
			class_id = matches[1];

			$.post("/<?= $APP_SUBDIR ?>/member.php","action=add_class&class=" + class_id);
	
			return false;
		});

                $('.remove_class').click(function(event,a) {
                        event.stopPropagation();
		        class_id = event.target.className;

			var rex = new RegExp(' class_([0-9]*)');
			matches = rex.exec( class_id );
			class_id = matches[1];
												
                        $.post("/<?= $APP_SUBDIR ?>/member.php","action=remove_class&class=" + class_id);

			$("#my-schedule li.class_item_" + class_id).hide();

			$(event.target).hide();

			return false;
                });


		$('.logout').click(function(event,a) {
                        event.stopPropagation();

                        $.post("/<?= $APP_SUBDIR ?>/member.php","action=logout");
                });

	
		$("#form_register").submit(function(event){
			event.preventDefault(); 
			    
			$.ajax({  
			  type: "POST",  
			  url: "/<?= $APP_SUBDIR ?>/member.php",  
			  data: $("#form_register").serialize(),  
			  success: function(data){
				if (data == 'OK'){
					$(".register_message").html("<div class='intro'>Thank you for registering! <a href='/<?= $APP_SUBDIR ?>/index.php?refresh=1' data-ajax='false'>Continue</a></div>");  
					$(".register_message").show();
					$("#form_register").hide();
					
				}else{
					$(".register_message").html( data );
                                        $(".register_message").show();
				}
			  },
			});

			return false;
		});

		$("#form_login").submit(function(event){
                        event.preventDefault();

                        $.ajax({
                          type: "POST",
                          url: "/<?= $APP_SUBDIR ?>/member.php",
                          data: $("#form_login").serialize(),
                          success: function(data){
                                if (data == 'OK'){
					window.location = "/<?= $APP_SUBDIR ?>/index.php?refresh=1";
                                }else{
                                        $(".login_message").html( data );
                                        $(".login_message").show();
                                }
                          },
                        });

                        return false;
                });

		
	} );


    $("#home").live('pageshow',function() {
	    

    });
    $(".club_page").live('pageshow', function(event, ui){
		    var club_id = this.id.replace('club-','');
		    var phone = $( '#' + this.id + ' .club_phone').html();
    		    var email = $( '#' + this.id + ' .club_email').html();
		    $('.contact_phone').html( phone );
		    $('.contact_email').html( email );

		 $('#select_club')[0].value = club_id;
	   });


	function _debug(s){
		console.log(s);
	}

	document.addEventListener("deviceready", onDeviceReady, false);

    // PhoneGap is ready
    //
    function onDeviceReady() {
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
    }



	$(".swipedays li").live('swiperight',function(event, ui){
		new_class = "";
		full_list = $(event.target).parents('.swipedays');
		active = $(event.target).parents('.swipeday');

		var classList = $(active).attr('class').split(/\s+/);
		$.each( classList, function(index, item){
		    if (item.indexOf('day-') == 0){
			var a = item.split('-');
			b =  (parseInt(a[1]) - 1);
			if (b <1 ){
				b = 1;
			}
			new_class = 'day-' + b;
		    }
		});

		$( full_list ).children().trigger( "collapse" );
		$( full_list ).children( '.' + new_class ).trigger("expand");
        });

	$(".swipedays li").live('swipeleft',function(event, ui){

                new_class = "";
                full_list = $(event.target).parents('.swipedays');
                active = $(event.target).parents('.swipeday');

                var classList = $(active).attr('class').split(/\s+/);
                $.each( classList, function(index, item){
                    if (item.indexOf('day-') == 0){
                        var a = item.split('-');
                        b =  (parseInt(a[1]) + 1);
                        if (b > 7 ){
                                b = 7;
                        }
                        new_class = 'day-' + b;
                    }
                });

                $( full_list ).children().trigger( "collapse" );
                $( full_list ).children( '.' + new_class ).trigger("expand");

        });
	


var onSuccess = function(position) {
    alert('Latitude: '          + position.coords.latitude          + '\n' +
          'Longitude: '         + position.coords.longitude         + '\n' +
          'Altitude: '          + position.coords.altitude          + '\n' +
          'Accuracy: '          + position.coords.accuracy          + '\n' +
          'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
          'Heading: '           + position.coords.heading           + '\n' +
          'Speed: '             + position.coords.speed             + '\n' +
          'Timestamp: '         + new Date(position.timestamp)      + '\n');
};

// onError Callback receives a PositionError object
//
function onError(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}

	

</script>  
  
</head>
<body>

<div data-role="page" id="home" data-theme="a">
</div>

<?

$club = $myClub;

if ($_GET["schedule"]){
	$club = new Club( $_GET["schedule"] );
}

echo $_GET["club_name"];
echo $all_clubs[$_GET["club_name"]];

if ($_GET["club_name"] > ''){
	$club = new Club( $all_clubs[$_GET["club_name"]] );
}

$mode = $_GET["mode"];

?>

<div data-role="page" id="studio" data-theme="a"> 

  <div data-role="header">
	<a href="#" class="back" onclick="return window.history.back()"><img src="back.png"/></a>
  </div> 

  <div data-role="content">

   <p>
	<h2><?= $club->ClubName ?> Studio Schedule</h2>
          <div class="views"><a href="#studio">View By Studio</a> <a href="#daily">View By Day</a></div>
	</p>
	  <div data-role="collapsible-set" class="schedule">	

<?	$studios = $studioManager->SelectStudioByLocationAll($club->ID);
$classes = $studioManager->SelectClassesByClub( $club->ID );

	$days = array( 1=>"Monday", 2=>"Tuesday", 3=>"Wednesday", 4=>"Thursday", 5=>"Friday", 6=>"Saturday", 7=>"Sunday" );

	while ($studio = $studios->NextItem()) {  ?>
	<div data-role="collapsible" class="level1">
		<h3><?= $studio->StudioName ?> <span class="arrow"/></h3>   

		<ul data-role="listview" class="classes">
		<? foreach ($days as $day_id => $day ){ ?>			
			<li data-role="list-divider" class="divider"><?= $day ?></li>
			<? foreach ($classes as $c ){
				if ( ($c->Day == $day_id) && ($studio->ID == $c->StudioId) ){ 
					print_class( $c, $member );
				}	
			}
		} ?>
		</ul>
	</div>
	<? } ?>
  </div>
  </div>
</div>


<div data-role="page" id="daily" data-theme="a"> 
  <div data-role="header">
        <a href="#" class="back" onclick="return window.history.back()"><img src="back.png"/></a>
  </div>

  <div data-role="content">
  
	   <p>
		   <h2><?= $club->ClubName ?> Daily Schedule</h2>

          <div class="views"><a href="#studio">View By Studio</a> <a href="#daily">View By Day</a></div>
	
		</p>
  <div data-role="collapsible-set" class="swipedays-disabled">

<?

$classes = $studioManager->SelectClassesByClub( $club->ID );

	$days = array( 1=>"Monday", 2=>"Tuesday", 3=>"Wednesday", 4=>"Thursday", 5=>"Friday", 6=>"Saturday", 7=>"Sunday" );


	foreach ($days as $day_id => $day ){ 

	$studios = $studioManager->SelectStudioByLocationAll($club->ID);

	?>			
	<div data-role="collapsible" class="swipeday day-<?= $day_id ?>">
		<h3><?= $day ?></h3>   

		<ul data-role="listview" class="classes" data-theme="c">
			<? while ($studio = $studios->NextItem()) {  ?>
			<li data-role="list-divider" class="divider"><?= $studio->StudioName ?></li>
			<? foreach ($classes as $c ){
				if ( ($c->Day == $day_id) && ($studio->ID == $c->StudioId) ){
					print_class($c, $member);	
				}
			}
			 ?>
		<? } ?>
		</ul>
	</div>
	<? } ?>
  </div>
</div>
</div>
  <div data-role="content"> 

    </div>  
  </div> 
 
</div>



<div data-role="page" id="register" data-theme="a"> 
 
<div data-role="header" data-position="fixed"> 
	  <h1>Registration</h1> 
  </div>


<div class="ui-body"> 
<div class="regsiter_form">
<h3 class="register_message" style="display: none;"></h3>

<form id="form_register">
<p>Please fill out the form below:</p>

<input type="hidden" name="action" value="register"/>
<div data-role="fieldcontain" class="aui-hide-label">
	<label for="username">Email:</label>
	<input type="text" name="email" id="username" value="" placeholder="Email"/>
</div>
<div data-role="fieldcontain" class="aui-hide-label">
	<label for="select_club">Your Club:</label>
	<select name="club" id="select_club">
<?
$clubs = $clubManager->SelectAll();
 while ($club = $clubs->NextItem()) { ?>
		<?  if ($club->ID > 0) { ?>
		<option value="<?= $club->ID ?>"><?= $club->ClubName ?></option>
	<? }
	} ?>

	</select>
</div>
<div data-role="fieldcontain" class="aui-hide-label">

	<label for="password">Password:</label>
	<input type="password" name="password" id="password" value="" placeholder=""/>
</div>
<div data-role="fieldcontain" class="aui-hide-label">
	<label for="password">Password (confirm):</label>
	<input type="password" name="password2" id="password" value="" placeholder=""/>
</div>
<div data-role="fieldcontain" class="aui-hide-label">
	<input type="submit" name="submit" id="submit" value="Register"/>
</div>
</div>
</form>
  </div>

  <div data-role="content"> 

    </div>  
  </div> 
 
</div>


 <script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21947500-1']);
  _gaq.push(['_setDomainName', '.theathleticclubs.ca']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

window.location.hash="<?= $_GET["view"]  ?>";

</script>

</body>
</html>