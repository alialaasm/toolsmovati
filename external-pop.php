<?php
/*********************************************************************
 * FILE: class.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Displays a content section of the site.
 * *****************************************************************
 *
 */
session_start();

if(!isset($_GET['CID'])) $_GET['CID']=1;

require_once("config.php");
require_once("rhinoflow/fckeditor/fckeditor.php");
require_once("DAL/ClassManager.php");

$classManager = new ClassManager();
$classes = $classManager->SelectByClassID($_GET['CID']);

while ($class = $classes->NextItem()) {
	
?>
<style type="text/css">
<!--
#classwrapper {
	font-size: 12px;
	color: #FFF;
	width: 100%;
}
#classwrapper #title {
	font-size: 18px;
	font-weight: bold;
	color: #FFF;
}
#classwrapper #description {
	margin-top: 10px;
	margin-bottom: 10px;
}
-->
</style>
<script type="text/javascript" src="/js/flowplayer-3.2.0.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/flowplayerstyle.css">

<div id="classwrapper">
	<div id="title"><?=$class->ClassName?></div>
	<div id="category">Category: <?=$class->Category?></div><div id="code">Difficulty: <?=$class->ClassCode?></div>
	<div id="description"><?=$class->Description?></div>
	<? if($class->VideoLink != ""){ ?>
        <a  
             href="<?=$class->VideoLink?>"  
             style="display:block;width:510px;height:330px;margin-left:auto;margin-right:auto;"  
             id="player"> 
        </a> 
        <script>
            flowplayer("player", "/videos/flowplayer-3.2.0.swf",  {
				clip: {
					// these two configuration variables does the trick
					autoPlay: true, 
					autoBuffering: true // <- do not place a comma here  
				}
			});
        </script>
	<? } ?> 
</div>
<? } ?>