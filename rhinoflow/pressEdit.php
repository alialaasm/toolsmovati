<?php
/*********************************************************************
 * FILE: press-edit.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Displays a content section of the site.
 * *****************************************************************
 *
 */

require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/PressManager.php");

authenticate();

$message = "";
$url = "";

$pressID = get_int("pressID");

$pressManager = new PressManager();

$pressRelease = new Press($pressID);


if ($pressID > 0)
{
	$pressRelease = new Press($pressID);

	if ($pressRelease->LoadError)
	{
		echo "Error loading item.  This item cannot be accessed or does not exist.";
		exit();
	}
}
else
{
	$pressRelease = new Press();
}


if (IsPostBack)
{
	$action = post_text("txtAction");

	if ($action == "save" || $action == "apply")
	{
		$pressRelease->ReleaseDate = post_text("txtReleaseDate");
		$pressRelease->Title = post_text("txtTitle");
		$pressRelease->Description = post_text("txtDescription");
		$pressRelease->Active = post_bool("chkActive");

		$pressRelease->Update();

		$message = "Press release successfully updated.";

		$url = ($action == "apply") ? "rhinoflow/pressEdit.php?pressID=" . $pressRelease->ID : "rhinoflow/press.php";
		$url = SITE_URL . $url;
	}
}

$txtDescription = new FCKeditor("txtDescription");
$txtDescription->BasePath = "rhinoflow/fckeditor/";
$txtDescription->Value = $pressRelease->Description;
$txtDescription->Config["EnterMode"] = "br";
$txtDescription->Width = 600;
$txtDescription->Height = 350;

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message, $url); ?>


<script type="text/javascript" src="js/date.js"></script>

<script type="text/javascript">
	validateForm = function(Action)
	{

		if ($("#txtTitle").val() == "")
		{
			$("#txtTitle").focus();
		}
		else if (!(isDate($("#txtReleaseDate").val())))
		{
			alert($("#txtReleaseDate").val());
			alert("Please enter a valid release date in the format YYYY-MM-DD.");
			$("#txtReleaseDate").focus();
		}
		else
		{
			$("#txtAction").val(Action);
			return true;
		}

		return false;
	}
</script>


<div id="contentAdmin">

	<h1>Press Release</h1>

	<table class="edit" cellspacing="0">
		<tr>
			<th>Title</th>
			<td><input type="text" id="txtTitle" name="txtTitle" value="<?=htmlentities($pressRelease->Title) ?>" size="50" maxlength="100" />
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Release Date</th>
			<td>
				<input type="text" id="txtReleaseDate" name="txtReleaseDate" value="<?=$pressRelease->ReleaseDate ?>" size="10" maxlength="10" />
				(YYYY-MM-DD)
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Description</th>
			<td>
				<? $txtDescription->Create(); ?>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Active</th>
			<td><input type="checkbox" id="chkActive" name="chkActive" value="1" <? if ($pressRelease->Active) { echo "checked=\"checked\""; } ?> /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<td colspan="2" style="text-align: center;">
  				<input type="submit" name="txtSubmit" value="Save" title="Save changes and return to press list." onclick="return validateForm('save');" />
  				<input type="submit" name="txtApply" value="Apply" title="Apply changes and continue editing." onclick="return validateForm('apply');" />
  				<input type="button" name="txtReset" value="Cancel" title="Cancel all unsaved changes." onclick="window.location = SITE_URL + 'rhinoflow/press.php'" />
			</td>
		</tr>
	</table>

</div>

<? InsertFooter(); ?>