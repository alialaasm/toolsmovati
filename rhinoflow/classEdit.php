<?php
/*********************************************************************
 * FILE: classEdit.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Displays a content section of the site.
 * *****************************************************************
 *
 */

require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClassManager.php");

authenticate();

$message = "";
$url = "";

$classID = get_int("classID");

$classManager = new ClassManager();

$class = new ClassDescription($classID);

if ($classID > 0)
{
	$class = new ClassDescription($classID);

	if ($class->LoadError)
	{
		echo "Error loading item.  This item cannot be accessed or does not exist.";
		exit();
	}
}
else
{
	$class = new ClassDescription();
}


if (IsPostBack)
{
	
	$action = post_text("txtAction");

	if ($action == "save" || $action == "apply")
	{
		$class->CatId = post_text("dropCategory");
		$class->ClassName = post_text("txtClassName");
		$class->Description = post_text("txtDescription");
		$class->VideoLink = post_text("txtVideoLink");		
		$class->ClassCode = post_text("dropClassCode");
		$class->Active = post_bool("chkActive");

		$class->Update();

		$message = "Class successfully updated.";

		$url = ($action == "apply") ? "rhinoflow/classEdit.php?pressID=" . $class->ID : "rhinoflow/class.php";
		$url = SITE_URL . $url;
	}
}

$txtDescription = new FCKeditor("txtDescription");
$txtDescription->BasePath = "rhinoflow/fckeditor/";
$txtDescription->Value = $class->Description;
$txtDescription->Config["EnterMode"] = "br";
$txtDescription->Width = 600;
$txtDescription->Height = 350;

$categories = $classManager->SelectClassCategories();

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message, $url); ?>


<script type="text/javascript">
	validateForm = function(Action)
	{

		if ($("#txtCategory").val() == "")
		{
			alert("Please enter a Category.");
			$("#txtCategory").focus();
			
		}
		else
		{
			$("#txtAction").val(Action);
			return true;
		}

		return false;
	}
</script>


<div id="contentAdmin">

	<h1>Classes</h1>

	<table class="edit" cellspacing="0">
		<tr>
			<th>Category</th>
			<td>
				<select id="dropCategory" name="dropCategory">
					<? for ($i = 0; $i < count($categories); $i++) { ?>
						<option value="<?=$categories[$i+1][2] ?>" <? if ($categories[$i+1][2] == $class->ID) { echo "selected"; } ?>>
							<?=$categories[$i+1][1] ?>
						</option>
					<? } ?>

				</select>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Class Name</th>
			<td><input type="text" id="txtClassName" name="txtClassName" value="<?=htmlentities($class->ClassName) ?>" size="40" maxlength="45" />
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Description</th>
			<td>
				<? $txtDescription->Create(); ?>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
		  <th>Video Link</th>
		  <td><input name="txtVideoLink" type="text" id="txtVideoLink" value="<?=htmlentities($class->VideoLink) ?>" size="100" /></td>
	  </tr>
		<tr>
			<th>Active</th>
			<td><input type="checkbox" id="chkActive" name="chkActive" value="1" <? if ($class->Active) { echo "checked=\"checked\""; } ?> /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Class Code</th>
			<td>
				<select id="dropClassCode" name="dropClassCode">
					<option value="B" <? if ($class->ClassCode == "B") { echo "selected"; } ?>>B - Beginner</option>
					<option value="E" <? if ($class->ClassCode == "E") { echo "selected"; } ?>>E - Everyone Welcome</option>
					<option value="I" <? if ($class->ClassCode == "I") { echo "selected"; } ?>>I - Intermediate Class </option>
					<option value="A" <? if ($class->ClassCode == "A") { echo "selected"; } ?>>A - Advanced Class</option>
					<option value="INT" <? if ($class->ClassCode == "INT") { echo "selected"; } ?>>INT - A VERY intense class</option>
				</select>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>

		<tr>
			<td colspan="2" style="text-align: center;">
  				<input type="submit" name="txtSubmit" value="Save" title="Save changes and return to class item." onclick="return validateForm('save');" />
  				<input type="submit" name="txtApply" value="Apply" title="Apply changes and continue editing." onclick="return validateForm('apply');" />
  				<input type="button" name="txtReset" value="Cancel" title="Cancel all unsaved changes." onclick="window.location = SITE_URL + 'rhinoflow/class.php'" />
			</td>
		</tr>
	</table>

</div>

<? InsertFooter(); ?>