<?php


require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/PressManager.php");

authenticate();

$message = "";


$pressManager = new PressManager();

if (IsPostBack)
{
	$action = post_text("txtAction");

	if ($action == "delete")
	{
		$press = new Press(post_int("txtPressID"));

		if ($press->LoadError)
		{
			$message = "Press release does not exist or you do not have access.";
		}
		else
		{
			$press->Delete();
			$message = "Press release deleted.";
		}
	}
}

$press = $pressManager->SelectAll();

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message); ?>

<script type="text/javascript">
	DeletePress = function(ID)
	{
		var confirmed;

		confirmed = confirm ("Are you sure you want to delete this press release?");

		if (confirmed)
		{
			$("#txtPressID").val(ID);
			$("#txtAction").val("delete");

			$("#frmAdmin").submit();
		}

		return false;
	}
</script>

<input type="hidden" id="txtPressID" name="txtPressID" value="0" />

<div id="contentAdmin">

	<h1>Press Releases</h1>

	<input type="button" value="New Press Release" onclick="window.location = SITE_URL + 'rhinoflow/pressEdit.php'" />

	<table class="list" cellspacing="0">
		<tr>
			<th class="left"></th>
			<th>Date</th>
			<th>Title</th>
			<th class="edit"></th>
			<th class="remove"></th>
			<th class="right"></th>
		</tr>
		<tr><td class="big_spacer"></td></tr>

	<?  while ($pressRelease = $press->NextItem())
		{

			if (!($pressRelease->Active)) { $class = "inactive"; }
			elseif ($press->OddRow()) { $class = "odd_row"; }
			else { $class = "even_row"; }

		?>

		<tr class="<?=$class ?>">
			<td class="left"></td>
			<td><?=date("Y-m-d", strtotime($pressRelease->ReleaseDate)) ?></td>
			<td style="text-align:left;"><?=$pressRelease->Title ?></td>
			<td class="edit">[ <a href="rhinoflow/pressEdit.php?pressID=<?=$pressRelease->ID ?>">EDIT</a> ]</td>
			<td class="remove">[ <a href="delete" onclick="return DeletePress(<?=$pressRelease->ID ?>);">DELETE</a> ]</td>
			<td class="right"></td>
		</tr>
		<tr class="spacer"><td></td></tr>

	<? } ?>
	</table>

</div>

<? InsertFooter(); ?>