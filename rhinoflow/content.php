<?php


require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ContentManager.php");

authenticate();

$message = "";
$url = "";

$contentID = get_int("contentID");
$menuNodeID = get_int("nodeID");
$parentNodeID = get_int("parentNodeID");





if ($contentID > 0)
{
	$content = new Content($contentID);

	if ($content->LoadError)
	{
		echo "Error loading content item.  This item cannot be accessed or does not exist.";
		exit();
	}
}
else
{
	$content = new Content();

	$content->Title = "New Content Section";
}


if (IsPostBack)
{
	$action = post_text("txtAction");

	$content->Title = post_text("txtTitle");
	$content->Description = post_text("txtDescription");

	$content->Update();

	$message = "Content successfully updated.";

	$queryString = "?contentID=" . $content->ID . "&nodeID=" . $menuNodeID . "&parentNodeID=" . $parentNodeID;

	$menuSectionURL = ($parentNodeID == 0 && $menuNodeID == 0) ? "/rhinoflow/menu.php" : "menuEdit.php";

	$url = ($action == "apply") ? "/rhinoflow/content.php" . $queryString : $menuSectionURL . $queryString;


}


$oFCKeditor = new FCKeditor("txtDescription");
$oFCKeditor->BasePath = "rhinoflow/fckeditor/";
$oFCKeditor->Value = $content->Description;
$oFCKeditor->Config["EnterMode"] = "br";
$oFCKeditor->Width = 600;
$oFCKeditor->Height = 450;

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message, $url); ?>


<script type="text/javascript">
	validateForm = function(Action)
	{

		if ($("#txtTitle").val() == "")
		{
			alert("Please enter a Title.");
			$("#txtTitle").focus();
		}
		else
		{
			$("#txtAction").val(Action);
			return true;
		}

		return false;
	}
</script>


<div id="contentAdmin">

	<h1><?=$content->Title ?></h1>


	<table class="edit" cellspacing="0">
		<tr>
			<th>Title</th>
			<td><input type="text" id="txtTitle" name="txtTitle" value="<?=htmlentities($content->Title) ?>" size="50" maxlength="100" />
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Description</th>
			<td>
				<? $oFCKeditor->Create(); ?>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<td colspan="2" style="text-align: center;">
				<input type="submit" name="txtSubmit" value="Save" title="Save changes and return to menu." onclick="return validateForm('save');" />
  				<input type="submit" name="txtApply" value="Apply" title="Apply changes and continue editing." onclick="return validateForm('apply');" />
  			<? if ($parentNodeID == 0 && $menuNodeID == 0) { ?>
  				<input type="button" name="txtCancel" value="Cancel" title="Cancel all unsaved changes." onclick="window.location = SITE_URL + 'rhinoflow/menu.php'" />
  			<? } else { ?>
  				<input type="button" name="txtCancel" value="Cancel" title="Cancel all unsaved changes." onclick="window.location = SITE_URL + 'rhinoflow/menuEdit.php?contentID=<?=$content->ID ?>&amp;nodeID=<?=$menuNodeID ?>&amp;parentNodeID=<?=$parentNodeID ?>'" />
  			<? } ?>
			</td>
		</tr>
	</table>

</div>

<? InsertFooter(); ?>