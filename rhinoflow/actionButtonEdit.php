<?php
/*********************************************************************
 * FILE: actionButtonEdit.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Displays a content section of the site.
 * *****************************************************************
 *
 */
require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/file.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ActionButtonManager.php");

authenticate();

$message = "";
$url = "";

$actionButtonID = get_int("actionButtonID");

$actionButton = new ActionButton($actionButtonID);


if ($actionButtonID > 0)
{
	$actionButton = new ActionButton($actionButtonID);
	if ($actionButton->LoadError)
	{
		echo "Error loading item.  This item cannot be accessed or does not exist.";
		exit();
	}
}
else
{
	$actionButton = new ActionButton();
}

if (IsPostBack)
{

	$action = post_text("txtAction");

	if ($action == "save" || $action == "apply")
	{
		$actionButton->URL = post_text("txtURL");
		$actionButton->Title = post_text("txtTitle");
		$actionButton->SubTitle = post_text("txtSubTitle");
		$actionButton->Colour = post_text("dropColour");
		$actionButton->Active = post_bool("chkActive");


		$actionButton->Update();

		$message = "Action Button successfully updated.";

		$url = ($action == "apply") ? "rhinoflow/actionButtonEdit.php?actionButtonID=" . $actionButton->ID : "rhinoflow/actionButton.php";
		$url = SITE_URL . $url;

	}
}


define("TEXT", 0);
define("COLOUR", 1);
$colours = array();

$colours[] = array("Blue", "blue");
$colours[] = array("Green", "green");
$colours[] = array("Yellow", "yellow");
$colours[] = array("Orange", "orange");
$colours[] = array("Red", "red");
$colours[] = array("Purple", "purple");
$colours[] = array("Aqua", "aqua");

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message, $url); ?>


<script type="text/javascript">
	validateForm = function(Action)
	{

		if ($("#txtTitle").val() == "")
		{
			alert("Please enter a Title.");
			$("#txtTitle").focus();

		}
		else
		{
			$("#txtAction").val(Action);
			return true;
		}

		return false;
	}

	updateButton = function()
	{

	}
</script>


<div id="contentAdmin">

	<h1>Quick Action Buttons</h1>


	<table class="edit" cellspacing="0">
		<tr>
			<th>Title</th>
			<td><input type="text" id="txtTitle" name="txtTitle" value="<?=htmlentities($actionButton->Title) ?>" size="50" maxlength="75" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>

		<tr>
			<th>Sub-title</th>
			<td><input type="text" id="txtSubTitle" name="txtSubTitle" value="<?=htmlentities($actionButton->SubTitle) ?>" size="50" maxlength="75" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>

		<tr>
			<th>URL</th>
			<td><input type="text" id="txtURL" name="txtURL" value="<?=htmlentities($actionButton->URL) ?>" size="75" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>

		<tr>
			<th>Colour</th>
			<td>
				<select id="dropColour" name="dropColour">
					<option value="">Select a colour...</option>
					<? foreach ($colours as $colour) { ?>
						<option value="<?=$colour[COLOUR] ?>" <? if ($colour[COLOUR] == $actionButton->Colour) echo "selected=\"selected\"" ?>>
							<?=$colour[TEXT] ?>
						</option>
					<? } ?>
				</select>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>

		<tr>
			<th></th>
			<td style="display:<?=( $actionButton->Colour == "") ? "none" : "block" ?>"></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Active</th>
			<td><input type="checkbox" id="chkActive" name="chkActive" value="1" <? if ($actionButton->Active) { echo "checked=\"checked\""; } ?> /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<td colspan="2" style="text-align: center;">
  				<input type="submit" name="txtSubmit" value="Save" title="Save changes and return to action button listing." onclick="return validateForm('save');" />
  				<input type="submit" name="txtApply" value="Apply" title="Apply changes and continue editing." onclick="return validateForm('apply');" />
  				<input type="button" name="txtReset" value="Cancel" title="Cancel all unsaved changes." onclick="window.location = SITE_URL + 'rhinoflow/actionButton.php'" />
			</td>
		</tr>
	</table>

</div>

<? InsertFooter(); ?>