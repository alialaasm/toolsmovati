<?php
/*********************************************************************
 * FILE: careerEdit.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Displays a content section of the site.
 * *****************************************************************
 *
 */

require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/CareerManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClubManager.php");

authenticate();

$message = "";
$url = "";

$clubID = get_int("clubID");

$careerID = get_int("careerID");
$careerManager = new CareerManager();
$career = new Career($careerID);


if ($careerID > 0)
{
	$career = new Career($careerID);

	if ($career->LoadError)
	{
		echo "Error loading item.  This item cannot be accessed or does not exist.";
		exit();
	}
}
else
{
	$career = new Career();
}


if (IsPostBack)
{

	$action = post_text("txtAction");

	if ($action == "save" || $action == "apply")
	{
		$clubIDs = post_array("chkClub");

		$career->Position = post_text("dropPosition");
		$career->Description = post_text("txtDescription");
		$career->Active = true;

		$career->Update();

		$career->UpdateClubAvailability($clubIDs);

		$message = "Career successfully updated.";

		$url = ($action == "apply") ? "rhinoflow/careerEdit.php?careerID=" . $career->ID  : "rhinoflow/career.php?clubID=" . $clubID;
		$url = SITE_URL . $url;
	}
}

$clubManager = new ClubManager();
$clubs = $clubManager->SelectWithCareerAvailability($careerID);

$txtDescription = new FCKeditor("txtDescription");
$txtDescription->BasePath = "rhinoflow/fckeditor/";
$txtDescription->Value = $career->Description;
$txtDescription->Config["EnterMode"] = "br";
$txtDescription->Width = 600;
$txtDescription->Height = 350;

$clubPositions = $careerManager->SelectPositions("Club");
$numClubPositions = count($clubPositions);

$departmentalPositions = $careerManager->SelectPositions("Departmental");
$numDepartmentalPositions = count($departmentalPositions);

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message, $url); ?>


<script type="text/javascript">
	validateForm = function(Action)
	{
		if (true)
		{
			$("#txtAction").val(Action);
			return true;
		}

		return false;
	}
</script>


<div id="contentAdmin">

	<h1>Careers</h1>

	<table class="edit" cellspacing="0">
		<tr>
			<th>Position</th>
			<td>
				<select id="dropPosition" name="dropPosition">
					<option value="">Select a position...</option>
					<optgroup label="Club">
						<? for ($i = 0; $i < $numClubPositions; $i++) { ?>
							<option value="<?=$clubPositions[$i] ?>" <? if ($clubPositions[$i] == $career->Position) { echo "selected"; } ?>>
								<?=$clubPositions[$i] ?>
							</option>
						<? } ?>
					</optgroup>

					<optgroup></optgroup>

					<optgroup label="Departmental">
						<? for ($i = 0; $i < $numDepartmentalPositions; $i++) { ?>
							<option value="<?=$departmentalPositions[$i] ?>" <? if ($departmentalPositions[$i] == $career->Position) { echo "selected"; } ?>>
								<?=$departmentalPositions[$i] ?>
							</option>
						<? } ?>
					</optgroup>
				</select>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Description</th>
			<td>
				<? $txtDescription->Create(); ?>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>

		<tr>
			<th>Clubs</th>
			<td>
				<table cellpadding="2" cellspacing="1" border="0">
				<? while ($club = $clubs->NextItem()) { ?>
					<tr>
						<td>
							<input type="checkbox" id="chkClub<?=$club->ID ?>" name="chkClub[]" value="<?=$club->ID ?>" <? if ($club->PositionIsAvailable) { echo "checked"; } ?>>
							<?=$club->ClubName ?>
						</td>

					<? if ($club = $clubs->NextItem()) { ?>
						<td>
							<input type="checkbox" id="chkClub<?=$club->ID ?>" name="chkClub[]" value="<?=$club->ID ?>" <? if ($club->PositionIsAvailable) { echo "checked"; } ?>>
							<?=$club->ClubName ?>
						</td>
					<? } ?>
					</tr>

				<? } ?>
				</table>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>

		<tr>
			<td colspan="2" style="text-align: center;">
  				<input type="submit" name="txtSubmit" value="Save" title="Save changes and return to career listing." onclick="return validateForm('save');" />
  				<input type="submit" name="txtApply" value="Apply" title="Apply changes and continue editing." onclick="return validateForm('apply');" />
  				<input type="button" name="txtReset" value="Cancel" title="Cancel all unsaved changes." onclick="window.location = SITE_URL + 'rhinoflow/career.php?clubID=<?=$clubID ?>'" />
			</td>
		</tr>
	</table>

</div>

<? InsertFooter(); ?>