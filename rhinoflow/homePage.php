<?php


require_once("../config.php");

require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/HomePage.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/Menu.php");

authenticate();

$message = "";


$homePage = new HomePage();
$menu = new Menu();

if ($homePage->LoadError)
{
	echo "Error loading home page item.  This item cannot be accessed or does not exist.";
	exit();
}



if (IsPostBack)
{
	$homePage->Button1->Title = post_text("txtTitle1");
	$homePage->Button1->Description = post_text("txtDescription1");
	$homePage->Button1->NavigateText = post_text("txtNavigateText1");
	$homePage->Button1->NavigateURL = post_text("dropNavigateURL1");

	$homePage->Button2->Title = post_text("txtTitle2");
	$homePage->Button2->Description = post_text("txtDescription2");
	$homePage->Button2->NavigateText = post_text("txtNavigateText2");
	$homePage->Button2->NavigateURL = post_text("dropNavigateURL2");

	$homePage->Update();

	$message = "Home page successfully updated.";
}

$linkTitle = 0;
$linkURL = 1;
$links = $menu->RenderToArray();

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message); ?>


<div id="contentAdmin">

	<h1>Home Page</h1>

	<table class="edit" cellspacing="0">

		<tr>
			<th>Title 1</th>
			<td><input type="text" id="txtTitle1" name="txtTitle1" value="<?=htmlentities($homePage->Button1->Title) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Description 1</th>
			<td><textarea id="txtDescription1" name="txtDescription1" cols="50" rows="3"><?=htmlentities($homePage->Button1->Description) ?></textarea></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Link Text 1</th>
			<td><input type="text" id="txtNavigateText1" name="txtNavigateText1" value="<?=htmlentities($homePage->Button1->NavigateText) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Link 1</th>
			<td>
				<select id="dropNavigateURL1" name="dropNavigateURL1">
				<? for ($i = 0; $i < count($links[$linkTitle]); $i++) { ?>
					<option value="<?=$links[$linkURL][$i] ?>" <? if ($links[$linkURL][$i] == $homePage->Button1->NavigateURL) { echo "selected=\"selected\""; } ?>>
						<?=$links[$linkTitle][$i] ?>
					</option>
				<? } ?>
				</select>
			</td>
		</tr>
		<tr class="big_spacer"><td></td></tr>
		<tr>
			<th>Title 2</th>
			<td><input type="text" id="txtTitle2" name="txtTitle2" value="<?=htmlentities($homePage->Button2->Title) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Description 2</th>
			<td><textarea id="txtDescription2" name="txtDescription2" cols="50" rows="3"><?=htmlentities($homePage->Button2->Description) ?></textarea></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Link Text 2</th>
			<td><input type="text" id="txtNavigateText2" name="txtNavigateText2" value="<?=htmlentities($homePage->Button2->NavigateText) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Link 2</th>
			<td>
				<select id="dropNavigateURL2" name="dropNavigateURL2">
				<? for ($i = 0; $i < count($links[$linkTitle]); $i++) { ?>
					<option value="<?=$links[$linkURL][$i] ?>" <? if ($links[$linkURL][$i] == $homePage->Button2->NavigateURL) { echo "selected=\"selected\""; } ?>>
						<?=$links[$linkTitle][$i] ?>
					</option>
				<? } ?>
				</select>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>

		<tr>
			<td colspan="2" style="text-align: center;">
  				<input type="submit" name="txtSubmit" value="Update" title="Save changes." />
  				<input type="reset" name="txtReset" value="Cancel" title="Cancel all unsaved changes." />
			</td>
		</tr>
	</table>

</div>

<? InsertFooter(); ?>