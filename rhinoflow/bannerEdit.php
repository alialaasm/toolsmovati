<?php
/*********************************************************************
 * FILE: bannerEdit.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Displays a content section of the site.
 * *****************************************************************
 *
 */

require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/file.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/BannerManager.php");

authenticate();

$message = "";
$url = "";

$bannerID = get_int("bannerID");
$bannerManager = new BannerManager();

$banner = new Banner($bannerID);

$displayImage = "none";



if ($bannerID > 0)
{
	$banner = new Banner($bannerID);
	if ($banner->LoadError)
	{
		echo "Error loading item.  This item cannot be accessed or does not exist.";
		exit();
	}
	if ($banner->Filename != "")
		$displayImage = "";
}
else
{
	$banner = new Banner();
}



if (IsPostBack)
{

	$action = post_text("txtAction");

	if ($action == "save" || $action == "apply")
	{
		$banner->URL = post_text("txtURL");
		$banner->Title = post_text("txtTitle");
		$banner->Active = post_bool("chkActive");


		$filename = "";
		$success = "";


		// Upload the file chosen.
		// Constant Variables in config.php
		if ($_FILES["txtFilename"]["name"] != "")
		{
			if (is_image($_FILES["txtFilename"]["name"]))
			{
				if (! ($filename = upload_file("txtFilename", BANNER_GALLERY_PATH)) )
				{
					$message = "Error: unable to upload image file.";
				}
				else
				{
					$success = resize_image(BANNER_GALLERY_PATH . $filename, BANNER_GALLERY_THUMBNAIL_PATH . $filename, BANNER_GALLERY_THUMBNAIL_WIDTH, BANNER_GALLERY_THUMBNAIL_HEIGHT);
				}
			}
			else
			{
				$message = "Error: unrecognized file type, must be type jpg, jpeg, gif, png.";
			}
		}


		// no errors
		if ($message == "")
		{
			if ($filename != "")
			{
				// check for updating of file, if so get rid of old uploaded files.
				$oldFilename = $banner->Filename;
				if ($oldFilename != "")
				{
					@unlink(BANNER_GALLERY_PATH . $banner->Filename);
					@unlink(BANNER_GALLERY_DISPLAY_PATH . $banner->Filename);
				}
				$banner->Filename = $filename;
			}

			$banner->Update();

			$message = "Banner successfully updated.";

			$url = ($action == "apply") ? "bannerEdit.php?bannerID=" . $banner->ID : "banner.php";
			$url = SITE_URL . ADMIN_DIR . "/" . $url;
		}
	}
}

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message, $url); ?>


<script type="text/javascript">
	validateForm = function(Action)
	{

		$("#txtAction").val(Action);
		return true;

/*
		if ($("#txtTitle").val() == "")
		{
			alert("Please enter a Title.");
			$("#txtTitle").focus();

		}
		else
		{
			$("#txtAction").val(Action);
			return true;
		}

		return false;
*/
	}
</script>


<div id="contentAdmin">

	<h1>Images</h1>

	<table class="edit" cellspacing="0">
		<tr style="display:none;">
			<th>Title</th>
			<td><input type="text" id="txtTitle" name="txtTitle" value="<?=htmlentities($banner->Title) ?>" size="50" maxlength="75" /></td>
		</tr>
		<tr style="display:none;" class="spacer"><td></td></tr>
		<tr style="display:none;">
			<th>URL</th>
			<td><input type="text" id="txtURL" name="txtURL" value="<?=htmlentities($banner->URL) ?>" size="75" maxlength="100" />
		</tr>

		<tr>
			<th>Image Filename</th>
			<td><input type="file" id="txtFilename" name="txtFilename" size="75"></td>
		</tr>

		<tr style="display:<?=$displayImage ?>">
			<th>Current File</th>
			<td>
				
				<a href="<?= (BANNER_GALLERY . $banner->Filename) ?>" target="_blank"><img src="<?=BANNER_GALLERY_THUMBNAIL . $banner->Filename ?>" alt="" /></a>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Active</th>
			<td><input type="checkbox" id="chkActive" name="chkActive" value="1" <? if ($banner->Active) { echo "checked=\"checked\""; } ?> /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<td colspan="2" style="text-align: center;">
  				<input type="submit" name="txtSubmit" value="Save" title="Save changes and return to banner listing." onclick="return validateForm('save');" />
  				<input type="submit" name="txtApply" value="Apply" title="Apply changes and continue editing." onclick="return validateForm('apply');" />
  				<input type="button" name="txtReset" value="Cancel" title="Cancel all unsaved changes." onclick="window.location = ADMIN_URL + '/banner.php'" />
			</td>
		</tr>
	</table>

</div>

<? InsertFooter(); ?>