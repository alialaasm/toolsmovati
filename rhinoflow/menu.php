<?php
/*********************************************************************
 * FILE: menu.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Maintains the main menu.
 * *****************************************************************
 *
 */

require_once("../config.php");
require_once("../App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/Menu.php");


authenticate();



$message = "";
$url = "";


if (IsPostBack)
{

	$action = post_text("txtAction");
	$menuNodeID = post_int("txtMenuID");

	if ($menuNodeID > 0)
	{

		$menuNode = new MenuNode($menuNodeID);

		if (!($menuNode->LoadError))
		{
			if ($action == "moveup")
			{
				$menuNode->MoveUp();
			}
			else if ($action == "movedown")
			{
				$menuNode->MoveDown();
			}
			else if ($action == "delete")
			{
				$menuNode->Delete();
				$message = "Menu item successfully deleted.";
			}
		}

		$url = "/rhinoflow/menu.php";
	}
}


$menu = new Menu();


/* ----- Retrieve the Root Node ----- */
// Retrieve the root node so that the numeric bounds of the child
// nodes can be determined.

$result = $db->query("SELECT * FROM Menu WHERE ID = 1");

$rootNode = $result->fetch_object();

if ($rootNode === false) { return; }



/* ----- Retrieve the Menu ----- */
// Retrieve the descendants of the root node.
$query = "	SELECT
				Menu.*,
				IF (ISNULL(UserAccess.UserID), 0, 1) AS UserHasAccess
			FROM
				Menu
				LEFT JOIN UserAccess ON Menu.ID = UserAccess.MenuID AND UserAccess.UserID = " . session_int("USER_ID") . "
			WHERE
				LeftVal BETWEEN " . $rootNode->LeftVal . " AND " .$rootNode->RightVal . "
			ORDER BY
				LeftVal ASC";

$result = $db->query($query);


// Create an empty stack to hold the RightNode values.
$right = array();

$depth = -1;

$previousNode = null;
$currentNode = $result->fetch_object();
$nextNode = $result->fetch_object();

$rowNumber = 0;

$maxDepth = 2;

?>



<? InsertHeader(Template::$Admin); ?>


<? MessageBox($message, $url); ?>


<script type="text/javascript">
	DeleteMenuItem = function(ID)
	{
		var confirmed;

		confirmed = confirm ("This menu item and all sub items will be removed.  This action cannot be undone. Are you sure you want to delete this menu item?");

		if (confirmed)
		{
			$("#txtMenuID").val(ID);
			$("#txtAction").val("delete");

			$("#frmAdmin").submit();
		}

		return false;
	}

	ReOrder = function(ID, Direction)
	{
		$("#txtMenuID").val(ID);
		$("#txtAction").val(Direction);

		$("#frmAdmin").submit();

		return false;
	}
</script>



<input type="hidden" id="txtMenuID" name="txtMenuID" value="0" />


<div id="contentAdmin">

	<h1>Menu</h1>

	<? if (session_bool("USER_IS_SUPER")) { ?>
		<input type="button" value="New Main Section" onclick="window.location = SITE_URL + '<?=ADMIN_DIR ?>/menuEdit.php?parentNodeID=1'" />
	<? } ?>

	<table class="list" cellspacing="0">
		<tr>
			<th class="left"></th>
			<th style="text-align:left; padding-left: 5px;">Title</th>
			<th style="text-align:left;">Section Type</th>
			<th class="edit"></th>
			<th class="edit"></th>
			<th class="remove"></th>
			<th class="right"></th>
		</tr>
		<tr><td class="big_spacer"></td></tr>

		<?

		// Display each node in the menu.
		do
		{
			// The stack size is equal to the tree depth starting with row 0.
			$stack_size = count($right);

			// Only check stack if there is one.
			if ($stack_size > 0)
			{

				// Check if we should remove a node from the stack.
				while ($right[$stack_size - 1]->RightVal < $currentNode->RightVal)
				{
					array_pop($right);
					$stack_size--;
				}
			}

			$depth = count($right) - 1;

			if ($depth >= 0)
			{
				$rowNumber++;

				$class = "level_" . $depth;

				$paddingLeft = (35 * $depth) + 5;

				if ($currentNode->UserHasAccess || session_bool("USER_IS_SUPER"))
				{
					if ($currentNode->SectionType == "Careers") { $sectionLink = "career.php"; }
					else if ($currentNode->SectionType == "Classes") { $sectionLink = "class.php"; }
					else if ($currentNode->SectionType == "Club Locator") { $sectionLink = "club.php"; }
					else if ($currentNode->SectionType == "Contact") { $sectionLink = "club.php"; }
					else if ($currentNode->SectionType == "Content") { $sectionLink = "content.php?contentID=" . $currentNode->SectionTypeID; }
					else if ($currentNode->SectionType == "Department Directory") { $sectionLink = "department.php"; }
					else if ($currentNode->SectionType == "Group Fitness") { $sectionLink = "club.php"; }
					else if ($currentNode->SectionType == "Home Page") { $sectionLink = "homePage.php"; }
					else if ($currentNode->SectionType == "Pool Schedule") { $sectionLink = "club.php"; }
					else if ($currentNode->SectionType == "Press") { $sectionLink = "press.php"; }
					else if ($currentNode->SectionType == "Promotions") { $sectionLink = "club.php"; }
					else if ($currentNode->SectionType == "Membership Trial") { $sectionLink = "moduleData.php?module=Membership+Trial"; }
					else if ($currentNode->SectionType == "Online Promotion") { $sectionLink = "moduleData.php?module=Online+Promotion"; }
					else if ($currentNode->SectionType == "Personal Training Contact") { $sectionLink = "moduleData.php?module=Personal+Training+Contact"; }
					else { $sectionLink = ""; }
				}
				else
				{
					$sectionLink = "";
				}


			?>
            
			<? if ($sectionLink != "" || session_bool("USER_IS_SUPER")) { ?>
				<tr class="<?=$class ?>">

					<td class="left"></td>

					<td class="up_down_buttons" style="text-align:left; padding-left: <?=$paddingLeft ?>px">
							<a class="text_link" href="<?=ADMIN_DIR ?>/<?=$sectionLink ?>"><?=$currentNode->Title; ?></a>
					</td>

					<td style="text-align:left;">
							<a href="<?=ADMIN_DIR ?>/<?=$sectionLink ?>"><?=$currentNode->SectionType; ?></a>
                    </td>
					
                    <? if (session_bool("USER_IS_SUPER")) { ?>
                    
                    <td>
							<? if ($depth < $maxDepth) { ?>[ <a href="<?=ADMIN_DIR ?>/menuEdit.php?parentNodeID=<?=$currentNode->ID ?>">ADD</a> ]<? } ?>
					</td>


					<td>
							[ <a href="<?=ADMIN_DIR ?>/menuEdit.php?nodeID=<?=$currentNode->ID ?>">EDIT</a> ]
					</td>

					<td>
							[ <a href="<?=ADMIN_DIR ?>/delete-menu-item" onclick="DeleteMenuItem(<?=$currentNode->ID ?>); return false;">DELETE</a> ]
					</td>

					<td class="right"></td>
                    
                    <? } else { ?>
                    
                    
					<td>&nbsp;</td>


					<td>&nbsp;</td>

					<td>&nbsp;</td>

					<td class="right"></td>
                    
                    <? } ?>
                    
				</tr>
				<tr><td class="spacer"></td></tr>
						
			<? }?>
			<?

			}


			// Add this node to the stack.
			$right[] = $currentNode;

			$previousNode = $currentNode;
			$currentNode = $nextNode;
			$nextNode = $result->fetch_object();

		} while (!($currentNode === false && $nextNode === false));
		?>

	</table>
</div>

<? InsertFooter(); ?>