<?php
/*********************************************************************
 * FILE: class.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Displays a content section of the site.
 * *****************************************************************
 *
 */


require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClassManager.php");

authenticate();

$message = "";


$classManager = new ClassManager();

if (IsPostBack)
{
	$action = post_text("txtAction");

	if ($action == "delete")
	{
		$class = new ClassDescription(post_int("txtClassID"));

		if ($class->LoadError)
		{
			$message = "Class does not exist or you do not have access.";
		}
		else
		{
			$class->Delete();
			$message = "Class deleted.";
		}
	}
}

$classes = $classManager->SelectAll();
$classCats = $classManager->LoadCats();
?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message); ?>

<script type="text/javascript">
	DeleteClass = function(ID)
	{
		var confirmed;

		confirmed = confirm ("Are you sure you want to delete this class?");

		if (confirmed)
		{
			$("#txtClassID").val(ID);
			$("#txtAction").val("delete");

			$("#frmAdmin").submit();
		}

		return false;
	}
</script>

<input type="hidden" id="txtClassID" name="txtClassID" value="0" />

<div id="contentAdmin">

	<h1>Classes</h1>

	<input type="button" value="New Class" onclick="window.location = SITE_URL + 'rhinoflow/classEdit.php'" />

	<table class="list" cellspacing="0">
		<tr>
			<th class="left"></th>
			<th style="text-align:left;">Category</th>
			<th style="text-align:left;">Class Name</th>
			<th style="text-align:left;">Class Code</th>
			<th class="edit"></th>
			<th class="remove"></th>
			<th class="right"></th>
		</tr>
		<tr><td class="big_spacer"></td></tr>

	<?  while ($class = $classes->NextItem())
		{

			if (!($class->Active)) { $rowClass = "inactive"; }
			elseif ($classes->OddRow()) { $rowClass = "odd_row"; }
			else { $rowClass = "even_row"; }

		?>

		<tr class="<?=$rowClass ?>">
			<td class="left"></td>
			<td style="text-align:left;"><?=$classCats[$class->CatId - 1] ?></td>
			<td style="text-align:left;"><?=$class->ClassName ?></td>
			<td style="text-align:left;"><?=$class->ClassCode ?></td>
			<td class="edit">[ <a href="rhinoflow/classEdit.php?classID=<?=$class->ID ?>">EDIT</a> ]</td>
			<td class="remove">[ <a href="delete" onclick="return DeleteClass(<?=$class->ID ?>);">DELETE</a> ]</td>
			<td class="right"></td>
		</tr>
		<tr class="spacer"><td></td></tr>

	<? } ?>
	</table>

</div>

<? InsertFooter(); ?>