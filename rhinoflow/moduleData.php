<?php


require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ModuleData.php");

authenticate();

$message = "";
$url = "";


$module = new ModuleData(get_text("module"));

if ($module->LoadError)
{
	messageBox("Error loading content item.  This item cannot be accessed or does not exist.", "menu.php", true);
	exit();
}



if (IsPostBack)
{
	$action = post_text("txtAction");

	$module->Var1 = post_text("txtVar1");
	$module->Var2 = post_text("txtVar2");

	$module->Update();

	$message = $module->ModuleName . " successfully updated.";

	if ($action == "save") { $url = "rhinoflow/menu.php"; }
}


$oFCKeditor = new FCKeditor("txtVar1");
$oFCKeditor->BasePath = "rhinoflow/fckeditor/";
$oFCKeditor->Value = $module->Var1;
$oFCKeditor->Config["EnterMode"] = "br";
$oFCKeditor->Width = 600;
$oFCKeditor->Height = 450;

$oFCKeditor2 = new FCKeditor("txtVar2");
$oFCKeditor2->BasePath = "rhinoflow/fckeditor/";
$oFCKeditor2->Value = $module->Var2;
$oFCKeditor2->Config["EnterMode"] = "br";
$oFCKeditor2->Width = 600;
$oFCKeditor2->Height = 450;

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message, $url); ?>


<script type="text/javascript">
	validateForm = function(Action)
	{

		if (false)
		{
		}
		else
		{
			$("#txtAction").val(Action);
			return true;
		}

		return false;
	}
</script>


<div id="contentAdmin">

	<h1><?=$module->ModuleName ?></h1>


	<table class="edit" cellspacing="0">
		<tr>
			<th>Description</th>
			<td>
				<? $oFCKeditor->Create(); ?>
			</td>
		</tr>
		<tr>
			<th>After Submit</th>
			<td>
				<? $oFCKeditor2->Create(); ?>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<td colspan="2" style="text-align: center;">
				<input type="submit" name="txtSubmit" value="Save" title="Save changes and return to menu." onclick="return validateForm('save');" />
  				<input type="submit" name="txtApply" value="Apply" title="Apply changes and continue editing." onclick="return validateForm('apply');" />
  				<input type="button" name="txtCancel" value="Cancel" title="Cancel all unsaved changes." onclick="window.location = '/rhinoflow' + '/menu.php'" />
			</td>
		</tr>
	</table>

</div>

<? InsertFooter(); ?>