<?php

require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/DepartmentManager.php");


$message = "";


$departmentManager = new DepartmentManager();

if (IsPostBack)
{
	$action = post_text("txtAction");

	if ($action == "delete")
	{
		$department = new Department(post_int("txtDepartmentID"));

		if ($department->LoadError)
		{
			$message = "Department does not exist or you do not have access.";
		}
		else
		{
			$department->Delete();
			$message = "Department deleted.";
		}
	}
}

$departments = $departmentManager->SelectAll();

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message); ?>

<script type="text/javascript">
	DeleteDepartment = function(ID)
	{
		var confirmed;

		confirmed = confirm ("Are you sure you want to delete this department?");

		if (confirmed)
		{
			$("#txtDepartmentID").val(ID);
			$("#txtAction").val("delete");

			$("#frmAdmin").submit();
		}

		return false;
	}
</script>

<input type="hidden" id="txtDepartmentID" name="txtDepartmentID" value="0" />

<div id="contentAdmin">

	<h1>Departments</h1>

	<input type="button" value="New Department" onclick="window.location = SITE_URL + 'rhinoflow/departmentEdit.php'" />

	<table class="list" cellspacing="0">
		<tr>
			<th class="left"></th>
			<th style="text-align:left;">Department</th>
			<th style="text-align:left;">Email</th>
			<th class="edit"></th>
			<th class="remove"></th>
			<th class="right"></th>
		</tr>
		<tr><td class="big_spacer"></td></tr>

	<?  while ($department = $departments->NextItem())
		{

			if (!($department->Active)) { $class = "inactive"; }
			elseif ($departments->OddRow()) { $class = "odd_row"; }
			else { $class = "even_row"; }

		?>

		<tr class="<?=$class ?>">
			<td class="left"></td>
			<td style="text-align:left;"><?=htmlentities($department->Name) ?></td>
			<td style="text-align:left;"><?=htmlentities($department->Email) ?></td>
			<td class="edit">[ <a href="rhinoflow/departmentEdit.php?departmentID=<?=$department->ID ?>">EDIT</a> ]</td>
			<td class="remove">[ <a href="delete" onclick="return DeleteDepartment(<?=$department->ID ?>);">DELETE</a> ]</td>
			<td class="right"></td>
		</tr>
		<tr class="spacer"><td></td></tr>

	<? } ?>
	</table>

</div>

<? InsertFooter(); ?>