<?php


require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClubManager.php");

authenticate();


$message = "";


$clubManager = new ClubManager();

if (IsPostBack)
{
	$action = post_text("txtAction");

	if ($action == "delete")
	{
		$club = new Club(post_int("txtClubID"));

		if ($club->LoadError)
		{
			$message = "Club does not exist or you do not have access.";
		}
		else
		{
			$club->Delete();
			$message = "Club deleted.";
		}
	}
}
if ($_SESSION["USER_LEVEL2"] == 'SU') {
	$clubs = $clubManager->SelectAll();

} else {
	$clubs = $clubManager->SelectAllwithAccess();
}
?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message); ?>

<script type="text/javascript">
	DeleteClub = function(ID)
	{
		var confirmed;

		confirmed = confirm ("Are you sure you want to delete this club?");

		if (confirmed)
		{
			$("#txtClubID").val(ID);
			$("#txtAction").val("delete");

			$("#frmAdmin").submit();
		}

		return false;
	}
</script>

<input type="hidden" id="txtClubID" name="txtClubID" value="0" />

<div id="contentAdmin">

	<h1>Clubs</h1>

	<? if ($_SESSION["USER_LEVEL2"] == 'SU') { ?>
    	<input type="button" value="New Club" onclick="window.location = SITE_URL + '<?=ADMIN_DIR ?>/clubEdit.php'" />
	<? } ?>

	<table class="list" cellspacing="0">
		<tr>
			<th class="left"></th>
			<th>City</th>
			<th>Name</th>
			<th class="edit"></th>
			<th class="remove"></th>
			<th class="right"></th>
		</tr>
		<tr><td class="big_spacer"></td></tr>

	<?  while ($club = $clubs->NextItem())
		{

			if (!($club->Active)) { $class = "inactive"; }
			elseif ($clubs->OddRow()) { $class = "odd_row"; }
			else { $class = "even_row"; }

		?>
		<? if ($_SESSION["USER_LEVEL2"] == 'SU') { ?>
		<tr class="<?=$class ?>">
			<td class="left"></td>
			<td><?=$club->City ?></td>
			<td style="text-align:left;"><?=$club->ClubName ?></td>
			<td class="edit">[ <a href="rhinoflow/clubEdit.php?clubID=<?=$club->ID ?>">EDIT</a> ]</td>
			<td class="remove">[ <a href="delete" onclick="return DeleteClub(<?=$club->ID ?>);">DELETE</a> ]</td>
			<td class="right"></td>
		</tr>
        <? } elseif ($_SESSION["USER_LEVEL2"] == 'AD' && $club->ID != 0) {?>
		<tr class="<?=$class ?>">
			<td class="left"></td>
			<td><?=$club->City ?></td>
			<td style="text-align:left;"><?=$club->ClubName ?></td>
			<td class="edit">[ <a href="rhinoflow/clubEdit.php?clubID=<?=$club->ID ?>">EDIT</a> ]</td>
			<td class="remove"></td>
			<td class="right"></td>
		</tr>
        <? } else {?>
        <? } ?>
		<tr class="spacer"><td></td></tr>

	<? } ?>
	</table>

</div>

<? InsertFooter(); ?>