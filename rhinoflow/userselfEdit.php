<?php
/*********************************************************************
 * FILE: press-edit.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Displays a content section of the site.
 * *****************************************************************
 *
 */

require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/UserManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClubManager.php");

authenticate();

$message = "";
$url = "";

$clubID = get_int("clubID");
$clubManager = new ClubManager();
$club = new Club($clubID);
if ($clubID > 0){
	$club = new Club($clubID);
	if ($club->LoadError)	{
		echo "Error loading item.  This item cannot be accessed or does not exist.";
		exit();
	}
}
else {
	$club = new Club();
}

$userID = $_SESSION["USER_ID"];

$userManager = new UserManager();


if ($userID > 0)
{
	$user = new User($userID);

	if ($user->LoadError)
	{
		echo "Error loading user.  This item cannot be accessed or does not exist.";
		exit();
	}
}
else
{
	$user = new User();
}


if (IsPostBack)
{
	$action = post_text("txtAction");

	if ($action == "save" || $action == "apply")
	{

		$user->Name = post_text("txtName");
		$user->Username = post_text("txtUsername");
		$password1 = post_text("txtPassword1");

		if ($password1 != "" && ($password1 == post_text("txtPassword2"))) { $user->Password = $password1; }
		if ($user->IsNewRow) { $user->Password = $password1; }
		$user->Active = post_bool("chkActive");
		$ul = post_text("txtUserLevel");
		if($ul == 'SU') { $user->SuperUser = 1; } else { $user->SuperUser = 0; }
		$user->UserLevel = post_text("txtUserLevel");
		$user->ClubId = post_text("dropLocation");		

		$user->Update();

		$url = ($action == "apply") ? "rhinoflow/userEdit.php?userID=" . $user->ID : "rhinoflow/index.php";
		$url = SITE_URL . $url;

		$message = "User successfully updated.";
	}
}

$cities = $clubManager->SelectCities();

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message, $url); ?>


<script type="text/javascript" src="js/date.js"></script>

<script type="text/javascript">
	validateForm = function(Action)
	{

		if ($("#txtUsername").val() == "")
		{
			alert("Please enter a username.");
			$("#txtUsername").focus();
		}
		else if ($("#txtPassword1").val() != $("#txtPassword2").val())
		{
			alert("Your passwords don't match. Please re-enter them.");
			$("#txtPassword1").val("");
			$("#txtPassword2").val("");
			$("#txtPassword1").focus();
		}
		else
		{
			$("#txtAction").val(Action);
			return true;
		}

		return false;
	}
</script>


<div id="contentAdmin">

	<h1><?=htmlentities($user->Name) ?></h1>

	<table class="edit" cellspacing="0">
		<tr>
			<th>Username</th>
			<td><input type="text" id="txtUsername" name="txtUsername" value="<?=htmlentities($user->Username) ?>" size="12" maxlength="12" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Password 1</th>
			<td><input type="password" id="txtPassword1" name="txtPassword1" value="" size="12" maxlength="12" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Password 2</th>
			<td><input type="password" id="txtPassword2" name="txtPassword2" value="" size="12" maxlength="12" /></td>
		</tr>
        <input type="hidden" id="txtName" name="txtName" value="<?=htmlentities($user->Name) ?>" size="50" maxlength="100" />
      <input type="hidden" id="txtUserLevel" name="txtUserLevel" value="<?=htmlentities($user->UserLevel) ?>" size="12" maxlength="12" />
      <input type="hidden" id="dropLocation" name="dropLocation" value="<?=htmlentities($user->ClubId) ?>" size="12" maxlength="12" />
      <input type="hidden" id="chkActive" name="chkActive" <? if ($user->Active) { echo "value=\"1\""; } else { echo "value=\"0\""; } ?> />
		<tr class="spacer"><td></td></tr>
		<tr>
			<td colspan="2" style="text-align: center;">
  				<input type="submit" name="txtSubmit" value="Save" title="Save changes and return to user list." onclick="return validateForm('save');" />
  				<input type="button" name="txtReset" value="Cancel" title="Cancel all unsaved changes." onclick="window.location = SITE_URL + 'rhinoflow/index.php'" />
			</td>
		</tr>
	</table>

</div>

<? InsertFooter(); ?>