<?php
/*********************************************************************
 * FILE: press-edit.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Displays a content section of the site.
 * *****************************************************************
 *
 */

require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClassManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClubManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/StudioManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/EventManager.php");

authenticate();

$message = "";
$url = "";

$classID = get_int("classID");
$classManager = new ClassManager();
$class = new ClassDescription($classID);
if ($classID > 0){
	$class = new ClassDescription($classID);
	if ($class->LoadError)	{
		echo "Error loading item.  This item cannot be accessed or does not exist.";
		exit();
	}
}
else {
	$class = new ClassDescription();
}

$clubID = get_int("clubID");
$clubManager = new ClubManager();
$clubs = $clubManager->SelectAll();

$club = new Club($clubID);
if ($clubID > 0){
	$club = new Club($clubID);
	if ($club->LoadError)	{
		echo "Error loading item.  This item cannot be accessed or does not exist.";
		exit();
	}
}
else {
	$club = new Club();
}

$studioID = get_int("studioID");
$studioManager = new StudioManager();
$studio = new Studio($studioID);
if ($studioID > 0){
	$studio = new Studio($studioID);
	if ($studio->LoadError)	{
		echo "Error loading item.  This item cannot be accessed or does not exist.";
		exit();
	}
}
else {
	$studio = new Studio();
}

$eventID = get_int("scheduleID");
if ($eventID > 0){
	$event = new Event($eventID);
	if ($event->LoadError)	{
		echo "Error loading item.  This item cannot be accessed or does not exist.";
		exit();
	}
}
else {
	$event = new Event();
}

if($event->ClubId > 0) { $_GET['locid'] = $event->ClubId; }

if (IsPostBack){
	$action = post_text("txtAction");

	if ($action == "save" || $action == "apply")
	{
	
		$event->ClubId = $_GET['locid'];
		$event->StudioId = post_int("dropStudio");
		$event->ClassId = post_int("dropClass");
		$event->Instructor = post_text("txtInstructor");		
		$event->StartEventHour = post_int("dropStartEventHour");
		$event->StartEventMin = post_int("dropStartEventMin");
		$event->StartEventAP = post_text("dropStartEventAP");
		$event->EndEventHour = post_int("dropEndEventHour");
		$event->EndEventMin = post_int("dropEndEventMin");
		$event->EndEventAP = post_text("dropEndEventAP");
		$event->Day = post_int("dropDay");
		$event->Month = post_int("dropMonth");
		$event->Year = post_int("dropYear");		

		$event->Update();

		$url = ($action == "apply") ? "rhinoflow/scheduleEdit.php?scheduleID=" . $event->ID : "rhinoflow/schedules.php";
		$url = SITE_URL . $url;

		$message = "Event ".$event->ID." successfully updated.";

	}
}

$categories = $classManager->SelectClassName();
$cities = $clubManager->LoadClubs(); //SelectCities();

if(isset($_GET['locid'])){
	$studios = $studioManager->SelectStudioByLocation($_GET['locid']);
}

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message, $url); ?>


<script type="text/javascript" src="js/date.js"></script>

<script type="text/javascript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>
<script type="text/javascript">
	validateForm = function(Action)
	{
		if ($("#dropStudio").val() == "")
		{
			alert("Please enter a Studio.");
			$("#dropStudio").focus();
		}
		else if ($("#dropClass").val() == "")
		{
			alert("Please enter a Class.");
			$("#dropClass").focus();
		}		
		else if ($("#dropStartEventHour").val() == "")
		{
			alert("Please enter a Start Hour");
			$("#dropStartEventHour").focus();
		}		
		else if ($("#dropStartEventHour").val() == "")
		{
			alert("Please enter a Start Hour");
			$("#dropStartEventHour").focus();
		}	
		else if ($("#dropStartEventMin").val() == "")
		{
			alert("Please enter a Start Min");
			$("#dropStartEventMin").focus();
		}	
		else if ($("#dropEndEventHour").val() == "")
		{
			alert("Please enter a End Hour");
			$("#dropStartEventHour").focus();
		}	
		else if ($("#dropEndEventMin").val() == "")
		{
			alert("Please enter a End Min");
			$("#dropStartEventHour").focus();
		}
		else if ($("#dropStartEventAP").val() == 'am' && $("#dropEndEventAP").val() == 'am' && $("#dropEndEventHour").val() < $("#dropStartEventHour").val() && $("#dropStartEventAP").val() == 'pm' && $("#dropEndEventAP").val() == 'pm' && $("#dropEndEventHour").val() > 1 && $("#dropStartEventHour").val() < 12)
		{
				alert("Please enter an End Hour after the Start Hour");
				$("#dropEndEventHour").focus();
		}				
		else if ($("#dropDay").val() == "")
		{
			alert("Please enter a Day");
			$("#dropDay").focus();
		}
		else if ($("#dropMonth").val() == "")
		{
			alert("Please enter a Month");
			$("#dropMonth").focus();
		}	
		else if ($("#dropYear").val() == "")
		{
			alert("Please enter a Year");
			$("#dropYear").focus();
		}			
		else
		{
			$("#txtAction").val(Action);
			return true;
		}

		return false;
	}
</script>

<div id="contentAdmin">
  <h1>Schedule</h1>

	<table class="edit" cellspacing="0">
		<tr class="spacer"><td></td></tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Location</th>
			<td>
            <? if(($_SESSION['USER_LEVEL2'] == 'SAL') && ($_SESSION['LOCATION'] > 0)) {  ?>
                <select id="dropLocation" name="dropLocation" onchange="MM_jumpMenu('parent',this,0)">
                <option value="">-- Select Location Name --</option><option value="http://beta.theathleticclubs.ca/rhinoflow/scheduleEdit.php?locid=<?=$_SESSION['LOCATION']?>" <? if($_GET['locid'] == $_SESSION['LOCATION']) { echo "selected"; } ?>>
                    <?= $cities[$_SESSION['LOCATION']] ?>
                  </option>
                </select>			
			<? } else { ?>           
            <select id="dropLocation" name="dropLocation" onchange="MM_jumpMenu('parent',this,0)">
		<option value="">-- Select Location Name --</option>            
		<? while ($club = $clubs->NextItem() ) { ?>
	<option value="http://beta.theathleticclubs.ca/rhinoflow/scheduleEdit.php?locid=<?=$club->ID?>" <? if($_GET['locid'] == $club->ID) { echo "selected"; } ?>>
			    <?= $club->ClubName ?> 
		      </option>
			  <? } ?>
		    </select>
            <? } ?>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>
        <? if(isset($_GET['locid'])){ ?>
		<tr>
			<th>Studio</th>
			<td><select id="dropStudio" name="dropStudio">
		      <option value="" selected>-- Select Studio Name --</option>                        
			  <? for ($i = 0; $i < count($studios); $i++) { ?>
			  <option value="<?=$studios[$i+1][2] ?>" <? if ($event->StudioId == $studios[$i+1][2]) { echo "selected"; } ?>>
			    <?=$studios[$i+1][1] ?>
		      </option>
			  <? } ?>
		    </select></td>
		</tr>
        <? } ?>
		<tr class="spacer"><td></td></tr>
		<tr>
		  <th>Class <?=$event->ClassId?></th>
		  <td><select id="dropClass" name="dropClass">		
		    <option value="">-- Select Class Name --</option>
		    <? for ($i = 0; $i < count($categories); $i++) { ?>
		    <option value="<?=$categories[$i+1][2] ?>" <? if ($event->ClassId == $categories[$i+1][2]) { echo "selected"; } ?>>
		      <?=$categories[$i+1][1] ?>
	        </option>
		    <? } ?>
		    </select></td>
	  </tr>
		<tr>
		  <th>Instructor</th>
		  <td><label>
		    <input type="text" name="txtInstructor" id="txtInstructor" value="<?=$event->Instructor?>" />
		  </label></td>
	  </tr>
		<tr>
		  <th>Class StartTime</th>
		  <td><select name="dropStartEventHour" id="dropStartEventHour">
		    <option value="">Hour</option>
		    <option value="1" <? if ($event->StartEventHour == 1) { echo "selected"; } ?>>1</option>
		    <option value="2" <? if ($event->StartEventHour == 2) { echo "selected"; } ?>>2</option>
		    <option value="3" <? if ($event->StartEventHour == 3) { echo "selected"; } ?>>3</option>
		    <option value="4" <? if ($event->StartEventHour == 4) { echo "selected"; } ?>>4</option>
		    <option value="5" <? if ($event->StartEventHour == 5) { echo "selected"; } ?>>5</option>
		    <option value="6" <? if ($event->StartEventHour == 6) { echo "selected"; } ?>>6</option>
		    <option value="7" <? if ($event->StartEventHour == 7) { echo "selected"; } ?>>7</option>
		    <option value="8" <? if ($event->StartEventHour == 8) { echo "selected"; } ?>>8</option>
		    <option value="9" <? if ($event->StartEventHour == 9) { echo "selected"; } ?>>9</option>
		    <option value="10" <? if ($event->StartEventHour == 10) { echo "selected"; } ?>>10</option>
		    <option value="11" <? if ($event->StartEventHour == 11) { echo "selected"; } ?>>11</option>
		    <option value="0" <? if ($event->StartEventHour == 1) { echo "selected"; } ?>>12</option>
		    </select>
		    <select name="dropStartEventMin" id="dropStartEventMin">
		      <option value="">Minute</option>
		      <option value="00" <? if ($event->StartEventMin == "00") { echo "selected"; } ?>>00</option>
		      <option value="05" <? if ($event->StartEventMin == "05") { echo "selected"; } ?>>05</option>
		      <option value="10" <? if ($event->StartEventMin == "10") { echo "selected"; } ?>>10</option>
		      <option value="15" <? if ($event->StartEventMin == "15") { echo "selected"; } ?>>15</option>
		      <option value="20" <? if ($event->StartEventMin == "20") { echo "selected"; } ?>>20</option>
		      <option value="25" <? if ($event->StartEventMin == "25") { echo "selected"; } ?>>25</option>
		      <option value="30" <? if ($event->StartEventMin == "30") { echo "selected"; } ?>>30</option>
		      <option value="35" <? if ($event->StartEventMin == "35") { echo "selected"; } ?>>35</option>
		      <option value="40" <? if ($event->StartEventMin == "40") { echo "selected"; } ?>>40</option>
		      <option value="45" <? if ($event->StartEventMin == "45") { echo "selected"; } ?>>45</option>
		      <option value="50" <? if ($event->StartEventMin == "50") { echo "selected"; } ?>>50</option>
		      <option value="55" <? if ($event->StartEventMin == "55") { echo "selected"; } ?>>55</option>
	        </select>
		    <select name="dropStartEventAP" id="dropStartEventAP">
		      <option value="am" <? if ($event->StartEventAP == "am") { echo "selected"; } ?>>am</option>
		      <option value="pm" <? if ($event->StartEventAP == "pm") { echo "selected"; } ?>>pm</option>
	        </select></td>
	  </tr>
		<tr>
		  <th>Class End Time</th>
		  <td><select name="dropEndEventHour" id="dropEndEventHour">
		    <option value="">Hour</option>
		    <option value="1" <? if ($event->EndEventHour == 1) { echo "selected"; } ?>>1</option>
		    <option value="2" <? if ($event->EndEventHour == 2) { echo "selected"; } ?>>2</option>
		    <option value="3" <? if ($event->EndEventHour == 3) { echo "selected"; } ?>>3</option>
		    <option value="4" <? if ($event->EndEventHour == 4) { echo "selected"; } ?>>4</option>
		    <option value="5" <? if ($event->EndEventHour == 5) { echo "selected"; } ?>>5</option>
		    <option value="6" <? if ($event->EndEventHour == 6) { echo "selected"; } ?>>6</option>
		    <option value="7" <? if ($event->EndEventHour == 7) { echo "selected"; } ?>>7</option>
		    <option value="8" <? if ($event->EndEventHour == 8) { echo "selected"; } ?>>8</option>
		    <option value="9" <? if ($event->EndEventHour == 9) { echo "selected"; } ?>>9</option>
		    <option value="10" <? if ($event->EndEventHour == 10) { echo "selected"; } ?>>10</option>
		    <option value="11" <? if ($event->EndEventHour == 11) { echo "selected"; } ?>>11</option>
		    <option value="12" <? if ($event->EndEventHour == 12) { echo "selected"; } ?>>12</option>
		    </select>
		    <select name="dropEndEventMin" id="dropEndEventMin">
		      <option value="">Minute</option>
		      <option value="00" <? if ($event->EndEventMin == "00") { echo "selected"; } ?>>00</option>
		      <option value="05" <? if ($event->EndEventMin == "05") { echo "selected"; } ?>>05</option>
		      <option value="10" <? if ($event->EndEventMin == "10") { echo "selected"; } ?>>10</option>
		      <option value="15" <? if ($event->EndEventMin == "15") { echo "selected"; } ?>>15</option>
		      <option value="20" <? if ($event->EndEventMin == "20") { echo "selected"; } ?>>20</option>
		      <option value="25" <? if ($event->EndEventMin == "25") { echo "selected"; } ?>>25</option>
		      <option value="30" <? if ($event->EndEventMin == "30") { echo "selected"; } ?>>30</option>
		      <option value="35" <? if ($event->EndEventMin == "35") { echo "selected"; } ?>>35</option>
		      <option value="40" <? if ($event->EndEventMin == "40") { echo "selected"; } ?>>40</option>
		      <option value="45" <? if ($event->EndEventMin == "45") { echo "selected"; } ?>>45</option>
		      <option value="50" <? if ($event->EndEventMin == "50") { echo "selected"; } ?>>50</option>
		      <option value="55" <? if ($event->EndEventMin == "55") { echo "selected"; } ?>>55</option>
	        </select>
		    <select name="dropEndEventAP" id="dropEndEventAP">
		      <option value="am" <? if ($event->EndEventAP == "am") { echo "selected"; } ?>>am</option>
		      <option value="pm" <? if ($event->EndEventAP == "pm") { echo "selected"; } ?>>pm</option>
	        </select></td>
	  </tr>
		<tr>
		  <th>Day &amp; Month</th>
		  <td>
		    <select name="dropDay" id="dropDay">
		      <option value="" selected="selected">Day</option>
		      <option value="1" <? if ($event->Day == 1) { echo "selected"; } ?>>Mon</option>
		      <option value="2" <? if ($event->Day == 2) { echo "selected"; } ?>>Tue</option>
		      <option value="3" <? if ($event->Day == 3) { echo "selected"; } ?>>Wed</option>
		      <option value="4" <? if ($event->Day == 4) { echo "selected"; } ?>>Thur</option>
		      <option value="5" <? if ($event->Day == 5) { echo "selected"; } ?>>Fri</option>
		      <option value="6" <? if ($event->Day == 6) { echo "selected"; } ?>>Sat</option>
		      <option value="7" <? if ($event->Day == 7) { echo "selected"; } ?>>Sun</option>
	        </select>
            <input type="hidden" name="dropMonth" id="dropMonth" value="5">
            <input type="hidden" name="dropYear" id="dropYear" value="2010">
            <!--
		    <select name="dropMonth" id="dropMonth">
		      <option value="" selected="selected">Month</option>
		      <option value="1" <?// if ($event->Month == 1) { echo "selected"; } ?>>January</option>
		      <option value="2" <?// if ($event->Month == 2) { echo "selected"; } ?>>February</option>
		      <option value="3" <?// if ($event->Month == 3) { echo "selected"; } ?>>March</option>
		      <option value="4" <?// if ($event->Month == 4) { echo "selected"; } ?>>April</option>
		      <option value="5" <?// if ($event->Month == 5) { echo "selected"; } ?>>May</option>
		      <option value="6" <?// if ($event->Month == 6) { echo "selected"; } ?>>June</option>
		      <option value="7" <?// if ($event->Month == 7) { echo "selected"; } ?>>July</option>
		      <option value="8" <?// if ($event->Month == 8) { echo "selected"; } ?>>August</option>
		      <option value="9" <?// if ($event->Month == 9) { echo "selected"; } ?>>September</option>
		      <option value="10" <?// if ($event->Month == 10) { echo "selected"; } ?>>October</option>
		      <option value="11" <?// if ($event->Month == 11) { echo "selected"; } ?>>November</option>
		      <option value="12" <?// if ($event->Month == 12) { echo "selected"; } ?>>December</option>
	        </select>
		    <select name="dropYear" id="dropYear">
		      <option value="" selected="selected">Year</option>
		      <option value="2010" <?// if ($event->Year == 2010) { echo "selected"; } ?>>2010</option>
		      <option value="2011" <?// if ($event->Year == 2011) { echo "selected"; } ?>>2011</option>
		      <option value="2012" <?// if ($event->Year == 2012) { echo "selected"; } ?>>2012</option>
		      <option value="2013" <?// if ($event->Year == 2013) { echo "selected"; } ?>>2013</option>
		      <option value="2014" <?// if ($event->Year == 2014) { echo "selected"; } ?>>2014</option>
		      <option value="2015" <?// if ($event->Year == 2015) { echo "selected"; } ?>>2015</option>
		      <option value="2016" <?// if ($event->Year == 2016) { echo "selected"; } ?>>2016</option>
		      <option value="2017" <?// if ($event->Year == 2017) { echo "selected"; } ?>>2017</option>
		      <option value="2018" <?// if ($event->Year == 2018) { echo "selected"; } ?>>2018</option>
		      <option value="2019" <?// if ($event->Year == 2019) { echo "selected"; } ?>>2019</option>
		      <option value="2020" <?// if ($event->Year == 2020) { echo "selected"; } ?>>2020</option>
	        </select>-->				
		   </td>
	  </tr>
		<!--tr>
		  <th>Super User</th>
		  <td><input type="checkbox" id="chkSuperUser" name="chkSuperUser" value="1" <? if ($user->SuperUser) { echo "checked=\"checked\""; } ?> /> <? echo $user->SuperUser; ?></td>
	  </tr-->
		<tr class="spacer"><td></td></tr>
		<tr>
			<td colspan="2" style="text-align: center;">
  				<input type="hidden" name="ID" value="<?=$event->ID?>" />
                <input type="submit" name="txtSubmit" value="Save" title="Save changes and return to user list." onclick="return validateForm('save');" />
  				<input type="submit" name="txtApply" value="Apply" title="Apply changes and continue editing." onclick="return validateForm('apply');" />
  				<input type="button" name="txtReset" value="Cancel" title="Cancel all unsaved changes." onclick="window.location = SITE_URL + 'rhinoflow/schedules.php'" />
			</td>
		</tr>
	</table>
<? //} ?>
</div>

<? InsertFooter(); ?>
