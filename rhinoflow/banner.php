<?php
/*********************************************************************
 * FILE: banner.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Adds/removes banners.
 * *****************************************************************
 *
 */
require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/BannerManager.php");

authenticate();

$message = "";

$bannerManager = new BannerManager();

if (IsPostBack)
{
	$action = post_text("txtAction");

	if ($action == "delete")
	{
		$banner = new Banner(post_int("txtBannerID"));

		if ($banner->LoadError)
		{
			$message = "Banner does not exist or you do not have access.";
		}
		else
		{
			@unlink(BANNER_GALLERY_PATH . $banner->Filename);
			@unlink(BANNER_GALLERY_THUMBNAIL_PATH . $banner->Filename);
			$banner->Delete();
			$message = "Banner deleted.";
		}
	}
}

$banners = $bannerManager->SelectAll();

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message); ?>

<script type="text/javascript">
	DeleteBanner = function(ID)
	{
		var confirmed;

		confirmed = confirm ("Are you sure you want to delete this banner?");

		if (confirmed)
		{
			$("#txtBannerID").val(ID);
			$("#txtAction").val("delete");

			$("#frmAdmin").submit();
		}

		return false;
	}
</script>

<input type="hidden" id="txtBannerID" name="txtBannerID" value="0" />

<div id="contentAdmin">

	<h1>Images</h1>

	<input type="button" value="New Image" onclick="window.location = ADMIN_URL + '/bannerEdit.php'" />

	<table class="list" cellspacing="0">
		<tr>
			<th class="left"></th>
			<th style="text-align:left;">Thumbnail</th>
			<th style="text-align:left;">Filename</th>
			<th class="edit"></th>
			<th class="remove"></th>
			<th class="right"></th>
		</tr>
		<tr><td class="big_spacer"></td></tr>

	<?  while ($banner = $banners->NextItem())
		{

			if (!($banner->Active)) { $class = "inactive"; }
			elseif ($banners->OddRow()) { $class = "odd_row"; }
			else { $class = "even_row"; }

		?>

		<tr class="<?=$class ?>">
			<td class="left"></td>
			<td style="text-align:left;"><img src="<?= (BANNER_GALLERY_THUMBNAIL . $banner->Filename) ?>" alt="" /></td>
			<td style="text-align:left;"><?=$banner->Filename ?></td>
			<td class="edit">[ <a href="rhinoflow/bannerEdit.php?bannerID=<?=$banner->ID ?>">EDIT</a> ]</td>
			<td class="remove">[ <a href="delete" onclick="return DeleteBanner(<?=$banner->ID ?>);">DELETE</a> ]</td>
			<td class="right"></td>
		</tr>
		<tr class="spacer"><td></td></tr>

	<? } ?>
	</table>

</div>

<? InsertFooter(); ?>