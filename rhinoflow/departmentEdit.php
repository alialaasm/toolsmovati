<?php
/*********************************************************************
 * FILE: departmentEdit.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Edits the department section of the site.
 * *****************************************************************
 *
 */

require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/DepartmentManager.php");

authenticate();

$message = "";
$url = "";

$departmentID = get_int("departmentID");
$departmentManager = new DepartmentManager();

$department = new Department($departmentID);

if ($departmentID > 0)
{
	$department = new Department($departmentID);
	if ($department->LoadError)
	{
		echo "Error loading item.  This item cannot be accessed or does not exist.";
		exit();
	}
}
else
{
	$department = new Department();
}


if (IsPostBack)
{

	$action = post_text("txtAction");

	if ($action == "save" || $action == "apply")
	{
		$department->Name = post_text("txtName");
		$department->Email = post_text("txtEmail");
		$department->Active = post_bool("chkActive");


		$department->Update();

		$message = "Department successfully updated.";

		$url = ($action == "apply") ? "rhinoflow/departmentEdit.php?departmentID=" . $department->ID : "rhinoflow/department.php";
		$url = SITE_URL . $url;

	}
}

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message, $url); ?>


<script type="text/javascript">
	validateForm = function(Action)
	{

		if ($("#txtTitle").val() == "")
		{
			alert("Please enter a Title.");
			$("#txtTitle").focus();

		}
		else
		{
			$("#txtAction").val(Action);
			return true;
		}

		return false;
	}
</script>


<div id="contentAdmin">

	<h1>Departments</h1>

	<table class="edit" cellspacing="0">
		<tr>
			<th>Name</th>
			<td><input type="text" id="txtName" name="txtName" value="<?=htmlentities($department->Name) ?>" size="50" maxlength="100" />
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Email</th>
			<td><input type="text" id="txtEmail" name="txtEmail" value="<?=htmlentities($department->Email) ?>" size="50" maxlength="255" />
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Active</th>
			<td><input type="checkbox" id="chkActive" name="chkActive" value="1" <? if ($department->Active) { echo "checked=\"checked\""; } ?> /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<td colspan="2" style="text-align: center;">
  				<input type="submit" name="txtSubmit" value="Save" title="Save changes and return to department." onclick="return validateForm('save');" />
  				<input type="submit" name="txtApply" value="Apply" title="Apply changes and continue editing." onclick="return validateForm('apply');" />
  				<input type="button" name="txtReset" value="Cancel" title="Cancel all unsaved changes." onclick="window.location = SITE_URL + 'rhinoflow/department.php'" />
			</td>
		</tr>
	</table>

</div>

<? InsertFooter(); ?>