<?php


require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/UserManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClubManager.php");

authenticate();

$message = "";

$userManager = new UserManager();

if (IsPostBack)
{
	$action = post_text("txtAction");

	if ($action == "delete")
	{
		$user = new User(post_int("txtUserID"));

		if ($user->LoadError)
		{
			$message = "User does not exist or you do not have access.";
		}
		else
		{
			$user->Delete();
			$message = "User release deleted.";
		}
	}
}

$users = $userManager->SelectAll();
$clubManager = new ClubManager();
$clubs = $clubManager->LoadClubs();
?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message); ?>

<script type="text/javascript">
	DeleteUser = function(ID)
	{
		var confirmed;

		confirmed = confirm ("Are you sure you want to delete this user?");

		if (confirmed)
		{
			$("#txtUserID").val(ID);
			$("#txtAction").val("delete");

			$("#frmAdmin").submit();
		}

		return false;
	}
</script>

<input type="hidden" id="txtUserID" name="txtUserID" value="0" />

<div id="contentAdmin">

	<h1>Users</h1>

	<input type="button" value="New User" onclick="window.location = SITE_URL + 'rhinoflow/userEdit.php'" />

	<table class="list" cellspacing="0">
		<tr>
			<th class="left"></th>
			<th>&nbsp;</th>
			<th style="text-align: left;">Name</th>
			<th>User Location</th>
			<th>User Type</th>
			<th class="edit"></th>
			<th class="remove"></th>
			<th class="right"></th>
		</tr>

		<tr><td class="big_spacer"></td></tr>
	<? while ($user = $users->NextItem()) {
		if (!($user->Active)) { $class = "inactive"; }
		elseif ($users->OddRow()) { $class = "odd_row"; }
		else { $class = "even_row"; }
		?>

		<tr class="<?=$class ?>">
			<td class="left"></td>
			<td>&nbsp;</td>
			<td style="text-align: left;"><?=$user->Name ?></td>
			<td><?=$clubs[$user->ClubId]?></td>
			<td><? if($user->UserLevel == 'SU') {
						echo 'Super User';						
				   } else if($user->UserLevel == 'AD') {						
						echo 'Administrator';						
				   } else if($user->UserLevel == 'SA') {						
						echo 'Schedule Administrator';										   
				   } else if($user->UserLevel == 'SAL') {						
						echo 'Scheduler';										   
				   } ?></td>
			<td class="edit">[ <a href="rhinoflow/userEdit.php?userID=<?=$user->ID ?>">EDIT</a> ]</td>
			<td class="remove">[ <a href="delete" onclick="return DeleteUser(<?=$user->ID ?>);">DELETE</a> ]</td>
			<td class="right"></td>
		</tr>
		<tr class="spacer"><td></td></tr>

	<? } ?>
	</table>

</div>

<? InsertFooter(); ?>