<?php
/*********************************************************************
 * FILE: press-edit.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Displays a content section of the site.
 * *****************************************************************
 *
 */

require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/StudioManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClubManager.php");

authenticate();

$message = "";
$url = "";


$clubManager = new ClubManager();
$club = new Club($clubID);
if ($clubID > 0){
	$club = new Club($clubID);
	if ($club->LoadError)	{
		echo "Error loading item.  This item cannot be accessed or does not exist.";
		exit();
	}
}
else {
	$club = new Club();
}
$studioID = get_int("studioID");
if ($studioID > 0){
	$studio = new Studio($studioID);
	if ($studio->LoadError)	{
		echo "Error loading item.  This item cannot be accessed or does not exist.";
		exit();
	}
}
else {
	$studio = new Studio();
}

if (IsPostBack)
{
	$action = post_text("txtAction");

	if ($action == "save" || $action == "apply")
	{

		$studio->ClubId = post_text("dropLocation");
		$studio->StudioName = post_text("txtStudioName");

		$studio->Update();

		$url = ($action == "apply") ? "rhinoflow/studioEdit.php?studioID=" . $studio->ID : "rhinoflow/studios.php";
		$url = SITE_URL . $url;

		$message = "Studio successfully updated.";
	}
}

$clubs = $clubManager->SelectAll();
$studiomanager = new StudioManager();
if(isset($_GET['studioID'])){
	$studios = $studiomanager->SelectStudioByID($_GET['studioID']);
}

?>

<? InsertHeader(Template::$Admin); ?>


<? messageBox($message, $url); ?>


<script type="text/javascript" src="js/date.js"></script>

<script type="text/javascript">
	validateForm = function(Action)
	{

		if ($("#txtStudioName").val() == "")
		{
			alert("Please enter a studio name.");
			$("#txtStudioName").focus();
		}
		else
		{
			$("#txtAction").val(Action);
			return true;
		}

		return false;
	}
</script>


<div id="contentAdmin">

	<h1>Studio</h1>
<? //if(isset($_GET['studioID'])){ 
						//while ($studioitem = $studios->NextItem()) { ?>	
	<table class="edit" cellspacing="0">
		<tr>
		<th>Club</th>
			<td><select id="dropLocation" name="dropLocation">
			<? while ($club = $clubs->NextItem()) { ?>
			  <option value="<?= $club->ID ?>" <? if ($studio->ClubId == $club->ID) { echo "selected"; } ?>>
			    <?=$club->ClubName ?>
		      </option>
			  <? } ?>
	    </select>			</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Studio Name</th>
			<td>
				
											<input type="text" id="txtStudioName" name="txtStudioName" value="<?=htmlentities($studio->StudioName)?>" size="50" maxlength="50" />					
		
		
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<!--tr>
		  <th>Super User</th>
		  <td><input type="checkbox" id="chkSuperUser" name="chkSuperUser" value="1" <? if ($user->SuperUser) { echo "checked=\"checked\""; } ?> /> <? echo $user->SuperUser; ?></td>
	  </tr-->
		<tr class="spacer"><td></td></tr>
		<tr>
			<td colspan="2" style="text-align: center;">
  				<input type="submit" name="txtSubmit" value="Save" title="Save changes and return to studio list." onclick="return validateForm('save');" />
  				<input type="submit" name="txtApply" value="Apply" title="Apply changes and continue editing." onclick="return validateForm('apply');" />
  				<input type="button" name="txtReset" value="Cancel" title="Cancel all unsaved changes." onclick="window.location = SITE_URL + 'rhinoflow/studios.php'" />
			</td>
		</tr>
	</table>
	<? //} ?>
</div>

<? 
	 InsertFooter(); ?>
