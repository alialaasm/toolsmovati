<?php
/*********************************************************************
 * FILE: actionButton.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Edits the action buttons.
 * *****************************************************************
 *
 */

require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ActionButtonManager.php");


authenticate();


$message = "";

$actionButtonManager = new ActionButtonManager();


if (IsPostBack)
{
	$action = post_text("txtAction");

	$actionButton = new ActionButton(post_int("txtActionButtonID"));

	if ($actionButton->LoadError)
	{
		$message = "Action button does not exist or you do not have access.";
	}
	else
	{
		if ($action == "delete")
		{
			@unlink(BUTTON_GALLERY_PATH . $actionButton->Filename);
			@unlink(BUTTON_GALLERY_DISPLAY_PATH . $actionButton->Filename);
			$actionButton->Delete($actionButton->OrderIndex);
			$message = "Action button deleted.";
		}
		else if ($action == "moveup")
		{
			$actionButton->MoveUp();
		}
		else if ($action == "movedown")
		{
			$actionButton->MoveDown();
		}
	}
}

$actionButtons = $actionButtonManager->SelectAll();

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message); ?>

<script type="text/javascript">
	DeleteActionButton = function(ID)
	{
		var confirmed;

		confirmed = confirm ("Are you sure you want to delete this action button?");

		if (confirmed)
		{
			$("#txtActionButtonID").val(ID);
			$("#txtAction").val("delete");

			$("#frmAdmin").submit();
		}

		return false;
	}

	ActionButtonOrder = function(ID, Direction)
	{
		$("#txtActionButtonID").val(ID);
		$("#txtAction").val(Direction);

		$("#frmAdmin").submit();

		return false;
	}
</script>

<input type="hidden" id="txtActionButtonID" name="txtActionButtonID" value="0" />

<div id="contentAdmin">

	<h1>Quick Action Buttons</h1>

	<input type="button" value="New Action Button" onclick="window.location = SITE_URL + 'rhinoflow/actionButtonEdit.php'" />


<? if ($actionButtons->Count() > 0) { ?>
	<table class="list" cellspacing="0">
		<tr>
			<th class="left"></th>
			<th style="text-align:left;">Order</th>
			<th style="text-align:left;">Title</th>
			<th style="text-align:left;">Colour</th>
			<th class="edit"></th>
			<th class="remove"></th>
			<th class="right"></th>
		</tr>
		<tr><td class="big_spacer"></td></tr>

	<?  while ($actionButton = $actionButtons->NextItem())
		{

			if (!($actionButton->Active)) { $class = "inactive"; }
			elseif ($actionButtons->OddRow()) { $class = "odd_row"; }
			else { $class = "even_row"; }

		?>

		<tr class="<?=$class ?>">
			<td class="left"></td>
			<td style="text-align:left;" class="up_down_buttons">
				<a href="move-up"><img src="rhinoflow/images/uparrow.gif" onclick="return ActionButtonOrder(<?=$actionButton->ID ?>,'moveup');" alt="" /></a>
				<a href="move-down"><img src="rhinoflow/images/downarrow.gif" onclick="return ActionButtonOrder(<?=$actionButton->ID ?>,'movedown');" alt="" /></a>
			</td>
			<td style="text-align:left;"><?=$actionButton->Title ?></td>
			<td style="text-align:left;"><?=$actionButton->Colour ?></td>
			<td class="edit">[ <a href="rhinoflow/actionButtonEdit.php?actionButtonID=<?=$actionButton->ID ?>">EDIT</a> ]</td>
			<td class="remove">[ <a href="delete" onclick="return DeleteActionButton(<?=$actionButton->ID ?>);">DELETE</a> ]</td>
			<td class="right"></td>
		</tr>
		<tr class="spacer"><td></td></tr>

	<? } ?>

	</table>
<? } ?>
</div>

<? InsertFooter(); ?>