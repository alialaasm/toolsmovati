<?php
/*********************************************************************
 * FILE: press-edit.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Displays a content section of the site.
 * *****************************************************************
 *
 */

require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClubManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/UserManager.php");

authenticate();

$message = "";
$url = "";

$clubID = get_int("clubID");

$clubManager = new ClubManager();
$club = new Club($clubID);

$userManager = new UserManager();
$users = $userManager->SelectAll();


if ($clubID > 0)
{
	$club = new Club($clubID);

	if ($club->LoadError)
	{
		echo "Error loading club.  This club cannot be accessed or does not exist.";
		exit();
	}
}
else
{
	$club = new Club();
}


if (IsPostBack)
{
	$action = post_text("txtAction");

	if ($action == "save" || $action == "apply")
	{
		$club->ClubName = post_text("txtClubName");
		$club->Address = post_text("txtAddress");
		$club->City = post_text("txtCity");
		$club->Province = post_text("txtProvince");
		$club->PostalCode = post_text("txtPostalCode");
		$club->MapItAddress = post_text("txtMapItAddress");

		$club->TollFree = post_text("txtTollFree");
		$club->Phone = post_text("txtPhone");
		$club->ContactEmail = post_text("txtContactEmail");
		$club->ApplicationEmail = post_text("txtApplicationEmail");
		$club->PersonalTrainingEmail = post_text("txtPersonalTrainingEmail");

		$club->NewsletterURL = post_text("txtNewsletterURL");
		$club->ScheduleURL = post_text("txtScheduleURL");
		$club->PoolScheduleURL = post_text("txtPoolScheduleURL");
		$club->UseInternal = post_text("chkInternal");
		$club->Promotions = post_text("txtPromotions");
		$club->OnlinePromotion = post_text("txtOnlinePromotion");

		$club->SundayHours = post_text("txtSundayHours");
		$club->MondayHours = post_text("txtMondayHours");
		$club->TuesdayHours = post_text("txtTuesdayHours");
		$club->WednesdayHours = post_text("txtWednesdayHours");
		$club->ThursdayHours = post_text("txtThursdayHours");
		$club->FridayHours = post_text("txtFridayHours");
		$club->SaturdayHours = post_text("txtSaturdayHours");

		$club->SalesSundayHours = post_text("txtSalesSundayHours");
		$club->SalesMondayHours = post_text("txtSalesMondayHours");
		$club->SalesTuesdayHours = post_text("txtSalesTuesdayHours");
		$club->SalesWednesdayHours = post_text("txtSalesWednesdayHours");
		$club->SalesThursdayHours = post_text("txtSalesThursdayHours");
		$club->SalesFridayHours = post_text("txtSalesFridayHours");
		$club->SalesSaturdayHours = post_text("txtSalesSaturdayHours");
		
		$club->Active = post_text("chkActive");
		$club->SalesTrailer = post_text("chkSalesTrailer");
		$club->FreeTrial = post_text("chkFreeTrial");
		$club->OnlinePromo = post_text("chkOnlinePromo");
		$club->HolidayHours = post_text("txtHolidayHours");
		$club->NoticeTitle = post_text("txtNoticeTitle");
		$club->NoticeBody = post_text("txtNoticeBody");
		$club->PersonalTraining = post_text("txtPersonalTraining");

		$club->Update();

		$message = "Club successfully updated.";

		/* ----- Update User Access ----- */
		$userIDs = post_array("chkUser");
		$club->UpdateUserAccess($userIDs);
		
		$url = ($action == "apply") ? "rhinoflow/clubEdit.php?clubID=" . $club->ID : "rhinoflow/club.php";
		$url = SITE_URL . $url;
	}
}


$txtPromotions = new FCKeditor("txtPromotions");
$txtPromotions->BasePath = "rhinoflow/fckeditor/";
$txtPromotions->Value = $club->Promotions;
$txtPromotions->Config["EnterMode"] = "br";
$txtPromotions->Width = 600;
$txtPromotions->Height = 350;

$txtOnlinePromotion = new FCKeditor("txtOnlinePromotion");
$txtOnlinePromotion->BasePath = "rhinoflow/fckeditor/";
$txtOnlinePromotion->Value = $club->OnlinePromotion;
$txtOnlinePromotion->Config["EnterMode"] = "br";
$txtOnlinePromotion->Width = 600;
$txtOnlinePromotion->Height = 350;

$txtHolidayHours = new FCKeditor("txtHolidayHours");
$txtHolidayHours->BasePath = "rhinoflow/fckeditor/";
$txtHolidayHours->Value = $club->HolidayHours;
$txtHolidayHours->Config["EnterMode"] = "br";
$txtHolidayHours->Width = 600;
$txtHolidayHours->Height = 350;

$txtNoticeBody = new FCKeditor("txtNoticeBody");
$txtNoticeBody->BasePath = "rhinoflow/fckeditor/";
$txtNoticeBody->Value = $club->NoticeBody;
$txtNoticeBody->Config["EnterMode"] = "br";
$txtNoticeBody->Width = 600;
$txtNoticeBody->Height = 350;

$txtPersonalTraining = new FCKeditor("txtPersonalTraining");
$txtPersonalTraining->BasePath = "rhinoflow/fckeditor/";
$txtPersonalTraining->Value = $club->PersonalTraining;
$txtPersonalTraining->Config["EnterMode"] = "br";
$txtPersonalTraining->Width = 600;
$txtPersonalTraining->Height = 350;


?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message, $url); ?>


<script type="text/javascript" src="js/date.js"></script>

<script type="text/javascript">
	validateForm = function(Action)
	{

		if (true)
		{
			$("#txtAction").val(Action);
			return true;
		}

		return false;
	}
</script>


<div id="contentAdmin">

	<h1>Clubs</h1>

	<table class="edit" cellspacing="0">
		<tr>
			<th>Club Name</th>
			<td><input type="text" id="txtClubName" name="txtClubName" value="<?=htmlentities($club->ClubName) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Address</th>
			<td><input type="text" id="txtAddress" name="txtAddress" value="<?=htmlentities($club->Address) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>City</th>
			<td><input type="text" id="txtCity" name="txtCity" value="<?=htmlentities($club->City) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Province</th>
			<td><input type="text" id="txtProvince" name="txtProvince" value="<?=htmlentities($club->Province) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Postal Code</th>
			<td><input type="text" id="txtPostalCode" name="txtPostalCode" value="<?=htmlentities($club->PostalCode) ?>" size="7" maxlength="7" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Map It! Address</th>
			<td>
				<input type="text" id="txtMapItAddress" name="txtMapItAddress" value="<?=htmlentities($club->MapItAddress) ?>" size="75" maxlength="250" />
				<a href="map-it" onclick="this.href='http://maps.google.com/maps?q=' + $('#txtMapItAddress').val();" target="_blank">
					TEST
				</a>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Phone</th>
			<td><input type="text" id="txtPhone" name="txtPhone" value="<?=htmlentities($club->Phone) ?>" size="15" maxlength="15" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Toll Free</th>
			<td><input type="text" id="txtTollFree" name="txtTollFree" value="<?=htmlentities($club->TollFree) ?>" size="15" maxlength="15" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Contact Email</th>
			<td><input type="text" id="txtContactEmail" name="txtContactEmail" value="<?=htmlentities($club->ContactEmail) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Application Email</th>
			<td><input type="text" id="txtApplicationEmail" name="txtApplicationEmail" value="<?=htmlentities($club->ApplicationEmail) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Personal Training Email</th>
			<td><input type="text" id="txtPersonalTrainingEmail" name="txtPersonalTrainingEmail" value="<?=htmlentities($club->PersonalTrainingEmail) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>News Letter URL</th>
			<td><input type="text" id="txtNewsletterURL" name="txtNewsletterURL" value="<?=htmlentities($club->NewsletterURL) ?>" size="75" maxlength="255" /></td>
		</tr>
		<tr>
			<th>Schedule URL</th>
			<td><input type="text" id="txtScheduleURL" name="txtScheduleURL" value="<?=htmlentities($club->ScheduleURL) ?>" size="75" maxlength="255" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Pool Schedule URL</th>
			<td><input type="text" id="txtPoolScheduleURL" name="txtPoolScheduleURL" value="<?=htmlentities($club->PoolScheduleURL) ?>" size="75" maxlength="255" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
		  <th>Use Internal Schedule</th>
		  <td><input type="checkbox" id="chkInternal" name="chkInternal" value="1" <? if ($club->UseInternal != 0) echo "checked=\"checked\"";  ?> /></td>
	  </tr>
		<tr>
			<th>Promotions</th>
			<td>
				<? $txtPromotions->Create(); ?>
			</td>
		</tr>
		<tr class="big_spacer"><td></td></tr>
		<tr>
			<th>Sunday Hours</th>
			<td><input type="text" id="txtSundayHours" name="txtSundayHours" value="<?=htmlentities($club->SundayHours) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Monday Hours</th>
			<td><input type="text" id="txtMondayHours" name="txtMondayHours" value="<?=htmlentities($club->MondayHours) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Tuesday Hours</th>
			<td><input type="text" id="txtTuesdayHours" name="txtTuesdayHours" value="<?=htmlentities($club->TuesdayHours) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Wednesday Hours</th>
			<td><input type="text" id="txtWednesdayHours" name="txtWednesdayHours" value="<?=htmlentities($club->WednesdayHours) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Thursday Hours</th>
			<td><input type="text" id="txtThursdayHours" name="txtThursdayHours" value="<?=htmlentities($club->ThursdayHours) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Friday Hours</th>
			<td><input type="text" id="txtFridayHours" name="txtFridayHours" value="<?=htmlentities($club->FridayHours) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Saturday Hours</th>
			<td><input type="text" id="txtSaturdayHours" name="txtSaturdayHours" value="<?=htmlentities($club->SaturdayHours) ?>" size="50" maxlength="100" /></td>
		</tr>
        
		<tr class="big_spacer"><td></td></tr>
		<tr>
			<th>Active</th>
			<td><input type="checkbox" id="chkActive" name="chkActive" value="1" <? if ($club->Active != 0) echo "checked=\"checked\"";  ?> /></td>
		</tr>
		<tr>
			<th>Sales Trailer</th>
			<td><input type="checkbox" id="chkSalesTrailer" name="chkSalesTrailer" value="1" <? if ($club->SalesTrailer != 0) echo "checked=\"checked\"";  ?> /></td>
		</tr>
		<tr class="big_spacer"><td></td></tr>
		<tr>
			<th>Sales Sunday Hours</th>
			<td><input type="text" id="txtSalesSundayHours" name="txtSalesSundayHours" value="<?=htmlentities($club->SalesSundayHours) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
    <tr>
			<th>Sales Monday Hours</th>
			<td><input type="text" id="txtSalesMondayHours" name="txtSalesMondayHours" value="<?=htmlentities($club->SalesMondayHours) ?>" size="50" maxlength="100" /></td>
	  </tr>
		<tr class="spacer"><td></td></tr>
    <tr>
			<th>Sales Tuesday Hours</th>
			<td><input type="text" id="txtSalesTuesdayHours" name="txtSalesTuesdayHours" value="<?=htmlentities($club->SalesTuesdayHours) ?>" size="50" maxlength="100" /></td>
	  </tr>
		<tr class="spacer"><td></td></tr>
    <tr>
			<th>Sales Wednesday Hours</th>
			<td><input type="text" id="txtSalesWednesdayHours" name="txtSalesWednesdayHours" value="<?=htmlentities($club->SalesWednesdayHours) ?>" size="50" maxlength="100" /></td>
	  </tr>
		<tr class="spacer"><td></td></tr>
    <tr>
			<th>Sales Thursday Hours</th>
			<td><input type="text" id="txtSalesThursdayHours" name="txtSalesThursdayHours" value="<?=htmlentities($club->SalesThursdayHours) ?>" size="50" maxlength="100" /></td>
	  </tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Sales Friday Hours</th>
			<td><input type="text" id="txtSalesFridayHours" name="txtSalesFridayHours" value="<?=htmlentities($club->SalesFridayHours) ?>" size="50" maxlength="100" /></td>
	  </tr>
		<tr class="spacer"><td></td></tr>
		<tr>
			<th>Sales Saturday Hours</th>
			<td><input type="text" id="txtSalesSaturdayHours" name="txtSalesSaturdayHours" value="<?=htmlentities($club->SalesSaturdayHours) ?>" size="50" maxlength="100" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
		<tr>
			<th>Allow Free Trial</th>
			<td><input type="checkbox" id="chkFreeTrial" name="chkFreeTrial" value="1" <? if ($club->FreeTrial != 0) echo "checked=\"checked\"";  ?> /></td>
		</tr>
		<tr class="spacer"><td></td></tr>
		<tr>
		<tr>
			<th>Allow Online Promo</th>
			<td><input type="checkbox" id="chkOnlinePromo" name="chkOnlinePromo" value="1" <? if ($club->OnlinePromo != 0) echo "checked=\"checked\"";  ?> /></td>
		</tr>
		<tr>
			<th>Online Promotion</th>
			<td>
				<? $txtOnlinePromotion->Create(); ?>
			</td>
		</tr>
    	<tr>
			<th>Holiday Hours</th>
			<td>
				<? $txtHolidayHours->Create(); ?>
			</td>
		</tr>

		<tr>
			<th>Club Notice Title</th>
                        <td><input type="text" id="txtNoticeTitle" name="txtNoticeTitle" value="<?=htmlentities($club->NoticeTitle) ?>" size="50" maxlength="100" /></td>
                </tr>
		<tr>
			<th>Club Notice Body</th>
                        <td>
                                <? $txtNoticeBody->Create(); ?>
                        </td>
                </tr>
		<th>Personal Training</th>
                        <td>
                                <? $txtPersonalTraining->Create(); ?>
                        </td>
                </tr>

		<tr class="spacer"><td></td></tr>
		<? if ($_SESSION["USER_LEVEL2"] == 'SU') { ?>
            <tr>
                <th>User Access</th>
                <td>
                    <? while ($user = $users->NextItem()) { ?>
                        <? if($user->UserLevel == "AD" || $user->UserLevel == "SA") { ?>
                            <input type="checkbox" id="chkUser<?=$user->ID ?>" name="chkUser[]" value="<?=$user->ID ?>" <? if ($user->HasAccess) { echo "checked"; } ?>>
                            <?=$user->Name ?>
                        <? } ?>
                    <? } ?>
                </td>
            </tr>
            <tr class="spacer"><td></td></tr>
        <? } ?>
		<tr>
			<td colspan="2" style="text-align: center;">
  				<input type="submit" name="txtSubmit" value="Save" title="Save changes and return to press list." onclick="return validateForm('save');" />
  				<input type="submit" name="txtApply" value="Apply" title="Apply changes and continue editing." onclick="return validateForm('apply');" />
  				<input type="button" name="txtReset" value="Cancel" title="Cancel all unsaved changes." onclick="window.location = SITE_URL + '<?=ADMIN_DIR ?>/club.php'" />
			</td>
		</tr>
	</table>

</div>

<? InsertFooter(); ?>
