<?php


require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/StudioManager.php");


authenticate();


$message = "";

$studioManager = new StudioManager();

if (IsPostBack)
{
	$action = post_text("txtAction");

	if ($action == "delete")
	{
		$studio = new Studio(post_int("txtStudioID"));

		if ($studio->LoadError)
		{
			$message = "User does not exist or you do not have access.";
		}
		else
		{
			$studio->Delete();
			$message = "Studio release deleted.";
		}
	}
}

$studio = $studioManager->SelectAll();

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message); ?>

<script type="text/javascript">
	DeleteUser = function(ID)
	{
		var confirmed;

		confirmed = confirm ("Are you sure you want to delete this user?");

		if (confirmed)
		{
			$("#txtUserID").val(ID);
			$("#txtAction").val("delete");

			$("#frmAdmin").submit();
		}

		return false;
	}
</script>

<input type="hidden" id="txtStudioID" name="txtStudioID" value="0" />

<div id="contentAdmin">

	<h1>Users</h1>

	<input type="button" value="New User" onclick="window.location = SITE_URL + 'rhinoflow/userEdit.php'" />

	<table class="list" cellspacing="0">
		<tr>
			<th class="left"></th>
			<th>&nbsp;</th>
			<th style="text-align: left;">StudioId</th>
			<th>Club Name</th>
			<th>Studio Name</th>
			<th class="edit"></th>
			<th class="remove"></th>
			<th class="right"></th>
		</tr>

		<tr><td class="big_spacer"></td></tr>
	<? while ($studio = $studios->NextItem()) {
		if (!($studio->Active)) { $class = "inactive"; }
		elseif ($studios->OddRow()) { $class = "odd_row"; }
		else { $class = "even_row"; }
		?>

		<tr class="<?=$class ?>">
			<td class="left"></td>
			<td>&nbsp;</td>
			<td style="text-align: left;"><?=$studio->ClassId ?></td>
			<td style="text-align: left;"><?=$studio->ClubName ?></td>
			<td><?=(($studio->StusioName) ? "Super" : "Regular") ?></td>
			<td class="edit">[ <a href="rhinoflow/studioEdit.php?studioID=<?=$studio->ID ?>">EDIT</a> ]</td>
			<td class="remove">[ <a href="delete" onclick="return DeleteUser(<?=$studio->ID ?>);">DELETE</a> ]</td>
			<td class="right"></td>
		</tr>
		<tr class="spacer"><td></td></tr>

	<? } ?>
	</table>

</div>

<? InsertFooter(); ?>