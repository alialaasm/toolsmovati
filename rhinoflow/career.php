<?php

require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/CareerManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClubManager.php");

$message = "";

$clubManager = new ClubManager();

$clubID = get_int("clubID");

$careerManager = new CareerManager();

if (IsPostBack)
{
	$action = post_text("txtAction");

	if ($action == "delete")
	{
		$career = new Career(post_int("txtCareerID"));

		if ($career->LoadError)
		{
			$message = "Career listing does not exist or you do not have access.";
		}
		else
		{
			$career->Delete();
			$message = "Career listing deleted.";
		}
	}
}

$careers = $careerManager->SelectAll();

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message); ?>

<script type="text/javascript">
	DeleteCareer = function(ID, ID2)
	{
		var confirmed;

		confirmed = confirm ("Are you sure you want to delete this career listing?");

		if (confirmed)
		{
			$("#txtCareerID").val(ID);
			$("#txtClubID").val(ID2);
			$("#txtAction").val("delete");

			$("#frmAdmin").submit();
		}

		return false;
	}
</script>

<input type="hidden" id="txtCareerID" name="txtCareerID" value="0" />
<input type="hidden" id="txtClubID" name="txtClubID" value="0" />


<div id="contentAdmin">

	<h1>Careers</h1>


	<input type="button" value="Back to Menu" onclick="window.location = SITE_URL + '<?=ADMIN_DIR ?>/careerEdit.php'" />
	<input type="button" value="New Career" onclick="window.location = SITE_URL + '<?=ADMIN_DIR ?>/careerEdit.php'" />


	<table class="list" cellspacing="0">
		<tr>
			<th class="left"></th>
			<th style="text-align:left;">Position</th>
			<th class="edit"></th>
			<th class="remove"></th>
			<th class="right"></th>
		</tr>
		<tr><td class="big_spacer"></td></tr>

	<?  while ($career = $careers->NextItem())
		{

			if (!($career->Active)) { $class = "inactive"; }
			elseif ($careers->OddRow()) { $class = "odd_row"; }
			else { $class = "even_row"; }

		?>

		<tr class="<?=$class ?>">
			<td class="left"></td>
			<td style="text-align:left;"><?=$career->Position ?></td>
			<td class="edit">[ <a href="rhinoflow/careerEdit.php?careerID=<?=$career->ID ?>">EDIT</a> ]</td>
			<td class="remove">[ <a href="delete" onclick="return DeleteCareer(<?=$career->ID?>);">DELETE</a> ]</td>
			<td class="right"></td>
		</tr>
		<tr class="spacer"><td></td></tr>

	<? } ?>
	</table>

</div>

<? InsertFooter(); ?>