<?php
/*********************************************************************
 * FILE: class.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Displays a content section of the site.
 * *****************************************************************
 *
 */


require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/rhinoflow/fckeditor/fckeditor.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/EventManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/StudioManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClassManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ClubManager.php");

authenticate();

$message = "";

$eventManager = new EventManager();

if (IsPostBack)
{
	$action = post_text("txtAction");

	if ($action == "delete")
	{
		$eventt = new Event(post_int("txtEventID"));

		if ($eventt->LoadError)
		{
			$message = "Event does not exist or you do not have access.";
		}
		else
		{
			$eventt->Delete(post_int("txtEventID"));
			$message = "Event ".post_int("txtEventID")." deleted.";
			
		}
	}
}

$events = $eventManager->SelectAll();
$clubManager = new ClubManager();
$clubs = $clubManager->LoadClubs();
$clubs2 = $clubManager->SelectAll();
$cities = $clubManager->SelectCities();
$studioManager = new StudioManager();
if($_SESSION['LOCATION'] < 1) $_SESSION['LOCATION'] = 1;
if(isset($_GET['locid'])) $_SESSION['LOCATION'] = $_GET['locid']; else $_GET['locid'] = 1;
if(isset($_GET['month'])) $_SESSION['month'] = $_GET['month']; else $_GET['month'] = 5;
if(isset($_GET['year'])) $_SESSION['year'] = $_GET['year']; else $_GET['year'] = 2010;
$studios = $studioManager->SelectStudioByLocationAll($_SESSION['LOCATION']);

?>


<? InsertHeader(Template::$Admin); ?>


<? messageBox($message); ?>

<script type="text/javascript">
	DeleteEvent = function(ID)
	{
		var confirmed;

		confirmed = confirm ("Are you sure you want to delete this event?");

		if (confirmed)
		{
			$("#txtEventID").val(ID);
			$("#txtAction").val("delete");

			$("#frmAdmin").submit();
		}

		return false;
	}
</script>
<script type="text/javascript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>
<script type="text/javascript">
	    	var SITE_URL = "<?=SITE_URL ?>";
	    	var ADMIN_URL = SITE_URL + "<?=ADMIN_DIR ?>";
	    </script>

<input type="hidden" id="txtEventID" name="txtEventID" value="0" />

<div id="contentAdmin">
<h1>Group Fitness Schedules</h1>

<? //print_r($_SESSION); ?>

<? if(($_SESSION['USER_LEVEL2'] == 'SAL') && ($_SESSION['LOCATION'] > 0)) { ?>
	
    <input type="hidden" id="dropLocation" name="dropLocation" value="<?=$_SESSION['LOCATION']?>">
	<? $month_name = date( 'F', mktime(0, 0, 0, $_GET['month']) ); ?>
	<table class="list" cellspacing="0">
            <tr>
                <th class="left"></th>
		<th style="text-align:left;">Scheduled Classes In: <?=$clubs[$_SESSION['LOCATION']]?> <!--(<?//=$month_name?> <?//=$_GET['year']?>) --><input type="button" value="Add New Class Event" onclick="window.location = SITE_URL + '<?=ADMIN_DIR ?>/scheduleEdit.php'" /><?= $_SESSION['LOCATION'] ?> ?</th>
                <th class="right"></th>
            </tr>
    </table>
    
<? } else { ?>
	<input type="button" value="Add New Class Event" onclick="window.location = SITE_URL + '<?=ADMIN_DIR ?>/scheduleEdit.php'" />
	<? $month_name = date( 'F', mktime(0, 0, 0, $_GET['month']) ); ?>
    <table class="list" cellspacing="0">
            <tr>
                <th class="left"></th>
		<th style="text-align:left;">Scheduled Classes In: <?=$clubs[$_GET['locid']]?> <!--(<?//=$month_name?> <?//=$_GET['year']?>) --><select id="dropLocation" name="dropLocation" onchange="MM_jumpMenu('parent',this,0)">
			<? while ($club = $clubs2->NextItem()) { ?>
			  <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$club->ID;?>" <? if($_GET['locid'] == $club->ID){ echo "selected"; } ?>>
			    <?=$club->ClubName ?>
		      </option>
              <? } ?>

	    </select>
	  <!-- <select name="dropMonth" id="dropMonth" onchange="MM_jumpMenu('parent',this,0)">
	    <option value="" selected="selected">Month</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=1" <? if($_GET['month'] == 1){ echo "selected"; } ?>>January</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=2" <? if($_GET['month'] == 2){ echo "selected"; } ?>>February</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=3" <? if($_GET['month'] == 3){ echo "selected"; } ?>>March</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=4" <? if($_GET['month'] == 4){ echo "selected"; } ?>>April</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=5" <? if($_GET['month'] == 5){ echo "selected"; } ?>>May</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=6" <? if($_GET['month'] == 6){ echo "selected"; } ?>>June</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=7" <? if($_GET['month'] == 7){ echo "selected"; } ?>>July</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=8" <? if($_GET['month'] == 8){ echo "selected"; } ?>>August</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=9" <? if($_GET['month'] == 9){ echo "selected"; } ?>>September</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=10" <? if($_GET['month'] == 10){ echo "selected"; } ?>>October</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=11" <? if($_GET['month'] == 11){ echo "selected"; } ?>>November</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=12" <? if($_GET['month'] == 12){ echo "selected"; } ?>>December</option>
      </select>
	  <select name="dropYear" id="dropYear" onchange="MM_jumpMenu('parent',this,0)">
	    <option value="" selected="selected">Year</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=<?=$_GET['month']?>&year=2010" <? if($_GET['year'] == 2010){ echo "selected"; } ?>>2010</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=<?=$_GET['month']?>&year=2011" <? if($_GET['year'] == 2011){ echo "selected"; } ?>>2011</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=<?=$_GET['month']?>&year=2012" <? if($_GET['year'] == 2012){ echo "selected"; } ?>>2012</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=<?=$_GET['month']?>&year=2013" <? if($_GET['year'] == 2013){ echo "selected"; } ?>>2013</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=<?=$_GET['month']?>&year=2014" <? if($_GET['year'] == 2014){ echo "selected"; } ?>>2014</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=<?=$_GET['month']?>&year=2015" <? if($_GET['year'] == 2015){ echo "selected"; } ?>>2015</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=<?=$_GET['month']?>&year=2016" <? if($_GET['year'] == 2016){ echo "selected"; } ?>>2016</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=<?=$_GET['month']?>&year=2017" <? if($_GET['year'] == 2017){ echo "selected"; } ?>>2017</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=<?=$_GET['month']?>&year=2018" <? if($_GET['year'] == 2018){ echo "selected"; } ?>>2018</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=<?=$_GET['month']?>&year=2019" <? if($_GET['year'] == 2019){ echo "selected"; } ?>>2019</option>
	    <option value="<?=SITE_URL ?>rhinoflow/schedules.php?locid=<?=$_GET['locid']?>&month=<?=$_GET['month']?>&year=2020" <? if($_GET['year'] == 2020){ echo "selected"; } ?>>2020</option>
      </select>--></th>
                <th class="right"></th>
            </tr>
    </table>
    
	
<? } ?>




<? 
while ($studio = $studios->NextItem()) {
	
	echo "<h2>".$studio->StudioName."</h2>";
if(isset($_GET['locid']) && isset($_GET['month']) && isset($_GET['year'])) { 
	$monday = $eventManager->SelectStudioByLocationMonthDayAll(1,$_SESSION['LOCATION'],$studio->ID, $_GET['month'], $_GET['year']);
	$tuesday = $eventManager->SelectStudioByLocationMonthDayAll(2,$_SESSION['LOCATION'],$studio->ID, $_GET['month'], $_GET['year']);
	$wednesday = $eventManager->SelectStudioByLocationMonthDayAll(3,$_SESSION['LOCATION'],$studio->ID, $_GET['month'], $_GET['year']);
	$thursday = $eventManager->SelectStudioByLocationMonthDayAll(4,$_SESSION['LOCATION'],$studio->ID, $_GET['month'], $_GET['year']);
	$friday = $eventManager->SelectStudioByLocationMonthDayAll(5,$_SESSION['LOCATION'],$studio->ID, $_GET['month'], $_GET['year']);
	$saturday = $eventManager->SelectStudioByLocationMonthDayAll(6,$_SESSION['LOCATION'],$studio->ID, $_GET['month'], $_GET['year']);
	$sunday = $eventManager->SelectStudioByLocationMonthDayAll(7,$_SESSION['LOCATION'],$studio->ID, $_GET['month'], $_GET['year']);	
} else {
	echo "entered";
	$monday = $eventManager->SelectByDayCurrentMonth(1,$_SESSION['LOCATION'],$studio->ID);
	$tuesday = $eventManager->SelectByDayCurrentMonth(2,$_SESSION['LOCATION'],$studio->ID);
	$wednesday = $eventManager->SelectByDayCurrentMonth(3,$_SESSION['LOCATION'],$studio->ID);
	$thursday = $eventManager->SelectByDayCurrentMonth(4,$_SESSION['LOCATION'],$studio->ID);
	$friday = $eventManager->SelectByDayCurrentMonth(5,$_SESSION['LOCATION'],$studio->ID);
	$saturday = $eventManager->SelectByDayCurrentMonth(6,$_SESSION['LOCATION'],$studio->ID);
	$sunday = $eventManager->SelectByDayCurrentMonth(7,$_SESSION['LOCATION'],$studio->ID);
}
?>
 
	<?  //while ($event = $events->NextItem())
		//{
			//if ($events->OddRow()) { $rowClass = "odd_row"; }
			//else { $rowClass = "even_row"; }

		?>
        <div class="weekwrapper">
       	  <div class="doyofweek">
            	<div class="dowtitle">MONDAY</div>
				 <? 
					while ($me = $monday->NextItem()) {
					echo "<div class='time'>".$me->StartEventHour.":".$me->StartEventMin." ".$me->StartEventAP." - ".$me->EndEventHour.":".$me->EndEventMin." ".$me->EndEventAP."&nbsp;<a href='delete' onclick='return DeleteEvent(".$me->EID.");'><img src='../images/deleteIcon.png' width='11' height='11' alt='Delete Item' border='0' align='absmiddle' /></a> <a href='rhinoflow/scheduleEdit.php?scheduleID=".$me->EID."'><img src='../images/editIcon.png' alt='Edit Event' width='11' height='11' align='absmiddle' /></a></div>";
					echo "<div class='title'>".$me->ClassName." (".$me->ClassCode.")</div>";				
					echo "<div class='studio'>".$me->Instructor."</div>";	?>
                 
			  <?    }   ?>                
            </div>
        	<div class="doyofweek">
           	  <div class="dowtitle">TUESDAY</div>
				 <? 
					while ($me = $tuesday->NextItem()) {
					echo "<div class='time'>".$me->StartEventHour.":".$me->StartEventMin." ".$me->StartEventAP." - ".$me->EndEventHour.":".$me->EndEventMin." ".$me->EndEventAP."&nbsp;<a href='delete'onclick='return DeleteEvent(".$me->EID.");'><img src='../images/deleteIcon.png' width='11' height='11' alt='Delete Item' border='0' align='absmiddle' /></a> <a href='rhinoflow/scheduleEdit.php?scheduleID=".$me->EID."'><img src='../images/editIcon.png' alt='Edit Event' width='11' height='11' align='absmiddle' /></a></div>";
					echo "<div class='title'>".$me->ClassName." (".$me->ClassCode.")</div>";				
					echo "<div class='studio'>".$me->Instructor."</div>";	?>
            
			  <?    }   ?>                
          </div>
        	<div class="doyofweek">
            	<div class="dowtitle">WEDNESDAY</div>
				 <? 
					while ($me = $wednesday->NextItem()) {
					echo "<div class='time'>".$me->StartEventHour.":".$me->StartEventMin." ".$me->StartEventAP." - ".$me->EndEventHour.":".$me->EndEventMin." ".$me->EndEventAP."&nbsp;<a href='delete'onclick='return DeleteEvent(".$me->EID.");'><img src='../images/deleteIcon.png' width='11' height='11' alt='Delete Item' border='0' align='absmiddle' /></a> <a href='rhinoflow/scheduleEdit.php?scheduleID=".$me->EID."'><img src='../images/editIcon.png' alt='Edit Event' width='11' height='11' align='absmiddle' /></a></div>";
					echo "<div class='title'>".$me->ClassName." (".$me->ClassCode.")</div>";				
					echo "<div class='studio'>".$me->Instructor."</div>";	?>
                  
			  <?    }   ?>                
            </div>
        	<div class="doyofweek">
            	<div class="dowtitle">THURSDAY</div>
				 <? 
					while ($me = $thursday->NextItem()) {
					echo "<div class='time'>".$me->StartEventHour.":".$me->StartEventMin." ".$me->StartEventAP." - ".$me->EndEventHour.":".$me->EndEventMin." ".$me->EndEventAP."&nbsp;<a href='delete'onclick='return DeleteEvent(".$me->EID.");'><img src='../images/deleteIcon.png' width='11' height='11' alt='Delete Item' border='0' align='absmiddle' /></a> <a href='rhinoflow/scheduleEdit.php?scheduleID=".$me->EID."'><img src='../images/editIcon.png' alt='Edit Event' width='11' height='11' align='absmiddle' /></a></div>";
					echo "<div class='title'>".$me->ClassName." (".$me->ClassCode.")</div>";				
					echo "<div class='studio'>".$me->Instructor."</div>";	?>
                       
			  <?    }   ?>                
            </div>
        	<div class="doyofweek">
            	<div class="dowtitle">FRIDAY</div>
				 <? 
					while ($me = $friday->NextItem()) {
					echo "<div class='time'>".$me->StartEventHour.":".$me->StartEventMin." ".$me->StartEventAP." - ".$me->EndEventHour.":".$me->EndEventMin." ".$me->EndEventAP."&nbsp;<a href='delete'onclick='return DeleteEvent(".$me->EID.");'><img src='../images/deleteIcon.png' width='11' height='11' alt='Delete Item' border='0' align='absmiddle' /></a> <a href='rhinoflow/scheduleEdit.php?scheduleID=".$me->EID."'><img src='../images/editIcon.png' alt='Edit Event' width='11' height='11' align='absmiddle' /></a></div>";
					echo "<div class='title'>".$me->ClassName." (".$me->ClassCode.")</div>";				
					echo "<div class='studio'>".$me->Instructor."</div>";	?>
                        
			  <?    }   ?>                
            </div>
        	<div class="doyofweek">
            	<div class="dowtitle">SATURDAY</div>
				 <? 
					while ($me = $saturday->NextItem()) {
					echo "<div class='time'>".$me->StartEventHour.":".$me->StartEventMin." ".$me->StartEventAP." - ".$me->EndEventHour.":".$me->EndEventMin." ".$me->EndEventAP."&nbsp;<a href='delete'onclick='return DeleteEvent(".$me->EID.");'><img src='../images/deleteIcon.png' width='11' height='11' alt='Delete Item' border='0' align='absmiddle' /></a> <a href='rhinoflow/scheduleEdit.php?scheduleID=".$me->EID."'><img src='../images/editIcon.png' alt='Edit Event' width='11' height='11' align='absmiddle' /></a></div>";
					echo "<div class='title'>".$me->ClassName." (".$me->ClassCode.")</div>";				
					echo "<div class='studio'>".$me->Instructor."</div>";	?>
         
			  <?    }   ?>                
            </div>
        	<div class="doyofweek">
            	<div class="dowtitle">SUNDAY</div>
				 <? 
					while ($me = $sunday->NextItem()) {
					echo "<div class='time'>".$me->StartEventHour.":".$me->StartEventMin." ".$me->StartEventAP." - ".$me->EndEventHour.":".$me->EndEventMin." ".$me->EndEventAP."&nbsp;<a href='delete'onclick='return DeleteEvent(".$me->EID.");'><img src='../images/deleteIcon.png' width='11' height='11' alt='Delete Item' border='0' align='absmiddle' /></a> <a href='rhinoflow/scheduleEdit.php?scheduleID=".$me->EID."'><img src='../images/editIcon.png' alt='Edit Event' width='11' height='11' align='absmiddle' /></a></div>";
					echo "<div class='title'>".$me->ClassName." (".$me->ClassCode.")</div>";				
					echo "<div class='studio'>".$me->Instructor."</div>";	?>
               
			  <?    }   ?>                
            </div>            
		</div>

	<? //} ?>


	<? } ?>
</div>

<? InsertFooter(); ?>
