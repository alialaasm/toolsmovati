<?php
/*********************************************************************
 * FILE: menu.php
 * CREATED: December 15, 2008
 * *****************************************************************
 * Maintains the main menu.
 * *****************************************************************
 *
 */

require_once("../config.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/App_Code/authenticate.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/Menu.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ActionButtonManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/BannerManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/ContentManager.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/UserManager.php");


authenticate();


$menuNodeID = get_int("nodeID");
$parentNodeID = get_int("parentNodeID");


if ($menuNodeID > 0)
{
	$menuNode = new MenuNode($menuNodeID);
	if ($menuNode->LoadError)
	{
		echo "Error loading item.  This item cannot be accessed or does not exist.";
		exit();
	}
}
else
{
	$menuNode = new MenuNode();

	$menuNode->Template = Template::$Secondary;
	$menuNode->SectionType = "Content";
	$menuNode->Target = "_self";
}

$message = "";
$url = "";


if (IsPostBack)
{

	$action = post_text("txtAction");

	if ($action == "save" || $action == "apply")
	{

		/* ----- Request Post Values ----- */
		$menuNode->Template = post_text("dropTemplate");
		$menuNode->SectionType = post_text("dropSectionType");
		$menuNode->SectionTypeID = post_int("dropSectionTypeID");
		$menuNode->Title = post_text("txtTitle");
		$menuNode->ShowInMenu = post_bool("chkShowInMenu");
		$menuNode->URL = post_text("txtURL");
		$menuNode->BannerID = post_int("rdoBannerID");
		$menuNode->Target = post_text("dropTarget");
		$menuNode->Keywords = post_text("txtKeywords");
		$menuNode->Description = post_text("txtDescription");
		$menuNode->Active = post_bool("chkActive");



		/* ----- Encode Title ----- */
		// Rule 1: Lowercase
		// Rule 2: Acceptable characters: a-z, 0-9
		// Rule 3: Spaces are replaced with "-"

		// Get the title and make all characters lower case.
		$encodedTitle = strtolower($menuNode->Title);

		// Replace spaces with dashes.
		$encodedTitle = str_replace(" ", "-", $encodedTitle);

		// Strip non-alphanumeric characters or dashes.
		$encodedTitle = ereg_replace("[^A-Za-z0-9\-]", "", $encodedTitle);

		// Replace double dashes with a single dash.
		$encodedTitle = ereg_replace("\-\-", "-", $encodedTitle);

		$menuNode->EncodedTitle = $encodedTitle;



		/* ----- Update the Database -----*/
		// The parent node ID is only used by the update function in case of
		// an insert.  Otherwise the value is ignored.
		$menuNode->Update($parentNodeID);


		/* ----- Update User Access ----- */
		$userIDs = post_array("chkUser");

		$menuNode->UpdateUserAccess($userIDs);


		/* ----- UPDATE Action Buttons ----- */
		$actionButtonIDs = post_array("chkActionButton");

		$menuNode->UpdateActionButtons($actionButtonIDs);

		$message = "Menu successfully updated.";

		$url = ($action == "apply") ? ADMIN_DIR . "/menuEdit.php?nodeID=" . $menuNode->ID : ADMIN_DIR . "/menu.php";
		$url = SITE_URL . $url;

	}
}
else
{
	$contentID = get_int("contentID");

	if ($contentID > 0)
	{
		$menuNode->SectionTypeID = $contentID;
	}
}

$actionButtonManager = new ActionButtonManager();
$actionButtons = $actionButtonManager->SelectWithPageAssociation($menuNodeID);

$bannerManager = new BannerManager();
$banners = $bannerManager->SelectAll();

$contentManager = new ContentManager();
$contents = $contentManager->SelectAll();

$userManager = new UserManager();
$users = $userManager->SelectWithPageAccess($menuNodeID);

?>


<? InsertHeader(Template::$Admin); ?>


<? MessageBox($message, $url); ?>

<input type="hidden" id="txtMenuID" name="txtMenuID" value="0" />


<script type="text/javascript">

	dropSectionType_Change = function()
	{
		var sectionType = $("#dropSectionType").val();


		// Content
		var contentFieldDisplay = (sectionType == "Content") ? "block" : "none";
		$(".contentField").css({"display": contentFieldDisplay});

		// External URL
		var externalURLFieldDisplay = (sectionType == "External URL") ? "block" : "none";
		$(".externalURLField").css({"display": externalURLFieldDisplay});

	}

	validateForm = function(Action)
	{

		if ($("#txtTitle").val() == "")
		{
			alert("Please enter a Title.");
			$("#txtTitle").focus();
		}
		else
		{
			$("#txtAction").val(Action);
			return true;
		}

		return false;
	}
</script>


<div id="contentAdmin">

	<h1>Menu</h1>

	<table class="edit" cellspacing="0">

		<tr><th colspan="2"><h2>Page Details</h2></th></tr>

		<tr>
			<th>Title</th>
			<td><input type="text" id="txtTitle" name="txtTitle" value="<?=htmlentities($menuNode->Title) ?>" size="50" maxlength="75" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>


		<tr class="spacer"><td></td></tr>

		<tr>
			<th></th>
			<td class="explanation">
				If unchecked, this section and any sub-sections will not be shown in the menu.  This can<br />
				be useful for creating pages that are not available through the main menu, but will be<br />
				linked to elsewhere (e.g. Privacy Policy, Terms of Use).
			</td>
		</tr>
		<tr>
			<th>Show In Menu</th>
			<td><input type="checkbox" id="chkShowInMenu" name="chkShowInMenu" <? if ($menuNode->ShowInMenu) echo "checked=\"checked\"" ?> value="1" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>


		<tr>
			<th></th>
			<td class="explanation">
				Unchecking this box fully de-activates this section.  This page cannot be linked<br />
				to from anywhere. Any existing links to this page will return a "Page Not Found" error.
			</td>
		</tr>
		<tr>
			<th>Active</th>
			<td><input type="checkbox" id="chkActive" name="chkActive" <? if ($menuNode->Active) echo "checked=\"checked\"" ?> value="1" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>



		<tr><th colspan="2"><h2>Look and Feel</h2></th></tr>

		<tr>
			<th></th>
			<td>
				This is the frame that is displayed around the page.  For most pages, the template will<br />
				be "Secondary".  Only the home page will use "Primary".
			</td>
		</tr>
		<tr>
			<th>Template</th>
			<td>
				<select id="dropTemplate" name="dropTemplate">
					<option value="<?=Template::$Primary ?>" <? if ($menuNode->Template == Template::$Primary) echo "selected=\"selected\"" ?>>Primary</option>
					<option value="<?=Template::$Secondary ?>" <? if ($menuNode->Template == Template::$Secondary) echo "selected=\"selected\"" ?>>Secondary</option>
				</select>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>


		<tr>
			<th>Section Type</th>
			<td>
				<select id="dropSectionType" name="dropSectionType" onchange="dropSectionType_Change();">
					<option value="Careers" <? if ($menuNode->SectionType == "Careers") echo "selected=\"selected\"" ?>>Careers</option>
					<option value="Classes" <? if ($menuNode->SectionType == "Classes") echo "selected=\"selected\"" ?>>Classes</option>
					<option value="Club Locator" <? if ($menuNode->SectionType == "Club Locator") echo "selected=\"selected\"" ?>>Club Locator</option>
					<option value="Contact" <? if ($menuNode->SectionType == "Contact") echo "selected=\"selected\"" ?>>Contact</option>
					<option value="Content" <? if ($menuNode->SectionType == "Content") echo "selected=\"selected\"" ?>>Content</option>
					<option value="Department Directory" <? if ($menuNode->SectionType == "Department Directory") echo "selected=\"selected\"" ?>>Department Directory</option>
					<option value="External URL" <? if ($menuNode->SectionType == "External URL") echo "selected=\"selected\"" ?>>External URL</option>
					<option value="Group Fitness" <? if ($menuNode->SectionType == "Group Fitness") echo "selected=\"selected\"" ?>>Group Fitness</option>
					<option value="Home Page" <? if ($menuNode->SectionType == "Home Page") echo "selected=\"selected\"" ?>>Home Page</option>
					<option value="Membership Trial" <? if ($menuNode->SectionType == "Membership Trial") echo "selected=\"selected\"" ?>>Membership Trial</option>
					<option value="Personal Training Contact" <? if ($menuNode->SectionType == "Personal Training Contact") echo "selected=\"selected\"" ?>>Personal Training Contact</option>
					<option value="Pool Schedule" <? if ($menuNode->SectionType == "Pool Schedule") echo "selected=\"selected\"" ?>>Pool Schedule</option>
					<option value="Press" <? if ($menuNode->SectionType == "Press") echo "selected=\"selected\"" ?>>Press</option>
					<option value="Promotions" <? if ($menuNode->SectionType == "Promotions") echo "selected=\"selected\"" ?>>Promotions</option>
					<option value="Online Promotion" <? if ($menuNode->SectionType == "Online Promotion") echo "selected=\"selected\"" ?>>Online Promotion</option>
                                        <option value="Personal Training" <? if ($menuNode->SectionType == "Personal Training") echo "selected=\"selected\"" ?>>Personal Training</option>
				</select>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>


<!-- ----- Content Field ----- -->
		<tr class="contentField" style="display: <?=(($menuNode->SectionType == "Content") ? "block" : "none") ?>;">
			<th>Content</th>
			<td>
				<select id="dropSectionTypeID" name="dropSectionTypeID">
					<? for ($i = 0; $i < count($contents); $i++) { ?>
						<option value="<?=$contents[$i]->ID ?>" <? if ($contents[$i]->ID == $menuNode->SectionTypeID) { echo "selected=\"selected\""; } ?>>
							<?=$contents[$i]->Title ?>
						</option>
					<? } ?>
				</select>

				<input type="button" value="New Content Section" onclick="window.location = SITE_URL + 'rhinoflow/content.php?nodeID=<?=$menuNodeID ?>&amp;parentNodeID=<?=$parentNodeID ?>'" />
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>


<!-- ----- External URL Fields ----- -->
		<tr class="externalURLField" style="display: <?=(($menuNode->SectionType == "External URL") ? "block" : "none") ?>;">
			<th>URL</th>
			<td><input type="text" id="txtURL" name="txtURL" value="<?=htmlentities($menuNode->URL) ?>" size="50" maxlength="75" /></td>
		</tr>
		<tr class="spacer"><td></td></tr>


		<tr class="externalURLField" style="display: <?=(($menuNode->SectionType == "External URL") ? "block" : "none") ?>;">
			<th>Target Window</th>
			<td>
				<select id="dropTarget" name="dropTarget">
					<option value="_self" <? if ($menuNode->Target == "_self") echo "selected=\"selected\"" ?>>Current</option>
					<option value="_blank" <? if ($menuNode->Target == "_blank") echo "selected=\"selected\"" ?>>New Window</option>
				</select>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>

		<tr>
			<th>Quick Action Buttons</th>
			<td>
				<table cellpadding="2" cellspacing="1" border="0">
				<? while ($actionButton = $actionButtons->NextItem()) { ?>
					<tr>
						<td>
							<input type="checkbox" id="chkActionButton<?=$actionButton->ID ?>" name="chkActionButton[]" value="<?=$actionButton->ID ?>" <? if ($actionButton->HasPageAssociation) { echo "checked"; } ?>>
							<?=$actionButton->Title ?>
						</td>

					<? if ($actionButton = $actionButtons->NextItem()) { ?>
						<td>
							<input type="checkbox" id="chkActionButton<?=$actionButton->ID ?>" name="chkActionButton[]" value="<?=$actionButton->ID ?>" <? if ($actionButton->HasPageAssociation) { echo "checked"; } ?>>
							<?=$actionButton->Title ?>
						</td>
					<? } ?>

					<? if ($actionButton = $actionButtons->NextItem()) { ?>
						<td>
							<input type="checkbox" id="chkActionButton<?=$actionButton->ID ?>" name="chkActionButton[]" value="<?=$actionButton->ID ?>" <? if ($actionButton->HasPageAssociation) { echo "checked"; } ?>>
							<?=$actionButton->Title ?>
						</td>
					<? } ?>
					</tr>
				<? } ?>
				</table>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>

		<tr>
			<th>Side Image</th>
			<td>
				<table cellspadding="2" cellspacing="1" summary="">
					<? while ($banner = $banners->NextItem()) { ?>
					<tr>
						<td style="text-align:center; vertical-align: top;">
							<div style="height:50px;"><img style="height:50px;" src="<?= (BANNER_GALLERY_THUMBNAIL . $banner->Filename ) ?>" alt="" /></div>

							<input type="radio" id="rdoBannerID_<?=$banner->ID ?>" name="rdoBannerID" value="<?=$banner->ID ?>" <? if ($banner->ID == $menuNode->BannerID) { echo "checked=\"checked\""; } ?> />
						</td>
						<? if ($banner = $banners->NextItem()) { ?>
							<td style="text-align:center; vertical-align: top;">
								<div style="height:50px;"><img style="height:50px;" src="<?= (BANNER_GALLERY_THUMBNAIL . $banner->Filename ) ?>" alt="" /></div>
								<input type="radio" id="rdoBannerID_<?=$banner->ID ?>" name="rdoBannerID" value="<?=$banner->ID ?>" <? if ($banner->ID == $menuNode->BannerID) { echo "checked=\"checked\""; } ?> />
							</td>
						<? } ?>
						<? if ($banner = $banners->NextItem()) { ?>
							<td style="text-align:center; vertical-align: top;">
								<div style="height:50px;"><img style="height:50px;" src="<?= (BANNER_GALLERY_THUMBNAIL . $banner->Filename ) ?>" alt="" /></div>
								<input type="radio" id="rdoBannerID_<?=$banner->ID ?>" name="rdoBannerID" value="<?=$banner->ID ?>" <? if ($banner->ID == $menuNode->BannerID) { echo "checked=\"checked\""; } ?> />
							</td>
						<? } ?>
						<? if ($banner = $banners->NextItem()) { ?>
							<td style="text-align:center; vertical-align: top;">
								<div style="height:50px;"><img style="height:50px;" src="<?= (BANNER_GALLERY_THUMBNAIL . $banner->Filename ) ?>" alt="" /></div>
								<input type="radio" id="rdoBannerID_<?=$banner->ID ?>" name="rdoBannerID" value="<?=$banner->ID ?>" <? if ($banner->ID == $menuNode->BannerID) { echo "checked=\"checked\""; } ?> />
							</td>
						<? } ?>
						<? if ($banner = $banners->NextItem()) { ?>
							<td style="text-align:center; vertical-align: top;">
								<div style="height:50px;"><img style="height:50px;" src="<?= (BANNER_GALLERY_THUMBNAIL . $banner->Filename ) ?>" alt="" /></div>
								<input type="radio" id="rdoBannerID_<?=$banner->ID ?>" name="rdoBannerID" value="<?=$banner->ID ?>" <? if ($banner->ID == $menuNode->BannerID) { echo "checked=\"checked\""; } ?> />
							</td>
						<? } ?>
						<? if ($banner = $banners->NextItem()) { ?>
							<td style="text-align:center; vertical-align: top;">
								<div style="height:50px;"><img style="height:50px;" src="<?= (BANNER_GALLERY_THUMBNAIL . $banner->Filename ) ?>" alt="" /></div>
								<input type="radio" id="rdoBannerID_<?=$banner->ID ?>" name="rdoBannerID" value="<?=$banner->ID ?>" <? if ($banner->ID == $menuNode->BannerID) { echo "checked=\"checked\""; } ?> />
							</td>
						<? } ?>
					</tr>
					<? } ?>
					<tr>
						<td colspan="100%" style="text-align:center; vertical-align: middle; height: 70px; font-weight: bold;">
							None
							<div><input type="radio" id="rdoBannerID_0" name="rdoBannerID" value="0" <? if ($menuNode->BannerID == 0) { echo "checked=\"checked\""; } ?> /></div>

						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>

		<tr><th colspan="2"><h2>Search Engine Details</h2></th></tr>

		<tr>
			<th>Keywords</th>
			<td><textarea id="txtKeywords" name="txtKeywords" cols="60" rows="5"><?=htmlentities($menuNode->Keywords) ?></textarea></td>
		</tr>
		<tr class="spacer"><td></td></tr>


		<tr>
			<th>Description</th>
			<td><textarea id="txtDescription" name="txtDescription" cols="60" rows="5"><?=htmlentities($menuNode->Description) ?></textarea></td>
		</tr>
		<tr class="spacer"><td></td></tr>


		<tr><th colspan="2"><h2>User Access</h2></th></tr>

		<tr>
			<td colspan="2">Select users from the list below that can modify this section.</td>
		</tr>

		<tr>
			<td colspan="2">
				<? while ($user = $users->NextItem()) { ?>
                	<? if($user->UserLevel == "AD" || $user->UserLevel == "SA") { ?>
                    <input type="checkbox" id="chkUser<?=$user->ID ?>" name="chkUser[]" value="<?=$user->ID ?>" <? if ($user->HasAccess) { echo "checked"; } ?>>
                    <?=$user->Name ?>
                    <? } ?>
				<? } ?>
			</td>
		</tr>
		<tr class="spacer"><td></td></tr>

		<tr>
			<td colspan="2" style="text-align: center;">
  				<input type="submit" name="txtSubmit" value="Save" title="Save changes and return to menu." onclick="return validateForm('save');" />
  				<input type="submit" name="txtApply" value="Apply" title="Apply changes and continue editing." onclick="return validateForm('apply');" />
  				<input type="button" name="txtReset" value="Cancel" title="Cancel all unsaved changes." onclick="window.location = ADMIN_URL + '/menu.php'" />
			</td>
		</tr>
	</table>
</div>

<? InsertFooter(); ?>
