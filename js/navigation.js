home1 = function() {
	navRoot = document.getElementById("home1");
	for (i=0; i<navRoot.childNodes.length; i++) {
		node = navRoot.childNodes[i];
		if (node.nodeName=="LI") {
			node.onmouseover=function() {
				this.className+="_over over";
			}
			node.onmouseout=function() {
				this.className=this.className.replace(" _over over", "");
			}
		}
	}
}

onLoadFunctions = function() {
	home1();
}

window.onload=onLoadFunctions;