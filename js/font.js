/*

var myfunctions = {
	textresize : function() {
		// show text resizing links
		$(".textsize").show();
		var $cookie_name = "textsize";
		var originalFontSize = $("html").css("font-size");


		if($.cookie($cookie_name)) {
			var $getSize = $.cookie($cookie_name);
			$("html").css({fontSize : $getSize + ($getSize.indexOf("px")!=-1 ? "" : "px")});
		} else {
			$.cookie($cookie_name, originalFontSize);
		}

		// reset link
		$(".textsize-txt").bind("click", function() {
			$("html").css("font-size", originalFontSize);
			$.cookie($cookie_name, originalFontSize);
		});
		// text "+" link
		$(".textsize-inc").bind("click", function() {
			var currentFontSize = $("html").css("font-size");
			var currentFontSizeNum = parseFloat(currentFontSize, 10);
			var newFontSize = currentFontSizeNum*1.2;
			if (newFontSize > 11) {
				$("html").css("font-size", newFontSize);
				$.cookie($cookie_name, newFontSize);
			}
			return false;
		});
	}
}

$(document).ready(function(){
		myfunctions.textresize();
})

*/


$(document).ready(function()
{
	// Cookie to save font size.
	var $cookie_name = "textsize";

	// Reset Font Size
	var originalFontSize = $('html').css('font-size');

	if($.cookie($cookie_name))
	{
		var $getSize = $.cookie($cookie_name);
		$("html").css({fontSize : $getSize + ($getSize.indexOf("px")!=-1 ? "" : "px")});
	}
	else
	{
		$.cookie($cookie_name, originalFontSize);
	}


	// Reset Font Size
	$(".resetFont").click(function()
	{
		$('html').css('font-size', originalFontSize);
		$.cookie($cookie_name, originalFontSize);
		return false;
	});


 	// Increase Font Size
	$(".increaseFont").click(function()
	{
		var currentFontSize = $('html').css('font-size');
		var currentFontSizeNum = parseFloat(currentFontSize, 10);

		if (currentFontSizeNum < 22)
		{
			var newFontSize = ++currentFontSizeNum;
			$('html').css('font-size', newFontSize);
			$.cookie($cookie_name, newFontSize);
		}

		return false;
	});


	// Decrease Font Size
	$(".decreaseFont").click(function()
	{
		var currentFontSize = $('html').css('font-size');
		var currentFontSizeNum = parseFloat(currentFontSize, 10);

		if (currentFontSizeNum > 16)
		{
			var newFontSize = --currentFontSizeNum;
			$('html').css('font-size', newFontSize);
			$.cookie($cookie_name, newFontSize);
		}
		return false;
	});
});