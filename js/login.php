<?php
/********************************************************************
 FILE: login.php
 CREATED BY: Hetesi & Vurma Strategic Solutions
 ********************************************************************
	This is the login page for administrative section.  The
 client-side script uses SHA1 encryption to encrypt the password that
 is then authenticated by this page.  This prevents passwords from
 being sent in plain text.  Once validated, the user remains valid
 until the session expires or the user changes IP addresses.
 ********************************************************************/

include_once("config.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/DAL/UserManager.php");


$file = "login.html";

$username = post_text("username", false);

$message = "";

if ($username != "")
{
	$valid_user = false;

	$userManager = new UserManager();

	// Create query to retrieve users with the given username.
	$user = $userManager->SelectActiveByUsername($username);

	// If there are 0 rows then there were no valid usernames found.
	// If there are more than 1 row, the there has been SQL injection.
	if ($user != null)
	{
		$valid_user = (post_text("passenc") == sha1($user->Password . $_SESSION["LOG_IN_KEY"]));
		d($user);
	}

	if ($valid_user)
	{
		$_SESSION["IP_ADDRESS"] = $_SERVER["REMOTE_ADDR"];
		$_SESSION["USER_ID"] = $user->ID;
		$_SESSION["USER_LEVEL"] = ADMIN;
		$_SESSION["USER_NAME"] = $user->Name;
		session_write_close();

		header("Location: http://" . $_SERVER["HTTP_HOST"] . get_text("return_url"));
		exit();
	}
	else
		$message = "Invalid username or password.";
}


$_SESSION["LOG_IN_KEY"] = "". mt_rand();

$log_in_key = $_SESSION["LOG_IN_KEY"];

?>

<? InsertHeader(Template::$Secondary, 0, false); ?>

<script type="text/javascript" src="js/sha1.js"></script>
<script type="text/javascript">
	function CheckForm(form)
	{
		if (form.username.value == "")
		{
			alert("Please enter your username.");
			form.username.focus();
		}
		else if (form.password.value == "")
		{
			alert("Please enter your password.");
			form.password.focus();
		}
		else
		{
			form.passenc.value = hex_sha1(form.password.value + "<?=$log_in_key ?>");
			form.password.value = "";

			return true;
		}

		return false;
	}
</script>

<div style="width: 300px; margin: 0 auto;">
	<br />
	<?=$message ?>

	<form action="" method="post" onsubmit="return CheckForm(this);" name="Form1" id="Form1">
		<input type="hidden" name="passenc" />

		<table cellpadding="2" cellspacing="1" border="0">
			<tr>
				<td colspan="100%" align="left">
					<b><u>Adminstrative Login</u></b>
				</td>
			</tr>
			<tr>
				<td align="right">Username:</td>
				<td><input type="text" name="username" /></td>
			</tr>
			<tr>
				<td align="right">Password:</td>
				<td><input type="password" name="password" /></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" id="btnLogin" name="btnLogin" value="Log In" /></td>
			</tr>
		</table>
	</form>
</div>

<? InsertFooter(); ?>