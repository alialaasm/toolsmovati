
function daysInFebruary (year)
{
	// February has 29 days in any year evenly divisible by four,
	// EXCEPT for centurial years which are NOT divisible by 400.
	return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}


function myParseInt(str) {
	if (str.charAt(0) == '0') { str = str.substring(1); }

	return parseInt(str);
}


function isDate(str)
{
	var datemask = /^\d\d\d\d-\d\d-\d\d$/;
	if (!datemask.test(str)) { return false; }

	var parts = str.split("-");

	var year = myParseInt(parts[0]);
	// month is a function
	var month = myParseInt(parts[1]);
	var day = myParseInt(parts[2]);

	switch (month)
	{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return ((0 < day) && (day <= 31));
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			return ((0 < day) && (day <= 30));
			break;
		case 2:
			return ((0 < day) && (day <= daysInFebruary(year)));
			break;
	}

	return false;
}