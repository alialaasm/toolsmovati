
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- ActionButton
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ActionButton`;

CREATE TABLE `ActionButton`
(
    `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `Colour` VARCHAR(75) DEFAULT '' NOT NULL,
    `SubTitle` VARCHAR(250) DEFAULT '' NOT NULL,
    `Title` VARCHAR(45) DEFAULT '' NOT NULL,
    `DisplayOrder` smallint(5) unsigned DEFAULT 0 NOT NULL,
    `URL` VARCHAR(100) DEFAULT '' NOT NULL,
    `Active` TINYINT(1) DEFAULT 1 NOT NULL,
    PRIMARY KEY (`ID`),
    INDEX `Index_Active` (`Active`),
    INDEX `Index_OrderIndex` (`DisplayOrder`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- Banner
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Banner`;

CREATE TABLE `Banner`
(
    `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `Filename` VARCHAR(75) DEFAULT '' NOT NULL,
    `Title` VARCHAR(75) DEFAULT '' NOT NULL,
    `URL` VARCHAR(100) DEFAULT '' NOT NULL,
    `Active` TINYINT(1) DEFAULT 1 NOT NULL,
    PRIMARY KEY (`ID`),
    INDEX `Index_Title` (`Title`),
    INDEX `Index_Active` (`Active`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- Career
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Career`;

CREATE TABLE `Career`
(
    `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `Position` VARCHAR(75) DEFAULT '' NOT NULL,
    `Description` TEXT NOT NULL,
    `Active` TINYINT(1) DEFAULT 1 NOT NULL,
    PRIMARY KEY (`ID`),
    INDEX `Index_Position` (`Position`),
    INDEX `Index_Active` (`Active`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- CareerPosition
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `CareerPosition`;

CREATE TABLE `CareerPosition`
(
    `Type` VARCHAR(50) DEFAULT '' NOT NULL,
    `Name` VARCHAR(100) DEFAULT '' NOT NULL,
    PRIMARY KEY (`Type`,`Name`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- Class
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Class`;

CREATE TABLE `Class`
(
    `ID` INTEGER(10) NOT NULL AUTO_INCREMENT,
    `Category` VARCHAR(100) DEFAULT '' NOT NULL,
    `CatId` INTEGER(10) DEFAULT 0 NOT NULL,
    `ClassName` VARCHAR(45) DEFAULT '' NOT NULL,
    `Description` TEXT NOT NULL,
    `VideoLink` TEXT NOT NULL,
    `ClassCode` CHAR(3) DEFAULT '' NOT NULL,
    `Active` TINYINT(1) DEFAULT 1 NOT NULL,
    PRIMARY KEY (`ID`),
    INDEX `Index_Category` (`Category`),
    INDEX `Index_ClassCode` (`ClassCode`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- ClassCategory
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ClassCategory`;

CREATE TABLE `ClassCategory`
(
    `ID` INTEGER(12) NOT NULL AUTO_INCREMENT,
    `CategoryName` VARCHAR(100) DEFAULT '' NOT NULL,
    `Colour` INTEGER(1) DEFAULT 0 NOT NULL,
    PRIMARY KEY (`ID`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- ClassResult
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ClassResult`;

CREATE TABLE `ClassResult`
(
    `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `ClassID` INTEGER(12),
    `InstructorID` int(10) unsigned NOT NULL,
    `SubstituteForInstructorID` int(10) unsigned,
    `Date` DATETIME NOT NULL,
    `Notes` VARCHAR(255) DEFAULT '',
    `ClubID` int(10) unsigned,
    `Participants` INTEGER(10),
    `last_updated` DATETIME NOT NULL,
    `Hours` DECIMAL,
    `date_entered` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `StartTime` VARCHAR(255),
    PRIMARY KEY (`ID`),
    INDEX `fk_ClassResult_Instructor1` (`InstructorID`),
    INDEX `fk_ClassResult_Instructor2` (`SubstituteForInstructorID`),
    INDEX `fk_ClassResult_Club1` (`ClubID`),
    INDEX `fk_ClassResult_Event1` (`ClassID`),
    CONSTRAINT `fk_ClassResult_Class1`
        FOREIGN KEY (`ClassID`)
        REFERENCES `Class` (`ID`),
    CONSTRAINT `fk_ClassResult_Club1`
        FOREIGN KEY (`ClubID`)
        REFERENCES `Club` (`ID`),
    CONSTRAINT `fk_ClassResult_Instructor1`
        FOREIGN KEY (`InstructorID`)
        REFERENCES `Instructor` (`ID`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- Club
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Club`;

CREATE TABLE `Club`
(
    `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `ClubName` VARCHAR(45) DEFAULT '' NOT NULL,
    `Address` VARCHAR(100) DEFAULT '' NOT NULL,
    `City` VARCHAR(45) DEFAULT '' NOT NULL,
    `Province` VARCHAR(45) DEFAULT '' NOT NULL,
    `PostalCode` VARCHAR(7) DEFAULT '' NOT NULL,
    `MapItAddress` VARCHAR(250) DEFAULT '' NOT NULL,
    `Phone` VARCHAR(15) DEFAULT '' NOT NULL,
    `TollFree` VARCHAR(15) DEFAULT '' NOT NULL,
    `ContactEmail` VARCHAR(100) DEFAULT '' NOT NULL,
    `ApplicationEmail` VARCHAR(100) DEFAULT '' NOT NULL,
    `PersonalTrainingEmail` VARCHAR(255) DEFAULT '' NOT NULL,
    `Promotions` TEXT NOT NULL,
    `ScheduleURL` VARCHAR(255) DEFAULT '' NOT NULL,
    `PoolScheduleURL` VARCHAR(250) DEFAULT '' NOT NULL,
    `UseInternal` TINYINT(1) DEFAULT 0 NOT NULL,
    `SundayHours` VARCHAR(45) DEFAULT '' NOT NULL,
    `MondayHours` VARCHAR(45) DEFAULT '' NOT NULL,
    `TuesdayHours` VARCHAR(45) DEFAULT '' NOT NULL,
    `WednesdayHours` VARCHAR(45) DEFAULT '' NOT NULL,
    `ThursdayHours` VARCHAR(45) DEFAULT '' NOT NULL,
    `FridayHours` VARCHAR(45) DEFAULT '' NOT NULL,
    `SaturdayHours` VARCHAR(45) DEFAULT '' NOT NULL,
    `Active` TINYINT(1) DEFAULT 1 NOT NULL,
    `FreeTrial` TINYINT(1) DEFAULT 1 NOT NULL,
    `SalesSundayHours` VARCHAR(45) DEFAULT '' NOT NULL,
    `SalesMondayHours` VARCHAR(45) DEFAULT '' NOT NULL,
    `SalesTuesdayHours` VARCHAR(45) DEFAULT '' NOT NULL,
    `SalesWednesdayHours` VARCHAR(45) DEFAULT '' NOT NULL,
    `SalesThursdayHours` VARCHAR(45) DEFAULT '' NOT NULL,
    `SalesFridayHours` VARCHAR(45) DEFAULT '' NOT NULL,
    `SalesSaturdayHours` VARCHAR(45) DEFAULT '' NOT NULL,
    `SalesTrailer` TINYINT(1) DEFAULT 0 NOT NULL,
    `OnlinePromo` CHAR DEFAULT '0' NOT NULL,
    `OnlinePromotion` TEXT,
    `NewsletterURL` VARCHAR(255) NOT NULL,
    `facebook_url` VARCHAR(255),
    `mobile_image` VARCHAR(255),
    PRIMARY KEY (`ID`),
    INDEX `IndexClub` (`ClubName`),
    INDEX `IndexCity` (`City`),
    INDEX `IndexProvince` (`Province`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- ClubCareer
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ClubCareer`;

CREATE TABLE `ClubCareer`
(
    `ClubID` int(10) unsigned DEFAULT 0 NOT NULL,
    `CareerID` int(10) unsigned DEFAULT 0 NOT NULL,
    PRIMARY KEY (`ClubID`,`CareerID`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- Content
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Content`;

CREATE TABLE `Content`
(
    `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `Title` VARCHAR(100) DEFAULT '' NOT NULL,
    `Description` TEXT NOT NULL,
    PRIMARY KEY (`ID`),
    INDEX `Index_Title` (`Title`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- Department
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Department`;

CREATE TABLE `Department`
(
    `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `Name` VARCHAR(100) DEFAULT '' NOT NULL,
    `Email` VARCHAR(255) DEFAULT '' NOT NULL,
    `Active` tinyint(3) unsigned DEFAULT 0 NOT NULL,
    PRIMARY KEY (`ID`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- Event
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Event`;

CREATE TABLE `Event`
(
    `id` INTEGER(12) NOT NULL,
    `CategoryId` INTEGER(12),
    `ClassId` INTEGER(10),
    `ClubId` INTEGER(12),
    `StudioId` INTEGER(12),
    `Instructor` VARCHAR(255) DEFAULT '' NOT NULL,
    `StartEventHour` TINYINT,
    `StartEventMin` TINYINT,
    `StartEventAP` CHAR(2),
    `EndEventHour` TINYINT,
    `EndEventMin` TINYINT,
    `EndEventAP` CHAR(2),
    `Day` TINYINT(2),
    `Month` TINYINT(2),
    `Year` INTEGER(4),
    PRIMARY KEY (`id`),
    INDEX `fk_Event_Class1` (`ClassId`),
    CONSTRAINT `fk_Event_Class1`
        FOREIGN KEY (`ClassId`)
        REFERENCES `Class` (`ID`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- Instructor
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Instructor`;

CREATE TABLE `Instructor`
(
    `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `Email` VARCHAR(45) DEFAULT '' NOT NULL,
    `Password` VARCHAR(45) DEFAULT '' NOT NULL,
    `FirstName` VARCHAR(45) DEFAULT '' NOT NULL,
    `LastName` VARCHAR(45) DEFAULT '' NOT NULL,
    `ClubId` int(10) unsigned,
    `is_admin` INTEGER,
    PRIMARY KEY (`ID`),
    INDEX `fk_Instructor_Club` (`ClubId`),
    CONSTRAINT `fk_Instructor_Club`
        FOREIGN KEY (`ClubId`)
        REFERENCES `Club` (`ID`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- Member
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Member`;

CREATE TABLE `Member`
(
    `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `Email` VARCHAR(45) DEFAULT '' NOT NULL,
    `Password` VARCHAR(45) DEFAULT '' NOT NULL,
    `ClubId` INTEGER(2),
    PRIMARY KEY (`ID`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- MemberClass
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `MemberClass`;

CREATE TABLE `MemberClass`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `MemberId` INTEGER,
    `ClassId` INTEGER,
    PRIMARY KEY (`ID`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- Menu
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Menu`;

CREATE TABLE `Menu`
(
    `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `LeftVal` int(10) unsigned DEFAULT 0 NOT NULL,
    `RightVal` INTEGER DEFAULT 0 NOT NULL,
    `Template` VARCHAR(10) DEFAULT 'secondary' NOT NULL,
    `SectionType` VARCHAR(45) DEFAULT 'content' NOT NULL,
    `SectionTypeID` INTEGER DEFAULT 0 NOT NULL,
    `Title` VARCHAR(100) DEFAULT '' NOT NULL,
    `EncodedTitle` VARCHAR(100) DEFAULT '' NOT NULL,
    `ShowInMenu` TINYINT DEFAULT 1 NOT NULL,
    `BannerID` INTEGER DEFAULT 0 NOT NULL,
    `URL` VARCHAR(250) DEFAULT '' NOT NULL,
    `Target` VARCHAR(10) DEFAULT '' NOT NULL,
    `Keywords` VARCHAR(100) DEFAULT '' NOT NULL,
    `Description` VARCHAR(255) DEFAULT '' NOT NULL,
    `Active` tinyint(3) unsigned DEFAULT 1 NOT NULL,
    `RowMark` tinyint(3) unsigned DEFAULT 0 NOT NULL,
    PRIMARY KEY (`ID`),
    INDEX `Index_Active` (`Active`),
    INDEX `EncodedTitle` (`EncodedTitle`),
    INDEX `Index_LeftVal` (`LeftVal`),
    INDEX `Index_RightVal` (`RightVal`),
    INDEX `BannerID` (`BannerID`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- MenuActionButton
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `MenuActionButton`;

CREATE TABLE `MenuActionButton`
(
    `MenuID` int(10) unsigned DEFAULT 0 NOT NULL,
    `ActionButtonID` int(10) unsigned DEFAULT 0 NOT NULL,
    PRIMARY KEY (`MenuID`,`ActionButtonID`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- ModuleData
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ModuleData`;

CREATE TABLE `ModuleData`
(
    `ModuleName` VARCHAR(50) DEFAULT '' NOT NULL,
    `Var1` TEXT NOT NULL,
    `Var2` TEXT NOT NULL,
    `Var3` TEXT NOT NULL,
    `Var4` TEXT NOT NULL,
    `Var5` TEXT NOT NULL,
    `Var6` TEXT NOT NULL,
    `Var7` TEXT NOT NULL,
    `Var8` TEXT NOT NULL,
    PRIMARY KEY (`ModuleName`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- Press
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Press`;

CREATE TABLE `Press`
(
    `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `ReleaseDate` DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
    `Title` VARCHAR(100) DEFAULT '' NOT NULL,
    `Description` TEXT NOT NULL,
    `Active` tinyint(3) unsigned DEFAULT 0 NOT NULL,
    PRIMARY KEY (`ID`),
    INDEX `Index_ReleaseDate` (`ReleaseDate`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- Studio
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Studio`;

CREATE TABLE `Studio`
(
    `ID` INTEGER(12) NOT NULL AUTO_INCREMENT,
    `ClubId` INTEGER(12),
    `StudioName` VARCHAR(255),
    `StudioStyle` VARCHAR(255) DEFAULT 'studioblue' NOT NULL,
    PRIMARY KEY (`ID`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- User
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `User`;

CREATE TABLE `User`
(
    `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `Username` VARCHAR(45) DEFAULT '' NOT NULL,
    `Name` VARCHAR(45) DEFAULT '' NOT NULL,
    `Password` VARCHAR(45) DEFAULT '' NOT NULL,
    `Active` TINYINT(1) DEFAULT 1 NOT NULL,
    `SuperUser` tinyint(3) unsigned DEFAULT 0 NOT NULL,
    `UserLevel` VARCHAR(10) DEFAULT '' NOT NULL,
    `ClubId` INTEGER(2),
    PRIMARY KEY (`ID`),
    INDEX `Index_Username` (`Username`),
    INDEX `Index_Active` (`Active`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- UserAccess
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `UserAccess`;

CREATE TABLE `UserAccess`
(
    `UserID` int(10) unsigned DEFAULT 0 NOT NULL,
    `MenuID` int(10) unsigned DEFAULT 0 NOT NULL,
    `ClubID` INTEGER(10) DEFAULT 0 NOT NULL,
    `StudioID` INTEGER(10) DEFAULT 0 NOT NULL,
    `ID` INTEGER(10) NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (`ID`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
