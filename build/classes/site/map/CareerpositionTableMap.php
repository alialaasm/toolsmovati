<?php



/**
 * This class defines the structure of the 'CareerPosition' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.site.map
 */
class CareerpositionTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'site.map.CareerpositionTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('CareerPosition');
        $this->setPhpName('Careerposition');
        $this->setClassname('Careerposition');
        $this->setPackage('site');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('TYPE', 'Type', 'VARCHAR', true, 50, '');
        $this->addPrimaryKey('NAME', 'Name', 'VARCHAR', true, 100, '');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CareerpositionTableMap
