<?php



/**
 * This class defines the structure of the 'ActionButton' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.site.map
 */
class ActionbuttonTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'site.map.ActionbuttonTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ActionButton');
        $this->setPhpName('Actionbutton');
        $this->setClassname('Actionbutton');
        $this->setPackage('site');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('COLOUR', 'Colour', 'VARCHAR', true, 75, '');
        $this->addColumn('SUBTITLE', 'Subtitle', 'VARCHAR', true, 250, '');
        $this->addColumn('TITLE', 'Title', 'VARCHAR', true, 45, '');
        $this->addColumn('DISPLAYORDER', 'Displayorder', 'SMALLINT', true, 5, 0);
        $this->addColumn('URL', 'Url', 'VARCHAR', true, 100, '');
        $this->addColumn('ACTIVE', 'Active', 'BOOLEAN', true, 1, true);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // ActionbuttonTableMap
