<?php



/**
 * This class defines the structure of the 'Club' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.site.map
 */
class ClubTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'site.map.ClubTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Club');
        $this->setPhpName('Club');
        $this->setClassname('Club');
        $this->setPackage('site');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('CLUBNAME', 'Clubname', 'VARCHAR', true, 45, '');
        $this->addColumn('ADDRESS', 'Address', 'VARCHAR', true, 100, '');
        $this->addColumn('CITY', 'City', 'VARCHAR', true, 45, '');
        $this->addColumn('PROVINCE', 'Province', 'VARCHAR', true, 45, '');
        $this->addColumn('POSTALCODE', 'Postalcode', 'VARCHAR', true, 7, '');
        $this->addColumn('MAPITADDRESS', 'Mapitaddress', 'VARCHAR', true, 250, '');
        $this->addColumn('PHONE', 'Phone', 'VARCHAR', true, 15, '');
        $this->addColumn('TOLLFREE', 'Tollfree', 'VARCHAR', true, 15, '');
        $this->addColumn('CONTACTEMAIL', 'Contactemail', 'VARCHAR', true, 100, '');
        $this->addColumn('APPLICATIONEMAIL', 'Applicationemail', 'VARCHAR', true, 100, '');
        $this->addColumn('PERSONALTRAININGEMAIL', 'Personaltrainingemail', 'VARCHAR', true, 255, '');
        $this->addColumn('PROMOTIONS', 'Promotions', 'LONGVARCHAR', true, null, null);
        $this->addColumn('SCHEDULEURL', 'Scheduleurl', 'VARCHAR', true, 255, '');
        $this->addColumn('POOLSCHEDULEURL', 'Poolscheduleurl', 'VARCHAR', true, 250, '');
        $this->addColumn('USEINTERNAL', 'Useinternal', 'BOOLEAN', true, 1, false);
        $this->addColumn('SUNDAYHOURS', 'Sundayhours', 'VARCHAR', true, 45, '');
        $this->addColumn('MONDAYHOURS', 'Mondayhours', 'VARCHAR', true, 45, '');
        $this->addColumn('TUESDAYHOURS', 'Tuesdayhours', 'VARCHAR', true, 45, '');
        $this->addColumn('WEDNESDAYHOURS', 'Wednesdayhours', 'VARCHAR', true, 45, '');
        $this->addColumn('THURSDAYHOURS', 'Thursdayhours', 'VARCHAR', true, 45, '');
        $this->addColumn('FRIDAYHOURS', 'Fridayhours', 'VARCHAR', true, 45, '');
        $this->addColumn('SATURDAYHOURS', 'Saturdayhours', 'VARCHAR', true, 45, '');
        $this->addColumn('ACTIVE', 'Active', 'BOOLEAN', true, 1, true);
        $this->addColumn('FREETRIAL', 'Freetrial', 'BOOLEAN', true, 1, true);
        $this->addColumn('SALESSUNDAYHOURS', 'Salessundayhours', 'VARCHAR', true, 45, '');
        $this->addColumn('SALESMONDAYHOURS', 'Salesmondayhours', 'VARCHAR', true, 45, '');
        $this->addColumn('SALESTUESDAYHOURS', 'Salestuesdayhours', 'VARCHAR', true, 45, '');
        $this->addColumn('SALESWEDNESDAYHOURS', 'Saleswednesdayhours', 'VARCHAR', true, 45, '');
        $this->addColumn('SALESTHURSDAYHOURS', 'Salesthursdayhours', 'VARCHAR', true, 45, '');
        $this->addColumn('SALESFRIDAYHOURS', 'Salesfridayhours', 'VARCHAR', true, 45, '');
        $this->addColumn('SALESSATURDAYHOURS', 'Salessaturdayhours', 'VARCHAR', true, 45, '');
        $this->addColumn('SALESTRAILER', 'Salestrailer', 'BOOLEAN', true, 1, false);
        $this->addColumn('ONLINEPROMO', 'Onlinepromo', 'CHAR', true, null, '0');
        $this->addColumn('ONLINEPROMOTION', 'Onlinepromotion', 'LONGVARCHAR', false, null, null);
        $this->addColumn('NEWSLETTERURL', 'Newsletterurl', 'VARCHAR', true, 255, null);
        $this->addColumn('FACEBOOK_URL', 'FacebookUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('MOBILE_IMAGE', 'MobileImage', 'VARCHAR', false, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Classresult', 'Classresult', RelationMap::ONE_TO_MANY, array('ID' => 'ClubID', ), null, null, 'Classresults');
        $this->addRelation('Instructor', 'Instructor', RelationMap::ONE_TO_MANY, array('ID' => 'ClubId', ), null, null, 'Instructors');
    } // buildRelations()

} // ClubTableMap
