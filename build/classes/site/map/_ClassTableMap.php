<?php



/**
 * This class defines the structure of the 'Class' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.site.map
 */
class _ClassTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'site.map._ClassTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Class');
        $this->setPhpName('_Class');
        $this->setClassname('_Class');
        $this->setPackage('site');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('CATEGORY', 'Category', 'VARCHAR', true, 100, '');
        $this->addColumn('CATID', 'Catid', 'INTEGER', true, 10, 0);
        $this->addColumn('CLASSNAME', 'Classname', 'VARCHAR', true, 45, '');
        $this->addColumn('DESCRIPTION', 'Description', 'LONGVARCHAR', true, null, null);
        $this->addColumn('VIDEOLINK', 'Videolink', 'LONGVARCHAR', true, null, null);
        $this->addColumn('CLASSCODE', 'Classcode', 'CHAR', true, 3, '');
        $this->addColumn('ACTIVE', 'Active', 'BOOLEAN', true, 1, true);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Classresult', 'Classresult', RelationMap::ONE_TO_MANY, array('ID' => 'ClassID', ), null, null, 'Classresults');
        $this->addRelation('Event', 'Event', RelationMap::ONE_TO_MANY, array('ID' => 'ClassId', ), null, null, 'Events');
    } // buildRelations()

} // _ClassTableMap
