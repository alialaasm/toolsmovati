<?php



/**
 * This class defines the structure of the 'ModuleData' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.site.map
 */
class ModuledataTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'site.map.ModuledataTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ModuleData');
        $this->setPhpName('Moduledata');
        $this->setClassname('Moduledata');
        $this->setPackage('site');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('MODULENAME', 'Modulename', 'VARCHAR', true, 50, '');
        $this->addColumn('VAR1', 'Var1', 'LONGVARCHAR', true, null, null);
        $this->addColumn('VAR2', 'Var2', 'LONGVARCHAR', true, null, null);
        $this->addColumn('VAR3', 'Var3', 'LONGVARCHAR', true, null, null);
        $this->addColumn('VAR4', 'Var4', 'LONGVARCHAR', true, null, null);
        $this->addColumn('VAR5', 'Var5', 'LONGVARCHAR', true, null, null);
        $this->addColumn('VAR6', 'Var6', 'LONGVARCHAR', true, null, null);
        $this->addColumn('VAR7', 'Var7', 'LONGVARCHAR', true, null, null);
        $this->addColumn('VAR8', 'Var8', 'LONGVARCHAR', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // ModuledataTableMap
