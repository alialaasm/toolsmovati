<?php



/**
 * This class defines the structure of the 'Menu' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.site.map
 */
class MenuTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'site.map.MenuTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Menu');
        $this->setPhpName('Menu');
        $this->setClassname('Menu');
        $this->setPackage('site');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('LEFTVAL', 'Leftval', 'INTEGER', true, 10, 0);
        $this->addColumn('RIGHTVAL', 'Rightval', 'INTEGER', true, null, 0);
        $this->addColumn('TEMPLATE', 'Template', 'VARCHAR', true, 10, 'secondary');
        $this->addColumn('SECTIONTYPE', 'Sectiontype', 'VARCHAR', true, 45, 'content');
        $this->addColumn('SECTIONTYPEID', 'Sectiontypeid', 'INTEGER', true, null, 0);
        $this->addColumn('TITLE', 'Title', 'VARCHAR', true, 100, '');
        $this->addColumn('ENCODEDTITLE', 'Encodedtitle', 'VARCHAR', true, 100, '');
        $this->addColumn('SHOWINMENU', 'Showinmenu', 'TINYINT', true, null, 1);
        $this->addColumn('BANNERID', 'Bannerid', 'INTEGER', true, null, 0);
        $this->addColumn('URL', 'Url', 'VARCHAR', true, 250, '');
        $this->addColumn('TARGET', 'Target', 'VARCHAR', true, 10, '');
        $this->addColumn('KEYWORDS', 'Keywords', 'VARCHAR', true, 100, '');
        $this->addColumn('DESCRIPTION', 'Description', 'VARCHAR', true, 255, '');
        $this->addColumn('ACTIVE', 'Active', 'TINYINT', true, 3, 1);
        $this->addColumn('ROWMARK', 'Rowmark', 'TINYINT', true, 3, 0);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // MenuTableMap
