<?php



/**
 * This class defines the structure of the 'Studio' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.site.map
 */
class StudioTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'site.map.StudioTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Studio');
        $this->setPhpName('Studio');
        $this->setClassname('Studio');
        $this->setPackage('site');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, 12, null);
        $this->addColumn('CLUBID', 'Clubid', 'INTEGER', false, 12, null);
        $this->addColumn('STUDIONAME', 'Studioname', 'VARCHAR', false, 255, null);
        $this->addColumn('STUDIOSTYLE', 'Studiostyle', 'VARCHAR', true, 255, 'studioblue');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // StudioTableMap
