<?php



/**
 * Skeleton subclass for performing query and update operations on the 'Instructor' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.site
 */
class InstructorQuery extends BaseInstructorQuery {

	public function InstructorQuery()
	 {
        parent::__construct();
     }

	public function Instructor_login($email,$pass, $con = null)
	{
		 $sql = 'SELECT `ID`, `EMAIL`, `PASSWORD`, `FIRSTNAME`, `LASTNAME`, `CLUBID` FROM `Instructor` WHERE `EMAIL` = :p0 and `PASSWORD` = :p1';
        try 
		{
			if ($con === null) {
				$con = Propel::getConnection(InstructorPeer::DATABASE_NAME, Propel::CONNECTION_READ);
			}
            $stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $email, PDO::PARAM_STR,45);
			$stmt->bindValue(':p1', md5($pass), PDO::PARAM_STR,45);
			$stmt->execute();
        } catch (Exception $e) {
		
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Instructor();
            $obj->hydrate($row);
            InstructorPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
	}
	public function selectall($con = null)
	{
		$sql = 'SELECT `ID`, `EMAIL`, `PASSWORD`, `FIRSTNAME`, `LASTNAME`, `CLUBID` FROM `Instructor` order by `FIRSTNAME`, `LASTNAME`' ;
		try 
		{
			if ($con === null) {
				$con = Propel::getConnection(InstructorPeer::DATABASE_NAME, Propel::CONNECTION_READ);
			}
            $stmt = $con->prepare($sql);
			$stmt->execute();
        } catch (Exception $e) {
		
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        $obj = $stmt->fetchALL();
		$stmt->closeCursor();

        return $obj;
	}

} // InstructorQuery
