<?php


/**
 * Base class that represents a row from the 'Menu' table.
 *
 * 
 *
 * @package    propel.generator.site.om
 */
abstract class BaseMenu extends BaseObject 
{

    /**
     * Peer class name
     */
    const PEER = 'MenuPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        MenuPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the leftval field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $leftval;

    /**
     * The value for the rightval field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $rightval;

    /**
     * The value for the template field.
     * Note: this column has a database default value of: 'secondary'
     * @var        string
     */
    protected $template;

    /**
     * The value for the sectiontype field.
     * Note: this column has a database default value of: 'content'
     * @var        string
     */
    protected $sectiontype;

    /**
     * The value for the sectiontypeid field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $sectiontypeid;

    /**
     * The value for the title field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $title;

    /**
     * The value for the encodedtitle field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $encodedtitle;

    /**
     * The value for the showinmenu field.
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $showinmenu;

    /**
     * The value for the bannerid field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $bannerid;

    /**
     * The value for the url field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $url;

    /**
     * The value for the target field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $target;

    /**
     * The value for the keywords field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $keywords;

    /**
     * The value for the description field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $description;

    /**
     * The value for the active field.
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $active;

    /**
     * The value for the rowmark field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $rowmark;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->leftval = 0;
        $this->rightval = 0;
        $this->template = 'secondary';
        $this->sectiontype = 'content';
        $this->sectiontypeid = 0;
        $this->title = '';
        $this->encodedtitle = '';
        $this->showinmenu = 1;
        $this->bannerid = 0;
        $this->url = '';
        $this->target = '';
        $this->keywords = '';
        $this->description = '';
        $this->active = 1;
        $this->rowmark = 0;
    }

    /**
     * Initializes internal state of BaseMenu object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     * 
     * @return   int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [leftval] column value.
     * 
     * @return   int
     */
    public function getLeftval()
    {

        return $this->leftval;
    }

    /**
     * Get the [rightval] column value.
     * 
     * @return   int
     */
    public function getRightval()
    {

        return $this->rightval;
    }

    /**
     * Get the [template] column value.
     * 
     * @return   string
     */
    public function getTemplate()
    {

        return $this->template;
    }

    /**
     * Get the [sectiontype] column value.
     * 
     * @return   string
     */
    public function getSectiontype()
    {

        return $this->sectiontype;
    }

    /**
     * Get the [sectiontypeid] column value.
     * 
     * @return   int
     */
    public function getSectiontypeid()
    {

        return $this->sectiontypeid;
    }

    /**
     * Get the [title] column value.
     * 
     * @return   string
     */
    public function getTitle()
    {

        return $this->title;
    }

    /**
     * Get the [encodedtitle] column value.
     * 
     * @return   string
     */
    public function getEncodedtitle()
    {

        return $this->encodedtitle;
    }

    /**
     * Get the [showinmenu] column value.
     * 
     * @return   int
     */
    public function getShowinmenu()
    {

        return $this->showinmenu;
    }

    /**
     * Get the [bannerid] column value.
     * 
     * @return   int
     */
    public function getBannerid()
    {

        return $this->bannerid;
    }

    /**
     * Get the [url] column value.
     * 
     * @return   string
     */
    public function getUrl()
    {

        return $this->url;
    }

    /**
     * Get the [target] column value.
     * 
     * @return   string
     */
    public function getTarget()
    {

        return $this->target;
    }

    /**
     * Get the [keywords] column value.
     * 
     * @return   string
     */
    public function getKeywords()
    {

        return $this->keywords;
    }

    /**
     * Get the [description] column value.
     * 
     * @return   string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     * Get the [active] column value.
     * 
     * @return   int
     */
    public function getActive()
    {

        return $this->active;
    }

    /**
     * Get the [rowmark] column value.
     * 
     * @return   int
     */
    public function getRowmark()
    {

        return $this->rowmark;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param      int $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = MenuPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [leftval] column.
     * 
     * @param      int $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setLeftval($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->leftval !== $v) {
            $this->leftval = $v;
            $this->modifiedColumns[] = MenuPeer::LEFTVAL;
        }


        return $this;
    } // setLeftval()

    /**
     * Set the value of [rightval] column.
     * 
     * @param      int $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setRightval($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->rightval !== $v) {
            $this->rightval = $v;
            $this->modifiedColumns[] = MenuPeer::RIGHTVAL;
        }


        return $this;
    } // setRightval()

    /**
     * Set the value of [template] column.
     * 
     * @param      string $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setTemplate($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->template !== $v) {
            $this->template = $v;
            $this->modifiedColumns[] = MenuPeer::TEMPLATE;
        }


        return $this;
    } // setTemplate()

    /**
     * Set the value of [sectiontype] column.
     * 
     * @param      string $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setSectiontype($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sectiontype !== $v) {
            $this->sectiontype = $v;
            $this->modifiedColumns[] = MenuPeer::SECTIONTYPE;
        }


        return $this;
    } // setSectiontype()

    /**
     * Set the value of [sectiontypeid] column.
     * 
     * @param      int $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setSectiontypeid($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sectiontypeid !== $v) {
            $this->sectiontypeid = $v;
            $this->modifiedColumns[] = MenuPeer::SECTIONTYPEID;
        }


        return $this;
    } // setSectiontypeid()

    /**
     * Set the value of [title] column.
     * 
     * @param      string $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[] = MenuPeer::TITLE;
        }


        return $this;
    } // setTitle()

    /**
     * Set the value of [encodedtitle] column.
     * 
     * @param      string $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setEncodedtitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->encodedtitle !== $v) {
            $this->encodedtitle = $v;
            $this->modifiedColumns[] = MenuPeer::ENCODEDTITLE;
        }


        return $this;
    } // setEncodedtitle()

    /**
     * Set the value of [showinmenu] column.
     * 
     * @param      int $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setShowinmenu($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->showinmenu !== $v) {
            $this->showinmenu = $v;
            $this->modifiedColumns[] = MenuPeer::SHOWINMENU;
        }


        return $this;
    } // setShowinmenu()

    /**
     * Set the value of [bannerid] column.
     * 
     * @param      int $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setBannerid($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->bannerid !== $v) {
            $this->bannerid = $v;
            $this->modifiedColumns[] = MenuPeer::BANNERID;
        }


        return $this;
    } // setBannerid()

    /**
     * Set the value of [url] column.
     * 
     * @param      string $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[] = MenuPeer::URL;
        }


        return $this;
    } // setUrl()

    /**
     * Set the value of [target] column.
     * 
     * @param      string $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setTarget($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->target !== $v) {
            $this->target = $v;
            $this->modifiedColumns[] = MenuPeer::TARGET;
        }


        return $this;
    } // setTarget()

    /**
     * Set the value of [keywords] column.
     * 
     * @param      string $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setKeywords($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->keywords !== $v) {
            $this->keywords = $v;
            $this->modifiedColumns[] = MenuPeer::KEYWORDS;
        }


        return $this;
    } // setKeywords()

    /**
     * Set the value of [description] column.
     * 
     * @param      string $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = MenuPeer::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Set the value of [active] column.
     * 
     * @param      int $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setActive($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->active !== $v) {
            $this->active = $v;
            $this->modifiedColumns[] = MenuPeer::ACTIVE;
        }


        return $this;
    } // setActive()

    /**
     * Set the value of [rowmark] column.
     * 
     * @param      int $v new value
     * @return   Menu The current object (for fluent API support)
     */
    public function setRowmark($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->rowmark !== $v) {
            $this->rowmark = $v;
            $this->modifiedColumns[] = MenuPeer::ROWMARK;
        }


        return $this;
    } // setRowmark()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->leftval !== 0) {
                return false;
            }

            if ($this->rightval !== 0) {
                return false;
            }

            if ($this->template !== 'secondary') {
                return false;
            }

            if ($this->sectiontype !== 'content') {
                return false;
            }

            if ($this->sectiontypeid !== 0) {
                return false;
            }

            if ($this->title !== '') {
                return false;
            }

            if ($this->encodedtitle !== '') {
                return false;
            }

            if ($this->showinmenu !== 1) {
                return false;
            }

            if ($this->bannerid !== 0) {
                return false;
            }

            if ($this->url !== '') {
                return false;
            }

            if ($this->target !== '') {
                return false;
            }

            if ($this->keywords !== '') {
                return false;
            }

            if ($this->description !== '') {
                return false;
            }

            if ($this->active !== 1) {
                return false;
            }

            if ($this->rowmark !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->leftval = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->rightval = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->template = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->sectiontype = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->sectiontypeid = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->title = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->encodedtitle = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->showinmenu = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->bannerid = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->url = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->target = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->keywords = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->description = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->active = ($row[$startcol + 14] !== null) ? (int) $row[$startcol + 14] : null;
            $this->rowmark = ($row[$startcol + 15] !== null) ? (int) $row[$startcol + 15] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 16; // 16 = MenuPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Menu object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(MenuPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = MenuPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(MenuPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = MenuQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(MenuPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                MenuPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = MenuPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . MenuPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(MenuPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(MenuPeer::LEFTVAL)) {
            $modifiedColumns[':p' . $index++]  = '`LEFTVAL`';
        }
        if ($this->isColumnModified(MenuPeer::RIGHTVAL)) {
            $modifiedColumns[':p' . $index++]  = '`RIGHTVAL`';
        }
        if ($this->isColumnModified(MenuPeer::TEMPLATE)) {
            $modifiedColumns[':p' . $index++]  = '`TEMPLATE`';
        }
        if ($this->isColumnModified(MenuPeer::SECTIONTYPE)) {
            $modifiedColumns[':p' . $index++]  = '`SECTIONTYPE`';
        }
        if ($this->isColumnModified(MenuPeer::SECTIONTYPEID)) {
            $modifiedColumns[':p' . $index++]  = '`SECTIONTYPEID`';
        }
        if ($this->isColumnModified(MenuPeer::TITLE)) {
            $modifiedColumns[':p' . $index++]  = '`TITLE`';
        }
        if ($this->isColumnModified(MenuPeer::ENCODEDTITLE)) {
            $modifiedColumns[':p' . $index++]  = '`ENCODEDTITLE`';
        }
        if ($this->isColumnModified(MenuPeer::SHOWINMENU)) {
            $modifiedColumns[':p' . $index++]  = '`SHOWINMENU`';
        }
        if ($this->isColumnModified(MenuPeer::BANNERID)) {
            $modifiedColumns[':p' . $index++]  = '`BANNERID`';
        }
        if ($this->isColumnModified(MenuPeer::URL)) {
            $modifiedColumns[':p' . $index++]  = '`URL`';
        }
        if ($this->isColumnModified(MenuPeer::TARGET)) {
            $modifiedColumns[':p' . $index++]  = '`TARGET`';
        }
        if ($this->isColumnModified(MenuPeer::KEYWORDS)) {
            $modifiedColumns[':p' . $index++]  = '`KEYWORDS`';
        }
        if ($this->isColumnModified(MenuPeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`DESCRIPTION`';
        }
        if ($this->isColumnModified(MenuPeer::ACTIVE)) {
            $modifiedColumns[':p' . $index++]  = '`ACTIVE`';
        }
        if ($this->isColumnModified(MenuPeer::ROWMARK)) {
            $modifiedColumns[':p' . $index++]  = '`ROWMARK`';
        }

        $sql = sprintf(
            'INSERT INTO `Menu` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':
						$stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`LEFTVAL`':
						$stmt->bindValue($identifier, $this->leftval, PDO::PARAM_INT);
                        break;
                    case '`RIGHTVAL`':
						$stmt->bindValue($identifier, $this->rightval, PDO::PARAM_INT);
                        break;
                    case '`TEMPLATE`':
						$stmt->bindValue($identifier, $this->template, PDO::PARAM_STR);
                        break;
                    case '`SECTIONTYPE`':
						$stmt->bindValue($identifier, $this->sectiontype, PDO::PARAM_STR);
                        break;
                    case '`SECTIONTYPEID`':
						$stmt->bindValue($identifier, $this->sectiontypeid, PDO::PARAM_INT);
                        break;
                    case '`TITLE`':
						$stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case '`ENCODEDTITLE`':
						$stmt->bindValue($identifier, $this->encodedtitle, PDO::PARAM_STR);
                        break;
                    case '`SHOWINMENU`':
						$stmt->bindValue($identifier, $this->showinmenu, PDO::PARAM_INT);
                        break;
                    case '`BANNERID`':
						$stmt->bindValue($identifier, $this->bannerid, PDO::PARAM_INT);
                        break;
                    case '`URL`':
						$stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                    case '`TARGET`':
						$stmt->bindValue($identifier, $this->target, PDO::PARAM_STR);
                        break;
                    case '`KEYWORDS`':
						$stmt->bindValue($identifier, $this->keywords, PDO::PARAM_STR);
                        break;
                    case '`DESCRIPTION`':
						$stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case '`ACTIVE`':
						$stmt->bindValue($identifier, $this->active, PDO::PARAM_INT);
                        break;
                    case '`ROWMARK`':
						$stmt->bindValue($identifier, $this->rowmark, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
			$pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param      mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        } else {
            $this->validationFailures = $res;

            return false;
        }
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param      array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = MenuPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = MenuPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getLeftval();
                break;
            case 2:
                return $this->getRightval();
                break;
            case 3:
                return $this->getTemplate();
                break;
            case 4:
                return $this->getSectiontype();
                break;
            case 5:
                return $this->getSectiontypeid();
                break;
            case 6:
                return $this->getTitle();
                break;
            case 7:
                return $this->getEncodedtitle();
                break;
            case 8:
                return $this->getShowinmenu();
                break;
            case 9:
                return $this->getBannerid();
                break;
            case 10:
                return $this->getUrl();
                break;
            case 11:
                return $this->getTarget();
                break;
            case 12:
                return $this->getKeywords();
                break;
            case 13:
                return $this->getDescription();
                break;
            case 14:
                return $this->getActive();
                break;
            case 15:
                return $this->getRowmark();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['Menu'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Menu'][$this->getPrimaryKey()] = true;
        $keys = MenuPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getLeftval(),
            $keys[2] => $this->getRightval(),
            $keys[3] => $this->getTemplate(),
            $keys[4] => $this->getSectiontype(),
            $keys[5] => $this->getSectiontypeid(),
            $keys[6] => $this->getTitle(),
            $keys[7] => $this->getEncodedtitle(),
            $keys[8] => $this->getShowinmenu(),
            $keys[9] => $this->getBannerid(),
            $keys[10] => $this->getUrl(),
            $keys[11] => $this->getTarget(),
            $keys[12] => $this->getKeywords(),
            $keys[13] => $this->getDescription(),
            $keys[14] => $this->getActive(),
            $keys[15] => $this->getRowmark(),
        );

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param      string $name peer name
     * @param      mixed $value field value
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = MenuPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @param      mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setLeftval($value);
                break;
            case 2:
                $this->setRightval($value);
                break;
            case 3:
                $this->setTemplate($value);
                break;
            case 4:
                $this->setSectiontype($value);
                break;
            case 5:
                $this->setSectiontypeid($value);
                break;
            case 6:
                $this->setTitle($value);
                break;
            case 7:
                $this->setEncodedtitle($value);
                break;
            case 8:
                $this->setShowinmenu($value);
                break;
            case 9:
                $this->setBannerid($value);
                break;
            case 10:
                $this->setUrl($value);
                break;
            case 11:
                $this->setTarget($value);
                break;
            case 12:
                $this->setKeywords($value);
                break;
            case 13:
                $this->setDescription($value);
                break;
            case 14:
                $this->setActive($value);
                break;
            case 15:
                $this->setRowmark($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = MenuPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setLeftval($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setRightval($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setTemplate($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setSectiontype($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setSectiontypeid($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setTitle($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setEncodedtitle($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setShowinmenu($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setBannerid($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setUrl($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setTarget($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setKeywords($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setDescription($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setActive($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setRowmark($arr[$keys[15]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(MenuPeer::DATABASE_NAME);

        if ($this->isColumnModified(MenuPeer::ID)) $criteria->add(MenuPeer::ID, $this->id);
        if ($this->isColumnModified(MenuPeer::LEFTVAL)) $criteria->add(MenuPeer::LEFTVAL, $this->leftval);
        if ($this->isColumnModified(MenuPeer::RIGHTVAL)) $criteria->add(MenuPeer::RIGHTVAL, $this->rightval);
        if ($this->isColumnModified(MenuPeer::TEMPLATE)) $criteria->add(MenuPeer::TEMPLATE, $this->template);
        if ($this->isColumnModified(MenuPeer::SECTIONTYPE)) $criteria->add(MenuPeer::SECTIONTYPE, $this->sectiontype);
        if ($this->isColumnModified(MenuPeer::SECTIONTYPEID)) $criteria->add(MenuPeer::SECTIONTYPEID, $this->sectiontypeid);
        if ($this->isColumnModified(MenuPeer::TITLE)) $criteria->add(MenuPeer::TITLE, $this->title);
        if ($this->isColumnModified(MenuPeer::ENCODEDTITLE)) $criteria->add(MenuPeer::ENCODEDTITLE, $this->encodedtitle);
        if ($this->isColumnModified(MenuPeer::SHOWINMENU)) $criteria->add(MenuPeer::SHOWINMENU, $this->showinmenu);
        if ($this->isColumnModified(MenuPeer::BANNERID)) $criteria->add(MenuPeer::BANNERID, $this->bannerid);
        if ($this->isColumnModified(MenuPeer::URL)) $criteria->add(MenuPeer::URL, $this->url);
        if ($this->isColumnModified(MenuPeer::TARGET)) $criteria->add(MenuPeer::TARGET, $this->target);
        if ($this->isColumnModified(MenuPeer::KEYWORDS)) $criteria->add(MenuPeer::KEYWORDS, $this->keywords);
        if ($this->isColumnModified(MenuPeer::DESCRIPTION)) $criteria->add(MenuPeer::DESCRIPTION, $this->description);
        if ($this->isColumnModified(MenuPeer::ACTIVE)) $criteria->add(MenuPeer::ACTIVE, $this->active);
        if ($this->isColumnModified(MenuPeer::ROWMARK)) $criteria->add(MenuPeer::ROWMARK, $this->rowmark);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(MenuPeer::DATABASE_NAME);
        $criteria->add(MenuPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return   int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of Menu (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setLeftval($this->getLeftval());
        $copyObj->setRightval($this->getRightval());
        $copyObj->setTemplate($this->getTemplate());
        $copyObj->setSectiontype($this->getSectiontype());
        $copyObj->setSectiontypeid($this->getSectiontypeid());
        $copyObj->setTitle($this->getTitle());
        $copyObj->setEncodedtitle($this->getEncodedtitle());
        $copyObj->setShowinmenu($this->getShowinmenu());
        $copyObj->setBannerid($this->getBannerid());
        $copyObj->setUrl($this->getUrl());
        $copyObj->setTarget($this->getTarget());
        $copyObj->setKeywords($this->getKeywords());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setActive($this->getActive());
        $copyObj->setRowmark($this->getRowmark());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return                 Menu Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return   MenuPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new MenuPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->leftval = null;
        $this->rightval = null;
        $this->template = null;
        $this->sectiontype = null;
        $this->sectiontypeid = null;
        $this->title = null;
        $this->encodedtitle = null;
        $this->showinmenu = null;
        $this->bannerid = null;
        $this->url = null;
        $this->target = null;
        $this->keywords = null;
        $this->description = null;
        $this->active = null;
        $this->rowmark = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(MenuPeer::DEFAULT_STRING_FORMAT);
    }

} // BaseMenu
