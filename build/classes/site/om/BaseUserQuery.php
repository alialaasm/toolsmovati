<?php


/**
 * Base class that represents a query for the 'User' table.
 *
 * 
 *
 * @method     UserQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method     UserQuery orderByUsername($order = Criteria::ASC) Order by the Username column
 * @method     UserQuery orderByName($order = Criteria::ASC) Order by the Name column
 * @method     UserQuery orderByPassword($order = Criteria::ASC) Order by the Password column
 * @method     UserQuery orderByActive($order = Criteria::ASC) Order by the Active column
 * @method     UserQuery orderBySuperuser($order = Criteria::ASC) Order by the SuperUser column
 * @method     UserQuery orderByUserlevel($order = Criteria::ASC) Order by the UserLevel column
 * @method     UserQuery orderByClubid($order = Criteria::ASC) Order by the ClubId column
 *
 * @method     UserQuery groupById() Group by the ID column
 * @method     UserQuery groupByUsername() Group by the Username column
 * @method     UserQuery groupByName() Group by the Name column
 * @method     UserQuery groupByPassword() Group by the Password column
 * @method     UserQuery groupByActive() Group by the Active column
 * @method     UserQuery groupBySuperuser() Group by the SuperUser column
 * @method     UserQuery groupByUserlevel() Group by the UserLevel column
 * @method     UserQuery groupByClubid() Group by the ClubId column
 *
 * @method     UserQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     UserQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     UserQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     User findOne(PropelPDO $con = null) Return the first User matching the query
 * @method     User findOneOrCreate(PropelPDO $con = null) Return the first User matching the query, or a new User object populated from the query conditions when no match is found
 *
 * @method     User findOneById(int $ID) Return the first User filtered by the ID column
 * @method     User findOneByUsername(string $Username) Return the first User filtered by the Username column
 * @method     User findOneByName(string $Name) Return the first User filtered by the Name column
 * @method     User findOneByPassword(string $Password) Return the first User filtered by the Password column
 * @method     User findOneByActive(boolean $Active) Return the first User filtered by the Active column
 * @method     User findOneBySuperuser(int $SuperUser) Return the first User filtered by the SuperUser column
 * @method     User findOneByUserlevel(string $UserLevel) Return the first User filtered by the UserLevel column
 * @method     User findOneByClubid(int $ClubId) Return the first User filtered by the ClubId column
 *
 * @method     array findById(int $ID) Return User objects filtered by the ID column
 * @method     array findByUsername(string $Username) Return User objects filtered by the Username column
 * @method     array findByName(string $Name) Return User objects filtered by the Name column
 * @method     array findByPassword(string $Password) Return User objects filtered by the Password column
 * @method     array findByActive(boolean $Active) Return User objects filtered by the Active column
 * @method     array findBySuperuser(int $SuperUser) Return User objects filtered by the SuperUser column
 * @method     array findByUserlevel(string $UserLevel) Return User objects filtered by the UserLevel column
 * @method     array findByClubid(int $ClubId) Return User objects filtered by the ClubId column
 *
 * @package    propel.generator.site.om
 */
abstract class BaseUserQuery extends ModelCriteria
{
    
    /**
     * Initializes internal state of BaseUserQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'site', $modelName = 'User', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new UserQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     UserQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return UserQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof UserQuery) {
            return $criteria;
        }
        $query = new UserQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   User|User[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = UserPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(UserPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   User A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `USERNAME`, `NAME`, `PASSWORD`, `ACTIVE`, `SUPERUSER`, `USERLEVEL`, `CLUBID` FROM `User` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new User();
            $obj->hydrate($row);
            UserPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return User|User[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|User[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(UserPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the Username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE Username = 'fooValue'
     * $query->filterByUsername('%fooValue%'); // WHERE Username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $username)) {
                $username = str_replace('*', '%', $username);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the Name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE Name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE Name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the Password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE Password = 'fooValue'
     * $query->filterByPassword('%fooValue%'); // WHERE Password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $password)) {
                $password = str_replace('*', '%', $password);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the Active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(true); // WHERE Active = true
     * $query->filterByActive('yes'); // WHERE Active = true
     * </code>
     *
     * @param     boolean|string $active The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (is_string($active)) {
            $Active = in_array(strtolower($active), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserPeer::ACTIVE, $active, $comparison);
    }

    /**
     * Filter the query on the SuperUser column
     *
     * Example usage:
     * <code>
     * $query->filterBySuperuser(1234); // WHERE SuperUser = 1234
     * $query->filterBySuperuser(array(12, 34)); // WHERE SuperUser IN (12, 34)
     * $query->filterBySuperuser(array('min' => 12)); // WHERE SuperUser > 12
     * </code>
     *
     * @param     mixed $superuser The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterBySuperuser($superuser = null, $comparison = null)
    {
        if (is_array($superuser)) {
            $useMinMax = false;
            if (isset($superuser['min'])) {
                $this->addUsingAlias(UserPeer::SUPERUSER, $superuser['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($superuser['max'])) {
                $this->addUsingAlias(UserPeer::SUPERUSER, $superuser['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserPeer::SUPERUSER, $superuser, $comparison);
    }

    /**
     * Filter the query on the UserLevel column
     *
     * Example usage:
     * <code>
     * $query->filterByUserlevel('fooValue');   // WHERE UserLevel = 'fooValue'
     * $query->filterByUserlevel('%fooValue%'); // WHERE UserLevel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $userlevel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByUserlevel($userlevel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($userlevel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $userlevel)) {
                $userlevel = str_replace('*', '%', $userlevel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::USERLEVEL, $userlevel, $comparison);
    }

    /**
     * Filter the query on the ClubId column
     *
     * Example usage:
     * <code>
     * $query->filterByClubid(1234); // WHERE ClubId = 1234
     * $query->filterByClubid(array(12, 34)); // WHERE ClubId IN (12, 34)
     * $query->filterByClubid(array('min' => 12)); // WHERE ClubId > 12
     * </code>
     *
     * @param     mixed $clubid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByClubid($clubid = null, $comparison = null)
    {
        if (is_array($clubid)) {
            $useMinMax = false;
            if (isset($clubid['min'])) {
                $this->addUsingAlias(UserPeer::CLUBID, $clubid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clubid['max'])) {
                $this->addUsingAlias(UserPeer::CLUBID, $clubid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserPeer::CLUBID, $clubid, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   User $user Object to remove from the list of results
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function prune($user = null)
    {
        if ($user) {
            $this->addUsingAlias(UserPeer::ID, $user->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

} // BaseUserQuery