<?php


/**
 * Base class that represents a query for the 'ClassResult' table.
 *
 * 
 *
 * @method     ClassresultQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method     ClassresultQuery orderByClassid($order = Criteria::ASC) Order by the ClassID column
 * @method     ClassresultQuery orderByInstructorid($order = Criteria::ASC) Order by the InstructorID column
 * @method     ClassresultQuery orderBySubstituteforinstructorid($order = Criteria::ASC) Order by the SubstituteForInstructorID column
 * @method     ClassresultQuery orderByDate($order = Criteria::ASC) Order by the Date column
 * @method     ClassresultQuery orderByNotes($order = Criteria::ASC) Order by the Notes column
 * @method     ClassresultQuery orderByClubid($order = Criteria::ASC) Order by the ClubID column
 * @method     ClassresultQuery orderByParticipants($order = Criteria::ASC) Order by the Participants column
 * @method     ClassresultQuery orderByLastUpdated($order = Criteria::ASC) Order by the last_updated column
 * @method     ClassresultQuery orderByHours($order = Criteria::ASC) Order by the Hours column
 * @method     ClassresultQuery orderByDateEntered($order = Criteria::ASC) Order by the date_entered column
 * @method     ClassresultQuery orderByStarttime($order = Criteria::ASC) Order by the StartTime column
 *
 * @method     ClassresultQuery groupById() Group by the ID column
 * @method     ClassresultQuery groupByClassid() Group by the ClassID column
 * @method     ClassresultQuery groupByInstructorid() Group by the InstructorID column
 * @method     ClassresultQuery groupBySubstituteforinstructorid() Group by the SubstituteForInstructorID column
 * @method     ClassresultQuery groupByDate() Group by the Date column
 * @method     ClassresultQuery groupByNotes() Group by the Notes column
 * @method     ClassresultQuery groupByClubid() Group by the ClubID column
 * @method     ClassresultQuery groupByParticipants() Group by the Participants column
 * @method     ClassresultQuery groupByLastUpdated() Group by the last_updated column
 * @method     ClassresultQuery groupByHours() Group by the Hours column
 * @method     ClassresultQuery groupByDateEntered() Group by the date_entered column
 * @method     ClassresultQuery groupByStarttime() Group by the StartTime column
 *
 * @method     ClassresultQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ClassresultQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ClassresultQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ClassresultQuery leftJoin_Class($relationAlias = null) Adds a LEFT JOIN clause to the query using the _Class relation
 * @method     ClassresultQuery rightJoin_Class($relationAlias = null) Adds a RIGHT JOIN clause to the query using the _Class relation
 * @method     ClassresultQuery innerJoin_Class($relationAlias = null) Adds a INNER JOIN clause to the query using the _Class relation
 *
 * @method     ClassresultQuery leftJoinClub($relationAlias = null) Adds a LEFT JOIN clause to the query using the Club relation
 * @method     ClassresultQuery rightJoinClub($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Club relation
 * @method     ClassresultQuery innerJoinClub($relationAlias = null) Adds a INNER JOIN clause to the query using the Club relation
 *
 * @method     ClassresultQuery leftJoinInstructor($relationAlias = null) Adds a LEFT JOIN clause to the query using the Instructor relation
 * @method     ClassresultQuery rightJoinInstructor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Instructor relation
 * @method     ClassresultQuery innerJoinInstructor($relationAlias = null) Adds a INNER JOIN clause to the query using the Instructor relation
 *
 * @method     Classresult findOne(PropelPDO $con = null) Return the first Classresult matching the query
 * @method     Classresult findOneOrCreate(PropelPDO $con = null) Return the first Classresult matching the query, or a new Classresult object populated from the query conditions when no match is found
 *
 * @method     Classresult findOneById(int $ID) Return the first Classresult filtered by the ID column
 * @method     Classresult findOneByClassid(int $ClassID) Return the first Classresult filtered by the ClassID column
 * @method     Classresult findOneByInstructorid(int $InstructorID) Return the first Classresult filtered by the InstructorID column
 * @method     Classresult findOneBySubstituteforinstructorid(int $SubstituteForInstructorID) Return the first Classresult filtered by the SubstituteForInstructorID column
 * @method     Classresult findOneByDate(string $Date) Return the first Classresult filtered by the Date column
 * @method     Classresult findOneByNotes(string $Notes) Return the first Classresult filtered by the Notes column
 * @method     Classresult findOneByClubid(int $ClubID) Return the first Classresult filtered by the ClubID column
 * @method     Classresult findOneByParticipants(int $Participants) Return the first Classresult filtered by the Participants column
 * @method     Classresult findOneByLastUpdated(string $last_updated) Return the first Classresult filtered by the last_updated column
 * @method     Classresult findOneByHours(string $Hours) Return the first Classresult filtered by the Hours column
 * @method     Classresult findOneByDateEntered(string $date_entered) Return the first Classresult filtered by the date_entered column
 * @method     Classresult findOneByStarttime(string $StartTime) Return the first Classresult filtered by the StartTime column
 *
 * @method     array findById(int $ID) Return Classresult objects filtered by the ID column
 * @method     array findByClassid(int $ClassID) Return Classresult objects filtered by the ClassID column
 * @method     array findByInstructorid(int $InstructorID) Return Classresult objects filtered by the InstructorID column
 * @method     array findBySubstituteforinstructorid(int $SubstituteForInstructorID) Return Classresult objects filtered by the SubstituteForInstructorID column
 * @method     array findByDate(string $Date) Return Classresult objects filtered by the Date column
 * @method     array findByNotes(string $Notes) Return Classresult objects filtered by the Notes column
 * @method     array findByClubid(int $ClubID) Return Classresult objects filtered by the ClubID column
 * @method     array findByParticipants(int $Participants) Return Classresult objects filtered by the Participants column
 * @method     array findByLastUpdated(string $last_updated) Return Classresult objects filtered by the last_updated column
 * @method     array findByHours(string $Hours) Return Classresult objects filtered by the Hours column
 * @method     array findByDateEntered(string $date_entered) Return Classresult objects filtered by the date_entered column
 * @method     array findByStarttime(string $StartTime) Return Classresult objects filtered by the StartTime column
 *
 * @package    propel.generator.site.om
 */
abstract class BaseClassresultQuery extends ModelCriteria
{
    
    /**
     * Initializes internal state of BaseClassresultQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'site', $modelName = 'Classresult', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ClassresultQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     ClassresultQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ClassresultQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ClassresultQuery) {
            return $criteria;
        }
        $query = new ClassresultQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Classresult|Classresult[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ClassresultPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ClassresultPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   Classresult A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `CLASSID`, `INSTRUCTORID`, `SUBSTITUTEFORINSTRUCTORID`, `DATE`, `NOTES`, `CLUBID`, `PARTICIPANTS`, `LAST_UPDATED`, `HOURS`, `DATE_ENTERED`, `STARTTIME` FROM `ClassResult` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Classresult();
            $obj->hydrate($row);
            ClassresultPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Classresult|Classresult[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Classresult[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ClassresultPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ClassresultPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(ClassresultPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the ClassID column
     *
     * Example usage:
     * <code>
     * $query->filterByClassid(1234); // WHERE ClassID = 1234
     * $query->filterByClassid(array(12, 34)); // WHERE ClassID IN (12, 34)
     * $query->filterByClassid(array('min' => 12)); // WHERE ClassID > 12
     * </code>
     *
     * @see       filterBy_Class()
     *
     * @param     mixed $classid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function filterByClassid($classid = null, $comparison = null)
    {
        if (is_array($classid)) {
            $useMinMax = false;
            if (isset($classid['min'])) {
                $this->addUsingAlias(ClassresultPeer::CLASSID, $classid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($classid['max'])) {
                $this->addUsingAlias(ClassresultPeer::CLASSID, $classid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassresultPeer::CLASSID, $classid, $comparison);
    }

    /**
     * Filter the query on the InstructorID column
     *
     * Example usage:
     * <code>
     * $query->filterByInstructorid(1234); // WHERE InstructorID = 1234
     * $query->filterByInstructorid(array(12, 34)); // WHERE InstructorID IN (12, 34)
     * $query->filterByInstructorid(array('min' => 12)); // WHERE InstructorID > 12
     * </code>
     *
     * @see       filterByInstructor()
     *
     * @param     mixed $instructorid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function filterByInstructorid($instructorid = null, $comparison = null)
    {
        if (is_array($instructorid)) {
            $useMinMax = false;
            if (isset($instructorid['min'])) {
                $this->addUsingAlias(ClassresultPeer::INSTRUCTORID, $instructorid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($instructorid['max'])) {
                $this->addUsingAlias(ClassresultPeer::INSTRUCTORID, $instructorid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassresultPeer::INSTRUCTORID, $instructorid, $comparison);
    }

    /**
     * Filter the query on the SubstituteForInstructorID column
     *
     * Example usage:
     * <code>
     * $query->filterBySubstituteforinstructorid(1234); // WHERE SubstituteForInstructorID = 1234
     * $query->filterBySubstituteforinstructorid(array(12, 34)); // WHERE SubstituteForInstructorID IN (12, 34)
     * $query->filterBySubstituteforinstructorid(array('min' => 12)); // WHERE SubstituteForInstructorID > 12
     * </code>
     *
     * @param     mixed $substituteforinstructorid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function filterBySubstituteforinstructorid($substituteforinstructorid = null, $comparison = null)
    {
        if (is_array($substituteforinstructorid)) {
            $useMinMax = false;
            if (isset($substituteforinstructorid['min'])) {
                $this->addUsingAlias(ClassresultPeer::SUBSTITUTEFORINSTRUCTORID, $substituteforinstructorid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($substituteforinstructorid['max'])) {
                $this->addUsingAlias(ClassresultPeer::SUBSTITUTEFORINSTRUCTORID, $substituteforinstructorid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassresultPeer::SUBSTITUTEFORINSTRUCTORID, $substituteforinstructorid, $comparison);
    }

    /**
     * Filter the query on the Date column
     *
     * Example usage:
     * <code>
     * $query->filterByDate('2011-03-14'); // WHERE Date = '2011-03-14'
     * $query->filterByDate('now'); // WHERE Date = '2011-03-14'
     * $query->filterByDate(array('max' => 'yesterday')); // WHERE Date > '2011-03-13'
     * </code>
     *
     * @param     mixed $date The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function filterByDate($date = null, $comparison = null)
    {
        if (is_array($date)) {
            $useMinMax = false;
            if (isset($date['min'])) {
                $this->addUsingAlias(ClassresultPeer::DATE, $date['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($date['max'])) {
                $this->addUsingAlias(ClassresultPeer::DATE, $date['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassresultPeer::DATE, $date, $comparison);
    }

    /**
     * Filter the query on the Notes column
     *
     * Example usage:
     * <code>
     * $query->filterByNotes('fooValue');   // WHERE Notes = 'fooValue'
     * $query->filterByNotes('%fooValue%'); // WHERE Notes LIKE '%fooValue%'
     * </code>
     *
     * @param     string $notes The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function filterByNotes($notes = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($notes)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $notes)) {
                $notes = str_replace('*', '%', $notes);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClassresultPeer::NOTES, $notes, $comparison);
    }

    /**
     * Filter the query on the ClubID column
     *
     * Example usage:
     * <code>
     * $query->filterByClubid(1234); // WHERE ClubID = 1234
     * $query->filterByClubid(array(12, 34)); // WHERE ClubID IN (12, 34)
     * $query->filterByClubid(array('min' => 12)); // WHERE ClubID > 12
     * </code>
     *
     * @see       filterByClub()
     *
     * @param     mixed $clubid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function filterByClubid($clubid = null, $comparison = null)
    {
        if (is_array($clubid)) {
            $useMinMax = false;
            if (isset($clubid['min'])) {
                $this->addUsingAlias(ClassresultPeer::CLUBID, $clubid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clubid['max'])) {
                $this->addUsingAlias(ClassresultPeer::CLUBID, $clubid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassresultPeer::CLUBID, $clubid, $comparison);
    }

    /**
     * Filter the query on the Participants column
     *
     * Example usage:
     * <code>
     * $query->filterByParticipants(1234); // WHERE Participants = 1234
     * $query->filterByParticipants(array(12, 34)); // WHERE Participants IN (12, 34)
     * $query->filterByParticipants(array('min' => 12)); // WHERE Participants > 12
     * </code>
     *
     * @param     mixed $participants The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function filterByParticipants($participants = null, $comparison = null)
    {
        if (is_array($participants)) {
            $useMinMax = false;
            if (isset($participants['min'])) {
                $this->addUsingAlias(ClassresultPeer::PARTICIPANTS, $participants['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($participants['max'])) {
                $this->addUsingAlias(ClassresultPeer::PARTICIPANTS, $participants['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassresultPeer::PARTICIPANTS, $participants, $comparison);
    }

    /**
     * Filter the query on the last_updated column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdated('2011-03-14'); // WHERE last_updated = '2011-03-14'
     * $query->filterByLastUpdated('now'); // WHERE last_updated = '2011-03-14'
     * $query->filterByLastUpdated(array('max' => 'yesterday')); // WHERE last_updated > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdated The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function filterByLastUpdated($lastUpdated = null, $comparison = null)
    {
        if (is_array($lastUpdated)) {
            $useMinMax = false;
            if (isset($lastUpdated['min'])) {
                $this->addUsingAlias(ClassresultPeer::LAST_UPDATED, $lastUpdated['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdated['max'])) {
                $this->addUsingAlias(ClassresultPeer::LAST_UPDATED, $lastUpdated['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassresultPeer::LAST_UPDATED, $lastUpdated, $comparison);
    }

    /**
     * Filter the query on the Hours column
     *
     * Example usage:
     * <code>
     * $query->filterByHours(1234); // WHERE Hours = 1234
     * $query->filterByHours(array(12, 34)); // WHERE Hours IN (12, 34)
     * $query->filterByHours(array('min' => 12)); // WHERE Hours > 12
     * </code>
     *
     * @param     mixed $hours The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function filterByHours($hours = null, $comparison = null)
    {
        if (is_array($hours)) {
            $useMinMax = false;
            if (isset($hours['min'])) {
                $this->addUsingAlias(ClassresultPeer::HOURS, $hours['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($hours['max'])) {
                $this->addUsingAlias(ClassresultPeer::HOURS, $hours['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassresultPeer::HOURS, $hours, $comparison);
    }

    /**
     * Filter the query on the date_entered column
     *
     * Example usage:
     * <code>
     * $query->filterByDateEntered('2011-03-14'); // WHERE date_entered = '2011-03-14'
     * $query->filterByDateEntered('now'); // WHERE date_entered = '2011-03-14'
     * $query->filterByDateEntered(array('max' => 'yesterday')); // WHERE date_entered > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateEntered The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function filterByDateEntered($dateEntered = null, $comparison = null)
    {
        if (is_array($dateEntered)) {
            $useMinMax = false;
            if (isset($dateEntered['min'])) {
                $this->addUsingAlias(ClassresultPeer::DATE_ENTERED, $dateEntered['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateEntered['max'])) {
                $this->addUsingAlias(ClassresultPeer::DATE_ENTERED, $dateEntered['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassresultPeer::DATE_ENTERED, $dateEntered, $comparison);
    }

    /**
     * Filter the query on the StartTime column
     *
     * Example usage:
     * <code>
     * $query->filterByStarttime('fooValue');   // WHERE StartTime = 'fooValue'
     * $query->filterByStarttime('%fooValue%'); // WHERE StartTime LIKE '%fooValue%'
     * </code>
     *
     * @param     string $starttime The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function filterByStarttime($starttime = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($starttime)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $starttime)) {
                $starttime = str_replace('*', '%', $starttime);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClassresultPeer::STARTTIME, $starttime, $comparison);
    }

    /**
     * Filter the query by a related _Class object
     *
     * @param   _Class|PropelObjectCollection $_Class The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   ClassresultQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBy_Class($_Class, $comparison = null)
    {
        if ($_Class instanceof _Class) {
            return $this
                ->addUsingAlias(ClassresultPeer::CLASSID, $_Class->getId(), $comparison);
        } elseif ($_Class instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ClassresultPeer::CLASSID, $_Class->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBy_Class() only accepts arguments of type _Class or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the _Class relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function join_Class($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('_Class');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, '_Class');
        }

        return $this;
    }

    /**
     * Use the _Class relation _Class object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   _ClassQuery A secondary query class using the current class as primary query
     */
    public function use_ClassQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->join_Class($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : '_Class', '_ClassQuery');
    }

    /**
     * Filter the query by a related Club object
     *
     * @param   Club|PropelObjectCollection $club The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   ClassresultQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByClub($club, $comparison = null)
    {
        if ($club instanceof Club) {
            return $this
                ->addUsingAlias(ClassresultPeer::CLUBID, $club->getId(), $comparison);
        } elseif ($club instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ClassresultPeer::CLUBID, $club->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByClub() only accepts arguments of type Club or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Club relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function joinClub($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Club');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Club');
        }

        return $this;
    }

    /**
     * Use the Club relation Club object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ClubQuery A secondary query class using the current class as primary query
     */
    public function useClubQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinClub($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Club', 'ClubQuery');
    }

    /**
     * Filter the query by a related Instructor object
     *
     * @param   Instructor|PropelObjectCollection $instructor The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   ClassresultQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByInstructor($instructor, $comparison = null)
    {
        if ($instructor instanceof Instructor) {
            return $this
                ->addUsingAlias(ClassresultPeer::INSTRUCTORID, $instructor->getId(), $comparison);
        } elseif ($instructor instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ClassresultPeer::INSTRUCTORID, $instructor->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByInstructor() only accepts arguments of type Instructor or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Instructor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function joinInstructor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Instructor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Instructor');
        }

        return $this;
    }

    /**
     * Use the Instructor relation Instructor object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   InstructorQuery A secondary query class using the current class as primary query
     */
    public function useInstructorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinInstructor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Instructor', 'InstructorQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Classresult $classresult Object to remove from the list of results
     *
     * @return ClassresultQuery The current query, for fluid interface
     */
    public function prune($classresult = null)
    {
        if ($classresult) {
            $this->addUsingAlias(ClassresultPeer::ID, $classresult->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

} // BaseClassresultQuery