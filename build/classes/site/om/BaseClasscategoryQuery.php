<?php


/**
 * Base class that represents a query for the 'ClassCategory' table.
 *
 * 
 *
 * @method     ClasscategoryQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method     ClasscategoryQuery orderByCategoryname($order = Criteria::ASC) Order by the CategoryName column
 * @method     ClasscategoryQuery orderByColour($order = Criteria::ASC) Order by the Colour column
 *
 * @method     ClasscategoryQuery groupById() Group by the ID column
 * @method     ClasscategoryQuery groupByCategoryname() Group by the CategoryName column
 * @method     ClasscategoryQuery groupByColour() Group by the Colour column
 *
 * @method     ClasscategoryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ClasscategoryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ClasscategoryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     Classcategory findOne(PropelPDO $con = null) Return the first Classcategory matching the query
 * @method     Classcategory findOneOrCreate(PropelPDO $con = null) Return the first Classcategory matching the query, or a new Classcategory object populated from the query conditions when no match is found
 *
 * @method     Classcategory findOneById(int $ID) Return the first Classcategory filtered by the ID column
 * @method     Classcategory findOneByCategoryname(string $CategoryName) Return the first Classcategory filtered by the CategoryName column
 * @method     Classcategory findOneByColour(int $Colour) Return the first Classcategory filtered by the Colour column
 *
 * @method     array findById(int $ID) Return Classcategory objects filtered by the ID column
 * @method     array findByCategoryname(string $CategoryName) Return Classcategory objects filtered by the CategoryName column
 * @method     array findByColour(int $Colour) Return Classcategory objects filtered by the Colour column
 *
 * @package    propel.generator.site.om
 */
abstract class BaseClasscategoryQuery extends ModelCriteria
{
    
    /**
     * Initializes internal state of BaseClasscategoryQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'site', $modelName = 'Classcategory', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ClasscategoryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     ClasscategoryQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ClasscategoryQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ClasscategoryQuery) {
            return $criteria;
        }
        $query = new ClasscategoryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Classcategory|Classcategory[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ClasscategoryPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ClasscategoryPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   Classcategory A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `CATEGORYNAME`, `COLOUR` FROM `ClassCategory` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Classcategory();
            $obj->hydrate($row);
            ClasscategoryPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Classcategory|Classcategory[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Classcategory[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ClasscategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ClasscategoryPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ClasscategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ClasscategoryPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClasscategoryQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(ClasscategoryPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the CategoryName column
     *
     * Example usage:
     * <code>
     * $query->filterByCategoryname('fooValue');   // WHERE CategoryName = 'fooValue'
     * $query->filterByCategoryname('%fooValue%'); // WHERE CategoryName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $categoryname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClasscategoryQuery The current query, for fluid interface
     */
    public function filterByCategoryname($categoryname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($categoryname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $categoryname)) {
                $categoryname = str_replace('*', '%', $categoryname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ClasscategoryPeer::CATEGORYNAME, $categoryname, $comparison);
    }

    /**
     * Filter the query on the Colour column
     *
     * Example usage:
     * <code>
     * $query->filterByColour(1234); // WHERE Colour = 1234
     * $query->filterByColour(array(12, 34)); // WHERE Colour IN (12, 34)
     * $query->filterByColour(array('min' => 12)); // WHERE Colour > 12
     * </code>
     *
     * @param     mixed $colour The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClasscategoryQuery The current query, for fluid interface
     */
    public function filterByColour($colour = null, $comparison = null)
    {
        if (is_array($colour)) {
            $useMinMax = false;
            if (isset($colour['min'])) {
                $this->addUsingAlias(ClasscategoryPeer::COLOUR, $colour['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($colour['max'])) {
                $this->addUsingAlias(ClasscategoryPeer::COLOUR, $colour['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClasscategoryPeer::COLOUR, $colour, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Classcategory $classcategory Object to remove from the list of results
     *
     * @return ClasscategoryQuery The current query, for fluid interface
     */
    public function prune($classcategory = null)
    {
        if ($classcategory) {
            $this->addUsingAlias(ClasscategoryPeer::ID, $classcategory->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

} // BaseClasscategoryQuery