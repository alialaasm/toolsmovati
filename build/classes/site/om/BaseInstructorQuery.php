<?php


/**
 * Base class that represents a query for the 'Instructor' table.
 *
 * 
 *
 * @method     InstructorQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method     InstructorQuery orderByEmail($order = Criteria::ASC) Order by the Email column
 * @method     InstructorQuery orderByPassword($order = Criteria::ASC) Order by the Password column
 * @method     InstructorQuery orderByFirstname($order = Criteria::ASC) Order by the FirstName column
 * @method     InstructorQuery orderByLastname($order = Criteria::ASC) Order by the LastName column
 * @method     InstructorQuery orderByClubid($order = Criteria::ASC) Order by the ClubId column
 * @method     InstructorQuery orderByIsAdmin($order = Criteria::ASC) Order by the is_admin column
 *
 * @method     InstructorQuery groupById() Group by the ID column
 * @method     InstructorQuery groupByEmail() Group by the Email column
 * @method     InstructorQuery groupByPassword() Group by the Password column
 * @method     InstructorQuery groupByFirstname() Group by the FirstName column
 * @method     InstructorQuery groupByLastname() Group by the LastName column
 * @method     InstructorQuery groupByClubid() Group by the ClubId column
 * @method     InstructorQuery groupByIsAdmin() Group by the is_admin column
 *
 * @method     InstructorQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     InstructorQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     InstructorQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     InstructorQuery leftJoinClub($relationAlias = null) Adds a LEFT JOIN clause to the query using the Club relation
 * @method     InstructorQuery rightJoinClub($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Club relation
 * @method     InstructorQuery innerJoinClub($relationAlias = null) Adds a INNER JOIN clause to the query using the Club relation
 *
 * @method     InstructorQuery leftJoinClassresult($relationAlias = null) Adds a LEFT JOIN clause to the query using the Classresult relation
 * @method     InstructorQuery rightJoinClassresult($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Classresult relation
 * @method     InstructorQuery innerJoinClassresult($relationAlias = null) Adds a INNER JOIN clause to the query using the Classresult relation
 *
 * @method     Instructor findOne(PropelPDO $con = null) Return the first Instructor matching the query
 * @method     Instructor findOneOrCreate(PropelPDO $con = null) Return the first Instructor matching the query, or a new Instructor object populated from the query conditions when no match is found
 *
 * @method     Instructor findOneById(int $ID) Return the first Instructor filtered by the ID column
 * @method     Instructor findOneByEmail(string $Email) Return the first Instructor filtered by the Email column
 * @method     Instructor findOneByPassword(string $Password) Return the first Instructor filtered by the Password column
 * @method     Instructor findOneByFirstname(string $FirstName) Return the first Instructor filtered by the FirstName column
 * @method     Instructor findOneByLastname(string $LastName) Return the first Instructor filtered by the LastName column
 * @method     Instructor findOneByClubid(int $ClubId) Return the first Instructor filtered by the ClubId column
 * @method     Instructor findOneByIsAdmin(int $is_admin) Return the first Instructor filtered by the is_admin column
 *
 * @method     array findById(int $ID) Return Instructor objects filtered by the ID column
 * @method     array findByEmail(string $Email) Return Instructor objects filtered by the Email column
 * @method     array findByPassword(string $Password) Return Instructor objects filtered by the Password column
 * @method     array findByFirstname(string $FirstName) Return Instructor objects filtered by the FirstName column
 * @method     array findByLastname(string $LastName) Return Instructor objects filtered by the LastName column
 * @method     array findByClubid(int $ClubId) Return Instructor objects filtered by the ClubId column
 * @method     array findByIsAdmin(int $is_admin) Return Instructor objects filtered by the is_admin column
 *
 * @package    propel.generator.site.om
 */
abstract class BaseInstructorQuery extends ModelCriteria
{
    
    /**
     * Initializes internal state of BaseInstructorQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'site', $modelName = 'Instructor', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new InstructorQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     InstructorQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return InstructorQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof InstructorQuery) {
            return $criteria;
        }
        $query = new InstructorQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Instructor|Instructor[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = InstructorPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(InstructorPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   Instructor A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `EMAIL`, `PASSWORD`, `FIRSTNAME`, `LASTNAME`, `CLUBID`, `IS_ADMIN` FROM `Instructor` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Instructor();
            $obj->hydrate($row);
            InstructorPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Instructor|Instructor[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Instructor[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return InstructorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(InstructorPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return InstructorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(InstructorPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InstructorQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(InstructorPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the Email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE Email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE Email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InstructorQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InstructorPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the Password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE Password = 'fooValue'
     * $query->filterByPassword('%fooValue%'); // WHERE Password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InstructorQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $password)) {
                $password = str_replace('*', '%', $password);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InstructorPeer::PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the FirstName column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstname('fooValue');   // WHERE FirstName = 'fooValue'
     * $query->filterByFirstname('%fooValue%'); // WHERE FirstName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InstructorQuery The current query, for fluid interface
     */
    public function filterByFirstname($firstname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $firstname)) {
                $firstname = str_replace('*', '%', $firstname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InstructorPeer::FIRSTNAME, $firstname, $comparison);
    }

    /**
     * Filter the query on the LastName column
     *
     * Example usage:
     * <code>
     * $query->filterByLastname('fooValue');   // WHERE LastName = 'fooValue'
     * $query->filterByLastname('%fooValue%'); // WHERE LastName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InstructorQuery The current query, for fluid interface
     */
    public function filterByLastname($lastname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lastname)) {
                $lastname = str_replace('*', '%', $lastname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InstructorPeer::LASTNAME, $lastname, $comparison);
    }

    /**
     * Filter the query on the ClubId column
     *
     * Example usage:
     * <code>
     * $query->filterByClubid(1234); // WHERE ClubId = 1234
     * $query->filterByClubid(array(12, 34)); // WHERE ClubId IN (12, 34)
     * $query->filterByClubid(array('min' => 12)); // WHERE ClubId > 12
     * </code>
     *
     * @see       filterByClub()
     *
     * @param     mixed $clubid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InstructorQuery The current query, for fluid interface
     */
    public function filterByClubid($clubid = null, $comparison = null)
    {
        if (is_array($clubid)) {
            $useMinMax = false;
            if (isset($clubid['min'])) {
                $this->addUsingAlias(InstructorPeer::CLUBID, $clubid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clubid['max'])) {
                $this->addUsingAlias(InstructorPeer::CLUBID, $clubid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InstructorPeer::CLUBID, $clubid, $comparison);
    }

    /**
     * Filter the query on the is_admin column
     *
     * Example usage:
     * <code>
     * $query->filterByIsAdmin(1234); // WHERE is_admin = 1234
     * $query->filterByIsAdmin(array(12, 34)); // WHERE is_admin IN (12, 34)
     * $query->filterByIsAdmin(array('min' => 12)); // WHERE is_admin > 12
     * </code>
     *
     * @param     mixed $isAdmin The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InstructorQuery The current query, for fluid interface
     */
    public function filterByIsAdmin($isAdmin = null, $comparison = null)
    {
        if (is_array($isAdmin)) {
            $useMinMax = false;
            if (isset($isAdmin['min'])) {
                $this->addUsingAlias(InstructorPeer::IS_ADMIN, $isAdmin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($isAdmin['max'])) {
                $this->addUsingAlias(InstructorPeer::IS_ADMIN, $isAdmin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InstructorPeer::IS_ADMIN, $isAdmin, $comparison);
    }

    /**
     * Filter the query by a related Club object
     *
     * @param   Club|PropelObjectCollection $club The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   InstructorQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByClub($club, $comparison = null)
    {
        if ($club instanceof Club) {
            return $this
                ->addUsingAlias(InstructorPeer::CLUBID, $club->getId(), $comparison);
        } elseif ($club instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(InstructorPeer::CLUBID, $club->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByClub() only accepts arguments of type Club or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Club relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InstructorQuery The current query, for fluid interface
     */
    public function joinClub($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Club');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Club');
        }

        return $this;
    }

    /**
     * Use the Club relation Club object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ClubQuery A secondary query class using the current class as primary query
     */
    public function useClubQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinClub($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Club', 'ClubQuery');
    }

    /**
     * Filter the query by a related Classresult object
     *
     * @param   Classresult|PropelObjectCollection $classresult  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   InstructorQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByClassresult($classresult, $comparison = null)
    {
        if ($classresult instanceof Classresult) {
            return $this
                ->addUsingAlias(InstructorPeer::ID, $classresult->getInstructorid(), $comparison);
        } elseif ($classresult instanceof PropelObjectCollection) {
            return $this
                ->useClassresultQuery()
                ->filterByPrimaryKeys($classresult->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByClassresult() only accepts arguments of type Classresult or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Classresult relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InstructorQuery The current query, for fluid interface
     */
    public function joinClassresult($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Classresult');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Classresult');
        }

        return $this;
    }

    /**
     * Use the Classresult relation Classresult object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ClassresultQuery A secondary query class using the current class as primary query
     */
    public function useClassresultQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinClassresult($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Classresult', 'ClassresultQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Instructor $instructor Object to remove from the list of results
     *
     * @return InstructorQuery The current query, for fluid interface
     */
    public function prune($instructor = null)
    {
        if ($instructor) {
            $this->addUsingAlias(InstructorPeer::ID, $instructor->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

} // BaseInstructorQuery