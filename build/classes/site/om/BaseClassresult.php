<?php


/**
 * Base class that represents a row from the 'ClassResult' table.
 *
 * 
 *
 * @package    propel.generator.site.om
 */
abstract class BaseClassresult extends BaseObject 
{

    /**
     * Peer class name
     */
    const PEER = 'ClassresultPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        ClassresultPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the classid field.
     * @var        int
     */
    protected $classid;

    /**
     * The value for the instructorid field.
     * @var        int
     */
    protected $instructorid;

    /**
     * The value for the substituteforinstructorid field.
     * @var        int
     */
    protected $substituteforinstructorid;

    /**
     * The value for the date field.
     * @var        string
     */
    protected $date;

    /**
     * The value for the notes field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $notes;

    /**
     * The value for the clubid field.
     * @var        int
     */
    protected $clubid;

    /**
     * The value for the participants field.
     * @var        int
     */
    protected $participants;

    /**
     * The value for the last_updated field.
     * @var        string
     */
    protected $last_updated;

    /**
     * The value for the hours field.
     * @var        string
     */
    protected $hours;

    /**
     * The value for the date_entered field.
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        string
     */
    protected $date_entered;

    /**
     * The value for the starttime field.
     * @var        string
     */
    protected $starttime;

    /**
     * @var        _Class
     */
    protected $a_Class;

    /**
     * @var        Club
     */
    protected $aClub;

    /**
     * @var        Instructor
     */
    protected $aInstructor;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->notes = '';
    }

    /**
     * Initializes internal state of BaseClassresult object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     * 
     * @return   int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [classid] column value.
     * 
     * @return   int
     */
    public function getClassid()
    {

        return $this->classid;
    }

    /**
     * Get the [instructorid] column value.
     * 
     * @return   int
     */
    public function getInstructorid()
    {

        return $this->instructorid;
    }

    /**
     * Get the [substituteforinstructorid] column value.
     * 
     * @return   int
     */
    public function getSubstituteforinstructorid()
    {

        return $this->substituteforinstructorid;
    }

    /**
     * Get the [optionally formatted] temporal [date] column value.
     * 
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *							If format is NULL, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDate($format = 'Y-m-d H:i:s')
    {
        if ($this->date === null) {
            return null;
        }


        if ($this->date === '0000-00-00 00:00:00') {
            // while technically this is not a default value of NULL,
            // this seems to be closest in meaning.
            return null;
        } else {
            try {
                $dt = new DateTime($this->date);
            } catch (Exception $x) {
                throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date, true), $x);
            }
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is TRUE, we return a DateTime object.
            return $dt;
        } elseif (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        } else {
            return $dt->format($format);
        }
    }

    /**
     * Get the [notes] column value.
     * 
     * @return   string
     */
    public function getNotes()
    {

        return $this->notes;
    }

    /**
     * Get the [clubid] column value.
     * 
     * @return   int
     */
    public function getClubid()
    {

        return $this->clubid;
    }

    /**
     * Get the [participants] column value.
     * 
     * @return   int
     */
    public function getParticipants()
    {

        return $this->participants;
    }

    /**
     * Get the [optionally formatted] temporal [last_updated] column value.
     * 
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *							If format is NULL, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdated($format = 'Y-m-d H:i:s')
    {
        if ($this->last_updated === null) {
            return null;
        }


        if ($this->last_updated === '0000-00-00 00:00:00') {
            // while technically this is not a default value of NULL,
            // this seems to be closest in meaning.
            return null;
        } else {
            try {
                $dt = new DateTime($this->last_updated);
            } catch (Exception $x) {
                throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_updated, true), $x);
            }
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is TRUE, we return a DateTime object.
            return $dt;
        } elseif (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        } else {
            return $dt->format($format);
        }
    }

    /**
     * Get the [hours] column value.
     * 
     * @return   string
     */
    public function getHours()
    {

        return $this->hours;
    }

    /**
     * Get the [optionally formatted] temporal [date_entered] column value.
     * 
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *							If format is NULL, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateEntered($format = 'Y-m-d H:i:s')
    {
        if ($this->date_entered === null) {
            return null;
        }


        if ($this->date_entered === '0000-00-00 00:00:00') {
            // while technically this is not a default value of NULL,
            // this seems to be closest in meaning.
            return null;
        } else {
            try {
                $dt = new DateTime($this->date_entered);
            } catch (Exception $x) {
                throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_entered, true), $x);
            }
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is TRUE, we return a DateTime object.
            return $dt;
        } elseif (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        } else {
            return $dt->format($format);
        }
    }

    /**
     * Get the [starttime] column value.
     * 
     * @return   string
     */
    public function getStarttime()
    {

        return $this->starttime;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param      int $v new value
     * @return   Classresult The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = ClassresultPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [classid] column.
     * 
     * @param      int $v new value
     * @return   Classresult The current object (for fluent API support)
     */
    public function setClassid($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->classid !== $v) {
            $this->classid = $v;
            $this->modifiedColumns[] = ClassresultPeer::CLASSID;
        }

        if ($this->a_Class !== null && $this->a_Class->getId() !== $v) {
            $this->a_Class = null;
        }


        return $this;
    } // setClassid()

    /**
     * Set the value of [instructorid] column.
     * 
     * @param      int $v new value
     * @return   Classresult The current object (for fluent API support)
     */
    public function setInstructorid($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->instructorid !== $v) {
            $this->instructorid = $v;
            $this->modifiedColumns[] = ClassresultPeer::INSTRUCTORID;
        }

        if ($this->aInstructor !== null && $this->aInstructor->getId() !== $v) {
            $this->aInstructor = null;
        }


        return $this;
    } // setInstructorid()

    /**
     * Set the value of [substituteforinstructorid] column.
     * 
     * @param      int $v new value
     * @return   Classresult The current object (for fluent API support)
     */
    public function setSubstituteforinstructorid($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->substituteforinstructorid !== $v) {
            $this->substituteforinstructorid = $v;
            $this->modifiedColumns[] = ClassresultPeer::SUBSTITUTEFORINSTRUCTORID;
        }


        return $this;
    } // setSubstituteforinstructorid()

    /**
     * Sets the value of [date] column to a normalized version of the date/time value specified.
     * 
     * @param      mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as NULL.
     * @return   Classresult The current object (for fluent API support)
     */
    public function setDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date !== null || $dt !== null) {
            $currentDateAsString = ($this->date !== null && $tmpDt = new DateTime($this->date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date = $newDateAsString;
                $this->modifiedColumns[] = ClassresultPeer::DATE;
            }
        } // if either are not null


        return $this;
    } // setDate()

    /**
     * Set the value of [notes] column.
     * 
     * @param      string $v new value
     * @return   Classresult The current object (for fluent API support)
     */
    public function setNotes($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->notes !== $v) {
            $this->notes = $v;
            $this->modifiedColumns[] = ClassresultPeer::NOTES;
        }


        return $this;
    } // setNotes()

    /**
     * Set the value of [clubid] column.
     * 
     * @param      int $v new value
     * @return   Classresult The current object (for fluent API support)
     */
    public function setClubid($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->clubid !== $v) {
            $this->clubid = $v;
            $this->modifiedColumns[] = ClassresultPeer::CLUBID;
        }

        if ($this->aClub !== null && $this->aClub->getId() !== $v) {
            $this->aClub = null;
        }


        return $this;
    } // setClubid()

    /**
     * Set the value of [participants] column.
     * 
     * @param      int $v new value
     * @return   Classresult The current object (for fluent API support)
     */
    public function setParticipants($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->participants !== $v) {
            $this->participants = $v;
            $this->modifiedColumns[] = ClassresultPeer::PARTICIPANTS;
        }


        return $this;
    } // setParticipants()

    /**
     * Sets the value of [last_updated] column to a normalized version of the date/time value specified.
     * 
     * @param      mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as NULL.
     * @return   Classresult The current object (for fluent API support)
     */
    public function setLastUpdated($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_updated !== null || $dt !== null) {
            $currentDateAsString = ($this->last_updated !== null && $tmpDt = new DateTime($this->last_updated)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_updated = $newDateAsString;
                $this->modifiedColumns[] = ClassresultPeer::LAST_UPDATED;
            }
        } // if either are not null


        return $this;
    } // setLastUpdated()

    /**
     * Set the value of [hours] column.
     * 
     * @param      string $v new value
     * @return   Classresult The current object (for fluent API support)
     */
    public function setHours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->hours !== $v) {
            $this->hours = $v;
            $this->modifiedColumns[] = ClassresultPeer::HOURS;
        }


        return $this;
    } // setHours()

    /**
     * Sets the value of [date_entered] column to a normalized version of the date/time value specified.
     * 
     * @param      mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as NULL.
     * @return   Classresult The current object (for fluent API support)
     */
    public function setDateEntered($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_entered !== null || $dt !== null) {
            $currentDateAsString = ($this->date_entered !== null && $tmpDt = new DateTime($this->date_entered)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_entered = $newDateAsString;
                $this->modifiedColumns[] = ClassresultPeer::DATE_ENTERED;
            }
        } // if either are not null


        return $this;
    } // setDateEntered()

    /**
     * Set the value of [starttime] column.
     * 
     * @param      string $v new value
     * @return   Classresult The current object (for fluent API support)
     */
    public function setStarttime($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->starttime !== $v) {
            $this->starttime = $v;
            $this->modifiedColumns[] = ClassresultPeer::STARTTIME;
        }


        return $this;
    } // setStarttime()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->notes !== '') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->classid = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->instructorid = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->substituteforinstructorid = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->date = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->notes = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->clubid = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->participants = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->last_updated = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->hours = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->date_entered = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->starttime = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 12; // 12 = ClassresultPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Classresult object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->a_Class !== null && $this->classid !== $this->a_Class->getId()) {
            $this->a_Class = null;
        }
        if ($this->aInstructor !== null && $this->instructorid !== $this->aInstructor->getId()) {
            $this->aInstructor = null;
        }
        if ($this->aClub !== null && $this->clubid !== $this->aClub->getId()) {
            $this->aClub = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ClassresultPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ClassresultPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->a_Class = null;
            $this->aClub = null;
            $this->aInstructor = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ClassresultPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ClassresultQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ClassresultPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ClassresultPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->a_Class !== null) {
                if ($this->a_Class->isModified() || $this->a_Class->isNew()) {
                    $affectedRows += $this->a_Class->save($con);
                }
                $this->set_Class($this->a_Class);
            }

            if ($this->aClub !== null) {
                if ($this->aClub->isModified() || $this->aClub->isNew()) {
                    $affectedRows += $this->aClub->save($con);
                }
                $this->setClub($this->aClub);
            }

            if ($this->aInstructor !== null) {
                if ($this->aInstructor->isModified() || $this->aInstructor->isNew()) {
                    $affectedRows += $this->aInstructor->save($con);
                }
                $this->setInstructor($this->aInstructor);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = ClassresultPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ClassresultPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ClassresultPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(ClassresultPeer::CLASSID)) {
            $modifiedColumns[':p' . $index++]  = '`CLASSID`';
        }
        if ($this->isColumnModified(ClassresultPeer::INSTRUCTORID)) {
            $modifiedColumns[':p' . $index++]  = '`INSTRUCTORID`';
        }
        if ($this->isColumnModified(ClassresultPeer::SUBSTITUTEFORINSTRUCTORID)) {
            $modifiedColumns[':p' . $index++]  = '`SUBSTITUTEFORINSTRUCTORID`';
        }
        if ($this->isColumnModified(ClassresultPeer::DATE)) {
            $modifiedColumns[':p' . $index++]  = '`DATE`';
        }
        if ($this->isColumnModified(ClassresultPeer::NOTES)) {
            $modifiedColumns[':p' . $index++]  = '`NOTES`';
        }
        if ($this->isColumnModified(ClassresultPeer::CLUBID)) {
            $modifiedColumns[':p' . $index++]  = '`CLUBID`';
        }
        if ($this->isColumnModified(ClassresultPeer::PARTICIPANTS)) {
            $modifiedColumns[':p' . $index++]  = '`PARTICIPANTS`';
        }
        if ($this->isColumnModified(ClassresultPeer::LAST_UPDATED)) {
            $modifiedColumns[':p' . $index++]  = '`LAST_UPDATED`';
        }
        if ($this->isColumnModified(ClassresultPeer::HOURS)) {
            $modifiedColumns[':p' . $index++]  = '`HOURS`';
        }
        if ($this->isColumnModified(ClassresultPeer::DATE_ENTERED)) {
            $modifiedColumns[':p' . $index++]  = '`DATE_ENTERED`';
        }
        if ($this->isColumnModified(ClassresultPeer::STARTTIME)) {
            $modifiedColumns[':p' . $index++]  = '`STARTTIME`';
        }

        $sql = sprintf(
            'INSERT INTO `ClassResult` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':
						$stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`CLASSID`':
						$stmt->bindValue($identifier, $this->classid, PDO::PARAM_INT);
                        break;
                    case '`INSTRUCTORID`':
						$stmt->bindValue($identifier, $this->instructorid, PDO::PARAM_INT);
                        break;
                    case '`SUBSTITUTEFORINSTRUCTORID`':
						$stmt->bindValue($identifier, $this->substituteforinstructorid, PDO::PARAM_INT);
                        break;
                    case '`DATE`':
						$stmt->bindValue($identifier, $this->date, PDO::PARAM_STR);
                        break;
                    case '`NOTES`':
						$stmt->bindValue($identifier, $this->notes, PDO::PARAM_STR);
                        break;
                    case '`CLUBID`':
						$stmt->bindValue($identifier, $this->clubid, PDO::PARAM_INT);
                        break;
                    case '`PARTICIPANTS`':
						$stmt->bindValue($identifier, $this->participants, PDO::PARAM_INT);
                        break;
                    case '`LAST_UPDATED`':
						$stmt->bindValue($identifier, $this->last_updated, PDO::PARAM_STR);
                        break;
                    case '`HOURS`':
						$stmt->bindValue($identifier, $this->hours, PDO::PARAM_STR);
                        break;
                    case '`DATE_ENTERED`':
						$stmt->bindValue($identifier, $this->date_entered, PDO::PARAM_STR);
                        break;
                    case '`STARTTIME`':
						$stmt->bindValue($identifier, $this->starttime, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
			$pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param      mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        } else {
            $this->validationFailures = $res;

            return false;
        }
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param      array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->a_Class !== null) {
                if (!$this->a_Class->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->a_Class->getValidationFailures());
                }
            }

            if ($this->aClub !== null) {
                if (!$this->aClub->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aClub->getValidationFailures());
                }
            }

            if ($this->aInstructor !== null) {
                if (!$this->aInstructor->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aInstructor->getValidationFailures());
                }
            }


            if (($retval = ClassresultPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ClassresultPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getClassid();
                break;
            case 2:
                return $this->getInstructorid();
                break;
            case 3:
                return $this->getSubstituteforinstructorid();
                break;
            case 4:
                return $this->getDate();
                break;
            case 5:
                return $this->getNotes();
                break;
            case 6:
                return $this->getClubid();
                break;
            case 7:
                return $this->getParticipants();
                break;
            case 8:
                return $this->getLastUpdated();
                break;
            case 9:
                return $this->getHours();
                break;
            case 10:
                return $this->getDateEntered();
                break;
            case 11:
                return $this->getStarttime();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Classresult'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Classresult'][$this->getPrimaryKey()] = true;
        $keys = ClassresultPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getClassid(),
            $keys[2] => $this->getInstructorid(),
            $keys[3] => $this->getSubstituteforinstructorid(),
            $keys[4] => $this->getDate(),
            $keys[5] => $this->getNotes(),
            $keys[6] => $this->getClubid(),
            $keys[7] => $this->getParticipants(),
            $keys[8] => $this->getLastUpdated(),
            $keys[9] => $this->getHours(),
            $keys[10] => $this->getDateEntered(),
            $keys[11] => $this->getStarttime(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->a_Class) {
                $result['_Class'] = $this->a_Class->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aClub) {
                $result['Club'] = $this->aClub->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aInstructor) {
                $result['Instructor'] = $this->aInstructor->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param      string $name peer name
     * @param      mixed $value field value
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ClassresultPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @param      mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setClassid($value);
                break;
            case 2:
                $this->setInstructorid($value);
                break;
            case 3:
                $this->setSubstituteforinstructorid($value);
                break;
            case 4:
                $this->setDate($value);
                break;
            case 5:
                $this->setNotes($value);
                break;
            case 6:
                $this->setClubid($value);
                break;
            case 7:
                $this->setParticipants($value);
                break;
            case 8:
                $this->setLastUpdated($value);
                break;
            case 9:
                $this->setHours($value);
                break;
            case 10:
                $this->setDateEntered($value);
                break;
            case 11:
                $this->setStarttime($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ClassresultPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setClassid($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setInstructorid($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setSubstituteforinstructorid($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setDate($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setNotes($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setClubid($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setParticipants($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setLastUpdated($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setHours($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setDateEntered($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setStarttime($arr[$keys[11]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ClassresultPeer::DATABASE_NAME);

        if ($this->isColumnModified(ClassresultPeer::ID)) $criteria->add(ClassresultPeer::ID, $this->id);
        if ($this->isColumnModified(ClassresultPeer::CLASSID)) $criteria->add(ClassresultPeer::CLASSID, $this->classid);
        if ($this->isColumnModified(ClassresultPeer::INSTRUCTORID)) $criteria->add(ClassresultPeer::INSTRUCTORID, $this->instructorid);
        if ($this->isColumnModified(ClassresultPeer::SUBSTITUTEFORINSTRUCTORID)) $criteria->add(ClassresultPeer::SUBSTITUTEFORINSTRUCTORID, $this->substituteforinstructorid);
        if ($this->isColumnModified(ClassresultPeer::DATE)) $criteria->add(ClassresultPeer::DATE, $this->date);
        if ($this->isColumnModified(ClassresultPeer::NOTES)) $criteria->add(ClassresultPeer::NOTES, $this->notes);
        if ($this->isColumnModified(ClassresultPeer::CLUBID)) $criteria->add(ClassresultPeer::CLUBID, $this->clubid);
        if ($this->isColumnModified(ClassresultPeer::PARTICIPANTS)) $criteria->add(ClassresultPeer::PARTICIPANTS, $this->participants);
        if ($this->isColumnModified(ClassresultPeer::LAST_UPDATED)) $criteria->add(ClassresultPeer::LAST_UPDATED, $this->last_updated);
        if ($this->isColumnModified(ClassresultPeer::HOURS)) $criteria->add(ClassresultPeer::HOURS, $this->hours);
        if ($this->isColumnModified(ClassresultPeer::DATE_ENTERED)) $criteria->add(ClassresultPeer::DATE_ENTERED, $this->date_entered);
        if ($this->isColumnModified(ClassresultPeer::STARTTIME)) $criteria->add(ClassresultPeer::STARTTIME, $this->starttime);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ClassresultPeer::DATABASE_NAME);
        $criteria->add(ClassresultPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return   int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of Classresult (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setClassid($this->getClassid());
        $copyObj->setInstructorid($this->getInstructorid());
        $copyObj->setSubstituteforinstructorid($this->getSubstituteforinstructorid());
        $copyObj->setDate($this->getDate());
        $copyObj->setNotes($this->getNotes());
        $copyObj->setClubid($this->getClubid());
        $copyObj->setParticipants($this->getParticipants());
        $copyObj->setLastUpdated($this->getLastUpdated());
        $copyObj->setHours($this->getHours());
        $copyObj->setDateEntered($this->getDateEntered());
        $copyObj->setStarttime($this->getStarttime());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return                 Classresult Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return   ClassresultPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ClassresultPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a _Class object.
     *
     * @param                  _Class $v
     * @return                 Classresult The current object (for fluent API support)
     * @throws PropelException
     */
    public function set_Class(_Class $v = null)
    {
        if ($v === null) {
            $this->setClassid(NULL);
        } else {
            $this->setClassid($v->getId());
        }

        $this->a_Class = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the _Class object, it will not be re-added.
        if ($v !== null) {
            $v->addClassresult($this);
        }


        return $this;
    }


    /**
     * Get the associated _Class object
     *
     * @param      PropelPDO $con Optional Connection object.
     * @return                 _Class The associated _Class object.
     * @throws PropelException
     */
    public function get_Class(PropelPDO $con = null)
    {
        if ($this->a_Class === null && ($this->classid !== null)) {
            $this->a_Class = _ClassQuery::create()->findPk($this->classid, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->a_Class->addClassresults($this);
             */
        }

        return $this->a_Class;
    }

    /**
     * Declares an association between this object and a Club object.
     *
     * @param                  Club $v
     * @return                 Classresult The current object (for fluent API support)
     * @throws PropelException
     */
    public function setClub(Club $v = null)
    {
        if ($v === null) {
            $this->setClubid(NULL);
        } else {
            $this->setClubid($v->getId());
        }

        $this->aClub = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Club object, it will not be re-added.
        if ($v !== null) {
            $v->addClassresult($this);
        }


        return $this;
    }


    /**
     * Get the associated Club object
     *
     * @param      PropelPDO $con Optional Connection object.
     * @return                 Club The associated Club object.
     * @throws PropelException
     */
    public function getClub(PropelPDO $con = null)
    {
        if ($this->aClub === null && ($this->clubid !== null)) {
            $this->aClub = ClubQuery::create()->findPk($this->clubid, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aClub->addClassresults($this);
             */
        }

        return $this->aClub;
    }

    /**
     * Declares an association between this object and a Instructor object.
     *
     * @param                  Instructor $v
     * @return                 Classresult The current object (for fluent API support)
     * @throws PropelException
     */
    public function setInstructor(Instructor $v = null)
    {
        if ($v === null) {
            $this->setInstructorid(NULL);
        } else {
            $this->setInstructorid($v->getId());
        }

        $this->aInstructor = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Instructor object, it will not be re-added.
        if ($v !== null) {
            $v->addClassresult($this);
        }


        return $this;
    }


    /**
     * Get the associated Instructor object
     *
     * @param      PropelPDO $con Optional Connection object.
     * @return                 Instructor The associated Instructor object.
     * @throws PropelException
     */
    public function getInstructor(PropelPDO $con = null)
    {
        if ($this->aInstructor === null && ($this->instructorid !== null)) {
            $this->aInstructor = InstructorQuery::create()->findPk($this->instructorid, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aInstructor->addClassresults($this);
             */
        }

        return $this->aInstructor;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->classid = null;
        $this->instructorid = null;
        $this->substituteforinstructorid = null;
        $this->date = null;
        $this->notes = null;
        $this->clubid = null;
        $this->participants = null;
        $this->last_updated = null;
        $this->hours = null;
        $this->date_entered = null;
        $this->starttime = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->a_Class = null;
        $this->aClub = null;
        $this->aInstructor = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ClassresultPeer::DEFAULT_STRING_FORMAT);
    }

} // BaseClassresult
