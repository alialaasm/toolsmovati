<?php


/**
 * Base class that represents a row from the 'Club' table.
 *
 * 
 *
 * @package    propel.generator.site.om
 */
abstract class BaseClub extends BaseObject 
{

    /**
     * Peer class name
     */
    const PEER = 'ClubPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        ClubPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the clubname field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $clubname;

    /**
     * The value for the address field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $address;

    /**
     * The value for the city field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $city;

    /**
     * The value for the province field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $province;

    /**
     * The value for the postalcode field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $postalcode;

    /**
     * The value for the mapitaddress field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $mapitaddress;

    /**
     * The value for the phone field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $phone;

    /**
     * The value for the tollfree field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $tollfree;

    /**
     * The value for the contactemail field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $contactemail;

    /**
     * The value for the applicationemail field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $applicationemail;

    /**
     * The value for the personaltrainingemail field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $personaltrainingemail;

    /**
     * The value for the promotions field.
     * @var        string
     */
    protected $promotions;

    /**
     * The value for the scheduleurl field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $scheduleurl;

    /**
     * The value for the poolscheduleurl field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $poolscheduleurl;

    /**
     * The value for the useinternal field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $useinternal;

    /**
     * The value for the sundayhours field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $sundayhours;

    /**
     * The value for the mondayhours field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $mondayhours;

    /**
     * The value for the tuesdayhours field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $tuesdayhours;

    /**
     * The value for the wednesdayhours field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $wednesdayhours;

    /**
     * The value for the thursdayhours field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $thursdayhours;

    /**
     * The value for the fridayhours field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $fridayhours;

    /**
     * The value for the saturdayhours field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $saturdayhours;

    /**
     * The value for the active field.
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $active;

    /**
     * The value for the freetrial field.
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $freetrial;

    /**
     * The value for the salessundayhours field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $salessundayhours;

    /**
     * The value for the salesmondayhours field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $salesmondayhours;

    /**
     * The value for the salestuesdayhours field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $salestuesdayhours;

    /**
     * The value for the saleswednesdayhours field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $saleswednesdayhours;

    /**
     * The value for the salesthursdayhours field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $salesthursdayhours;

    /**
     * The value for the salesfridayhours field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $salesfridayhours;

    /**
     * The value for the salessaturdayhours field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $salessaturdayhours;

    /**
     * The value for the salestrailer field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $salestrailer;

    /**
     * The value for the onlinepromo field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $onlinepromo;

    /**
     * The value for the onlinepromotion field.
     * @var        string
     */
    protected $onlinepromotion;

    /**
     * The value for the newsletterurl field.
     * @var        string
     */
    protected $newsletterurl;

    /**
     * The value for the facebook_url field.
     * @var        string
     */
    protected $facebook_url;

    /**
     * The value for the mobile_image field.
     * @var        string
     */
    protected $mobile_image;

    /**
     * @var        PropelObjectCollection|Classresult[] Collection to store aggregation of Classresult objects.
     */
    protected $collClassresults;

    /**
     * @var        PropelObjectCollection|Instructor[] Collection to store aggregation of Instructor objects.
     */
    protected $collInstructors;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $classresultsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $instructorsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->clubname = '';
        $this->address = '';
        $this->city = '';
        $this->province = '';
        $this->postalcode = '';
        $this->mapitaddress = '';
        $this->phone = '';
        $this->tollfree = '';
        $this->contactemail = '';
        $this->applicationemail = '';
        $this->personaltrainingemail = '';
        $this->scheduleurl = '';
        $this->poolscheduleurl = '';
        $this->useinternal = false;
        $this->sundayhours = '';
        $this->mondayhours = '';
        $this->tuesdayhours = '';
        $this->wednesdayhours = '';
        $this->thursdayhours = '';
        $this->fridayhours = '';
        $this->saturdayhours = '';
        $this->active = true;
        $this->freetrial = true;
        $this->salessundayhours = '';
        $this->salesmondayhours = '';
        $this->salestuesdayhours = '';
        $this->saleswednesdayhours = '';
        $this->salesthursdayhours = '';
        $this->salesfridayhours = '';
        $this->salessaturdayhours = '';
        $this->salestrailer = false;
        $this->onlinepromo = '0';
    }

    /**
     * Initializes internal state of BaseClub object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     * 
     * @return   int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [clubname] column value.
     * 
     * @return   string
     */
    public function getClubname()
    {

        return $this->clubname;
    }

    /**
     * Get the [address] column value.
     * 
     * @return   string
     */
    public function getAddress()
    {

        return $this->address;
    }

    /**
     * Get the [city] column value.
     * 
     * @return   string
     */
    public function getCity()
    {

        return $this->city;
    }

    /**
     * Get the [province] column value.
     * 
     * @return   string
     */
    public function getProvince()
    {

        return $this->province;
    }

    /**
     * Get the [postalcode] column value.
     * 
     * @return   string
     */
    public function getPostalcode()
    {

        return $this->postalcode;
    }

    /**
     * Get the [mapitaddress] column value.
     * 
     * @return   string
     */
    public function getMapitaddress()
    {

        return $this->mapitaddress;
    }

    /**
     * Get the [phone] column value.
     * 
     * @return   string
     */
    public function getPhone()
    {

        return $this->phone;
    }

    /**
     * Get the [tollfree] column value.
     * 
     * @return   string
     */
    public function getTollfree()
    {

        return $this->tollfree;
    }

    /**
     * Get the [contactemail] column value.
     * 
     * @return   string
     */
    public function getContactemail()
    {

        return $this->contactemail;
    }

    /**
     * Get the [applicationemail] column value.
     * 
     * @return   string
     */
    public function getApplicationemail()
    {

        return $this->applicationemail;
    }

    /**
     * Get the [personaltrainingemail] column value.
     * 
     * @return   string
     */
    public function getPersonaltrainingemail()
    {

        return $this->personaltrainingemail;
    }

    /**
     * Get the [promotions] column value.
     * 
     * @return   string
     */
    public function getPromotions()
    {

        return $this->promotions;
    }

    /**
     * Get the [scheduleurl] column value.
     * 
     * @return   string
     */
    public function getScheduleurl()
    {

        return $this->scheduleurl;
    }

    /**
     * Get the [poolscheduleurl] column value.
     * 
     * @return   string
     */
    public function getPoolscheduleurl()
    {

        return $this->poolscheduleurl;
    }

    /**
     * Get the [useinternal] column value.
     * 
     * @return   boolean
     */
    public function getUseinternal()
    {

        return $this->useinternal;
    }

    /**
     * Get the [sundayhours] column value.
     * 
     * @return   string
     */
    public function getSundayhours()
    {

        return $this->sundayhours;
    }

    /**
     * Get the [mondayhours] column value.
     * 
     * @return   string
     */
    public function getMondayhours()
    {

        return $this->mondayhours;
    }

    /**
     * Get the [tuesdayhours] column value.
     * 
     * @return   string
     */
    public function getTuesdayhours()
    {

        return $this->tuesdayhours;
    }

    /**
     * Get the [wednesdayhours] column value.
     * 
     * @return   string
     */
    public function getWednesdayhours()
    {

        return $this->wednesdayhours;
    }

    /**
     * Get the [thursdayhours] column value.
     * 
     * @return   string
     */
    public function getThursdayhours()
    {

        return $this->thursdayhours;
    }

    /**
     * Get the [fridayhours] column value.
     * 
     * @return   string
     */
    public function getFridayhours()
    {

        return $this->fridayhours;
    }

    /**
     * Get the [saturdayhours] column value.
     * 
     * @return   string
     */
    public function getSaturdayhours()
    {

        return $this->saturdayhours;
    }

    /**
     * Get the [active] column value.
     * 
     * @return   boolean
     */
    public function getActive()
    {

        return $this->active;
    }

    /**
     * Get the [freetrial] column value.
     * 
     * @return   boolean
     */
    public function getFreetrial()
    {

        return $this->freetrial;
    }

    /**
     * Get the [salessundayhours] column value.
     * 
     * @return   string
     */
    public function getSalessundayhours()
    {

        return $this->salessundayhours;
    }

    /**
     * Get the [salesmondayhours] column value.
     * 
     * @return   string
     */
    public function getSalesmondayhours()
    {

        return $this->salesmondayhours;
    }

    /**
     * Get the [salestuesdayhours] column value.
     * 
     * @return   string
     */
    public function getSalestuesdayhours()
    {

        return $this->salestuesdayhours;
    }

    /**
     * Get the [saleswednesdayhours] column value.
     * 
     * @return   string
     */
    public function getSaleswednesdayhours()
    {

        return $this->saleswednesdayhours;
    }

    /**
     * Get the [salesthursdayhours] column value.
     * 
     * @return   string
     */
    public function getSalesthursdayhours()
    {

        return $this->salesthursdayhours;
    }

    /**
     * Get the [salesfridayhours] column value.
     * 
     * @return   string
     */
    public function getSalesfridayhours()
    {

        return $this->salesfridayhours;
    }

    /**
     * Get the [salessaturdayhours] column value.
     * 
     * @return   string
     */
    public function getSalessaturdayhours()
    {

        return $this->salessaturdayhours;
    }

    /**
     * Get the [salestrailer] column value.
     * 
     * @return   boolean
     */
    public function getSalestrailer()
    {

        return $this->salestrailer;
    }

    /**
     * Get the [onlinepromo] column value.
     * 
     * @return   string
     */
    public function getOnlinepromo()
    {

        return $this->onlinepromo;
    }

    /**
     * Get the [onlinepromotion] column value.
     * 
     * @return   string
     */
    public function getOnlinepromotion()
    {

        return $this->onlinepromotion;
    }

    /**
     * Get the [newsletterurl] column value.
     * 
     * @return   string
     */
    public function getNewsletterurl()
    {

        return $this->newsletterurl;
    }

    /**
     * Get the [facebook_url] column value.
     * 
     * @return   string
     */
    public function getFacebookUrl()
    {

        return $this->facebook_url;
    }

    /**
     * Get the [mobile_image] column value.
     * 
     * @return   string
     */
    public function getMobileImage()
    {

        return $this->mobile_image;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param      int $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = ClubPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [clubname] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setClubname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->clubname !== $v) {
            $this->clubname = $v;
            $this->modifiedColumns[] = ClubPeer::CLUBNAME;
        }


        return $this;
    } // setClubname()

    /**
     * Set the value of [address] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->address !== $v) {
            $this->address = $v;
            $this->modifiedColumns[] = ClubPeer::ADDRESS;
        }


        return $this;
    } // setAddress()

    /**
     * Set the value of [city] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setCity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->city !== $v) {
            $this->city = $v;
            $this->modifiedColumns[] = ClubPeer::CITY;
        }


        return $this;
    } // setCity()

    /**
     * Set the value of [province] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setProvince($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->province !== $v) {
            $this->province = $v;
            $this->modifiedColumns[] = ClubPeer::PROVINCE;
        }


        return $this;
    } // setProvince()

    /**
     * Set the value of [postalcode] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setPostalcode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->postalcode !== $v) {
            $this->postalcode = $v;
            $this->modifiedColumns[] = ClubPeer::POSTALCODE;
        }


        return $this;
    } // setPostalcode()

    /**
     * Set the value of [mapitaddress] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setMapitaddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mapitaddress !== $v) {
            $this->mapitaddress = $v;
            $this->modifiedColumns[] = ClubPeer::MAPITADDRESS;
        }


        return $this;
    } // setMapitaddress()

    /**
     * Set the value of [phone] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phone !== $v) {
            $this->phone = $v;
            $this->modifiedColumns[] = ClubPeer::PHONE;
        }


        return $this;
    } // setPhone()

    /**
     * Set the value of [tollfree] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setTollfree($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tollfree !== $v) {
            $this->tollfree = $v;
            $this->modifiedColumns[] = ClubPeer::TOLLFREE;
        }


        return $this;
    } // setTollfree()

    /**
     * Set the value of [contactemail] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setContactemail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->contactemail !== $v) {
            $this->contactemail = $v;
            $this->modifiedColumns[] = ClubPeer::CONTACTEMAIL;
        }


        return $this;
    } // setContactemail()

    /**
     * Set the value of [applicationemail] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setApplicationemail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->applicationemail !== $v) {
            $this->applicationemail = $v;
            $this->modifiedColumns[] = ClubPeer::APPLICATIONEMAIL;
        }


        return $this;
    } // setApplicationemail()

    /**
     * Set the value of [personaltrainingemail] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setPersonaltrainingemail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->personaltrainingemail !== $v) {
            $this->personaltrainingemail = $v;
            $this->modifiedColumns[] = ClubPeer::PERSONALTRAININGEMAIL;
        }


        return $this;
    } // setPersonaltrainingemail()

    /**
     * Set the value of [promotions] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setPromotions($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->promotions !== $v) {
            $this->promotions = $v;
            $this->modifiedColumns[] = ClubPeer::PROMOTIONS;
        }


        return $this;
    } // setPromotions()

    /**
     * Set the value of [scheduleurl] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setScheduleurl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->scheduleurl !== $v) {
            $this->scheduleurl = $v;
            $this->modifiedColumns[] = ClubPeer::SCHEDULEURL;
        }


        return $this;
    } // setScheduleurl()

    /**
     * Set the value of [poolscheduleurl] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setPoolscheduleurl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->poolscheduleurl !== $v) {
            $this->poolscheduleurl = $v;
            $this->modifiedColumns[] = ClubPeer::POOLSCHEDULEURL;
        }


        return $this;
    } // setPoolscheduleurl()

    /**
     * Sets the value of the [useinternal] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param      boolean|integer|string $v The new value
     * @return   Club The current object (for fluent API support)
     */
    public function setUseinternal($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->useinternal !== $v) {
            $this->useinternal = $v;
            $this->modifiedColumns[] = ClubPeer::USEINTERNAL;
        }


        return $this;
    } // setUseinternal()

    /**
     * Set the value of [sundayhours] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setSundayhours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sundayhours !== $v) {
            $this->sundayhours = $v;
            $this->modifiedColumns[] = ClubPeer::SUNDAYHOURS;
        }


        return $this;
    } // setSundayhours()

    /**
     * Set the value of [mondayhours] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setMondayhours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mondayhours !== $v) {
            $this->mondayhours = $v;
            $this->modifiedColumns[] = ClubPeer::MONDAYHOURS;
        }


        return $this;
    } // setMondayhours()

    /**
     * Set the value of [tuesdayhours] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setTuesdayhours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tuesdayhours !== $v) {
            $this->tuesdayhours = $v;
            $this->modifiedColumns[] = ClubPeer::TUESDAYHOURS;
        }


        return $this;
    } // setTuesdayhours()

    /**
     * Set the value of [wednesdayhours] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setWednesdayhours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->wednesdayhours !== $v) {
            $this->wednesdayhours = $v;
            $this->modifiedColumns[] = ClubPeer::WEDNESDAYHOURS;
        }


        return $this;
    } // setWednesdayhours()

    /**
     * Set the value of [thursdayhours] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setThursdayhours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->thursdayhours !== $v) {
            $this->thursdayhours = $v;
            $this->modifiedColumns[] = ClubPeer::THURSDAYHOURS;
        }


        return $this;
    } // setThursdayhours()

    /**
     * Set the value of [fridayhours] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setFridayhours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->fridayhours !== $v) {
            $this->fridayhours = $v;
            $this->modifiedColumns[] = ClubPeer::FRIDAYHOURS;
        }


        return $this;
    } // setFridayhours()

    /**
     * Set the value of [saturdayhours] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setSaturdayhours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->saturdayhours !== $v) {
            $this->saturdayhours = $v;
            $this->modifiedColumns[] = ClubPeer::SATURDAYHOURS;
        }


        return $this;
    } // setSaturdayhours()

    /**
     * Sets the value of the [active] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param      boolean|integer|string $v The new value
     * @return   Club The current object (for fluent API support)
     */
    public function setActive($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->active !== $v) {
            $this->active = $v;
            $this->modifiedColumns[] = ClubPeer::ACTIVE;
        }


        return $this;
    } // setActive()

    /**
     * Sets the value of the [freetrial] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param      boolean|integer|string $v The new value
     * @return   Club The current object (for fluent API support)
     */
    public function setFreetrial($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->freetrial !== $v) {
            $this->freetrial = $v;
            $this->modifiedColumns[] = ClubPeer::FREETRIAL;
        }


        return $this;
    } // setFreetrial()

    /**
     * Set the value of [salessundayhours] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setSalessundayhours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->salessundayhours !== $v) {
            $this->salessundayhours = $v;
            $this->modifiedColumns[] = ClubPeer::SALESSUNDAYHOURS;
        }


        return $this;
    } // setSalessundayhours()

    /**
     * Set the value of [salesmondayhours] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setSalesmondayhours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->salesmondayhours !== $v) {
            $this->salesmondayhours = $v;
            $this->modifiedColumns[] = ClubPeer::SALESMONDAYHOURS;
        }


        return $this;
    } // setSalesmondayhours()

    /**
     * Set the value of [salestuesdayhours] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setSalestuesdayhours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->salestuesdayhours !== $v) {
            $this->salestuesdayhours = $v;
            $this->modifiedColumns[] = ClubPeer::SALESTUESDAYHOURS;
        }


        return $this;
    } // setSalestuesdayhours()

    /**
     * Set the value of [saleswednesdayhours] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setSaleswednesdayhours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->saleswednesdayhours !== $v) {
            $this->saleswednesdayhours = $v;
            $this->modifiedColumns[] = ClubPeer::SALESWEDNESDAYHOURS;
        }


        return $this;
    } // setSaleswednesdayhours()

    /**
     * Set the value of [salesthursdayhours] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setSalesthursdayhours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->salesthursdayhours !== $v) {
            $this->salesthursdayhours = $v;
            $this->modifiedColumns[] = ClubPeer::SALESTHURSDAYHOURS;
        }


        return $this;
    } // setSalesthursdayhours()

    /**
     * Set the value of [salesfridayhours] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setSalesfridayhours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->salesfridayhours !== $v) {
            $this->salesfridayhours = $v;
            $this->modifiedColumns[] = ClubPeer::SALESFRIDAYHOURS;
        }


        return $this;
    } // setSalesfridayhours()

    /**
     * Set the value of [salessaturdayhours] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setSalessaturdayhours($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->salessaturdayhours !== $v) {
            $this->salessaturdayhours = $v;
            $this->modifiedColumns[] = ClubPeer::SALESSATURDAYHOURS;
        }


        return $this;
    } // setSalessaturdayhours()

    /**
     * Sets the value of the [salestrailer] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param      boolean|integer|string $v The new value
     * @return   Club The current object (for fluent API support)
     */
    public function setSalestrailer($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->salestrailer !== $v) {
            $this->salestrailer = $v;
            $this->modifiedColumns[] = ClubPeer::SALESTRAILER;
        }


        return $this;
    } // setSalestrailer()

    /**
     * Set the value of [onlinepromo] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setOnlinepromo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->onlinepromo !== $v) {
            $this->onlinepromo = $v;
            $this->modifiedColumns[] = ClubPeer::ONLINEPROMO;
        }


        return $this;
    } // setOnlinepromo()

    /**
     * Set the value of [onlinepromotion] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setOnlinepromotion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->onlinepromotion !== $v) {
            $this->onlinepromotion = $v;
            $this->modifiedColumns[] = ClubPeer::ONLINEPROMOTION;
        }


        return $this;
    } // setOnlinepromotion()

    /**
     * Set the value of [newsletterurl] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setNewsletterurl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->newsletterurl !== $v) {
            $this->newsletterurl = $v;
            $this->modifiedColumns[] = ClubPeer::NEWSLETTERURL;
        }


        return $this;
    } // setNewsletterurl()

    /**
     * Set the value of [facebook_url] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setFacebookUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->facebook_url !== $v) {
            $this->facebook_url = $v;
            $this->modifiedColumns[] = ClubPeer::FACEBOOK_URL;
        }


        return $this;
    } // setFacebookUrl()

    /**
     * Set the value of [mobile_image] column.
     * 
     * @param      string $v new value
     * @return   Club The current object (for fluent API support)
     */
    public function setMobileImage($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mobile_image !== $v) {
            $this->mobile_image = $v;
            $this->modifiedColumns[] = ClubPeer::MOBILE_IMAGE;
        }


        return $this;
    } // setMobileImage()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->clubname !== '') {
                return false;
            }

            if ($this->address !== '') {
                return false;
            }

            if ($this->city !== '') {
                return false;
            }

            if ($this->province !== '') {
                return false;
            }

            if ($this->postalcode !== '') {
                return false;
            }

            if ($this->mapitaddress !== '') {
                return false;
            }

            if ($this->phone !== '') {
                return false;
            }

            if ($this->tollfree !== '') {
                return false;
            }

            if ($this->contactemail !== '') {
                return false;
            }

            if ($this->applicationemail !== '') {
                return false;
            }

            if ($this->personaltrainingemail !== '') {
                return false;
            }

            if ($this->scheduleurl !== '') {
                return false;
            }

            if ($this->poolscheduleurl !== '') {
                return false;
            }

            if ($this->useinternal !== false) {
                return false;
            }

            if ($this->sundayhours !== '') {
                return false;
            }

            if ($this->mondayhours !== '') {
                return false;
            }

            if ($this->tuesdayhours !== '') {
                return false;
            }

            if ($this->wednesdayhours !== '') {
                return false;
            }

            if ($this->thursdayhours !== '') {
                return false;
            }

            if ($this->fridayhours !== '') {
                return false;
            }

            if ($this->saturdayhours !== '') {
                return false;
            }

            if ($this->active !== true) {
                return false;
            }

            if ($this->freetrial !== true) {
                return false;
            }

            if ($this->salessundayhours !== '') {
                return false;
            }

            if ($this->salesmondayhours !== '') {
                return false;
            }

            if ($this->salestuesdayhours !== '') {
                return false;
            }

            if ($this->saleswednesdayhours !== '') {
                return false;
            }

            if ($this->salesthursdayhours !== '') {
                return false;
            }

            if ($this->salesfridayhours !== '') {
                return false;
            }

            if ($this->salessaturdayhours !== '') {
                return false;
            }

            if ($this->salestrailer !== false) {
                return false;
            }

            if ($this->onlinepromo !== '0') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->clubname = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->address = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->city = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->province = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->postalcode = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->mapitaddress = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->phone = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->tollfree = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->contactemail = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->applicationemail = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->personaltrainingemail = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->promotions = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->scheduleurl = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->poolscheduleurl = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->useinternal = ($row[$startcol + 15] !== null) ? (boolean) $row[$startcol + 15] : null;
            $this->sundayhours = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->mondayhours = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->tuesdayhours = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->wednesdayhours = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->thursdayhours = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->fridayhours = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->saturdayhours = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->active = ($row[$startcol + 23] !== null) ? (boolean) $row[$startcol + 23] : null;
            $this->freetrial = ($row[$startcol + 24] !== null) ? (boolean) $row[$startcol + 24] : null;
            $this->salessundayhours = ($row[$startcol + 25] !== null) ? (string) $row[$startcol + 25] : null;
            $this->salesmondayhours = ($row[$startcol + 26] !== null) ? (string) $row[$startcol + 26] : null;
            $this->salestuesdayhours = ($row[$startcol + 27] !== null) ? (string) $row[$startcol + 27] : null;
            $this->saleswednesdayhours = ($row[$startcol + 28] !== null) ? (string) $row[$startcol + 28] : null;
            $this->salesthursdayhours = ($row[$startcol + 29] !== null) ? (string) $row[$startcol + 29] : null;
            $this->salesfridayhours = ($row[$startcol + 30] !== null) ? (string) $row[$startcol + 30] : null;
            $this->salessaturdayhours = ($row[$startcol + 31] !== null) ? (string) $row[$startcol + 31] : null;
            $this->salestrailer = ($row[$startcol + 32] !== null) ? (boolean) $row[$startcol + 32] : null;
            $this->onlinepromo = ($row[$startcol + 33] !== null) ? (string) $row[$startcol + 33] : null;
            $this->onlinepromotion = ($row[$startcol + 34] !== null) ? (string) $row[$startcol + 34] : null;
            $this->newsletterurl = ($row[$startcol + 35] !== null) ? (string) $row[$startcol + 35] : null;
            $this->facebook_url = ($row[$startcol + 36] !== null) ? (string) $row[$startcol + 36] : null;
            $this->mobile_image = ($row[$startcol + 37] !== null) ? (string) $row[$startcol + 37] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 38; // 38 = ClubPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Club object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ClubPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ClubPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collClassresults = null;

            $this->collInstructors = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ClubPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ClubQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ClubPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ClubPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->classresultsScheduledForDeletion !== null) {
                if (!$this->classresultsScheduledForDeletion->isEmpty()) {
                    foreach ($this->classresultsScheduledForDeletion as $classresult) {
                        // need to save related object because we set the relation to null
                        $classresult->save($con);
                    }
                    $this->classresultsScheduledForDeletion = null;
                }
            }

            if ($this->collClassresults !== null) {
                foreach ($this->collClassresults as $referrerFK) {
                    if (!$referrerFK->isDeleted()) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->instructorsScheduledForDeletion !== null) {
                if (!$this->instructorsScheduledForDeletion->isEmpty()) {
                    foreach ($this->instructorsScheduledForDeletion as $instructor) {
                        // need to save related object because we set the relation to null
                        $instructor->save($con);
                    }
                    $this->instructorsScheduledForDeletion = null;
                }
            }

            if ($this->collInstructors !== null) {
                foreach ($this->collInstructors as $referrerFK) {
                    if (!$referrerFK->isDeleted()) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = ClubPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ClubPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ClubPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(ClubPeer::CLUBNAME)) {
            $modifiedColumns[':p' . $index++]  = '`CLUBNAME`';
        }
        if ($this->isColumnModified(ClubPeer::ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = '`ADDRESS`';
        }
        if ($this->isColumnModified(ClubPeer::CITY)) {
            $modifiedColumns[':p' . $index++]  = '`CITY`';
        }
        if ($this->isColumnModified(ClubPeer::PROVINCE)) {
            $modifiedColumns[':p' . $index++]  = '`PROVINCE`';
        }
        if ($this->isColumnModified(ClubPeer::POSTALCODE)) {
            $modifiedColumns[':p' . $index++]  = '`POSTALCODE`';
        }
        if ($this->isColumnModified(ClubPeer::MAPITADDRESS)) {
            $modifiedColumns[':p' . $index++]  = '`MAPITADDRESS`';
        }
        if ($this->isColumnModified(ClubPeer::PHONE)) {
            $modifiedColumns[':p' . $index++]  = '`PHONE`';
        }
        if ($this->isColumnModified(ClubPeer::TOLLFREE)) {
            $modifiedColumns[':p' . $index++]  = '`TOLLFREE`';
        }
        if ($this->isColumnModified(ClubPeer::CONTACTEMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`CONTACTEMAIL`';
        }
        if ($this->isColumnModified(ClubPeer::APPLICATIONEMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`APPLICATIONEMAIL`';
        }
        if ($this->isColumnModified(ClubPeer::PERSONALTRAININGEMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`PERSONALTRAININGEMAIL`';
        }
        if ($this->isColumnModified(ClubPeer::PROMOTIONS)) {
            $modifiedColumns[':p' . $index++]  = '`PROMOTIONS`';
        }
        if ($this->isColumnModified(ClubPeer::SCHEDULEURL)) {
            $modifiedColumns[':p' . $index++]  = '`SCHEDULEURL`';
        }
        if ($this->isColumnModified(ClubPeer::POOLSCHEDULEURL)) {
            $modifiedColumns[':p' . $index++]  = '`POOLSCHEDULEURL`';
        }
        if ($this->isColumnModified(ClubPeer::USEINTERNAL)) {
            $modifiedColumns[':p' . $index++]  = '`USEINTERNAL`';
        }
        if ($this->isColumnModified(ClubPeer::SUNDAYHOURS)) {
            $modifiedColumns[':p' . $index++]  = '`SUNDAYHOURS`';
        }
        if ($this->isColumnModified(ClubPeer::MONDAYHOURS)) {
            $modifiedColumns[':p' . $index++]  = '`MONDAYHOURS`';
        }
        if ($this->isColumnModified(ClubPeer::TUESDAYHOURS)) {
            $modifiedColumns[':p' . $index++]  = '`TUESDAYHOURS`';
        }
        if ($this->isColumnModified(ClubPeer::WEDNESDAYHOURS)) {
            $modifiedColumns[':p' . $index++]  = '`WEDNESDAYHOURS`';
        }
        if ($this->isColumnModified(ClubPeer::THURSDAYHOURS)) {
            $modifiedColumns[':p' . $index++]  = '`THURSDAYHOURS`';
        }
        if ($this->isColumnModified(ClubPeer::FRIDAYHOURS)) {
            $modifiedColumns[':p' . $index++]  = '`FRIDAYHOURS`';
        }
        if ($this->isColumnModified(ClubPeer::SATURDAYHOURS)) {
            $modifiedColumns[':p' . $index++]  = '`SATURDAYHOURS`';
        }
        if ($this->isColumnModified(ClubPeer::ACTIVE)) {
            $modifiedColumns[':p' . $index++]  = '`ACTIVE`';
        }
        if ($this->isColumnModified(ClubPeer::FREETRIAL)) {
            $modifiedColumns[':p' . $index++]  = '`FREETRIAL`';
        }
        if ($this->isColumnModified(ClubPeer::SALESSUNDAYHOURS)) {
            $modifiedColumns[':p' . $index++]  = '`SALESSUNDAYHOURS`';
        }
        if ($this->isColumnModified(ClubPeer::SALESMONDAYHOURS)) {
            $modifiedColumns[':p' . $index++]  = '`SALESMONDAYHOURS`';
        }
        if ($this->isColumnModified(ClubPeer::SALESTUESDAYHOURS)) {
            $modifiedColumns[':p' . $index++]  = '`SALESTUESDAYHOURS`';
        }
        if ($this->isColumnModified(ClubPeer::SALESWEDNESDAYHOURS)) {
            $modifiedColumns[':p' . $index++]  = '`SALESWEDNESDAYHOURS`';
        }
        if ($this->isColumnModified(ClubPeer::SALESTHURSDAYHOURS)) {
            $modifiedColumns[':p' . $index++]  = '`SALESTHURSDAYHOURS`';
        }
        if ($this->isColumnModified(ClubPeer::SALESFRIDAYHOURS)) {
            $modifiedColumns[':p' . $index++]  = '`SALESFRIDAYHOURS`';
        }
        if ($this->isColumnModified(ClubPeer::SALESSATURDAYHOURS)) {
            $modifiedColumns[':p' . $index++]  = '`SALESSATURDAYHOURS`';
        }
        if ($this->isColumnModified(ClubPeer::SALESTRAILER)) {
            $modifiedColumns[':p' . $index++]  = '`SALESTRAILER`';
        }
        if ($this->isColumnModified(ClubPeer::ONLINEPROMO)) {
            $modifiedColumns[':p' . $index++]  = '`ONLINEPROMO`';
        }
        if ($this->isColumnModified(ClubPeer::ONLINEPROMOTION)) {
            $modifiedColumns[':p' . $index++]  = '`ONLINEPROMOTION`';
        }
        if ($this->isColumnModified(ClubPeer::NEWSLETTERURL)) {
            $modifiedColumns[':p' . $index++]  = '`NEWSLETTERURL`';
        }
        if ($this->isColumnModified(ClubPeer::FACEBOOK_URL)) {
            $modifiedColumns[':p' . $index++]  = '`FACEBOOK_URL`';
        }
        if ($this->isColumnModified(ClubPeer::MOBILE_IMAGE)) {
            $modifiedColumns[':p' . $index++]  = '`MOBILE_IMAGE`';
        }

        $sql = sprintf(
            'INSERT INTO `Club` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':
						$stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`CLUBNAME`':
						$stmt->bindValue($identifier, $this->clubname, PDO::PARAM_STR);
                        break;
                    case '`ADDRESS`':
						$stmt->bindValue($identifier, $this->address, PDO::PARAM_STR);
                        break;
                    case '`CITY`':
						$stmt->bindValue($identifier, $this->city, PDO::PARAM_STR);
                        break;
                    case '`PROVINCE`':
						$stmt->bindValue($identifier, $this->province, PDO::PARAM_STR);
                        break;
                    case '`POSTALCODE`':
						$stmt->bindValue($identifier, $this->postalcode, PDO::PARAM_STR);
                        break;
                    case '`MAPITADDRESS`':
						$stmt->bindValue($identifier, $this->mapitaddress, PDO::PARAM_STR);
                        break;
                    case '`PHONE`':
						$stmt->bindValue($identifier, $this->phone, PDO::PARAM_STR);
                        break;
                    case '`TOLLFREE`':
						$stmt->bindValue($identifier, $this->tollfree, PDO::PARAM_STR);
                        break;
                    case '`CONTACTEMAIL`':
						$stmt->bindValue($identifier, $this->contactemail, PDO::PARAM_STR);
                        break;
                    case '`APPLICATIONEMAIL`':
						$stmt->bindValue($identifier, $this->applicationemail, PDO::PARAM_STR);
                        break;
                    case '`PERSONALTRAININGEMAIL`':
						$stmt->bindValue($identifier, $this->personaltrainingemail, PDO::PARAM_STR);
                        break;
                    case '`PROMOTIONS`':
						$stmt->bindValue($identifier, $this->promotions, PDO::PARAM_STR);
                        break;
                    case '`SCHEDULEURL`':
						$stmt->bindValue($identifier, $this->scheduleurl, PDO::PARAM_STR);
                        break;
                    case '`POOLSCHEDULEURL`':
						$stmt->bindValue($identifier, $this->poolscheduleurl, PDO::PARAM_STR);
                        break;
                    case '`USEINTERNAL`':
						$stmt->bindValue($identifier, (int) $this->useinternal, PDO::PARAM_INT);
                        break;
                    case '`SUNDAYHOURS`':
						$stmt->bindValue($identifier, $this->sundayhours, PDO::PARAM_STR);
                        break;
                    case '`MONDAYHOURS`':
						$stmt->bindValue($identifier, $this->mondayhours, PDO::PARAM_STR);
                        break;
                    case '`TUESDAYHOURS`':
						$stmt->bindValue($identifier, $this->tuesdayhours, PDO::PARAM_STR);
                        break;
                    case '`WEDNESDAYHOURS`':
						$stmt->bindValue($identifier, $this->wednesdayhours, PDO::PARAM_STR);
                        break;
                    case '`THURSDAYHOURS`':
						$stmt->bindValue($identifier, $this->thursdayhours, PDO::PARAM_STR);
                        break;
                    case '`FRIDAYHOURS`':
						$stmt->bindValue($identifier, $this->fridayhours, PDO::PARAM_STR);
                        break;
                    case '`SATURDAYHOURS`':
						$stmt->bindValue($identifier, $this->saturdayhours, PDO::PARAM_STR);
                        break;
                    case '`ACTIVE`':
						$stmt->bindValue($identifier, (int) $this->active, PDO::PARAM_INT);
                        break;
                    case '`FREETRIAL`':
						$stmt->bindValue($identifier, (int) $this->freetrial, PDO::PARAM_INT);
                        break;
                    case '`SALESSUNDAYHOURS`':
						$stmt->bindValue($identifier, $this->salessundayhours, PDO::PARAM_STR);
                        break;
                    case '`SALESMONDAYHOURS`':
						$stmt->bindValue($identifier, $this->salesmondayhours, PDO::PARAM_STR);
                        break;
                    case '`SALESTUESDAYHOURS`':
						$stmt->bindValue($identifier, $this->salestuesdayhours, PDO::PARAM_STR);
                        break;
                    case '`SALESWEDNESDAYHOURS`':
						$stmt->bindValue($identifier, $this->saleswednesdayhours, PDO::PARAM_STR);
                        break;
                    case '`SALESTHURSDAYHOURS`':
						$stmt->bindValue($identifier, $this->salesthursdayhours, PDO::PARAM_STR);
                        break;
                    case '`SALESFRIDAYHOURS`':
						$stmt->bindValue($identifier, $this->salesfridayhours, PDO::PARAM_STR);
                        break;
                    case '`SALESSATURDAYHOURS`':
						$stmt->bindValue($identifier, $this->salessaturdayhours, PDO::PARAM_STR);
                        break;
                    case '`SALESTRAILER`':
						$stmt->bindValue($identifier, (int) $this->salestrailer, PDO::PARAM_INT);
                        break;
                    case '`ONLINEPROMO`':
						$stmt->bindValue($identifier, $this->onlinepromo, PDO::PARAM_STR);
                        break;
                    case '`ONLINEPROMOTION`':
						$stmt->bindValue($identifier, $this->onlinepromotion, PDO::PARAM_STR);
                        break;
                    case '`NEWSLETTERURL`':
						$stmt->bindValue($identifier, $this->newsletterurl, PDO::PARAM_STR);
                        break;
                    case '`FACEBOOK_URL`':
						$stmt->bindValue($identifier, $this->facebook_url, PDO::PARAM_STR);
                        break;
                    case '`MOBILE_IMAGE`':
						$stmt->bindValue($identifier, $this->mobile_image, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
			$pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param      mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        } else {
            $this->validationFailures = $res;

            return false;
        }
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param      array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = ClubPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collClassresults !== null) {
                    foreach ($this->collClassresults as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collInstructors !== null) {
                    foreach ($this->collInstructors as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ClubPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getClubname();
                break;
            case 2:
                return $this->getAddress();
                break;
            case 3:
                return $this->getCity();
                break;
            case 4:
                return $this->getProvince();
                break;
            case 5:
                return $this->getPostalcode();
                break;
            case 6:
                return $this->getMapitaddress();
                break;
            case 7:
                return $this->getPhone();
                break;
            case 8:
                return $this->getTollfree();
                break;
            case 9:
                return $this->getContactemail();
                break;
            case 10:
                return $this->getApplicationemail();
                break;
            case 11:
                return $this->getPersonaltrainingemail();
                break;
            case 12:
                return $this->getPromotions();
                break;
            case 13:
                return $this->getScheduleurl();
                break;
            case 14:
                return $this->getPoolscheduleurl();
                break;
            case 15:
                return $this->getUseinternal();
                break;
            case 16:
                return $this->getSundayhours();
                break;
            case 17:
                return $this->getMondayhours();
                break;
            case 18:
                return $this->getTuesdayhours();
                break;
            case 19:
                return $this->getWednesdayhours();
                break;
            case 20:
                return $this->getThursdayhours();
                break;
            case 21:
                return $this->getFridayhours();
                break;
            case 22:
                return $this->getSaturdayhours();
                break;
            case 23:
                return $this->getActive();
                break;
            case 24:
                return $this->getFreetrial();
                break;
            case 25:
                return $this->getSalessundayhours();
                break;
            case 26:
                return $this->getSalesmondayhours();
                break;
            case 27:
                return $this->getSalestuesdayhours();
                break;
            case 28:
                return $this->getSaleswednesdayhours();
                break;
            case 29:
                return $this->getSalesthursdayhours();
                break;
            case 30:
                return $this->getSalesfridayhours();
                break;
            case 31:
                return $this->getSalessaturdayhours();
                break;
            case 32:
                return $this->getSalestrailer();
                break;
            case 33:
                return $this->getOnlinepromo();
                break;
            case 34:
                return $this->getOnlinepromotion();
                break;
            case 35:
                return $this->getNewsletterurl();
                break;
            case 36:
                return $this->getFacebookUrl();
                break;
            case 37:
                return $this->getMobileImage();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Club'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Club'][$this->getPrimaryKey()] = true;
        $keys = ClubPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getClubname(),
            $keys[2] => $this->getAddress(),
            $keys[3] => $this->getCity(),
            $keys[4] => $this->getProvince(),
            $keys[5] => $this->getPostalcode(),
            $keys[6] => $this->getMapitaddress(),
            $keys[7] => $this->getPhone(),
            $keys[8] => $this->getTollfree(),
            $keys[9] => $this->getContactemail(),
            $keys[10] => $this->getApplicationemail(),
            $keys[11] => $this->getPersonaltrainingemail(),
            $keys[12] => $this->getPromotions(),
            $keys[13] => $this->getScheduleurl(),
            $keys[14] => $this->getPoolscheduleurl(),
            $keys[15] => $this->getUseinternal(),
            $keys[16] => $this->getSundayhours(),
            $keys[17] => $this->getMondayhours(),
            $keys[18] => $this->getTuesdayhours(),
            $keys[19] => $this->getWednesdayhours(),
            $keys[20] => $this->getThursdayhours(),
            $keys[21] => $this->getFridayhours(),
            $keys[22] => $this->getSaturdayhours(),
            $keys[23] => $this->getActive(),
            $keys[24] => $this->getFreetrial(),
            $keys[25] => $this->getSalessundayhours(),
            $keys[26] => $this->getSalesmondayhours(),
            $keys[27] => $this->getSalestuesdayhours(),
            $keys[28] => $this->getSaleswednesdayhours(),
            $keys[29] => $this->getSalesthursdayhours(),
            $keys[30] => $this->getSalesfridayhours(),
            $keys[31] => $this->getSalessaturdayhours(),
            $keys[32] => $this->getSalestrailer(),
            $keys[33] => $this->getOnlinepromo(),
            $keys[34] => $this->getOnlinepromotion(),
            $keys[35] => $this->getNewsletterurl(),
            $keys[36] => $this->getFacebookUrl(),
            $keys[37] => $this->getMobileImage(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collClassresults) {
                $result['Classresults'] = $this->collClassresults->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collInstructors) {
                $result['Instructors'] = $this->collInstructors->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param      string $name peer name
     * @param      mixed $value field value
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ClubPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @param      mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setClubname($value);
                break;
            case 2:
                $this->setAddress($value);
                break;
            case 3:
                $this->setCity($value);
                break;
            case 4:
                $this->setProvince($value);
                break;
            case 5:
                $this->setPostalcode($value);
                break;
            case 6:
                $this->setMapitaddress($value);
                break;
            case 7:
                $this->setPhone($value);
                break;
            case 8:
                $this->setTollfree($value);
                break;
            case 9:
                $this->setContactemail($value);
                break;
            case 10:
                $this->setApplicationemail($value);
                break;
            case 11:
                $this->setPersonaltrainingemail($value);
                break;
            case 12:
                $this->setPromotions($value);
                break;
            case 13:
                $this->setScheduleurl($value);
                break;
            case 14:
                $this->setPoolscheduleurl($value);
                break;
            case 15:
                $this->setUseinternal($value);
                break;
            case 16:
                $this->setSundayhours($value);
                break;
            case 17:
                $this->setMondayhours($value);
                break;
            case 18:
                $this->setTuesdayhours($value);
                break;
            case 19:
                $this->setWednesdayhours($value);
                break;
            case 20:
                $this->setThursdayhours($value);
                break;
            case 21:
                $this->setFridayhours($value);
                break;
            case 22:
                $this->setSaturdayhours($value);
                break;
            case 23:
                $this->setActive($value);
                break;
            case 24:
                $this->setFreetrial($value);
                break;
            case 25:
                $this->setSalessundayhours($value);
                break;
            case 26:
                $this->setSalesmondayhours($value);
                break;
            case 27:
                $this->setSalestuesdayhours($value);
                break;
            case 28:
                $this->setSaleswednesdayhours($value);
                break;
            case 29:
                $this->setSalesthursdayhours($value);
                break;
            case 30:
                $this->setSalesfridayhours($value);
                break;
            case 31:
                $this->setSalessaturdayhours($value);
                break;
            case 32:
                $this->setSalestrailer($value);
                break;
            case 33:
                $this->setOnlinepromo($value);
                break;
            case 34:
                $this->setOnlinepromotion($value);
                break;
            case 35:
                $this->setNewsletterurl($value);
                break;
            case 36:
                $this->setFacebookUrl($value);
                break;
            case 37:
                $this->setMobileImage($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ClubPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setClubname($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setAddress($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setCity($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setProvince($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPostalcode($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setMapitaddress($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setPhone($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setTollfree($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setContactemail($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setApplicationemail($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setPersonaltrainingemail($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setPromotions($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setScheduleurl($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setPoolscheduleurl($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setUseinternal($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setSundayhours($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setMondayhours($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setTuesdayhours($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setWednesdayhours($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setThursdayhours($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setFridayhours($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setSaturdayhours($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setActive($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setFreetrial($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setSalessundayhours($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setSalesmondayhours($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setSalestuesdayhours($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setSaleswednesdayhours($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setSalesthursdayhours($arr[$keys[29]]);
        if (array_key_exists($keys[30], $arr)) $this->setSalesfridayhours($arr[$keys[30]]);
        if (array_key_exists($keys[31], $arr)) $this->setSalessaturdayhours($arr[$keys[31]]);
        if (array_key_exists($keys[32], $arr)) $this->setSalestrailer($arr[$keys[32]]);
        if (array_key_exists($keys[33], $arr)) $this->setOnlinepromo($arr[$keys[33]]);
        if (array_key_exists($keys[34], $arr)) $this->setOnlinepromotion($arr[$keys[34]]);
        if (array_key_exists($keys[35], $arr)) $this->setNewsletterurl($arr[$keys[35]]);
        if (array_key_exists($keys[36], $arr)) $this->setFacebookUrl($arr[$keys[36]]);
        if (array_key_exists($keys[37], $arr)) $this->setMobileImage($arr[$keys[37]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ClubPeer::DATABASE_NAME);

        if ($this->isColumnModified(ClubPeer::ID)) $criteria->add(ClubPeer::ID, $this->id);
        if ($this->isColumnModified(ClubPeer::CLUBNAME)) $criteria->add(ClubPeer::CLUBNAME, $this->clubname);
        if ($this->isColumnModified(ClubPeer::ADDRESS)) $criteria->add(ClubPeer::ADDRESS, $this->address);
        if ($this->isColumnModified(ClubPeer::CITY)) $criteria->add(ClubPeer::CITY, $this->city);
        if ($this->isColumnModified(ClubPeer::PROVINCE)) $criteria->add(ClubPeer::PROVINCE, $this->province);
        if ($this->isColumnModified(ClubPeer::POSTALCODE)) $criteria->add(ClubPeer::POSTALCODE, $this->postalcode);
        if ($this->isColumnModified(ClubPeer::MAPITADDRESS)) $criteria->add(ClubPeer::MAPITADDRESS, $this->mapitaddress);
        if ($this->isColumnModified(ClubPeer::PHONE)) $criteria->add(ClubPeer::PHONE, $this->phone);
        if ($this->isColumnModified(ClubPeer::TOLLFREE)) $criteria->add(ClubPeer::TOLLFREE, $this->tollfree);
        if ($this->isColumnModified(ClubPeer::CONTACTEMAIL)) $criteria->add(ClubPeer::CONTACTEMAIL, $this->contactemail);
        if ($this->isColumnModified(ClubPeer::APPLICATIONEMAIL)) $criteria->add(ClubPeer::APPLICATIONEMAIL, $this->applicationemail);
        if ($this->isColumnModified(ClubPeer::PERSONALTRAININGEMAIL)) $criteria->add(ClubPeer::PERSONALTRAININGEMAIL, $this->personaltrainingemail);
        if ($this->isColumnModified(ClubPeer::PROMOTIONS)) $criteria->add(ClubPeer::PROMOTIONS, $this->promotions);
        if ($this->isColumnModified(ClubPeer::SCHEDULEURL)) $criteria->add(ClubPeer::SCHEDULEURL, $this->scheduleurl);
        if ($this->isColumnModified(ClubPeer::POOLSCHEDULEURL)) $criteria->add(ClubPeer::POOLSCHEDULEURL, $this->poolscheduleurl);
        if ($this->isColumnModified(ClubPeer::USEINTERNAL)) $criteria->add(ClubPeer::USEINTERNAL, $this->useinternal);
        if ($this->isColumnModified(ClubPeer::SUNDAYHOURS)) $criteria->add(ClubPeer::SUNDAYHOURS, $this->sundayhours);
        if ($this->isColumnModified(ClubPeer::MONDAYHOURS)) $criteria->add(ClubPeer::MONDAYHOURS, $this->mondayhours);
        if ($this->isColumnModified(ClubPeer::TUESDAYHOURS)) $criteria->add(ClubPeer::TUESDAYHOURS, $this->tuesdayhours);
        if ($this->isColumnModified(ClubPeer::WEDNESDAYHOURS)) $criteria->add(ClubPeer::WEDNESDAYHOURS, $this->wednesdayhours);
        if ($this->isColumnModified(ClubPeer::THURSDAYHOURS)) $criteria->add(ClubPeer::THURSDAYHOURS, $this->thursdayhours);
        if ($this->isColumnModified(ClubPeer::FRIDAYHOURS)) $criteria->add(ClubPeer::FRIDAYHOURS, $this->fridayhours);
        if ($this->isColumnModified(ClubPeer::SATURDAYHOURS)) $criteria->add(ClubPeer::SATURDAYHOURS, $this->saturdayhours);
        if ($this->isColumnModified(ClubPeer::ACTIVE)) $criteria->add(ClubPeer::ACTIVE, $this->active);
        if ($this->isColumnModified(ClubPeer::FREETRIAL)) $criteria->add(ClubPeer::FREETRIAL, $this->freetrial);
        if ($this->isColumnModified(ClubPeer::SALESSUNDAYHOURS)) $criteria->add(ClubPeer::SALESSUNDAYHOURS, $this->salessundayhours);
        if ($this->isColumnModified(ClubPeer::SALESMONDAYHOURS)) $criteria->add(ClubPeer::SALESMONDAYHOURS, $this->salesmondayhours);
        if ($this->isColumnModified(ClubPeer::SALESTUESDAYHOURS)) $criteria->add(ClubPeer::SALESTUESDAYHOURS, $this->salestuesdayhours);
        if ($this->isColumnModified(ClubPeer::SALESWEDNESDAYHOURS)) $criteria->add(ClubPeer::SALESWEDNESDAYHOURS, $this->saleswednesdayhours);
        if ($this->isColumnModified(ClubPeer::SALESTHURSDAYHOURS)) $criteria->add(ClubPeer::SALESTHURSDAYHOURS, $this->salesthursdayhours);
        if ($this->isColumnModified(ClubPeer::SALESFRIDAYHOURS)) $criteria->add(ClubPeer::SALESFRIDAYHOURS, $this->salesfridayhours);
        if ($this->isColumnModified(ClubPeer::SALESSATURDAYHOURS)) $criteria->add(ClubPeer::SALESSATURDAYHOURS, $this->salessaturdayhours);
        if ($this->isColumnModified(ClubPeer::SALESTRAILER)) $criteria->add(ClubPeer::SALESTRAILER, $this->salestrailer);
        if ($this->isColumnModified(ClubPeer::ONLINEPROMO)) $criteria->add(ClubPeer::ONLINEPROMO, $this->onlinepromo);
        if ($this->isColumnModified(ClubPeer::ONLINEPROMOTION)) $criteria->add(ClubPeer::ONLINEPROMOTION, $this->onlinepromotion);
        if ($this->isColumnModified(ClubPeer::NEWSLETTERURL)) $criteria->add(ClubPeer::NEWSLETTERURL, $this->newsletterurl);
        if ($this->isColumnModified(ClubPeer::FACEBOOK_URL)) $criteria->add(ClubPeer::FACEBOOK_URL, $this->facebook_url);
        if ($this->isColumnModified(ClubPeer::MOBILE_IMAGE)) $criteria->add(ClubPeer::MOBILE_IMAGE, $this->mobile_image);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ClubPeer::DATABASE_NAME);
        $criteria->add(ClubPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return   int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of Club (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setClubname($this->getClubname());
        $copyObj->setAddress($this->getAddress());
        $copyObj->setCity($this->getCity());
        $copyObj->setProvince($this->getProvince());
        $copyObj->setPostalcode($this->getPostalcode());
        $copyObj->setMapitaddress($this->getMapitaddress());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setTollfree($this->getTollfree());
        $copyObj->setContactemail($this->getContactemail());
        $copyObj->setApplicationemail($this->getApplicationemail());
        $copyObj->setPersonaltrainingemail($this->getPersonaltrainingemail());
        $copyObj->setPromotions($this->getPromotions());
        $copyObj->setScheduleurl($this->getScheduleurl());
        $copyObj->setPoolscheduleurl($this->getPoolscheduleurl());
        $copyObj->setUseinternal($this->getUseinternal());
        $copyObj->setSundayhours($this->getSundayhours());
        $copyObj->setMondayhours($this->getMondayhours());
        $copyObj->setTuesdayhours($this->getTuesdayhours());
        $copyObj->setWednesdayhours($this->getWednesdayhours());
        $copyObj->setThursdayhours($this->getThursdayhours());
        $copyObj->setFridayhours($this->getFridayhours());
        $copyObj->setSaturdayhours($this->getSaturdayhours());
        $copyObj->setActive($this->getActive());
        $copyObj->setFreetrial($this->getFreetrial());
        $copyObj->setSalessundayhours($this->getSalessundayhours());
        $copyObj->setSalesmondayhours($this->getSalesmondayhours());
        $copyObj->setSalestuesdayhours($this->getSalestuesdayhours());
        $copyObj->setSaleswednesdayhours($this->getSaleswednesdayhours());
        $copyObj->setSalesthursdayhours($this->getSalesthursdayhours());
        $copyObj->setSalesfridayhours($this->getSalesfridayhours());
        $copyObj->setSalessaturdayhours($this->getSalessaturdayhours());
        $copyObj->setSalestrailer($this->getSalestrailer());
        $copyObj->setOnlinepromo($this->getOnlinepromo());
        $copyObj->setOnlinepromotion($this->getOnlinepromotion());
        $copyObj->setNewsletterurl($this->getNewsletterurl());
        $copyObj->setFacebookUrl($this->getFacebookUrl());
        $copyObj->setMobileImage($this->getMobileImage());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getClassresults() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addClassresult($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getInstructors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addInstructor($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return                 Club Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return   ClubPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ClubPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Classresult' == $relationName) {
            $this->initClassresults();
        }
        if ('Instructor' == $relationName) {
            $this->initInstructors();
        }
    }

    /**
     * Clears out the collClassresults collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addClassresults()
     */
    public function clearClassresults()
    {
        $this->collClassresults = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Initializes the collClassresults collection.
     *
     * By default this just sets the collClassresults collection to an empty array (like clearcollClassresults());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initClassresults($overrideExisting = true)
    {
        if (null !== $this->collClassresults && !$overrideExisting) {
            return;
        }
        $this->collClassresults = new PropelObjectCollection();
        $this->collClassresults->setModel('Classresult');
    }

    /**
     * Gets an array of Classresult objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Club is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      PropelPDO $con optional connection object
     * @return PropelObjectCollection|Classresult[] List of Classresult objects
     * @throws PropelException
     */
    public function getClassresults($criteria = null, PropelPDO $con = null)
    {
        if (null === $this->collClassresults || null !== $criteria) {
            if ($this->isNew() && null === $this->collClassresults) {
                // return empty collection
                $this->initClassresults();
            } else {
                $collClassresults = ClassresultQuery::create(null, $criteria)
                    ->filterByClub($this)
                    ->find($con);
                if (null !== $criteria) {
                    return $collClassresults;
                }
                $this->collClassresults = $collClassresults;
            }
        }

        return $this->collClassresults;
    }

    /**
     * Sets a collection of Classresult objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      PropelCollection $classresults A Propel collection.
     * @param      PropelPDO $con Optional connection object
     */
    public function setClassresults(PropelCollection $classresults, PropelPDO $con = null)
    {
        $this->classresultsScheduledForDeletion = $this->getClassresults(new Criteria(), $con)->diff($classresults);

        foreach ($this->classresultsScheduledForDeletion as $classresultRemoved) {
            $classresultRemoved->setClub(null);
        }

        $this->collClassresults = null;
        foreach ($classresults as $classresult) {
            $this->addClassresult($classresult);
        }

        $this->collClassresults = $classresults;
    }

    /**
     * Returns the number of related Classresult objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      PropelPDO $con
     * @return int             Count of related Classresult objects.
     * @throws PropelException
     */
    public function countClassresults(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        if (null === $this->collClassresults || null !== $criteria) {
            if ($this->isNew() && null === $this->collClassresults) {
                return 0;
            } else {
                $query = ClassresultQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByClub($this)
                    ->count($con);
            }
        } else {
            return count($this->collClassresults);
        }
    }

    /**
     * Method called to associate a Classresult object to this object
     * through the Classresult foreign key attribute.
     *
     * @param    Classresult $l Classresult
     * @return   Club The current object (for fluent API support)
     */
    public function addClassresult(Classresult $l)
    {
        if ($this->collClassresults === null) {
            $this->initClassresults();
        }
        if (!$this->collClassresults->contains($l)) { // only add it if the **same** object is not already associated
            $this->doAddClassresult($l);
        }

        return $this;
    }

    /**
     * @param	Classresult $classresult The classresult object to add.
     */
    protected function doAddClassresult($classresult)
    {
        $this->collClassresults[]= $classresult;
        $classresult->setClub($this);
    }

    /**
     * @param	Classresult $classresult The classresult object to remove.
     */
    public function removeClassresult($classresult)
    {
        if ($this->getClassresults()->contains($classresult)) {
            $this->collClassresults->remove($this->collClassresults->search($classresult));
            if (null === $this->classresultsScheduledForDeletion) {
                $this->classresultsScheduledForDeletion = clone $this->collClassresults;
                $this->classresultsScheduledForDeletion->clear();
            }
            $this->classresultsScheduledForDeletion[]= $classresult;
            $classresult->setClub(null);
        }
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Club is new, it will return
     * an empty collection; or if this Club has previously
     * been saved, it will retrieve related Classresults from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Club.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      PropelPDO $con optional connection object
     * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Classresult[] List of Classresult objects
     */
    public function getClassresultsJoin_Class($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ClassresultQuery::create(null, $criteria);
        $query->joinWith('_Class', $join_behavior);

        return $this->getClassresults($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Club is new, it will return
     * an empty collection; or if this Club has previously
     * been saved, it will retrieve related Classresults from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Club.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      PropelPDO $con optional connection object
     * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Classresult[] List of Classresult objects
     */
    public function getClassresultsJoinInstructor($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ClassresultQuery::create(null, $criteria);
        $query->joinWith('Instructor', $join_behavior);

        return $this->getClassresults($query, $con);
    }

    /**
     * Clears out the collInstructors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addInstructors()
     */
    public function clearInstructors()
    {
        $this->collInstructors = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Initializes the collInstructors collection.
     *
     * By default this just sets the collInstructors collection to an empty array (like clearcollInstructors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initInstructors($overrideExisting = true)
    {
        if (null !== $this->collInstructors && !$overrideExisting) {
            return;
        }
        $this->collInstructors = new PropelObjectCollection();
        $this->collInstructors->setModel('Instructor');
    }

    /**
     * Gets an array of Instructor objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Club is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      PropelPDO $con optional connection object
     * @return PropelObjectCollection|Instructor[] List of Instructor objects
     * @throws PropelException
     */
    public function getInstructors($criteria = null, PropelPDO $con = null)
    {
        if (null === $this->collInstructors || null !== $criteria) {
            if ($this->isNew() && null === $this->collInstructors) {
                // return empty collection
                $this->initInstructors();
            } else {
                $collInstructors = InstructorQuery::create(null, $criteria)
                    ->filterByClub($this)
                    ->find($con);
                if (null !== $criteria) {
                    return $collInstructors;
                }
                $this->collInstructors = $collInstructors;
            }
        }

        return $this->collInstructors;
    }

    /**
     * Sets a collection of Instructor objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      PropelCollection $instructors A Propel collection.
     * @param      PropelPDO $con Optional connection object
     */
    public function setInstructors(PropelCollection $instructors, PropelPDO $con = null)
    {
        $this->instructorsScheduledForDeletion = $this->getInstructors(new Criteria(), $con)->diff($instructors);

        foreach ($this->instructorsScheduledForDeletion as $instructorRemoved) {
            $instructorRemoved->setClub(null);
        }

        $this->collInstructors = null;
        foreach ($instructors as $instructor) {
            $this->addInstructor($instructor);
        }

        $this->collInstructors = $instructors;
    }

    /**
     * Returns the number of related Instructor objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      PropelPDO $con
     * @return int             Count of related Instructor objects.
     * @throws PropelException
     */
    public function countInstructors(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        if (null === $this->collInstructors || null !== $criteria) {
            if ($this->isNew() && null === $this->collInstructors) {
                return 0;
            } else {
                $query = InstructorQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByClub($this)
                    ->count($con);
            }
        } else {
            return count($this->collInstructors);
        }
    }

    /**
     * Method called to associate a Instructor object to this object
     * through the Instructor foreign key attribute.
     *
     * @param    Instructor $l Instructor
     * @return   Club The current object (for fluent API support)
     */
    public function addInstructor(Instructor $l)
    {
        if ($this->collInstructors === null) {
            $this->initInstructors();
        }
        if (!$this->collInstructors->contains($l)) { // only add it if the **same** object is not already associated
            $this->doAddInstructor($l);
        }

        return $this;
    }

    /**
     * @param	Instructor $instructor The instructor object to add.
     */
    protected function doAddInstructor($instructor)
    {
        $this->collInstructors[]= $instructor;
        $instructor->setClub($this);
    }

    /**
     * @param	Instructor $instructor The instructor object to remove.
     */
    public function removeInstructor($instructor)
    {
        if ($this->getInstructors()->contains($instructor)) {
            $this->collInstructors->remove($this->collInstructors->search($instructor));
            if (null === $this->instructorsScheduledForDeletion) {
                $this->instructorsScheduledForDeletion = clone $this->collInstructors;
                $this->instructorsScheduledForDeletion->clear();
            }
            $this->instructorsScheduledForDeletion[]= $instructor;
            $instructor->setClub(null);
        }
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->clubname = null;
        $this->address = null;
        $this->city = null;
        $this->province = null;
        $this->postalcode = null;
        $this->mapitaddress = null;
        $this->phone = null;
        $this->tollfree = null;
        $this->contactemail = null;
        $this->applicationemail = null;
        $this->personaltrainingemail = null;
        $this->promotions = null;
        $this->scheduleurl = null;
        $this->poolscheduleurl = null;
        $this->useinternal = null;
        $this->sundayhours = null;
        $this->mondayhours = null;
        $this->tuesdayhours = null;
        $this->wednesdayhours = null;
        $this->thursdayhours = null;
        $this->fridayhours = null;
        $this->saturdayhours = null;
        $this->active = null;
        $this->freetrial = null;
        $this->salessundayhours = null;
        $this->salesmondayhours = null;
        $this->salestuesdayhours = null;
        $this->saleswednesdayhours = null;
        $this->salesthursdayhours = null;
        $this->salesfridayhours = null;
        $this->salessaturdayhours = null;
        $this->salestrailer = null;
        $this->onlinepromo = null;
        $this->onlinepromotion = null;
        $this->newsletterurl = null;
        $this->facebook_url = null;
        $this->mobile_image = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collClassresults) {
                foreach ($this->collClassresults as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collInstructors) {
                foreach ($this->collInstructors as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collClassresults instanceof PropelCollection) {
            $this->collClassresults->clearIterator();
        }
        $this->collClassresults = null;
        if ($this->collInstructors instanceof PropelCollection) {
            $this->collInstructors->clearIterator();
        }
        $this->collInstructors = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ClubPeer::DEFAULT_STRING_FORMAT);
    }

} // BaseClub
