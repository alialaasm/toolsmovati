<?php


/**
 * Base class that represents a query for the 'Event' table.
 *
 * 
 *
 * @method     EventQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     EventQuery orderByCategoryid($order = Criteria::ASC) Order by the CategoryId column
 * @method     EventQuery orderByClassid($order = Criteria::ASC) Order by the ClassId column
 * @method     EventQuery orderByClubid($order = Criteria::ASC) Order by the ClubId column
 * @method     EventQuery orderByStudioid($order = Criteria::ASC) Order by the StudioId column
 * @method     EventQuery orderByInstructor($order = Criteria::ASC) Order by the Instructor column
 * @method     EventQuery orderByStarteventhour($order = Criteria::ASC) Order by the StartEventHour column
 * @method     EventQuery orderByStarteventmin($order = Criteria::ASC) Order by the StartEventMin column
 * @method     EventQuery orderByStarteventap($order = Criteria::ASC) Order by the StartEventAP column
 * @method     EventQuery orderByEndeventhour($order = Criteria::ASC) Order by the EndEventHour column
 * @method     EventQuery orderByEndeventmin($order = Criteria::ASC) Order by the EndEventMin column
 * @method     EventQuery orderByEndeventap($order = Criteria::ASC) Order by the EndEventAP column
 * @method     EventQuery orderByDay($order = Criteria::ASC) Order by the Day column
 * @method     EventQuery orderByMonth($order = Criteria::ASC) Order by the Month column
 * @method     EventQuery orderByYear($order = Criteria::ASC) Order by the Year column
 *
 * @method     EventQuery groupById() Group by the id column
 * @method     EventQuery groupByCategoryid() Group by the CategoryId column
 * @method     EventQuery groupByClassid() Group by the ClassId column
 * @method     EventQuery groupByClubid() Group by the ClubId column
 * @method     EventQuery groupByStudioid() Group by the StudioId column
 * @method     EventQuery groupByInstructor() Group by the Instructor column
 * @method     EventQuery groupByStarteventhour() Group by the StartEventHour column
 * @method     EventQuery groupByStarteventmin() Group by the StartEventMin column
 * @method     EventQuery groupByStarteventap() Group by the StartEventAP column
 * @method     EventQuery groupByEndeventhour() Group by the EndEventHour column
 * @method     EventQuery groupByEndeventmin() Group by the EndEventMin column
 * @method     EventQuery groupByEndeventap() Group by the EndEventAP column
 * @method     EventQuery groupByDay() Group by the Day column
 * @method     EventQuery groupByMonth() Group by the Month column
 * @method     EventQuery groupByYear() Group by the Year column
 *
 * @method     EventQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     EventQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     EventQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     EventQuery leftJoin_Class($relationAlias = null) Adds a LEFT JOIN clause to the query using the _Class relation
 * @method     EventQuery rightJoin_Class($relationAlias = null) Adds a RIGHT JOIN clause to the query using the _Class relation
 * @method     EventQuery innerJoin_Class($relationAlias = null) Adds a INNER JOIN clause to the query using the _Class relation
 *
 * @method     Event findOne(PropelPDO $con = null) Return the first Event matching the query
 * @method     Event findOneOrCreate(PropelPDO $con = null) Return the first Event matching the query, or a new Event object populated from the query conditions when no match is found
 *
 * @method     Event findOneById(int $id) Return the first Event filtered by the id column
 * @method     Event findOneByCategoryid(int $CategoryId) Return the first Event filtered by the CategoryId column
 * @method     Event findOneByClassid(int $ClassId) Return the first Event filtered by the ClassId column
 * @method     Event findOneByClubid(int $ClubId) Return the first Event filtered by the ClubId column
 * @method     Event findOneByStudioid(int $StudioId) Return the first Event filtered by the StudioId column
 * @method     Event findOneByInstructor(string $Instructor) Return the first Event filtered by the Instructor column
 * @method     Event findOneByStarteventhour(int $StartEventHour) Return the first Event filtered by the StartEventHour column
 * @method     Event findOneByStarteventmin(int $StartEventMin) Return the first Event filtered by the StartEventMin column
 * @method     Event findOneByStarteventap(string $StartEventAP) Return the first Event filtered by the StartEventAP column
 * @method     Event findOneByEndeventhour(int $EndEventHour) Return the first Event filtered by the EndEventHour column
 * @method     Event findOneByEndeventmin(int $EndEventMin) Return the first Event filtered by the EndEventMin column
 * @method     Event findOneByEndeventap(string $EndEventAP) Return the first Event filtered by the EndEventAP column
 * @method     Event findOneByDay(int $Day) Return the first Event filtered by the Day column
 * @method     Event findOneByMonth(int $Month) Return the first Event filtered by the Month column
 * @method     Event findOneByYear(int $Year) Return the first Event filtered by the Year column
 *
 * @method     array findById(int $id) Return Event objects filtered by the id column
 * @method     array findByCategoryid(int $CategoryId) Return Event objects filtered by the CategoryId column
 * @method     array findByClassid(int $ClassId) Return Event objects filtered by the ClassId column
 * @method     array findByClubid(int $ClubId) Return Event objects filtered by the ClubId column
 * @method     array findByStudioid(int $StudioId) Return Event objects filtered by the StudioId column
 * @method     array findByInstructor(string $Instructor) Return Event objects filtered by the Instructor column
 * @method     array findByStarteventhour(int $StartEventHour) Return Event objects filtered by the StartEventHour column
 * @method     array findByStarteventmin(int $StartEventMin) Return Event objects filtered by the StartEventMin column
 * @method     array findByStarteventap(string $StartEventAP) Return Event objects filtered by the StartEventAP column
 * @method     array findByEndeventhour(int $EndEventHour) Return Event objects filtered by the EndEventHour column
 * @method     array findByEndeventmin(int $EndEventMin) Return Event objects filtered by the EndEventMin column
 * @method     array findByEndeventap(string $EndEventAP) Return Event objects filtered by the EndEventAP column
 * @method     array findByDay(int $Day) Return Event objects filtered by the Day column
 * @method     array findByMonth(int $Month) Return Event objects filtered by the Month column
 * @method     array findByYear(int $Year) Return Event objects filtered by the Year column
 *
 * @package    propel.generator.site.om
 */
abstract class BaseEventQuery extends ModelCriteria
{
    
    /**
     * Initializes internal state of BaseEventQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'site', $modelName = 'Event', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new EventQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     EventQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return EventQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof EventQuery) {
            return $criteria;
        }
        $query = new EventQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Event|Event[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = EventPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(EventPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   Event A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `CATEGORYID`, `CLASSID`, `CLUBID`, `STUDIOID`, `INSTRUCTOR`, `STARTEVENTHOUR`, `STARTEVENTMIN`, `STARTEVENTAP`, `ENDEVENTHOUR`, `ENDEVENTMIN`, `ENDEVENTAP`, `DAY`, `MONTH`, `YEAR` FROM `Event` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Event();
            $obj->hydrate($row);
            EventPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Event|Event[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Event[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EventPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EventPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(EventPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the CategoryId column
     *
     * Example usage:
     * <code>
     * $query->filterByCategoryid(1234); // WHERE CategoryId = 1234
     * $query->filterByCategoryid(array(12, 34)); // WHERE CategoryId IN (12, 34)
     * $query->filterByCategoryid(array('min' => 12)); // WHERE CategoryId > 12
     * </code>
     *
     * @param     mixed $categoryid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByCategoryid($categoryid = null, $comparison = null)
    {
        if (is_array($categoryid)) {
            $useMinMax = false;
            if (isset($categoryid['min'])) {
                $this->addUsingAlias(EventPeer::CATEGORYID, $categoryid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($categoryid['max'])) {
                $this->addUsingAlias(EventPeer::CATEGORYID, $categoryid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventPeer::CATEGORYID, $categoryid, $comparison);
    }

    /**
     * Filter the query on the ClassId column
     *
     * Example usage:
     * <code>
     * $query->filterByClassid(1234); // WHERE ClassId = 1234
     * $query->filterByClassid(array(12, 34)); // WHERE ClassId IN (12, 34)
     * $query->filterByClassid(array('min' => 12)); // WHERE ClassId > 12
     * </code>
     *
     * @see       filterBy_Class()
     *
     * @param     mixed $classid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByClassid($classid = null, $comparison = null)
    {
        if (is_array($classid)) {
            $useMinMax = false;
            if (isset($classid['min'])) {
                $this->addUsingAlias(EventPeer::CLASSID, $classid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($classid['max'])) {
                $this->addUsingAlias(EventPeer::CLASSID, $classid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventPeer::CLASSID, $classid, $comparison);
    }

    /**
     * Filter the query on the ClubId column
     *
     * Example usage:
     * <code>
     * $query->filterByClubid(1234); // WHERE ClubId = 1234
     * $query->filterByClubid(array(12, 34)); // WHERE ClubId IN (12, 34)
     * $query->filterByClubid(array('min' => 12)); // WHERE ClubId > 12
     * </code>
     *
     * @param     mixed $clubid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByClubid($clubid = null, $comparison = null)
    {
        if (is_array($clubid)) {
            $useMinMax = false;
            if (isset($clubid['min'])) {
                $this->addUsingAlias(EventPeer::CLUBID, $clubid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clubid['max'])) {
                $this->addUsingAlias(EventPeer::CLUBID, $clubid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventPeer::CLUBID, $clubid, $comparison);
    }

    /**
     * Filter the query on the StudioId column
     *
     * Example usage:
     * <code>
     * $query->filterByStudioid(1234); // WHERE StudioId = 1234
     * $query->filterByStudioid(array(12, 34)); // WHERE StudioId IN (12, 34)
     * $query->filterByStudioid(array('min' => 12)); // WHERE StudioId > 12
     * </code>
     *
     * @param     mixed $studioid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByStudioid($studioid = null, $comparison = null)
    {
        if (is_array($studioid)) {
            $useMinMax = false;
            if (isset($studioid['min'])) {
                $this->addUsingAlias(EventPeer::STUDIOID, $studioid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($studioid['max'])) {
                $this->addUsingAlias(EventPeer::STUDIOID, $studioid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventPeer::STUDIOID, $studioid, $comparison);
    }

    /**
     * Filter the query on the Instructor column
     *
     * Example usage:
     * <code>
     * $query->filterByInstructor('fooValue');   // WHERE Instructor = 'fooValue'
     * $query->filterByInstructor('%fooValue%'); // WHERE Instructor LIKE '%fooValue%'
     * </code>
     *
     * @param     string $instructor The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByInstructor($instructor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($instructor)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $instructor)) {
                $instructor = str_replace('*', '%', $instructor);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EventPeer::INSTRUCTOR, $instructor, $comparison);
    }

    /**
     * Filter the query on the StartEventHour column
     *
     * Example usage:
     * <code>
     * $query->filterByStarteventhour(1234); // WHERE StartEventHour = 1234
     * $query->filterByStarteventhour(array(12, 34)); // WHERE StartEventHour IN (12, 34)
     * $query->filterByStarteventhour(array('min' => 12)); // WHERE StartEventHour > 12
     * </code>
     *
     * @param     mixed $starteventhour The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByStarteventhour($starteventhour = null, $comparison = null)
    {
        if (is_array($starteventhour)) {
            $useMinMax = false;
            if (isset($starteventhour['min'])) {
                $this->addUsingAlias(EventPeer::STARTEVENTHOUR, $starteventhour['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($starteventhour['max'])) {
                $this->addUsingAlias(EventPeer::STARTEVENTHOUR, $starteventhour['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventPeer::STARTEVENTHOUR, $starteventhour, $comparison);
    }

    /**
     * Filter the query on the StartEventMin column
     *
     * Example usage:
     * <code>
     * $query->filterByStarteventmin(1234); // WHERE StartEventMin = 1234
     * $query->filterByStarteventmin(array(12, 34)); // WHERE StartEventMin IN (12, 34)
     * $query->filterByStarteventmin(array('min' => 12)); // WHERE StartEventMin > 12
     * </code>
     *
     * @param     mixed $starteventmin The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByStarteventmin($starteventmin = null, $comparison = null)
    {
        if (is_array($starteventmin)) {
            $useMinMax = false;
            if (isset($starteventmin['min'])) {
                $this->addUsingAlias(EventPeer::STARTEVENTMIN, $starteventmin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($starteventmin['max'])) {
                $this->addUsingAlias(EventPeer::STARTEVENTMIN, $starteventmin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventPeer::STARTEVENTMIN, $starteventmin, $comparison);
    }

    /**
     * Filter the query on the StartEventAP column
     *
     * Example usage:
     * <code>
     * $query->filterByStarteventap('fooValue');   // WHERE StartEventAP = 'fooValue'
     * $query->filterByStarteventap('%fooValue%'); // WHERE StartEventAP LIKE '%fooValue%'
     * </code>
     *
     * @param     string $starteventap The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByStarteventap($starteventap = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($starteventap)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $starteventap)) {
                $starteventap = str_replace('*', '%', $starteventap);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EventPeer::STARTEVENTAP, $starteventap, $comparison);
    }

    /**
     * Filter the query on the EndEventHour column
     *
     * Example usage:
     * <code>
     * $query->filterByEndeventhour(1234); // WHERE EndEventHour = 1234
     * $query->filterByEndeventhour(array(12, 34)); // WHERE EndEventHour IN (12, 34)
     * $query->filterByEndeventhour(array('min' => 12)); // WHERE EndEventHour > 12
     * </code>
     *
     * @param     mixed $endeventhour The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByEndeventhour($endeventhour = null, $comparison = null)
    {
        if (is_array($endeventhour)) {
            $useMinMax = false;
            if (isset($endeventhour['min'])) {
                $this->addUsingAlias(EventPeer::ENDEVENTHOUR, $endeventhour['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($endeventhour['max'])) {
                $this->addUsingAlias(EventPeer::ENDEVENTHOUR, $endeventhour['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventPeer::ENDEVENTHOUR, $endeventhour, $comparison);
    }

    /**
     * Filter the query on the EndEventMin column
     *
     * Example usage:
     * <code>
     * $query->filterByEndeventmin(1234); // WHERE EndEventMin = 1234
     * $query->filterByEndeventmin(array(12, 34)); // WHERE EndEventMin IN (12, 34)
     * $query->filterByEndeventmin(array('min' => 12)); // WHERE EndEventMin > 12
     * </code>
     *
     * @param     mixed $endeventmin The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByEndeventmin($endeventmin = null, $comparison = null)
    {
        if (is_array($endeventmin)) {
            $useMinMax = false;
            if (isset($endeventmin['min'])) {
                $this->addUsingAlias(EventPeer::ENDEVENTMIN, $endeventmin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($endeventmin['max'])) {
                $this->addUsingAlias(EventPeer::ENDEVENTMIN, $endeventmin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventPeer::ENDEVENTMIN, $endeventmin, $comparison);
    }

    /**
     * Filter the query on the EndEventAP column
     *
     * Example usage:
     * <code>
     * $query->filterByEndeventap('fooValue');   // WHERE EndEventAP = 'fooValue'
     * $query->filterByEndeventap('%fooValue%'); // WHERE EndEventAP LIKE '%fooValue%'
     * </code>
     *
     * @param     string $endeventap The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByEndeventap($endeventap = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($endeventap)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $endeventap)) {
                $endeventap = str_replace('*', '%', $endeventap);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EventPeer::ENDEVENTAP, $endeventap, $comparison);
    }

    /**
     * Filter the query on the Day column
     *
     * Example usage:
     * <code>
     * $query->filterByDay(1234); // WHERE Day = 1234
     * $query->filterByDay(array(12, 34)); // WHERE Day IN (12, 34)
     * $query->filterByDay(array('min' => 12)); // WHERE Day > 12
     * </code>
     *
     * @param     mixed $day The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByDay($day = null, $comparison = null)
    {
        if (is_array($day)) {
            $useMinMax = false;
            if (isset($day['min'])) {
                $this->addUsingAlias(EventPeer::DAY, $day['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($day['max'])) {
                $this->addUsingAlias(EventPeer::DAY, $day['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventPeer::DAY, $day, $comparison);
    }

    /**
     * Filter the query on the Month column
     *
     * Example usage:
     * <code>
     * $query->filterByMonth(1234); // WHERE Month = 1234
     * $query->filterByMonth(array(12, 34)); // WHERE Month IN (12, 34)
     * $query->filterByMonth(array('min' => 12)); // WHERE Month > 12
     * </code>
     *
     * @param     mixed $month The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByMonth($month = null, $comparison = null)
    {
        if (is_array($month)) {
            $useMinMax = false;
            if (isset($month['min'])) {
                $this->addUsingAlias(EventPeer::MONTH, $month['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($month['max'])) {
                $this->addUsingAlias(EventPeer::MONTH, $month['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventPeer::MONTH, $month, $comparison);
    }

    /**
     * Filter the query on the Year column
     *
     * Example usage:
     * <code>
     * $query->filterByYear(1234); // WHERE Year = 1234
     * $query->filterByYear(array(12, 34)); // WHERE Year IN (12, 34)
     * $query->filterByYear(array('min' => 12)); // WHERE Year > 12
     * </code>
     *
     * @param     mixed $year The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function filterByYear($year = null, $comparison = null)
    {
        if (is_array($year)) {
            $useMinMax = false;
            if (isset($year['min'])) {
                $this->addUsingAlias(EventPeer::YEAR, $year['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($year['max'])) {
                $this->addUsingAlias(EventPeer::YEAR, $year['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventPeer::YEAR, $year, $comparison);
    }

    /**
     * Filter the query by a related _Class object
     *
     * @param   _Class|PropelObjectCollection $_Class The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   EventQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBy_Class($_Class, $comparison = null)
    {
        if ($_Class instanceof _Class) {
            return $this
                ->addUsingAlias(EventPeer::CLASSID, $_Class->getId(), $comparison);
        } elseif ($_Class instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EventPeer::CLASSID, $_Class->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBy_Class() only accepts arguments of type _Class or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the _Class relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function join_Class($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('_Class');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, '_Class');
        }

        return $this;
    }

    /**
     * Use the _Class relation _Class object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   _ClassQuery A secondary query class using the current class as primary query
     */
    public function use_ClassQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->join_Class($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : '_Class', '_ClassQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Event $event Object to remove from the list of results
     *
     * @return EventQuery The current query, for fluid interface
     */
    public function prune($event = null)
    {
        if ($event) {
            $this->addUsingAlias(EventPeer::ID, $event->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

} // BaseEventQuery