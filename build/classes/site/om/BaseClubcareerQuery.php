<?php


/**
 * Base class that represents a query for the 'ClubCareer' table.
 *
 * 
 *
 * @method     ClubcareerQuery orderByClubid($order = Criteria::ASC) Order by the ClubID column
 * @method     ClubcareerQuery orderByCareerid($order = Criteria::ASC) Order by the CareerID column
 *
 * @method     ClubcareerQuery groupByClubid() Group by the ClubID column
 * @method     ClubcareerQuery groupByCareerid() Group by the CareerID column
 *
 * @method     ClubcareerQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ClubcareerQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ClubcareerQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     Clubcareer findOne(PropelPDO $con = null) Return the first Clubcareer matching the query
 * @method     Clubcareer findOneOrCreate(PropelPDO $con = null) Return the first Clubcareer matching the query, or a new Clubcareer object populated from the query conditions when no match is found
 *
 * @method     Clubcareer findOneByClubid(int $ClubID) Return the first Clubcareer filtered by the ClubID column
 * @method     Clubcareer findOneByCareerid(int $CareerID) Return the first Clubcareer filtered by the CareerID column
 *
 * @method     array findByClubid(int $ClubID) Return Clubcareer objects filtered by the ClubID column
 * @method     array findByCareerid(int $CareerID) Return Clubcareer objects filtered by the CareerID column
 *
 * @package    propel.generator.site.om
 */
abstract class BaseClubcareerQuery extends ModelCriteria
{
    
    /**
     * Initializes internal state of BaseClubcareerQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'site', $modelName = 'Clubcareer', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ClubcareerQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     ClubcareerQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ClubcareerQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ClubcareerQuery) {
            return $criteria;
        }
        $query = new ClubcareerQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query 
                         A Primary key composition: [$ClubID, $CareerID]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Clubcareer|Clubcareer[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ClubcareerPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ClubcareerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   Clubcareer A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `CLUBID`, `CAREERID` FROM `ClubCareer` WHERE `CLUBID` = :p0 AND `CAREERID` = :p1';
        try {
            $stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
			$stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Clubcareer();
            $obj->hydrate($row);
            ClubcareerPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Clubcareer|Clubcareer[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Clubcareer[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ClubcareerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(ClubcareerPeer::CLUBID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(ClubcareerPeer::CAREERID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ClubcareerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(ClubcareerPeer::CLUBID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(ClubcareerPeer::CAREERID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the ClubID column
     *
     * Example usage:
     * <code>
     * $query->filterByClubid(1234); // WHERE ClubID = 1234
     * $query->filterByClubid(array(12, 34)); // WHERE ClubID IN (12, 34)
     * $query->filterByClubid(array('min' => 12)); // WHERE ClubID > 12
     * </code>
     *
     * @param     mixed $clubid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubcareerQuery The current query, for fluid interface
     */
    public function filterByClubid($clubid = null, $comparison = null)
    {
        if (is_array($clubid) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(ClubcareerPeer::CLUBID, $clubid, $comparison);
    }

    /**
     * Filter the query on the CareerID column
     *
     * Example usage:
     * <code>
     * $query->filterByCareerid(1234); // WHERE CareerID = 1234
     * $query->filterByCareerid(array(12, 34)); // WHERE CareerID IN (12, 34)
     * $query->filterByCareerid(array('min' => 12)); // WHERE CareerID > 12
     * </code>
     *
     * @param     mixed $careerid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ClubcareerQuery The current query, for fluid interface
     */
    public function filterByCareerid($careerid = null, $comparison = null)
    {
        if (is_array($careerid) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(ClubcareerPeer::CAREERID, $careerid, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Clubcareer $clubcareer Object to remove from the list of results
     *
     * @return ClubcareerQuery The current query, for fluid interface
     */
    public function prune($clubcareer = null)
    {
        if ($clubcareer) {
            $this->addCond('pruneCond0', $this->getAliasedColName(ClubcareerPeer::CLUBID), $clubcareer->getClubid(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(ClubcareerPeer::CAREERID), $clubcareer->getCareerid(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

} // BaseClubcareerQuery